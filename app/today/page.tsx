import Header from '../../components/Header';
import Navigation, { Pages } from '../../components/Navigation';
import formatDayCardHeader from '../../numberCrunching/formatDayCardHeader';
import { NamedCalAndEnergy } from '../../numberCrunching/calculateCalAndEnergyPerDayDiet';
import VerticalGrid from '../../components/VerticalGrid';
import Card from '../../components/Card';
import CardHeader from '../../components/CardHeader';
import CardContent from '../../components/CardContent';
import style from './today.module.css'

async function getData() {
  const dev = process.env.NODE_ENV !== 'production';
  const server = dev ? 'http://localhost:3000' : 'https://mydiet.chr1shaefn3r.dev';
  const apiEndpoint = `${server}/api/today`;
  const cachePolicy: RequestInit = { cache: 'no-store' };
  const res = await fetch(apiEndpoint, cachePolicy);

  if (!res.ok) {
    throw new Error(`Failed to fetch data from endpoint: ${apiEndpoint}`);
  }

  return res.json();
}

export default async function Page() {
  const diets = await getData();
  return (
    <>
      <Header />
      <VerticalGrid className={style.content}>
        {diets.length <= 0 && <h1 className={style.noDiet}>
          No diet for today and tomorrow available.
        </h1>}
        {diets.map(({ name, recipes }: NamedCalAndEnergy) => <Card className={style.todayCard} key={name}>
          <CardHeader title={formatDayCardHeader(name)} />
          <CardContent className={style.recipeList}>
            {recipes.join(", ")}
          </CardContent>
        </Card>)
        }
      </VerticalGrid>
      <Navigation currentPage={Pages.today} />
    </>
  );
}
