import Header from '../../components/Header';
import Navigation, { Pages } from '../../components/Navigation';
import VerticalGrid from '../../components/VerticalGrid';
import Card from '../../components/Card';
import CardHeader from '../../components/CardHeader';
import CardContent from '../../components/CardContent';
import style from './today.module.css'

export default async function Page() {
  return (
    <>
      <Header />
      <VerticalGrid className={style.content}>
        <Card className={style.todayCard}>
          <CardHeader title={"Loading..."} />
          <CardContent className={style.recipeList}>
          </CardContent>
        </Card>
        <Card className={style.todayCard}>
          <CardHeader title={""} />
          <CardContent className={style.recipeList}>
          </CardContent>
        </Card>
      </VerticalGrid>
      <Navigation currentPage={Pages.today} />
    </>
  );
}
