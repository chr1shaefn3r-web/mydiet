import Manifest from "../public/manifest.json"

export default function Head() {
  return (
    <>
      <title>My Diet</title>
      <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      <link href="/mydiet_96px.png" rel="icon" type="image/png" />
      <link href="/apple-touch-icon.png" rel="apple-touch-icon" type="image/png" />
      <meta name="theme-color" content={Manifest.theme_color} />
      <link rel="preload" as="style" href="/css/md-roboto-font.css" />
      <link href="/manifest.json" rel="manifest" />
    </>
  )
}
