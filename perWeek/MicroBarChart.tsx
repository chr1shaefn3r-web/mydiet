import React from "react";
import { ScaleLinear, scaleLinear } from "d3-scale";
import { sumAndaverage } from "../numberCrunching/sumAndAverage";

export interface MicroBarChartProps {
    data: number[];
    height: number;
    width: number;
    fillColor: string;
    actual?: number
}

const textWidthForAverage = 50;
const textWidthForActual = 50;
const textHeightForAverage = 16;
const paddingText = 10;
const lineOverlapOverChart = 5;

const leftLinePadding = textWidthForActual - lineOverlapOverChart;


const minimumLinearScale = (linearScaler: ScaleLinear<number, number>) => {
        return (value: number): number => linearScaler(value) || 0;
}

export default class MicroBarChart extends React.Component<MicroBarChartProps> {

    render() {
        const actualValue = this.props.actual || 0;
        const maxDataValue = Math.max(...this.props.data);
        const maxValue = Math.max(maxDataValue, actualValue);
        const { average } = sumAndaverage(this.props.data);
        const linearScaler = scaleLinear()
            .range([this.props.height, 0])
            .domain([0, maxValue])
        const linearScale = minimumLinearScale(linearScaler);
        const chartWidth = this.props.width - textWidthForAverage - textWidthForActual;
        const lineWidth = chartWidth + textWidthForActual + lineOverlapOverChart;
        const barWidth = chartWidth / this.props.data.length;
        return <svg width={this.props.width} height={this.props.height} >
            {this.props.data.map((value, index) =>
                <g transform={`translate(${(index * barWidth) + textWidthForActual},0)`} key={index}>
                    <rect
                        y={linearScale(value)}
                        height={this.props.height - linearScale(value)}
                        width={barWidth - 1}
                        fill={this.props.fillColor}
                    >
                    </rect>
                </g>)
            }
            <line x1={leftLinePadding} y1={linearScale(average)} x2={lineWidth} y2={linearScale(average)} stroke="black" />
            <text x={textWidthForActual + chartWidth + paddingText} y={linearScale(average) + textHeightForAverage / 1.5}>{average}</text>
            {actualValue > 0 && <>
                <line x1={leftLinePadding} y1={linearScale(actualValue)} x2={lineWidth} y2={linearScale(actualValue)} stroke="black" />
                <text x={paddingText} y={linearScale(actualValue) + textHeightForAverage / 1.5}>{actualValue}</text>
            </>}
        </svg>;
    }

};
