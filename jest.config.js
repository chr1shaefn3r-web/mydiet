const nextJest = require('next/jest')

const createJestConfig = nextJest({
    // Provide the path to your Next.js app to load next.config.js and .env files in your test environment
    dir: './',
})

const customJestConfig = {
    rootDir: "./",
    moduleDirectories: ['node_modules', '<rootDir>/'],
    coverageThreshold: {
        global: {
            statements: 96.4,
            branches: 100,
            functions: 85,
            lines: 96.9
        }
    }
}

module.exports = createJestConfig(customJestConfig)
