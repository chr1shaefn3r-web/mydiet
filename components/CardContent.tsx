import styles from './CardContent.module.css'

export interface CardContentProps {
    children: React.ReactNode;
    className?: string;
}

const CardContent = ({ children, className = '' }: CardContentProps) => {
    return (
        <div className={`${className} ${styles.cardContent}`}>
            {children}
        </div>
    )
}

export default CardContent;
