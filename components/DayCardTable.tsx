import { defFormat, floatFixedWidth } from '@thi.ng/strings';
import styles from './DayCardTable.module.css'
import nutritionClasses from './NutritionClasses.module.css';

export interface DayCardTableProps {
    cal: number;
    protein: number;
    className?: string;
}

const fmt = defFormat([floatFixedWidth(6, 0)]);

const DayCardTable = ({ cal, protein, className = '' }: DayCardTableProps) => {
    return (
        <table className={`${className} ${styles.table}`}>
            <tbody className={styles.tableBody}>
                <tr className={`${styles.tableRow} ${nutritionClasses.cal}`}>
                    <td className={styles.tableCell}>{"Cal (kCal)"}</td>
                    <td className={`${styles.tableCell} ${styles.alignRight}`}>{fmt(cal)}</td>
                </tr>
                <tr className={`${styles.tableRow} ${nutritionClasses.protein}`}>
                    <td className={styles.tableCell}>{"Protein (g)"}</td>
                    <td className={`${styles.tableCell} ${styles.alignRight}`}>{fmt(protein)}</td>
                </tr>
            </tbody>
        </table>
    )
}

export default DayCardTable;
