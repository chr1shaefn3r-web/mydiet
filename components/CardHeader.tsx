import styles from './CardHeader.module.css'

export interface CardHeaderProps {
    title: string;
    subheader?: string;
}

const CardHeader = ({ title, subheader }: CardHeaderProps) => {
    return (
        <div className={styles.cardHeader}>
            <span className={styles.title}>{title}</span>
            {subheader && <span className={styles.subheader}>{subheader}</span>}
        </div>
    )
}

export default CardHeader;
