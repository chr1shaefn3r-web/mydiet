import styles from './Navigation.module.css'
import NavigationItem from './NavigationItem';
import { AssignmentIcon, DateRangeIcon, NatureIcon, RestaurantIcon, TodayIcon } from './Icons';

export enum Pages {
    phases = "phases",
    today = "today",
    weeks = "weeks",
    recipes = "recipes",
    ingredients = "ingredients"
}

export interface NavigationProps {
    currentPage: Pages;
}

const Navigation = ({ currentPage }: NavigationProps) => {
    return (
        <div className={styles.navigation}>
            <NavigationItem icon={<AssignmentIcon />} label={"Phases"} highlight={currentPage === Pages.phases} />
            <NavigationItem icon={<TodayIcon />} label={"Today"} highlight={currentPage === Pages.today} />
            <NavigationItem icon={<DateRangeIcon />} label={"Weeks"} highlight={currentPage === Pages.weeks} href="/" />
            <NavigationItem icon={<RestaurantIcon />} label={"Recipes"} highlight={currentPage === Pages.recipes} />
            <NavigationItem icon={<NatureIcon />} label={"Ingredients"} highlight={currentPage === Pages.ingredients} />
        </div>
    )
}

export default Navigation;
