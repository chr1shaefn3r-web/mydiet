import * as React from 'react';
import NextLink from "next/link";
import styles from './NavigationItem.module.css'

export interface NavigationItemProps {
    icon: React.ReactNode;
    label: string;
    highlight: boolean;
    href?: string;
}

const NavigationItem = ({ icon, label, highlight, href }: NavigationItemProps) => {
    return (
        <div className={`${styles.navigationItem} ${highlight ? styles.selectedNavigationItem : ''}`}>
            <NextLink href={href || `/${label.toLowerCase()}`}>
                {icon}<span>{label}</span>
            </NextLink>
        </div>
    )
}

export default NavigationItem;
