import NextLink from "next/link";
import Manifest from "../public/manifest.json"
import styles from './Header.module.css'

const Header = () => <div className={styles.appBar}>
    <div className={styles.title}>
        <NextLink href="/">{Manifest.name}</NextLink>
    </div>
</div>

export default Header;
