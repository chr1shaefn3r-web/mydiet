import styles from './Card.module.css'

export interface CardProps {
    children: React.ReactNode;
    className?: string;
}

const Card = ({ children, className = '' }: CardProps) => {
    return (
        <div className={`${className} ${styles.card}`}>
            {children}
        </div>
    )
}

export default Card;
