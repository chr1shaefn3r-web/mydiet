import styles from './HorizontalGrid.module.css'

export interface HorizontalGridProps {
    children: React.ReactNode;
    className?: string;
}

const HorizontalGrid = ({ children, className = '' }: HorizontalGridProps) => {
    return (
        <div className={`${className} ${styles.horizontalGrid}`}>
            {children}
        </div>
    )
}

export default HorizontalGrid;
