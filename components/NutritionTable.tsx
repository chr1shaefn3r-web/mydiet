import { defFormat, floatFixedWidth } from '@thi.ng/strings';
import styles from './NutritionTable.module.css'
import nutritionClasses from './NutritionClasses.module.css';
import { Macros } from '../data/ingredients';

export interface NutritionTableProps {
    macros: Macros;
    className?: string;
}

const fmt = defFormat([floatFixedWidth(6, 2)]);

const NutritionTable = ({ macros, className = '' }: NutritionTableProps) => {
    return (
        <table className={`${className} ${styles.table}`}>
            <tbody className={styles.tableBody}>
                <tr className={`${styles.tableRow} ${nutritionClasses.cal}`}>
                    <td className={styles.tableCell}>{"Cal (kCal)"}</td>
                    <td className={`${styles.tableCell} ${styles.alignRight}`}>{fmt(macros.cal)}</td>
                </tr>
                <tr className={`${styles.tableRow} ${nutritionClasses.fat}`}>
                    <td className={styles.tableCell}>{"Fat (g)"}</td>
                    <td className={`${styles.tableCell} ${styles.alignRight}`}>{fmt(macros.fat)}</td>
                </tr>
                <tr className={`${styles.tableRow}`}>
                    <td className={styles.tableCell}>{"Saturated (g)"}</td>
                    <td className={`${styles.tableCell} ${styles.alignRight}`}>{fmt(macros.saturated)}</td>
                </tr>
                <tr className={`${styles.tableRow} ${nutritionClasses.carbs}`}>
                    <td className={styles.tableCell}>{"Carbs (g)"}</td>
                    <td className={`${styles.tableCell} ${styles.alignRight}`}>{fmt(macros.carbs)}</td>
                </tr>
                <tr className={`${styles.tableRow}`}>
                    <td className={styles.tableCell}>{"Sugar (g)"}</td>
                    <td className={`${styles.tableCell} ${styles.alignRight}`}>{fmt(macros.sugar)}</td>
                </tr>
                <tr className={`${styles.tableRow} ${nutritionClasses.protein}`}>
                    <td className={styles.tableCell}>{"Protein (g)"}</td>
                    <td className={`${styles.tableCell} ${styles.alignRight}`}>{fmt(macros.protein)}</td>
                </tr>
                <tr className={`${styles.tableRow}`}>
                    <td className={styles.tableCell}>{"Salt (g)"}</td>
                    <td className={`${styles.tableCell} ${styles.alignRight}`}>{fmt(macros.salt)}</td>
                </tr>
            </tbody>
        </table>
    )
}

export default NutritionTable;
