import { PieChart } from 'react-minimal-pie-chart'
import { fatColor, carbsColor, proteinColor } from './nutritionColors'
import { Fat, Carbs, Protein } from '../data/ingredients'

export interface EnergiePieChartProps {
    fat: Fat;
    carbs: Carbs;
    protein: Protein;
    height?: number;
}

const EnergiePieChart = ({fat, carbs, protein, height}: EnergiePieChartProps) => {
    return <PieChart
        style={{
            fontSize: '8px',
            fill: '#fff',
            height: height ? height : '100%'
        }}
        data={[
            {
                color: fatColor,
                title: 'Fat',
                value: fat
            },
            {
                color: carbsColor,
                title: 'Carbs',
                value: carbs
            },
            {
                color: proteinColor,
                title: 'Protein',
                value: protein
            }
        ]}
        radius={42}
        lineWidth={60}
        label={({ dataEntry }) => Math.round(dataEntry.percentage) + '%'}
        labelPosition={100 - 60 / 2}
    />
}

export default EnergiePieChart;
