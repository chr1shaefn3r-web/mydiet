import styles from './VerticalGrid.module.css'

export interface VerticalGridProps {
    children: React.ReactNode;
    className?: string;
}

const VerticalGrid = ({ children, className = '' }: VerticalGridProps) => {
    return (
        <div className={`${className} ${styles.verticalGrid}`}>
            {children}
        </div>
    )
}

export default VerticalGrid;
