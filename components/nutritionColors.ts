import { blue, brown, green, purple } from "./colors";

export const calColor = purple[400];
export const fatColor = brown[400];
export const carbsColor = blue[400];
export const proteinColor = green[400];
