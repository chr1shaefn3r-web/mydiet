import { Line, Serie } from '@nivo/line'

interface ActualTargetTrendLineChartProps {
    min: number;
    max: number;
    data: Serie[];
}

export const ActualTargetTrendLineChart = ({ min, max, data }: ActualTargetTrendLineChartProps) => {
    // only render line-chart if there is actually data available
    const lineChartDataIndex = 0;
    if (data[lineChartDataIndex].data.length <= 0) {
        return null;
    }
    return (<Line
        height={200}
        width={600}
        margin={{ top: 10, right: 120, bottom: 25, left: 25 }}
        xScale={{
            type: 'time',
            format: '%Y-%m-%d',
            useUTC: false,
        }}
        xFormat="time:%Y-%m-%d"
        yScale={{ type: 'linear', min, max }}
        axisBottom={{
            format: '%b %d',
        }}
        data={data}
        axisLeft={{}}
        axisRight={{}}
        legends={[
            {
                anchor: 'bottom-right',
                direction: 'column',
                justify: false,
                translateX: 115,
                translateY: 0,
                itemsSpacing: 0,
                itemDirection: 'left-to-right',
                itemWidth: 80,
                itemHeight: 20,
            }
        ]}
    />)
}