export type WeightInKg = number;

export interface OneRepMaxTest {
	bench?: WeightInKg;
	deadlift?: WeightInKg;
	squatLayDownMachine?: WeightInKg; // Gym80: 5001
	squatSmithMachine?: WeightInKg;
	bizepCurlSzBar?: WeightInKg;
	rudern?: WeightInKg; // Gym80: 3045
	latzugMaschine?: WeightInKg; // Gym80
	squat?: WeightInKg;
}

export type OneRapMaxTests = { [key: string]: OneRepMaxTest; };

export const oneRapMaxTests: OneRapMaxTests = {
	"2022-08-16": {
		bench: 70,
		deadlift: 105,
		squatLayDownMachine: 149,
		bizepCurlSzBar: 37.5
	},
	"2023-05-09": {
		deadlift: 125,
	},
	"2023-05-11": {
		squatSmithMachine: 75,
	},
	"2023-05-16": {
		bench: 72.5,
		bizepCurlSzBar: 42.5,
		squatLayDownMachine: 157,
		rudern: 105,
	},
	"2023-12-29": {
		latzugMaschine: 100,
	},
	"2024-03-03": {
		bench: 80,
	},
	"2024-03-04": {
		squat: 80,
	},
}
