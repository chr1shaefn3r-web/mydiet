import { blue, green, lightBlue, lightGreen, orange, red, yellow } from "../components/colors";

type Name = string;
type Weight = number;
export type StartWeek = string;
export type EndWeek = string;

export interface Phase {
  name: Name;
  startWeek: StartWeek;
  endWeek: EndWeek;
  targets: Targets;
  totalEnergyExpenditure: TotalEnergyExpenditure;
  coloricDifference: ColoricDifference;
  fakeWeight?: Weight;
  postprandialeThermogenese?: number;
  regularWeighingDays?: number[];
}

export enum Day {
  Sunday = 0,
  Monday = 1,
  Tuesday = 2,
  Wednesday = 3,
  Thursday = 4,
  Friday = 5,
  Saturday = 6
}

export enum ColoricDifference {
  LEAN_SURPLUS = "lean-surplus",
  SURPLUS = "surplus",
  DEFICIT = "deficit",
  LEAN_DEFICIT = "lean-deficit",
  MAINTENANCE = "maintenance",
  MAINGAINING = "maingaining"
}

export type ColorScheme = string[];

export const coloricDifferenceColorSchemas: { [key: string]: ColorScheme } = {
  [ColoricDifference.LEAN_SURPLUS]: [
    lightBlue[700],
    blue[700],
    lightGreen[700],
    green[700],
    yellow[700],
    orange[700],
    red[700],
  ],
  [ColoricDifference.SURPLUS]: [
    lightBlue[700],
    blue[700],
    lightGreen[700],
    green[700],
    yellow[700],
    orange[700],
    red[700],
  ],
  [ColoricDifference.DEFICIT]: [
    red[700],
    orange[700],
    yellow[700],
    green[700],
    lightGreen[700],
    blue[700],
    lightBlue[700],
  ],
  [ColoricDifference.LEAN_DEFICIT]: [
    red[700],
    orange[700],
    yellow[700],
    green[700],
    lightGreen[700],
    blue[700],
    lightBlue[700],
  ],
  [ColoricDifference.MAINTENANCE]: [
    blue[700],
    green[700],
    orange[700],
    red[700],
  ],
  [ColoricDifference.MAINGAINING]: [
    red[700],
    orange[700],
    green[700],
    blue[700],
    lightBlue[700],
    blue[700],
    green[700],
    orange[700],
    red[700],
  ],
};

export const coloricDifferenceMaxValue: { [key: string]: number } = {
  [ColoricDifference.LEAN_SURPLUS]: 300,
  [ColoricDifference.SURPLUS]: 600,
  [ColoricDifference.DEFICIT]: 0,
  [ColoricDifference.LEAN_DEFICIT]: 0,
  [ColoricDifference.MAINTENANCE]: 200,
  [ColoricDifference.MAINGAINING]: 300,
};

export const coloricDifferenceMinValue: { [key: string]: number } = {
  [ColoricDifference.LEAN_SURPLUS]: 0,
  [ColoricDifference.SURPLUS]: 0,
  [ColoricDifference.DEFICIT]: -800,
  [ColoricDifference.LEAN_DEFICIT]: -300,
  [ColoricDifference.MAINTENANCE]: -100,
  [ColoricDifference.MAINGAINING]: -300,
};

type Target = number;
type Duration = number;
type Intensity = number;

export interface TotalEnergyExpenditure {
  sleep: EnergyOutput;
  normal: EnergyOutput;
  training: EnergyOutput[];
}

export interface EnergyOutput {
  duration: Duration;
  intensity: Intensity;
}

const wednesdayHIIT = (): EnergyOutput => ({
  duration: 0.5,
  intensity: 9,
});

/*
 * Tracked via Fitbit
 * HIIT:
 * Mittwoch,  9. Juni, 293
 * Mittwoch, 16. Juni, 318
 * Mittwoch, 23. Juni, 317
 * Mittwoch, 23. Juni, 192
 * Mittwoch, 30. Juni, 242
 * Mittwoch,  7. Juli, 241
 * Mittwoch, 14. Juli, 292
 * Mittwoch, 21. Juli, 268
 * Mittwoch, 28. Juli, 323
 * Mittwoch,  4. August, 250
 * Mittwoch, 11. August, 246
 * Mittwoch, 18. August, 230
 * Mittwoch, 25. August, 330
 * Average: 3542/13 = 272
 * 272 = Intensity * 1 * 67
 * Intensity = 4
 */
const measuredWednesdayHIIT = (): EnergyOutput => ({
  duration: 1,
  intensity: 4,
});

const tennis = (): EnergyOutput => ({
  duration: 1,
  intensity: 8,
});

/*
 * Tracked via Fitbit
 * Push/Pull:
 * Freitag 30. Juli,   559
 * Samstag 31. Juli,   541
 * Sonntag  1. August, 625
 * Dienstg 10. August, 623
 * Freitag 13. August, 694
 * Samstag 14. August, 589
 * Sonntag 15. August, 620
 * Freitag 20. August, 535
 * Samstag 21. August, 514
 * Sonntag 22. August, 538
 * Average: 5838/10 = 584
 * 584 = Intensity * 1,5 * 67
 * Intensity = 6
 */
const measuredWeightTraining = (): EnergyOutput => ({
  duration: 1.5,
  intensity: 6,
});
// copy of measuredWeightTraining
const measuredWeightTrainingWithCardio = (): EnergyOutput => ({
  duration: 1.5,
  intensity: 6,
});

/*
 * Tracked via Fitbit
 * Push/Pull/Leg+High-Heartrate:
 * Sonntag    04. Juni,   521 (Pull)
 * Donnerstag 08. Juni,   532 (Pull)
 * Sonntag    11. Juni,   651 (Leg)
 * Average: ??/?? = ??
 * ?? = Intensity * 1,5 * 80
 * Intensity = 6
 */
const weightTraining = (): EnergyOutput => ({
  duration: 1.5,
  intensity: 9,
});

const weightTrainingTimeSloted = (): EnergyOutput => ({
  duration: 1,
  intensity: 9,
});

const homeWorkout = (): EnergyOutput => ({
  duration: 1,
  intensity: 7,
});

const joggen = (): EnergyOutput => ({
  duration: 0.75,
  intensity: 8,
});

interface Targets {
  bodyWeight: Target;
  bodyfatInPercent: Target;
  muscleInKg: Target;
  proteinIntakePerBodyWeight?: Target;
}

export type Phases = Phase[];

export const phases: Phases = [{
  name: "Maintenance (Full-Body, 3x)",
  startWeek: "2024CW44",
  endWeek: "2025CW44",
  coloricDifference: ColoricDifference.MAINTENANCE,
  postprandialeThermogenese: 5, // Energy spend on digesting food
  targets: {
    bodyWeight: 76,
    bodyfatInPercent: 16,
    muscleInKg: 36,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [],
  },
}, {
  name: "Lean-Cutting (Push-Pull-Leg-Split mit Cardio, 4x)",
  startWeek: "2023CW37",
  endWeek: "2024CW37",
  coloricDifference: ColoricDifference.LEAN_DEFICIT,
  postprandialeThermogenese: 5, // Energy spend on digesting food
  targets: {
    bodyWeight: 74,
    bodyfatInPercent: 14,
    muscleInKg: 36,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [tennis(), measuredWeightTraining(), measuredWednesdayHIIT(), measuredWeightTraining(), measuredWeightTraining(), measuredWeightTraining()],
  },
}, {
  name: "Cutting (Push-Pull-Leg-Split mit Cardio, 4x)",
  startWeek: "2023CW21",
  endWeek: "2023CW32",
  coloricDifference: ColoricDifference.DEFICIT,
  postprandialeThermogenese: 5, // Energy spend on digesting food
  targets: {
    bodyWeight: 78,
    bodyfatInPercent: 17,
    muscleInKg: 37,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [tennis(), measuredWeightTraining(), measuredWednesdayHIIT(), measuredWeightTraining(), measuredWeightTraining(), measuredWeightTraining()],
  },
}, {
  name: "Bulking (Push-Pull-Leg, 4x)",
  startWeek: "2022CW34",
  endWeek: "2023CW19",
  coloricDifference: ColoricDifference.LEAN_SURPLUS,
  postprandialeThermogenese: 5, // Energy spend on digesting food
  targets: {
    bodyWeight: 82,
    bodyfatInPercent: 19,
    muscleInKg: 38,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [tennis(), measuredWeightTraining(), measuredWednesdayHIIT(), measuredWeightTraining(), measuredWeightTraining(), measuredWeightTraining()],
  },
}, {
  name: "Cutting (Push-Pull-Leg-Split mit Cardio, 4x)",
  startWeek: "2022CW18",
  endWeek: "2022CW33",
  coloricDifference: ColoricDifference.DEFICIT,
  postprandialeThermogenese: 5, // Energy spend on digesting food
  targets: {
    bodyWeight: 72,
    bodyfatInPercent: 12,
    muscleInKg: 36,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [tennis(), measuredWeightTrainingWithCardio(), measuredWednesdayHIIT(), measuredWeightTrainingWithCardio(), measuredWeightTrainingWithCardio(), measuredWeightTrainingWithCardio()],
  },
}, {
  name: "Bulking (Push-Pull-Leg-Split, 4x)",
  startWeek: "2021CW48",
  endWeek: "2022CW17",
  coloricDifference: ColoricDifference.LEAN_SURPLUS,
  postprandialeThermogenese: 5, // Energy spend on digesting food
  targets: {
    bodyWeight: 75,
    bodyfatInPercent: 18,
    muscleInKg: 60,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [tennis(), measuredWeightTraining(), measuredWednesdayHIIT(), measuredWeightTraining(), measuredWeightTraining(), measuredWeightTraining()],
  },
}, {
  name: "Minicut (Bike, 3x)",
  startWeek: "2021CW44",
  endWeek: "2021CW47",
  coloricDifference: ColoricDifference.DEFICIT,
  targets: {
    bodyWeight: 70,
    bodyfatInPercent: 10,
    muscleInKg: 57,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [tennis(), measuredWeightTraining(), measuredWednesdayHIIT(), measuredWeightTraining(), measuredWeightTraining(), measuredWeightTraining()],
  },
}, {
  name: "Bulking (Push-Pull-Split, 4x)",
  startWeek: "2021CW30",
  endWeek: "2021CW43",
  coloricDifference: ColoricDifference.LEAN_SURPLUS,
  regularWeighingDays: [Day.Tuesday, Day.Friday],
  targets: {
    bodyWeight: 72,
    bodyfatInPercent: 17,
    muscleInKg: 58,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [tennis(), measuredWeightTraining(), measuredWednesdayHIIT(), measuredWeightTraining(), measuredWeightTraining(), measuredWeightTraining()],
  },
}, {
  name: "Bulking (self-programmed, sloted, 1-3x)",
  startWeek: "2021CW23",
  endWeek: "2021CW29",
  coloricDifference: ColoricDifference.SURPLUS,
  postprandialeThermogenese: 5, // Energy spend on digesting food
  targets: {
    bodyWeight: 70,
    bodyfatInPercent: 14,
    muscleInKg: 58,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [tennis(), weightTrainingTimeSloted(), wednesdayHIIT(), homeWorkout(), joggen()],
  },
}, {
  name: "Lockdown-Maintenance",
  startWeek: "2021CW09",
  endWeek: "2021CW22",
  coloricDifference: ColoricDifference.MAINTENANCE,
  fakeWeight: 69,
  targets: {
    bodyWeight: 69,
    bodyfatInPercent: 16,
    muscleInKg: 54,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [joggen(), wednesdayHIIT(), joggen()],
  },
}, {
  name: "Post-Op-Maintenance",
  startWeek: "2021CW06",
  endWeek: "2021CW08",
  coloricDifference: ColoricDifference.MAINTENANCE,
  fakeWeight: 69,
  targets: {
    bodyWeight: 69,
    bodyfatInPercent: 16,
    muscleInKg: 54,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [],
  },
}, {
  name: "Lockdown-Maintenance",
  startWeek: "2020CW45",
  endWeek: "2021CW05",
  coloricDifference: ColoricDifference.MAINTENANCE,
  targets: {
    bodyWeight: 70,
    bodyfatInPercent: 16,
    muscleInKg: 54,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [joggen(), wednesdayHIIT(), joggen()],
  },
}, {
  name: "Bulking",
  startWeek: "2020CW24",
  endWeek: "2020CW44",
  coloricDifference: ColoricDifference.SURPLUS,
  targets: {
    bodyWeight: 72,
    bodyfatInPercent: 16.5,
    muscleInKg: 58,
    proteinIntakePerBodyWeight: 2,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [
      weightTraining(),
      wednesdayHIIT(),
      weightTraining(),
      tennis(),
      weightTraining(),
    ],
  },
}, {
  name: "Anzugshose",
  startWeek: "2018CW43",
  endWeek: "2019CW23",
  coloricDifference: ColoricDifference.DEFICIT,
  targets: {
    bodyWeight: 70,
    bodyfatInPercent: 17.5,
    muscleInKg: 54,
  },
  totalEnergyExpenditure: {
    sleep: {
      duration: 8,
      intensity: 1,
    },
    normal: {
      duration: 16,
      intensity: 1.4,
    },
    training: [
      weightTraining(),
      wednesdayHIIT(),
      weightTraining(),
      tennis(),
      weightTraining(),
    ],
  },
}];

export function getExtendedMockPhase(extension: Partial<Phase>) {
  return {
    name: "MockPhase",
    startWeek: "1900CW01",
    endWeek: "1900CW02",
    coloricDifference: ColoricDifference.LEAN_SURPLUS,
    targets: {
      bodyWeight: 0,
      bodyfatInPercent: 0,
      muscleInKg: 0,
      proteinIntakePerBodyWeight: 0,
    },
    totalEnergyExpenditure: {
      sleep: {
        duration: 0,
        intensity: 0,
      },
      normal: {
        duration: 0,
        intensity: 0,
      },
      training: [],
    },
    ...extension
  };
}
