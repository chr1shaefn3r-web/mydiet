import { Recipe } from "./recipes";

export interface DayDiet {
	breakfast: string[];
	lunch: string[];
	dinner: string[];
	snack?: string[];
};
export type Diet = { [key: string]: DayDiet; };

export const diet: Diet = {
	"2020-05-05": {
		"breakfast": ["Muesli"],
		"lunch": ["NuddelnMitTomatensosse", "EinFerreroRocher"],
		"dinner": ["BananeMagerQuarkEiweissSmoothy"]
	},
	"2020-05-06": {
		"breakfast": ["Muesli"],
		"lunch": ["NuddelnMitTomatensosse", "EinFerreroRocher", "EinFerreroRocher"],
		"dinner": ["Ruehrei", "EinFerreroRocher"]
	},
	"2020-05-07": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat", "EinFerreroRocher"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy", "EinFerreroRocher"]
	},
	"2020-05-08": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat", "EinFerreroRocher"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy", "EinFerreroRocher"]
	},
	"2020-05-11": {
		"breakfast": ["Muesli"],
		"lunch": ["Ruehrei", "EinFerreroRocher"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy", "EinFerreroRocher"]
	},
	"2020-05-12": {
		"breakfast": ["Muesli"],
		"lunch": ["NuddelnMitTomatensosse", "EinFerreroRocher"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy", "EinFerreroRocher", "EinFerreroRocher"]
	},
	"2020-05-13": {
		"breakfast": ["Muesli"],
		"lunch": ["NuddelnMitTomatensosse", "EinFerreroRocher", "EinFerreroRocher"],
		"dinner": ["BioLeberwurstbrot", "EinGlasCola", "EinFerreroRocher"]
	},
	"2020-05-14": {
		"breakfast": ["Muesli"],
		"lunch": ["NuddelnMitTomatensosse", "EinFerreroRocher"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy", "EinFerreroRocher", "EinFerreroRocher"]
	},
	"2020-05-25": {
		"breakfast": ["Muesli"],
		"lunch": ["NuddelnMitTomatensosse"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-05-26": {
		"breakfast": ["OatmealHimbeere"],
		"lunch": ["NuddelnMitTomatensosse"],
		"dinner": ["BioRindfleischbrot", "EinFerreroRocher", "EinFerreroRocher"]
	},
	"2020-05-27": {
		"breakfast": ["OatmealHimbeere"],
		"lunch": ["GriechischerSalat", "EinFerreroRocher", "EinFerreroRocher"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["Muesli", "Marmeladenbrot"]
	},
	"2020-05-28": {
		"breakfast": ["OatmealHimbeere"],
		"lunch": ["GriechischerSalat", "EinFerreroRocher", "EinFerreroRocher"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot", "EinFerreroRocher"]
	},
	"2020-05-29": {
		"breakfast": ["OatmealHimbeere"],
		"lunch": ["GriechischerSalat", "EinFerreroRocher"],
		"dinner": ["Piccolinis", "EinGlasCola"]
	},
	"2020-05-30": {
		"breakfast": ["OatmealHimbeere"],
		"snack": ["FrootLoops", "MangoLeinsamenMagerQuarkSmoothy"],
		"lunch": ["Ruehrei", "EinFerreroRocher"],
		"dinner": ["MuesliDaheim", "EinMagnumDoubleChocolate"]
	},
	"2020-06-01": {
		"breakfast": ["OatmealHimbeere"],
		"lunch": ["NudelnMitTomatensosseV2", "EinFerreroRocher"],
		"snack": ["BioSchinkenwurstbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-06-02": {
		"breakfast": ["OatmealHimbeere"],
		"lunch": ["NudelnMitTomatensosseV2", "EinFerreroRocher"],
		"snack": ["BioRindfleischbrot"],
		"dinner": ["FrootLoops", "MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-06-03": {
		"breakfast": ["OatmealHimbeere"],
		"lunch": ["NudelnMitTomatensosseV2", "EinFerreroRocher"],
		"snack": ["Muesli"],
		"dinner": ["NudelnMitTomatensosseV2", "MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-06-04": {
		"breakfast": ["Muesli"],
		"lunch": ["Ruehrei"],
		"dinner": ["GriechischerSalat", "BananeMagerQuarkEiweissSmoothy"]
	},
	"2020-06-05": {
		"breakfast": ["OatmealHimbeere"],
		"lunch": ["GriechischerSalat", "EinFerreroRocher"],
		"snack": ["MangoLeinsamenMagerQuarkFettarmeMilchSmoothy"],
		"dinner": ["Piccolinis", "EinGlasCola"]
	},
	"2020-06-07": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat", "EinFerreroRocher"],
		"snack": ["MangoLeinsamenMagerQuarkFettarmeMilchSmoothy", "FrootLoops"],
		"dinner": ["NudelnMitTomatensosseV2"]
	},
	"2020-06-08": {
		"breakfast": ["OatmealSchokolade"],
		"lunch": ["NudelnMitTomatensosseV2", "EinFerreroRocher"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["BioRindfleischbrot", "EinFerreroRocher", "EinFerreroRocher"]
	},
	"2020-06-09": {
		"breakfast": ["OatmealSchokolade"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkFettarmeMilchSmoothy", "EineBanane", "FrootLoops"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-06-10": {
		"breakfast": ["OatmealSchokolade"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkFettarmeMilchSmoothy", "FrootLoops"],
		"dinner": ["Ruehrei"]
	},
	"2020-06-12": {
		"breakfast": ["OatmealSchokolade"],
		"lunch": ["FrostaHaehnchenCurry", "EinDuplo"],
		"snack": ["MuesliDaheim"],
		"dinner": ["Piccolinis", "EinMagnumDoubleChocolate"]
	},
	"2020-06-14": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["Marmeladenbrot", "NutellaBrot", "MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2020-06-15": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-06-16": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["Ruehrei", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EineKleineKarotte"],
		"dinner": ["NudelnMitTomatensosseV2"]
	},
	"2020-06-17": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EineKarotte"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2020-06-18": {
		"breakfast": ["BigOatmealHimbeere"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioSchinkenwurstbrot", "EinDuplo"]
	},
	"2020-06-19": {
		"breakfast": ["BigOatmealHimbeere"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["Piccolinis", "EinGlasCola", "EinLindtHelloCrunchyNougat"]
	},
	"2020-06-21": {
		"breakfast": ["BigOatmealHimbeere"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EineBanane"],
		"dinner": ["Piccolinis"]
	},
	"2020-06-22": {
		"breakfast": ["BigOatmealHimbeere"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["Lammbratwurstbrot"]
	},
	"2020-06-23": {
		"breakfast": ["BigOatmealHimbeere"],
		"lunch": ["Ruehrei", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-06-24": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["FrostaHaehnchenCurry", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat", "Marmeladenbrot"]
	},
	"2020-06-25": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-06-26": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2"]
	},
	"2020-06-27": {
		"breakfast": ["Muesli"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "FrootLoops", "EineBanane"],
		"dinner": ["GriechischerSalat", "EinGlasCola"]
	},
	"2020-06-29": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat"]
	},
	"2020-06-30": {
		"breakfast": ["BigOatmealHimbeere"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2", "EinDuplo"]
	},
	"2020-07-01": {
		"breakfast": ["Muesli"],
		"lunch": ["Ruehrei", "EinDuplo"],
		"snack": ["BioBierschinkenbrot", "EineBanane", "EinDuplo"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-07-02": {
		"breakfast": ["BigOatmealHimbeere"],
		"lunch": ["BioSchinkenwurstbrot", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2", "EinDuplo"]
	},
	"2020-07-03": {
		"breakfast": ["BigOatmealHimbeere"],
		"lunch": ["BioBierschinkenbrot", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2"]
	},
	"2020-07-04": {
		"breakfast": ["MuesliDaheim"],
		"lunch": ["KartoffelGratinBroccoliDaheim", "EinRinderSteak", "EinDuplo"],
		"snack": ["EineBanane", "MuesliDaheim"],
		"dinner": ["FrostaHaehnchenCurry", "EinMagnumMandel"]
	},
	"2020-07-06": {
		"breakfast": ["BigOatmealHimbeere"],
		"lunch": ["BioBierschinkenbrot", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2", "EinDuplo"]
	},
	"2020-07-07": {
		"breakfast": ["BigOatmealHimbeere"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2020-07-08": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["FrostaHaehnchenCurry", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["Ruehrei"]
	},
	"2020-07-09": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["GriechischerSalat"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["HalfPiccolinis", "EinMoevenpickWalnussWaffel"]
	},
	"2020-07-10": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["Piccolinis"]
	},
	"2020-07-11": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EineTasseCappuccino"],
		"dinner": ["GriechischerSalat", "EinDuplo"]
	},
	"2020-07-13": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy", "FrootLoops"],
		"dinner": ["BioSchinkenwurstbrot", "EinDuplo"]
	},
	"2020-07-14": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot",]
	},
	"2020-07-15": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot", "Marmeladenbrot"]
	},
	"2020-07-16": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["Ruehrei", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2"]
	},
	"2020-07-17": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["BioLyonerwurstbrot", "DreiBadLiebenzellerApfelsaftSchorle"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-07-18": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EineBanane"],
		"dinner": ["KartoffelGratinBroccoliDoubleCheese"]
	},
	"2020-07-19": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["Muesli", "EineBanane", "Marmeladenbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-07-20": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BigOatmealSchokolade"]
	},
	"2020-07-21": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"],
		"snack": ["BioSchinkenwurstbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-07-22": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"],
		"snack": ["BioBierschinkenbrot", "Marmeladenbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy", "EinDuplo"]
	},
	"2020-07-23": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["Ruehrei", "EinDuplo"],
		"dinner": ["BananeMagerQuarkEiweissSmoothy"]
	},
	"2020-07-24": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["Muesli", "EineBanane", "EinDuplo"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-07-26": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2"]
	},
	"2020-07-27": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioLyonerwurstbrot"]
	},
	"2020-07-28": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EineBanane"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-07-29": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["GanzesSubwayChickenTeriyaki", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2"]
	},
	"2020-07-30": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["Ruehrei", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["GriechischerSalat"]
	},
	"2020-07-31": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["Piccolinis", "EinGlasCola", "EinWhiteRussian"]
	},
	"2020-08-01": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["KartoffelGratinBroccoli", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat", "DreiBadLiebenzellerApfelsaftSchorle"]
	},
	"2020-08-03": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["KartoffelGratinBroccoli", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-08-04": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["KartoffelGratinBroccoli", "EinDuplo", "EineBanane"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-08-05": {
		"breakfast": ["BigOatmealSchokolade", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["KartoffelGratinBroccoli", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2"]
	},
	"2020-08-06": {
		"breakfast": ["BigOatmealHimbeereV2",],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EineBanane", "EinSeitenbacherProteinRiegelKakao"],
		"dinner": ["Piccolinis", "EinGlasCola"]
	},
	"2020-08-07": {
		"breakfast": ["SmallOatmealHimbeereV2"],
		"lunch": ["BioBratwurstbrot", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2"]
	},
	"2020-08-09": {
		"breakfast": ["SmallOatmealHimbeereV2", "Marmeladenbrot", "Marmeladenbrot", "EineBanane"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["FrostaHaehnchenCurry", "EinWhiteRussian"]
	},
	"2020-08-17": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["KartoffelGratinBroccoli", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot", "EinGlasCola"]
	},
	"2020-08-18": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["KartoffelGratinBroccoli"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["BioBratwurstbrot"]
	},
	"2020-08-19": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["KartoffelGratinBroccoli", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EineBanane"],
		"dinner": ["NudelnMitTomatensosseV2"]
	},
	"2020-08-20": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["NudelnMitTomatensosseV2"],
		"snack": ["BioRindfleischbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy", "EineKugelSchokoladenEisInDerWaffel", "EineKleinePopcorn"]
	},
	"2020-08-21": {
		"breakfast": ["Muesli"],
		"lunch": ["NudelnMitTomatensosseV2"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat"]
	},
	"2020-08-22": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["NudelnMitTomatensosseV2"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat", "EinCaffeFreddoCappuccino"]
	},
	"2020-08-23": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["Ruehrei", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["KartoffelGratinBroccoliNoCheese", "EinSeitenbacherProteinRiegelSchoko"]
	},
	"2020-08-24": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["KartoffelGratinBroccoliNoCheese", "EinDuplo"]
	},
	"2020-08-25": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["KartoffelGratinBroccoliNoCheese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EinSeitenbacherProteinRiegelKakao"],
		"dinner": ["BioRindfleischbrot"]
	},
	"2020-08-26": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["KartoffelGratinBroccoliNoCheese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot", "EineBanane"]
	},
	"2020-08-27": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["Ruehrei", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2", "EineBanane"]
	},
	"2020-08-28": {
		"breakfast": ["SmallOatmealHimbeereV2", "EineBanane"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["FrostaHaehnchenCurry", "EineMuellermilchProteinVanille"]
	},
	"2020-08-30": {
		"breakfast": ["SmallOatmealHimbeereV2", "EineTasseCappuccino", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["PfirsichLeinsamenMagerQuarkSmoothy"],
		"dinner": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"]
	},
	"2020-08-31": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioRindfleischbrot", "EinDuplo", "EineKiwi"]
	},
	"2020-09-01": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-09-02": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat"]
	},
	"2020-09-03": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["KartoffelGratinBroccoliDoubleCheese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat", "EineKiwi"]
	},
	"2020-09-04": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["NudelnMitTomatensosseV2", "EineKiwi"]
	},
	"2020-09-06": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["EineMuellermilchProteinSchoko"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["Piccolinis", "EinGlasCola", "EinDuplo", "EinDuplo", "EinDuplo", "EinDuplo"]
	},
	"2020-09-07": {
		"breakfast": ["MuesliDaheimHmilch"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["BioRindfleischbrot"]
	},
	"2020-09-08": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-09-09": {
		"breakfast": ["SmallOatmealHimbeereV2"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["EineKleinePopcorn", "EineBanane", "EinDuplo"]
	},
	"2020-09-10": {
		"breakfast": ["Muesli"],
		"lunch": ["NudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["Ruehrei"]
	},
	"2020-09-11": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioRindfleischbrot", "EineBanane"]
	},
	"2020-09-13": {
		"breakfast": ["MuesliDaheimHmilch"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["EineMuellermilchProteinBanane", "EinSeitenbacherProteinRiegelKakao", "EinDuplo"]
	},
	"2020-09-14": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust"]
	},
	"2020-09-15": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["Ruehrei", "EineKiwi"]
	},
	"2020-09-16": {
		"breakfast": ["SmallOatmealHimbeereV2"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EineBanane"],
		"dinner": ["SpiralenNudelnMitTomatensosseV2", "EinSeitenbacherProteinRiegelCappuccino", "EineKiwi"]
	},
	"2020-09-17": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["SpiralenNudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["FrostaHaehnchenCurry", "EinSeitenbacherProteinRiegelKakao"]
	},
	"2020-09-18": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["SpiralenNudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioRindfleischbrot", "EineBanane", "EineKiwi"]
	},
	"2020-09-19": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["SpiralenNudelnMitTomatensosseV2", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat", "EineKiwi"]
	},
	"2020-09-21": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["GriechischerSalat"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["EinChickenCaesarSalad", "EinHalberLiterCola"]
	},
	"2020-09-22": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["KartoffelGratinKaisergemuese", "EineMuellermilchProteinVanille"]
	},
	"2020-09-24": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["KartoffelGratinKaisergemuese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EinDuplo"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust", "EinSeitenbacherProteinRiegelSchoko", "EineBanane", "EineKiwi"]
	},
	"2020-09-25": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["KartoffelGratinKaisergemuese", "EineMuellermilchProteinSchoko", "EineBanane"]
	},
	"2020-09-27": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EinDuplo"],
		"dinner": ["Piccolinis", "EineKiwi"]
	},
	"2020-10-05": {
		"breakfast": ["Muesli", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["KartoffelGratinKaisergemuese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["EinChickenCaesarSalad", "EinHalberLiterCola"]
	},
	"2020-10-07": {
		"breakfast": ["SmallOatmealSchokolade", "EineBanane"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["KartoffelGratinKaisergemuese", "EineMuellermilchProteinVanille", "EinDuplo"]
	},
	"2020-10-08": {
		"breakfast": ["Muesli", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["KartoffelGratinKaisergemuese", "EinDuplo"]
	},
	"2020-10-09": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["BioRindfleischbrot", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["SpiralenNudelnMitTomatensosseV1GemischtesHack", "EinDuplo"]
	},
	"2020-10-11": {
		"breakfast": ["Muesli", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["Marmeladenbrot", "Marmeladenbrot", "EinDuplo", "EineBanane"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["SpiralenNudelnMitTomatensosseV1GemischtesHack", "EinDuplo"]
	},
	"2020-10-13": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["SpiralenNudelnMitTomatensosseV1GemischtesHack", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust"]
	},
	"2020-10-14": {
		"breakfast": ["SmallOatmealSchokolade", "EineBanane"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EineMuellermilchProteinVanille", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy", "EineKleinePopcorn"]
	},
	"2020-10-15": {
		"breakfast": ["Muesli", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["Piccolinis", "EinHalberLiterCola"]
	},
	"2020-10-16": {
		"breakfast": ["Muesli", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["GriechischerSalat"]
	},
	"2020-10-17": {
		"breakfast": ["SmallOatmealHimbeereV2", "EineBanane"],
		"lunch": ["BioBierschinkenbrot", "EinDuplo"],
		"snack": ["EineMuellermilchProteinVanille", "EinDuplo"],
		"dinner": ["MuesliDaheimHmilch", "EinSeitenbacherProteinRiegelVanille"]
	},
	"2020-10-20": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["BioRindfleischbrot", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["KartoffelGratinBroccoli", "EineMuellermilchProteinSchoko"]
	},
	"2020-10-21": {
		"breakfast": ["SmallOatmealHimbeereV2", "EineBanane"],
		"lunch": ["KartoffelGratinBroccoli", "KartoffelGratinBroccoli"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["Ruehrei"]
	},
	"2020-10-23": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["Piccolinis"]
	},
	"2020-10-24": {
		"breakfast": ["SmallOatmealHimbeereV2", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["KartoffelGratinBroccoli", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EinDuplo"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo", "Marmeladenbrot"]
	},
	"2020-10-26": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-10-27": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy", "EinSeitenbacherProteinRiegelKakao"],
		"dinner": ["KartoffelGratinBroccoli", "EineMuellermilchProteinVanille", "EinDuplo"]
	},
	"2020-10-28": {
		"breakfast": ["Muesli", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["KartoffelGratinBroccoli", "EinDuplo"],
		"snack": ["BioLeberwurstbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-10-29": {
		"breakfast": ["Muesli", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["KartoffelGratinBroccoli", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["EineMuellermilchProteinSchoko", "EinHalberLiterCola", "EineKleinePopcorn"]
	},
	"2020-10-31": {
		"breakfast": ["SmallOatmealSchokolade", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["EineSushiboxAyami"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["NudelnMitGarnelen"]
	},
	"2020-11-01": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothy"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EinDuplo", "EineBanane"],
		"dinner": ["Piccolinis", "EinGlasCola", "EinGlasCola"]
	},
	"2020-11-02": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["EineMuellermilchProteinVanille"],
		"dinner": ["Ruehrei"]
	},
	"2020-11-03": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBratwurstbrot"]
	},
	"2020-11-04": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["GriechischerSalat"]
	},
	"2020-11-05": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2020-11-06": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["GriechischerSalat"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust"]
	},
	"2020-11-09": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["BioBratwurstbrot"]
	},
	"2020-11-10": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-11-11": {
		"breakfast": ["SmallOatmealSchokolade", "EineBanane"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["KartoffelGratinKaisergemuese", "Marmeladenbrot"]
	},
	"2020-11-12": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemuese"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["Ruehrei"]
	},
	"2020-11-16": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["KartoffelGratinKaisergemuese", "EineBanane"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2020-11-17": {
		"breakfast": [],
		"lunch": ["BioBierschinkenbrot"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["NudelnMitGarnelen"]
	},
	"2020-11-18": {
		"breakfast": ["SmallOatmealHimbeereV2", "EineBanane"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["EineMuellermilchProteinVanille", "EinSeitenbacherProteinRiegelSchoko"]
	},
	"2020-11-19": {
		"breakfast": ["Muesli"],
		"lunch": ["NudelnMitGarnelen", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"]
	},
	"2020-11-20": {
		"breakfast": ["EineBanane"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["EineMuellermilchProteinSchoko"]
	},
	"2020-11-22": {
		"breakfast": ["Muesli"],
		"lunch": ["Ruehrei"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["KartoffelGratinKaisergemuese", "EinDuplo"]
	},
	"2020-11-24": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["KartoffelGratinKaisergemuese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["BioRindfleischbrot"]
	},
	"2020-11-25": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemuese"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat"]
	},
	"2020-11-26": {
		"breakfast": ["SmallOatmealHimbeereV2"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["Muesli"]
	},
	"2020-11-27": {
		"breakfast": ["EineBanane"],
		"lunch": ["GriechischerSalat"],
		"snack": ["BananeMagerQuarkEiweissSmoothy"],
		"dinner": ["SpirelliBolognese"]
	},
	"2020-11-28": {
		"breakfast": ["SmallOatmealHimbeereV2"],
		"lunch": ["FrostaHaehnchenCurry"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinDuplo", "EinSeitenbacherProteinRiegelCappuccino"]
	},
	"2020-11-29": {
		"breakfast": ["BigOatmealSchokolade"],
		"lunch": ["Ruehrei"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-11-30": {
		"breakfast": ["SmallOatmealHimbeereV2"],
		"lunch": ["SpirelliBolognese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-01": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["SpirelliBolognese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-02": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["SpirelliBolognese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-03": {
		"breakfast": ["Muesli"],
		"lunch": ["BioBierschinkenbrot", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-04": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothy", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust"]
	},
	"2020-12-07": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-08": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat"]
	},
	"2020-12-09": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat"]
	},
	"2020-12-10": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-11": {
		"breakfast": ["Muesli"],
		"lunch": ["BioBierschinkenbrot", "EinDuplo"],
		"snack": ["EineMuellermilchProteinVanille"],
		"dinner": ["Ruehrei"]
	},
	"2020-12-13": {
		"breakfast": ["Marmeladenbrot", "Marmeladenbrot", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-14": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-15": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-16": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["EinFerreroRocher", "EinFerreroRocher", "EinFerreroRocher", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-17": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothy"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2020-12-18": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothy"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["SpirelliBolognese", "EinDuplo"]
	},
	"2020-12-19": {
		"breakfast": ["Muesli", "EineMuellermilchProteinSchoko"],
		"lunch": ["EineSushiboxAyami", "EinFerreroRocher", "EinFerreroRocher"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy", "FrootLoops"]
	},
	"2020-12-20": {
		"breakfast": ["SmallOatmealSchokolade"],
		"lunch": ["SpirelliBolognese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-21": {
		"breakfast": ["Muesli"],
		"lunch": ["SpirelliBolognese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2020-12-22": {
		"breakfast": ["Muesli"],
		"lunch": ["SpirelliBolognese"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy",]
	},
	"2020-12-23": {
		"breakfast": ["SmallOatmealHimbeereV2"],
		"lunch": ["BananeMagerQuarkEiweissSmoothy", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EineSushiboxAyaka", "EineMuellermilchProteinVanille"]
	},
	"2020-12-27": {
		"breakfast": ["MuesliDaheim"],
		"lunch": ["FrostaHaehnchenCurry", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["FrootLoops"]
	},
	"2020-12-28": {
		"breakfast": ["EinDuplo"],
		"lunch": ["BioBratwurstbrot"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["GriechischerSalat"]
	},
	"2020-12-29": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["FrootLoops"]
	},
	"2020-12-30": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["GriechischerSalat"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothy"],
		"dinner": ["FrootLoops"]
	},
	"2021-01-05": {
		"breakfast": ["MuesliDaheimHmilch"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2021-01-06": {
		"breakfast": ["SmallOatmealHimbeereV2"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["Piccolinis"]
	},
	"2021-01-07": {
		"breakfast": ["BigOatmealHimbeereV2"],
		"lunch": ["NudelnMitGarnelen", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BananeMagerQuarkEiweissSmoothy"]
	},
	"2021-01-10": {
		"breakfast": ["Muesli"],
		"lunch": ["RuehreiRaw", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "FrootLoops"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust", "EinFerreroRocher"]
	},
	"2021-01-12": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["ApfelZimtLeinsamenMagerQuarkSmoothy"]
	},
	"2021-01-13": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy", "EinDuplo"]
	},
	"2021-01-14": {
		"breakfast": ["BioBierschinkenbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothy"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["GriechischerSalat"]
	},
	"2021-01-15": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2021-01-18": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2021-01-19": {
		"breakfast": ["BioBierschinkenbrot"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2021-01-20": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2021-01-21": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["NudelnMitGarnelen"]
	},
	"2021-01-22": {
		"breakfast": ["Muesli"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["Piccolinis", "EinGlasCola"]
	},
	"2021-01-24": {
		"breakfast": ["MuesliDaheim"],
		"lunch": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"snack": ["NudelnMitGarnelen"],
		"dinner": ["HeidelbeerenLeinsamenMagerQuarkSmoothy"]
	},
	"2021-01-25": {
		"breakfast": ["Muesli"],
		"lunch": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"snack": [],
		"dinner": ["NudelnMitGarnelen"]
	},
	"2021-01-26": {
		"breakfast": ["Muesli"],
		"lunch": ["HeidelbeerenLeinsamenMagerQuarkSmoothy"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "EineMuellermilchProteinVanille"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"]
	},
	"2021-01-27": {
		"breakfast": ["BioBierschinkenbrot"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2021-01-28": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["Ruehrei"]
	},
	"2021-01-29": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "EineMuellermilchProteinVanille"],
		"dinner": ["FrostaHaehnchenCurry", "EinDuplo"]
	},
	"2021-02-01": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EineMuellermilchProteinVanille"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2021-02-02": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothy"]
	},
	"2021-02-03": {
		"breakfast": ["BioBierschinkenbrot", "EineBanane", "EinDuplo"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BananeMagerQuarkEiweissSmoothy", "EinDuplo"]
	},
	"2021-02-04": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothy"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["Ruehrei"]
	},
	"2021-02-05": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothy"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["SpirelliBolognese", "EinDuplo"]
	},
	"2021-02-06": {
		"breakfast": ["Muesli"],
		"lunch": ["SpirelliBolognese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-02-08": {
		"breakfast": ["Muesli", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["SpirelliBolognese"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-02-09": {
		"breakfast": ["BioBierschinkenbrot"],
		"lunch": ["SpirelliBolognese"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-02-10": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["KirchererbsenCurry"]
	},
	"2021-02-11": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"snack": [],
		"dinner": ["KirchererbsenCurry"]
	},
	"2021-02-12": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["KirchererbsenCurry"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"dinner": ["Marmeladenbrot", "Marmeladenbrot"]
	},
	"2021-02-13": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["KirchererbsenCurry"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"dinner": ["Marmeladenbrot", "Marmeladenbrot"]
	},
	"2021-02-15": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"dinner": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"]
	},
	"2021-02-16": {
		"breakfast": ["BioBierschinkenbrot"],
		"lunch": ["GriechischerSalat"],
		"snack": ["EinDuplo"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-02-17": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"dinner": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"]
	},
	"2021-02-18": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"]
	},
	"2021-02-19": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"dinner": ["EinSeitenbacherProteinRiegelCappuccino", "EinMagnumMandel"]
	},
	"2021-02-22": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-02-23": {
		"breakfast": ["BioSchinkenwurstbrot"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BananeMagerQuarkEiweissSmoothyArlaMilch"]
	},
	"2021-02-24": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust"]
	},
	"2021-02-25": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-02-28": {
		"breakfast": ["EinKinderRiegel", "EinKinderRiegel", "EineBanane", "ProteinPancakes"],
		"lunch": ["FrostaHaehnchenCurry", "EinDuplo"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-03-01": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-03-02": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch", "EinDuplo"]
	},
	"2021-03-03": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"]
	},
	"2021-03-04": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BananeMagerQuarkEiweissSmoothyArlaMilch", "EinDuplo"]
	},
	"2021-03-05": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch", "Marmeladenbrot"]
	},
	"2021-03-06": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch", "FrootLoops"]
	},
	"2021-03-08": {
		"breakfast": ["Muesli"],
		"lunch": ["FrostaHaehnchenCurry", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-03-09": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["BananeMagerQuarkEiweissSmoothyArlaMilch"],
		"snack": ["EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["NudelnMitGarnelen"]
	},
	"2021-03-10": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-03-11": {
		"breakfast": ["Muesli"],
		"lunch": ["NudelnMitGarnelen"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-03-12": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["NudelnMitGarnelen"]
	},
	"2021-03-15": {
		"breakfast": ["MuesliDaheim"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"snack": ["EineBanane"],
		"dinner": ["Ruehrei", "FrootLoops"]
	},
	"2021-03-16": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"snack": ["EineBanane"],
		"dinner": ["Piccolinis", "EinGlasCola"]
	},
	"2021-03-17": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["SpirelliBolognese"]
	},
	"2021-03-18": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["SpirelliBolognese"]
	},
	"2021-03-19": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"snack": [],
		"dinner": ["SpirelliBolognese"]
	},
	"2021-03-20": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["GriechischerSalat"]
	},
	"2021-03-22": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["SpirelliBolognese"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-03-23": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch"]
	},
	"2021-03-24": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyArlaMilch", "EinDuplo"]
	},
	"2021-03-25": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["RuehreiArlaMilch", "EinDuplo"]
	},
	"2021-03-26": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["PapaSpezial"]
	},
	"2021-03-27": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["PapaSpezial"]
	},
	"2021-03-29": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["PapaSpezial"]
	},
	"2021-03-30": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["PapaSpezial"]
	},
	"2021-03-31": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": [],
		"dinner": ["NudelnMitGarnelenLaktosefrei"]
	},
	"2021-04-01": {
		"breakfast": ["Muesli"],
		"lunch": ["NudelnMitGarnelenLaktosefrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-04-02": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["NudelnMitGarnelenLaktosefrei"],
		"snack": ["EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-04-06": {
		"breakfast": ["Muesli"],
		"lunch": ["NudelnMitGarnelenLaktosefrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-04-07": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": [],
		"dinner": ["Piccolinis", "EinGlasCola"]
	},
	"2021-04-08": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["FrostaHaehnchenCurry", "EinDuplo"]
	},
	"2021-04-10": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["Piccolinis", "EinGlasCola"]
	},
	"2021-04-11": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["BananeMagerQuarkEiweissSmoothyLaktoseFrei"],
		"snack": [],
		"dinner": ["SpirelliBologneseLaktoseFrei"]
	},
	"2021-04-12": {
		"breakfast": ["AvocadoBrot"],
		"lunch": ["SpirelliBologneseLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["BananeMagerQuarkEiweissSmoothyLaktoseFrei"]
	},
	"2021-04-13": {
		"breakfast": ["Muesli"],
		"lunch": ["SpirelliBologneseLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-04-14": {
		"breakfast": ["BioLyonerwurstbrot"],
		"lunch": ["SpirelliBologneseLaktoseFrei"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-04-15": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["PapaSpezialBandnudeln"]
	},
	"2021-04-16": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": [],
		"dinner": ["PapaSpezialBandnudeln"]
	},
	"2021-04-19": {
		"breakfast": ["Muesli"],
		"lunch": ["PapaSpezialBandnudeln"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-04-20": {
		"breakfast": ["Muesli"],
		"lunch": ["PapaSpezialBandnudeln"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-04-21": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["KartoffelGratinKaisergemueseJaKaese"]
	},
	"2021-04-22": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"]
	},
	"2021-04-23": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"]
	},
	"2021-04-27": {
		"breakfast": ["Muesli", "EinCaffeFreddoCappuccino"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"]
	},
	"2021-04-28": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-04-29": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-04-30": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["BananeMagerQuarkEiweissSmoothyLaktoseFrei"],
		"dinner": ["SmallPiccolinis"]
	},
	"2021-05-03": {
		"breakfast": ["Muesli"],
		"lunch": ["RuehreiArlaMilch", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-05-04": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["EineMuellermilchProteinVanille", "EinDuplo"]
	},
	"2021-05-05": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["SpirelliBologneseLaktoseFreiMehrHackfleisch"]
	},
	"2021-05-06": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["SpirelliBologneseLaktoseFreiMehrHackfleisch"]
	},
	"2021-05-07": {
		"breakfast": ["BioLyonerwurstbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": [],
		"dinner": ["SpirelliBologneseLaktoseFreiMehrHackfleisch"]
	},
	"2021-05-08": {
		"breakfast": ["Muesli"],
		"lunch": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"snack": ["SpirelliBologneseLaktoseFreiMehrHackfleisch"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-05-09": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["KartoffelGratinKaisergemueseJaKaese", "FrootLoops"]
	},
	"2021-05-10": {
		"breakfast": ["BioBratwurstbrot"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["BananeMagerQuarkEiweissSmoothyLaktoseFrei"]
	},
	"2021-05-12": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-05-13": {
		"breakfast": ["BioLyonerwurstbrot"],
		"lunch": ["KartoffelGratinKaisergemueseJaKaese", "EinStarbucksCappuccino"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-05-16": {
		"breakfast": ["MuesliDaheim"],
		"lunch": ["EinSeitenbacherProteinRiegelCappuccino"],
		"snack": ["Piccolinis", "EinGlasCola"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-05-17": {
		"breakfast": ["Muesli"],
		"lunch": ["RuehreiArlaMilch"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-05-18": {
		"breakfast": ["BioLeberwurstbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2021-05-19": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["GriechischerSalat", "EinDuplo"]
	},
	"2021-05-20": {
		"breakfast": ["BioRindfleischbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["GriechischerSalat"]
	},
	"2021-05-21": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["GriechischerSalat", "Marmeladenbrot", "EinDuplo"]
	},
	"2021-05-22": {
		"breakfast": ["EinStarbucksCappuccino", "Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["KirchererbsenCurry", "EinDuplo", "Marmeladenbrot", "EinDuplo"]
	},
	"2021-05-24": {
		"breakfast": ["Muesli"],
		"lunch": ["KirchererbsenCurry", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-05-25": {
		"breakfast": ["LandmetzgereiSpringerBierschinkenBrot"],
		"lunch": ["KirchererbsenCurry"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-05-26": {
		"breakfast": ["Muesli"],
		"lunch": ["KirchererbsenCurry", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-05-27": {
		"breakfast": ["LandmetzgereiSpringerBierschinkenBrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EineBanane"],
		"dinner": ["SpirelliBologneseLaktoseFrei"]
	},
	"2021-05-31": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["SpirelliBologneseLaktoseFrei"]
	},
	"2021-06-01": {
		"breakfast": ["RehmBierschinkenBrot"],
		"lunch": ["SpirelliBologneseLaktoseFrei"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-06-02": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["SpirelliBologneseLaktoseFrei"]
	},
	"2021-06-03": {
		"breakfast": ["RehmSchinkenwurstBrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2021-06-07": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["GriechischerSalat", "EinDuplo"]
	},
	"2021-06-08": {
		"breakfast": ["GriechischerSalat"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["BioRindswurstbrot", "EinDuplo"]
	},
	"2021-06-09": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["GriechischerSalat"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-06-10": {
		"breakfast": ["BioSchinkenwurstbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust", "EinWhiteRussianArlaMilch"]
	},
	"2021-06-11": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane", "Marmeladenbrot"],
		"dinner": ["EineSushiboxAyami", "EinDuplo"]
	},
	"2021-06-13": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake", "EinDuplo"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["EineSushiboxAyaka", "EinDuplo"]
	},
	"2021-06-14": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["RuehreiArlaMilch", "EinWhiteRussianArlaMilch"]
	},
	"2021-06-15": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo", "FrootLoopsArlaMilch"]
	},
	"2021-06-16": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-06-17": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-06-18": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["SpirelliBologneseLaktoseFrei", "EinDuplo"]
	},
	"2021-06-19": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelSchoko", "EineBanane"],
		"dinner": ["SpirelliBologneseLaktoseFrei", "EinDuplo"]
	},
	"2021-06-21": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["SpirelliBologneseLaktoseFrei", "EinDuplo"]
	},
	"2021-06-22": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane", "EinDuplo", "EineWiltmannSalamissimoMiniSalami", "EineWiltmannSalamissimoMiniSalami"],
		"dinner": ["SpirelliBologneseLaktoseFrei", "EinDuplo"]
	},
	"2021-06-23": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["RuehreiArlaMilch", "EinDuplo"]
	},
	"2021-06-24": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["NudelnMitRiesenGarnelen"]
	},
	"2021-06-25": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": [],
		"dinner": ["Piccolinis", "EinGlasCola", "EinGlasCola"]
	},
	"2021-06-27": {
		"breakfast": ["Muesli", "BodyAttackWheyDeluxeShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["NudelnMitRiesenGarnelen"]
	},
	"2021-06-28": {
		"breakfast": ["Muesli"],
		"lunch": ["RehmSchinkenwurstBrot"],
		"snack": ["NudelnMitRiesenGarnelen"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-06-29": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane", "EinDuplo", "EineWiltmannSalamissimoMiniSalami", "EineWiltmannSalamissimoMiniSalami"],
		"dinner": ["ZAPProteinShakePro80", "NudelnMitRiesenGarnelen"]
	},
	"2021-06-30": {
		"breakfast": ["Muesli"],
		"lunch": ["RehmBierschinkenBrot"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["KartoffelGratinKaisergemuese"]
	},
	"2021-07-01": {
		"breakfast": ["Muesli"],
		"lunch": ["FrootLoops", "MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane", "EinDuplo", "EineWiltmannSalamissimoMiniSalami", "EineWiltmannSalamissimoMiniSalami"],
		"dinner": ["KartoffelGratinKaisergemuese", "EinDuplo"]
	},
	"2021-07-03": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemuese", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinSeitenbacherProteinRiegelCappuccino", "EineBanane", "EineWiltmannSalamissimoMiniSalami", "EineWiltmannSalamissimoMiniSalami"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust"]
	},
	"2021-07-05": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemuese", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EineBanane", "EinDuplo"],
		"dinner": ["EineMuellermilchProteinSchoko", "MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-07-07": {
		"breakfast": ["Muesli"],
		"lunch": ["RehmBierschinkenBrot"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["Piccolinis"]
	},
	"2021-07-08": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["SpirelliBologneseLaktoseFrei"]
	},
	"2021-07-09": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["SpirelliBologneseLaktoseFrei"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinSeitenbacherProteinRiegelVanille", "EineBanane"]
	},
	"2021-07-12": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"dinner": ["RehmSchinkenwurstBrot", "EineBanane", "EinSeitenbacherProteinRiegelVanille"]
	},
	"2021-07-13": {
		"breakfast": ["Muesli"],
		"lunch": ["SpirelliBologneseLaktoseFrei"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["ZAPProteinShakePro80", "KartoffelGratinKaisergemueseLaktosefreiVielSahne"]
	},
	"2021-07-14": {
		"breakfast": ["Muesli"],
		"lunch": ["BioRindswurstbrot"],
		"snack": ["SpirelliBologneseLaktoseFrei", "EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-07-16": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiVielSahne", "EinDuplo"],
		"snack": ["RuehreiArlaMilch"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-07-19": {
		"breakfast": ["Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiVielSahne", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["BioSchinkenwurstbrot"]
	},
	"2021-07-20": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiVielSahne"]
	},
	"2021-07-21": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust"]
	},
	"2021-07-22": {
		"breakfast": ["Muesli"],
		"lunch": ["GriechischerSalat", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust"]
	},
	"2021-07-23": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2021-07-25": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["SpirelliBologneseLaktoseFrei", "Marmeladenbrot"]
	},
	"2021-07-26": {
		"breakfast": ["Muesli"],
		"lunch": ["SpirelliBologneseLaktoseFrei", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EineBanane"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2021-07-27": {
		"breakfast": ["Muesli"],
		"lunch": ["SpirelliBologneseLaktoseFrei", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["RuehreiArlaMilch"]
	},
	"2021-07-28": {
		"breakfast": ["Muesli", "EineBanane"],
		"lunch": ["SpirelliBologneseLaktoseFrei"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["NudelnMitGarnelenLaktosefrei"]
	},
	"2021-07-29": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["NudelnMitGarnelenLaktosefrei", "EinDuplo"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinSeitenbacherProteinRiegelCappuccino", "EineBanane"]
	},
	"2021-07-30": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["NudelnMitGarnelenLaktosefrei", "EinDuplo"],
		"snack": ["FrootLoopsArlaMilch"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinSeitenbacherProteinRiegelCappuccino", "EineBanane"]
	},
	"2021-08-02": {
		"breakfast": ["Muesli", "EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["FrootLoopsArlaMilch"],
		"dinner": ["NudelnMitGarnelenLaktosefrei"]
	},
	"2021-08-03": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["MassamanCurry", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"]
	},
	"2021-08-04": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiVielSahne"]
	},
	"2021-08-05": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["MassamanCurry", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"]
	},
	"2021-08-09": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiVielSahne", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane", "FrootLoopsArlaMilch"]
	},
	"2021-08-10": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiVielSahne", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane", "FrootLoopsArlaMilch"]
	},
	"2021-08-11": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane", "FrootLoopsArlaMilch"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"]
	},
	"2021-08-12": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-08-13": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EinSeitenbacherProteinRiegelCappuccino", "EineBanane", "BioRindfleischbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-08-16": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["SpirelliBolognese", "EinDuplo"]
	},
	"2021-08-17": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["SpirelliBolognese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-08-18": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["SpirelliBolognese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-08-20": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MassamanCurryMehrKartoffel", "Marmeladenbrot", "EinDuplo"]
	},
	"2021-08-21": {
		"breakfast": ["Muesli", "EinCaffeFreddoCappuccino", "BodyAttackIsoWheyShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MassamanCurryMehrKartoffel", "EinDuplo"]
	},
	"2021-08-23": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["SpirelliBolognese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-08-24": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MassamanCurryMehrKartoffel"]
	},
	"2021-08-25": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MassamanCurryMehrKartoffel"]
	},
	"2021-08-27": {
		"breakfast": ["Muesli", "BioRindfleischbrot"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiVielSahne"]
	},
	"2021-08-28": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake", "EinCaffeFreddoCappuccino"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiVielSahne", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-08-30": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["BioBierschinkenbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-08-31": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiVielSahne", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-09-04": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiVielSahne", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EineKleineCola"]
	},
	"2021-09-05": {
		"breakfast": ["Muesli", "BodyAttackIsoWheyShake"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "FrootLoops"]
	},
	"2021-09-06": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["BioBierschinkenbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-09-07": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "Marmeladenbrot"],
		"dinner": ["RuehreiArlaMilch", "EinDuplo"]
	},
	"2021-09-08": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "Marmeladenbrot"],
		"dinner": ["SpirelliBolognese", "EinDuplo"]
	},
	"2021-09-09": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["SpirelliBolognese"]
	},
	"2021-09-10": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["BioRindfleischbrot"],
		"dinner": ["SpirelliBolognese"]
	},
	"2021-09-14": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["ParkkaffeeKaesekuchen", "EineTasseCappuccino"],
		"dinner": ["FrostaHaehnchenCurry", "EinDuplo"]
	},
	"2021-09-15": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["BioRindfleischbrot"],
		"dinner": ["SpirelliBolognese"]
	},
	"2021-09-16": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": [],
		"dinner": ["MassamanCurryMehrKartoffel"]
	},
	"2021-09-17": {
		"breakfast": ["Muesli"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["BioBierschinkenbrot"],
		"dinner": ["MassamanCurryMehrKartoffel"]
	},
	"2021-09-18": {
		"breakfast": ["Muesli", "EinCaffeFreddoCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["MassamanCurryMehrKartoffel"],
		"dinner": ["HalbesRuehreiArlaMilch"]
	},
	"2021-09-21": {
		"breakfast": ["Muesli"],
		"lunch": ["BioBierschinkenbrot", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust"]
	},
	"2021-09-23": {
		"breakfast": ["Muesli"],
		"lunch": ["BioRindswurstbrot", "EinDuplo"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"]
	},
	"2021-09-24": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"],
		"snack": ["Marmeladenbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinHalberLiterCola"]
	},
	"2021-09-25": {
		"breakfast": ["Muesli", "EinCaffeFreddoCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["SpirelliBologneseV2"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinMagnumMandel"]
	},
	"2021-09-27": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["SpirelliBologneseV2"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["EinChickenCaesarSalad", "EinGlasCola"]
	},
	"2021-09-28": {
		"breakfast": ["Muesli"],
		"lunch": ["SpirelliBologneseV2"],
		"snack": ["BioRindswurstbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-09-29": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["SpirelliBologneseV2"],
		"snack": ["Marmeladenbrot"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-10-02": {
		"breakfast": ["Muesli", "EinCaffeFreddoCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["MassamanCurryMehrKartoffel"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["HalbesRuehreiArlaMilch"]
	},
	"2021-10-05": {
		"breakfast": ["Muesli"],
		"lunch": ["MassamanCurryMehrKartoffel"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2021-10-06": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinDuplo"],
		"lunch": ["MassamanCurryMehrKartoffel"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2021-10-07": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["HalbesRuehreiArlaMilch"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust"]
	},
	"2021-10-08": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["EinBodyAttackProteinCoffee", "EinDuplo"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinGlasCola", "EinGlasCola", "EinGlasCola", "EinGlasCola"]
	},
	"2021-10-09": {
		"breakfast": ["Muesli", "EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["EinDuplo", "ReisBohnenPaprikaHaenchenbrust"],
		"snack": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"dinner": ["Marmeladenbrot", "EinDuplo"]
	},
	"2021-10-12": {
		"breakfast": ["Muesli"],
		"lunch": ["BioSchinkenwurstbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinBodyAttackProteinCoffee"],
		"dinner": ["ReisBohnenPaprikaHaenchenbrust", "EinDuplo"]
	},
	"2021-10-13": {
		"breakfast": ["Muesli"],
		"lunch": ["BioRindswurstbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["Piccolinis", "EineKleinePopcorn"]
	},
	"2021-10-14": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["RuehreiV2"],
		"snack": ["HalbesJackLinksBeefJerkyOriginal", "Marmeladenbrot"],
		"dinner": ["ReisBohnenPaprikaGarnelen", "EinDuplo"]
	},
	"2021-10-15": {
		"breakfast": ["Muesli", "MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"lunch": ["FrostaHaehnchenCurry"],
		"snack": ["EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ReisBohnenPaprikaGarnelen"]
	},
	"2021-10-16": {
		"breakfast": ["Muesli", "EinCaffeFreddoCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["ReisBohnenPaprikaGarnelen"],
		"snack": ["BodyAttackIsoWheyShake"],
		"dinner": ["ZwiebelBurger", "EinDuplo"]
	},
	"2021-10-19": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["ReisBohnenPaprikaGarnelen"],
		"snack": ["EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["RuehreiV2", "EinDuplo"]
	},
	"2021-10-20": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["BioSchinkenwurstbrot"],
		"snack": ["MarmeladenEiweissbrot"],
		"dinner": ["ZwiebelBurger"]
	},
	"2021-10-21": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["BioRindfleischbrot"],
		"snack": ["FrostaHaehnchenCurry"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-10-22": {
		"breakfast": ["Muesli"],
		"lunch": ["MarmeladenEiweissbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2021-10-26": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["BioBierschinkenbrot"],
		"snack": ["EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2021-10-27": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["BioRindfleischbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2021-10-28": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["RuehreiV2"],
		"snack": ["EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2021-11-02": {
		"breakfast": ["MarmeladenEiweissbrot"],
		"lunch": ["RuehreiV2"],
		"snack": ["EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2021-11-04": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["BioBierschinkenbrot"],
		"snack": [],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch"]
	},
	"2021-11-05": {
		"breakfast": [],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": ["EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "EineSushiboxAyami"]
	},
	"2021-11-06": {
		"breakfast": ["EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": [],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2021-11-07": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "RuehreiV2"],
		"lunch": ["MarmeladenEiweissbrot"],
		"snack": ["BodyAttackIsoWheyShake"],
		"dinner": ["EineSushiboxAyaka"]
	},
	"2021-11-09": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["BioBierschinkenbrot"],
		"snack": [],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch"]
	},
	"2021-11-10": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["BioRindfleischbrot"],
		"snack": ["MarmeladenEiweissbrot"],
		"dinner": ["ReisBohnenPaprikaDoppeltGarnelen"]
	},
	"2021-11-11": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"],
		"snack": ["MarmeladenEiweissbrot"],
		"dinner": ["ReisBohnenPaprikaDoppeltGarnelen"]
	},
	"2021-11-12": {
		"breakfast": ["RuehreiV2"],
		"lunch": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"snack": ["BodyAttackIsoWheyShake"],
		"dinner": ["ReisBohnenPaprikaDoppeltGarnelen"]
	},
	"2021-11-13": {
		"breakfast": ["MarmeladenEiweissbrot"],
		"lunch": ["EinYfoodClassicChoco"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2021-11-16": {
		"breakfast": ["MarmeladenEiweissbrot"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-11-17": {
		"breakfast": ["MarmeladenEiweissbrot"],
		"lunch": ["ReisBohnenPaprikaDoppeltGarnelen"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BodyAttackIsoWheyShake", "EineWiltmannSalamissimoMiniSalamiPikant", "EineWiltmannSalamissimoMiniSalamiPikant"]
	},
	"2021-11-18": {
		"breakfast": ["MarmeladenEiweissbrot"],
		"lunch": ["BioBierschinkenbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["RuehreiV2"]
	},
	"2021-11-19": {
		"breakfast": ["MarmeladenEiweissbrot"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BioRindfleischbrot"]
	},
	"2021-11-20": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": [],
		"dinner": ["EinYfoodClassicChoco"]
	},
	"2021-11-21": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["RuehreiV2"],
		"snack": [],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch"]
	},
	"2021-11-23": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-11-24": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": [],
		"dinner": ["BodyAttackIsoWheyShake", "EineWiltmannSalamissimoMiniSalamiPikant", "EineWiltmannSalamissimoMiniSalamiPikant"]
	},
	"2021-11-25": {
		"breakfast": ["MarmeladenEiweissbrot", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["FrostaHaehnchenCurry"],
		"snack": [],
		"dinner": ["BioRindfleischbrot", "EinGlasCola"]
	},
	"2021-11-26": {
		"breakfast": ["MarmeladenEiweissbrot", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["BioBierschinkenbrot"],
		"snack": [],
		"dinner": ["ReisBohnenPaprikaDoppeltGarnelen"]
	},
	"2021-11-27": {
		"breakfast": ["MarmeladenEiweissbrot", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["ReisBohnenPaprikaDoppeltGarnelen"],
		"snack": [],
		"dinner": ["RuehreiV2"]
	},
	"2021-11-28": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["RuehreiV2"],
		"snack": ["MarmeladenEiweissbrot",],
		"dinner": ["ReisBohnenPaprikaDoppeltGarnelen"]
	},
	"2021-11-29": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["ReisBohnenPaprikaDoppeltGarnelen", "EineWiltmannSalamissimoMiniSalamiPikant", "EineWiltmannSalamissimoMiniSalamiPikant"],
		"snack": ["MarmeladenEiweissbrot", "EinDuplo"],
		"dinner": ["EinChickenCaesarSalad", "EinHalberLiterCola"]
	},
	"2021-11-30": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["RuehreiV2"],
		"snack": ["BodyAttackIsoWheyShake", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2021-12-01": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["BioRindfleischbrot", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2021-12-02": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinYfoodClassicChoco"]
	},
	"2021-12-03": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack", "EinDuplo"]
	},
	"2021-12-04": {
		"breakfast": ["Muesli", "EinCaffeFreddoCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EineWiltmannSalamissimoMiniSalamiPikant", "EineWiltmannSalamissimoMiniSalamiPikant"],
		"lunch": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"],
		"snack": [],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2021-12-05": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack"],
		"snack": ["EinDuplo"],
		"dinner": ["BioBierschinkenbrot", "EineWiltmannSalamissimoMiniSalamiPikant", "EineWiltmannSalamissimoMiniSalamiPikant"]
	},
	"2021-12-07": {
		"breakfast": ["Muesli"],
		"lunch": ["BioBierschinkenbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinDuplo"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack", "EineWiltmannSalamissimoMiniSalamiPur", "EineWiltmannSalamissimoMiniSalamiPur"]
	},
	"2021-12-08": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack"],
		"snack": ["EinDuplo", "EineWiltmannSalamissimoMiniSalamiPur", "EineWiltmannSalamissimoMiniSalamiPur"],
		"dinner": ["FrostaHaehnchenCurry", "MarmeladenEiweissbrot"]
	},
	"2021-12-09": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["RuehreiV2"],
		"snack": [],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch"]
	},
	"2021-12-10": {
		"breakfast": ["Muesli"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["Piccolinis", "EinFerreroRocher", "EinFerreroRocher"]
	},
	"2021-12-11": {
		"breakfast": ["Muesli", "EinCaffeFreddoCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": [],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei"]
	},
	"2021-12-12": {
		"breakfast": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["Muesli", "BodyAttackIsoWheyShake"],
		"snack": ["MarmeladenEiweissbrot", "EinDuplo", "EineWiltmannSalamissimoMiniSalamiPur", "EineWiltmannSalamissimoMiniSalamiPur"],
		"dinner": ["ReisBohnenPaprikaDoppeltGarnelen"]
	},
	"2021-12-14": {
		"breakfast": ["Muesli"],
		"lunch": ["BioRindfleischbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch"]
	},
	"2021-12-16": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"lunch": ["EineKleineCola", "Muesli"],
		"snack": ["EinBurgerKingVeganLongChicken", "EineKleineCola"],
		"dinner": ["ReisBohnenPaprikaDoppeltGarnelen"]
	},
	"2021-12-21": {
		"breakfast": ["Muesli"],
		"lunch": ["RuehreiV2", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinBodyAttackProteinCoffee"],
		"dinner": ["FrostaHaehnchenCurry", "EinDuplo"]
	},
	"2021-12-22": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaDoppeltGarnelen", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "MarmeladenEiweissbrot"]
	},
	"2021-12-23": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinYfoodClassicChoco", "EinDuplo"]
	},
	"2021-12-28": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinBodyAttackProteinCoffee"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "MarmeladenEiweissbrot", "EinDuplo"],
		"dinner": ["EinYfoodClassicChoco", "EinDuplo"]
	},
	"2021-12-29": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["FrostaHaehnchenCurry", "MarmeladenEiweissbrot", "EinDuplo"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2022-01-06": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinBodyAttackProteinCoffee"],
		"lunch": ["BodyAttackWheyDeluxeShake", "EinDuplo"],
		"dinner": ["KartoffelGratinKaisergemueseLaktose", "EinDuplo"]
	},
	"2022-01-07": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["KartoffelGratinKaisergemueseLaktose", "EinDuplo"],
		"snack": ["BodyAttackWheyDeluxeShake"],
		"dinner": ["Piccolinis", "EinFerreroRocher", "EinFerreroRocher"]
	},
	"2022-01-08": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["KartoffelGratinKaisergemueseLaktose", "EinDuplo"],
		"snack": ["BodyAttackWheyDeluxeShake"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2022-01-09": {
		"breakfast": ["Muesli", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["RuehreiV2"],
		"dinner": ["BioRindfleischbrot", "EinDuplo"]
	},
	"2022-01-11": {
		"breakfast": ["Muesli"],
		"lunch": ["RuehreiV2", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinBodyAttackProteinCoffee"],
		"dinner": ["ReisBohnenPaprikaDoppeltPutenschnitzel", "EinDuplo"]
	},
	"2022-01-12": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["ReisBohnenPaprikaDoppeltPutenschnitzel", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["MangoLeinsamenMagerQuarkSmoothyLaktoseFrei", "EinDuplo"]
	},
	"2022-01-13": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot"],
		"lunch": ["ReisBohnenPaprikaDoppeltPutenschnitzel", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2022-01-14": {
		"breakfast": ["Muesli"],
		"lunch": ["BioRindfleischbrot", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"]
	},
	"2022-01-15": {
		"breakfast": ["Muesli", "EinCaffeFreddoCappuccino", "BodyAttackIsoWheyShake", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["EinYfoodClassicChoco"],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch"]
	},
	"2022-01-16": {
		"breakfast": ["Muesli", "EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["EinFrostaGemuesePfanneAsiaCurry"],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"]
	},
	"2022-01-17": {
		"breakfast": ["Muesli"],
		"lunch": ["ReisBohnenPaprikaDoppeltPutenschnitzel", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinDuplo"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2022-01-18": {
		"breakfast": ["Muesli"],
		"lunch": ["RuehreiV2", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"]
	},
	"2022-01-19": {
		"breakfast": ["Muesli", "MarmeladenEiweissbrot", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["FrostaHaehnchenCurry", "EinDuplo"],
		"dinner": ["BioRindfleischbrot", "EinDuplo"]
	},
	"2022-01-20": {
		"breakfast": ["MuesliHafterMilch", "MarmeladenEiweissbrot", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["EinYfoodClassicChoco"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHackGemischt", "EinDuplo"]
	},
	"2022-01-21": {
		"breakfast": ["MuesliHafterMilch", "MarmeladenEiweissbrot"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHackGemischt"],
		"snack": ["EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["RuehreiV2"]
	},
	"2022-01-22": {
		"breakfast": ["MuesliHafterMilch", "MarmeladenEiweissbrot"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHackGemischt"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "EinFrostaGemuesePfanneCurryKokos", "EinDuplo"]
	},
	"2022-01-24": {
		"breakfast": ["MuesliHafterMilch"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHackGemischt", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "EinFrostaGemuesePfanneSommergarten", "EinDuplo"]
	},
	"2022-01-25": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "Muesli"],
		"lunch": ["BioBierschinkenbrot", "EinDuplo"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "MarmeladenEiweissbrot"]
	},
	"2022-01-26": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "Muesli"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"dinner": ["EinAvocadoBrot", "RuehreiV2"]
	},
	"2022-01-27": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "Muesli"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-01-28": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "Muesli"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": ["MarmeladenEiweissbrot"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake"]
	},
	"2022-01-29": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "Muesli"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack", "EinDuplo"],
		"snack": ["MarmeladenEiweissbrot"],
		"dinner": ["BioRindfleischbrot", "EinDuplo"]
	},
	"2022-01-30": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "MuesliOatlyHafer"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack", "EinDuplo"],
		"snack": ["MarmeladenEiweissbrot"],
		"dinner": ["RuehreiV2"]
	},
	"2022-02-01": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "MuesliOatlyHafer"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack"],
		"snack": ["MarmeladenEiweissbrot", "EinKaesebrot", "EineKleineKarotte"],
		"dinner": ["RuehreiV2"]
	},
	"2022-02-02": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "MuesliOatlyHafer"],
		"lunch": ["BioBierschinkenbrot", "EinDuplo"],
		"snack": [],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"]
	},
	"2022-02-03": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "MuesliOatlyHaferCalcium"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"],
		"snack": ["BodyAttackIsoWheyShake"],
		"dinner": ["BioRindfleischbrot"]
	},
	"2022-02-06": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "MuesliOatlyHaferCalcium"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"],
		"snack": ["EinAvocadoBrot"],
		"dinner": ["EinFrostaGemuesePfanneMediterranea", "BodyAttackIsoWheyShake"]
	},
	"2022-02-08": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "MuesliOatlyHaferCalcium"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"],
		"snack": ["EinAvocadoBrot"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake"]
	},
	"2022-02-09": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "MuesliOatlyHaferCalcium"],
		"lunch": ["BioBierschinkenbrot", "EinDuplo"],
		"snack": ["EinKaesebrot"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"]
	},
	"2022-02-10": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "BodyAttackIsoWheyShake"],
		"dinner": ["RuehreiV2", "EinDuplo"]
	},
	"2022-02-11": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "MuesliOatlyHaferCalcium"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": ["BioRindfleischbrot"],
		"dinner": ["EinFrostaGemuesePfanneToscana", "BodyAttackIsoWheyShake"]
	},
	"2022-02-12": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "MuesliOatlyHaferCalcium"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHackGemischt", "EinDuplo"],
		"snack": ["EinAvocadoBrot"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake"]
	},
	"2022-02-13": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinYfoodClassicChoco"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHackGemischt", "EinDuplo"],
		"snack": ["MuesliOatlyHaferCalcium"],
		"dinner": ["RuehreiV2"]
	},
	"2022-02-14": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinYfoodClassicChoco"],
		"dinner": ["EinAvocadoBrot", "EinFerreroRocher"]
	},
	"2022-02-15": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHackGemischt", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinBodyAttackProteinCoffee"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinFerreroRocher"]
	},
	"2022-02-16": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"lunch": ["FrostaHaehnchenCurry"],
		"snack": ["BodyAttackIsoWheyShake"],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch", "EinFerreroRocher"]
	},
	"2022-02-17": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinBodyAttackProteinCoffee"],
		"dinner": ["BodyAttackIsoWheyShake", "EinAvocadoBrot", "EinFerreroRocher"]
	},
	"2022-02-18": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHackGemischt", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinFrostaMexicanStyleChicken", "BodyAttackIsoWheyShake", "EinFerreroRocher"]
	},
	"2022-02-19": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinBodyAttackProteinCoffee"],
		"lunch": ["MassamanCurry"],
		"snack": ["EinKaesebrot", "EinSeitenbacherEnergieRiegel"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake"]
	},
	"2022-02-21": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinKaesebrot"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake"]
	},
	"2022-02-22": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinBodyAttackProteinCoffee"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake"]
	},
	"2022-02-23": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["MassamanCurry", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BioRindfleischbrot", "EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-02-24": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["MassamanCurry", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinBodyAttackProteinCoffee"],
		"dinner": ["EinYfoodClassicChoco", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-02-25": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "MuesliOatlyHaferCalcium", "BodyAttackIsoWheyShake"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": [],
		"dinner": ["BioBierschinkenbrot", "EinDuplo", "EinFerreroRocher"]
	},
	"2022-02-26": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EinBodyAttackProteinCoffee"],
		"lunch": ["BodyAttackIsoWheyShake", "ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinAvocadoBrot", "EinDuplo"]
	},
	"2022-02-27": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["BodyAttackIsoWheyShake", "ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": [],
		"dinner": ["RuehreiV2", "EinDuplo"]
	},
	"2022-03-01": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["MassamanCurry", "EinDuplo"],
		"snack": ["BodyAttackIsoWheyShake", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["RuehreiV2", "EinDuplo"]
	},
	"2022-03-02": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["BioBierschinkenbrot"],
		"snack": ["BodyAttackIsoWheyShake", "EinAvocadoBrot"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiHackGemischt", "EinDuplo"]
	},
	"2022-03-03": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiHackGemischt", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinKaesebrot"],
		"dinner": ["BodyAttackIsoWheyShake", "EinFrostaGemuesePfanneAsiaCurry", "EinDuplo"]
	},
	"2022-03-04": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["BioRindfleischbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["Piccolinis", "EinHalberLiterCola", "EinFerreroRocher", "EinFerreroRocher"]
	},
	"2022-03-06": {
		"breakfast": ["MuesliDaheimHmilch"],
		"lunch": ["EinYfoodClassicChoco", "EinAvocadoBrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinBodyAttackProteinCoffee"],
		"dinner": ["BodyAttackIsoWheyShake", "EinFrostaGemuesePfanneCurryKokos"]
	},
	"2022-03-07": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["EinYfoodClassicChoco"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "Piccolinis", "EinHalberLiterCola"]
	},
	"2022-03-08": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionStudentenfutter"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiHackGemischt", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinKaesebrot"],
		"dinner": ["BodyAttackIsoWheyShake", "EinFrostaGemuesePfanneAsiaCurry", "EinDuplo"]
	},
	"2022-03-09": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionStudentenfutter"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiHackGemischt"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"]
	},
	"2022-03-10": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionStudentenfutter"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2022-03-11": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionStudentenfutter"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BioRindfleischbrot", "EinDuplo"]
	},
	"2022-03-14": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["EinAvocadoBrot", "EinDuplo", "KleinePortionMangostreifen"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "BodyAttackIsoWheyShake"],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"]
	},
	"2022-03-15": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionMangostreifen"],
		"lunch": ["RuehreiV2"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "BodyAttackIsoWheyShake"],
		"dinner": ["MassamanCurry", "EinDuplo"]
	},
	"2022-03-16": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["MassamanCurry", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "BodyAttackIsoWheyShake"],
		"dinner": ["EineSushiboxAyami", "EinKaesebrot", "EinDuplo"]
	},
	"2022-03-17": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["MassamanCurry", "KleinePortionMangostreifen", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "BodyAttackIsoWheyShake"],
		"dinner": ["EineSushiboxAyaka", "EinDuplo"]
	},
	"2022-03-18": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["BioBierschinkenbrot", "MarmeladenEiweissbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "BodyAttackIsoWheyShake"],
		"dinner": ["EinAvocadoBrot", "EinFrostaGemuesePfanneAsiaCurry"]
	},
	"2022-03-19": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["BioRindfleischbrot", "MarmeladenEiweissbrot", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "BodyAttackIsoWheyShake"],
		"dinner": ["ReisBohnenPaprikaDoppeltGarnelen", "EinDuplo"]
	},
	"2022-03-22": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["MassamanCurry"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["BioRindfleischbrot", "EinDuplo"]
	},
	"2022-03-23": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["ReisBohnenPaprikaDoppeltGarnelen", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2022-03-24": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["ReisBohnenPaprikaDoppeltGarnelen"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["SpirelliBologneseV2"]
	},
	"2022-03-25": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["SpirelliBologneseV2"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["RuehreiV2", "EinDuplo"]
	},
	"2022-03-26": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["SpirelliBologneseV2"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "MarmeladenEiweissbrot"],
		"dinner": ["EinFrostaGemuesePfanneCurryKokos", "BodyAttackIsoWheyShake"]
	},
	"2022-03-27": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["KartoffelGratinKaisergemueseLaktose", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2022-03-29": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["KartoffelGratinKaisergemueseLaktose", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["EinFrostaMexicanStyleChicken"]
	},
	"2022-03-30": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["SpirelliBolognese", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille",],
		"dinner": ["BodyAttackIsoWheyShake", "EinFrostaGemuesePfanneAsiaCurry", "EinDuplo"]
	},
	"2022-04-04": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["RuehreiV2", "FageHeidelbeeren"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "DemeterLeberwurstbrot"]
	},
	"2022-04-05": {
		"breakfast": ["MuesliOatlyHaferCalcium", "MarmeladenEiweissbrot"],
		"lunch": ["BioBierschinkenbrot", "SkyrHeidelbeeren", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "KartoffelGratinKaisergemueseLaktose", "EinDuplo"]
	},
	"2022-04-06": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EinAvocadoBrot"],
		"lunch": ["KartoffelGratinKaisergemueseLaktose", "SkyrHeidelbeeren"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "RuehreiV2"]
	},
	"2022-04-07": {
		"breakfast": ["MuesliOatlyHaferCalcium", "MarmeladenEiweissbrot"],
		"lunch": ["KartoffelGratinKaisergemueseLaktose", "FageHeidelbeeren", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "EinFrostaMexicanStyleChicken", "EinDuplo"]
	},
	"2022-04-08": {
		"breakfast": ["MuesliOatlyHaferCalcium", "MarmeladenEiweissbrot"],
		"lunch": ["KartoffelGratinKaisergemueseLaktose", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "SpirelliBologneseV2", "EinDuplo"]
	},
	"2022-04-09": {
		"breakfast": ["MuesliOatlyHaferCalcium", "MarmeladenEiweissbrot"],
		"lunch": ["SpirelliBologneseV2", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "EinAvocadoBrot", "EinDuplo"]
	},
	"2022-04-12": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SpirelliBologneseV2", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "MarmeladenEiweissbrot"],
		"dinner": ["BodyAttackIsoWheyShake", "EinAvocadoBrot", "EinDuplo"]
	},
	"2022-04-13": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SpirelliBologneseV2", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"]
	},
	"2022-04-14": {
		"breakfast": ["MuesliOatlyHaferCalcium", "MarmeladenEiweissbrot"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "BioBierschinkenbrot"]
	},
	"2022-04-15": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "BioBierwurstbrot", "EinDuplo"]
	},
	"2022-04-16": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "MarmeladenEiweissbrot"],
		"dinner": ["BodyAttackIsoWheyShake", "EinFrostaGemuesePfanneCurryKokos"]
	},
	"2022-04-18": {
		"breakfast": ["MuesliDaheimHmilch", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["EinYfoodClassicChoco"],
		"snack": ["RuehreiV2", "ViertelLindtGoldhase"],
		"dinner": ["BodyAttackIsoWheyShake", "MarmeladenEiweissbrot"]
	},
	"2022-04-19": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionNussMix"],
		"lunch": ["EinFrostaMexicanStyleChicken"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2022-04-20": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "ViertelLindtGoldhase"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "EinFrostaGemuesePfanneCurryKokos", "EinDuplo"]
	},
	"2022-04-22": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "ViertelLindtGoldhase"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "EinAvocadoBrot", "EinDuplo"]
	},
	"2022-04-23": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch", "ViertelLindtGoldhase"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "MarmeladenEiweissbrot"]
	},
	"2022-04-24": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackIsoWheyShake", "MarmeladenEiweissbrot"]
	},
	"2022-04-25": {
		"breakfast": ["MuesliOatlyHaferCalcium", "FageHeidelbeeren"],
		"lunch": ["RuehreiV2"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "BodyAttackIsoWheyShake"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2022-04-26": {
		"breakfast": ["MuesliOatlyHaferCalcium", "FageHeidelbeeren"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "BodyAttackIsoWheyShake"],
		"dinner": ["EinAvocadoBrot"]
	},
	"2022-04-27": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["RuehreiV2"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinDuplo"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-04-28": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "BodyAttackIsoWheyShake"],
		"dinner": ["EinAvocadoBrot"]
	},
	"2022-05-02": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"lunch": ["RuehreiV2"],
		"snack": [],
		"dinner": ["Piccolinis", "EinGlasCola"]
	},
	"2022-05-03": {
		"breakfast": ["MuesliOatlyHaferCalcium", "FageHeidelbeeren"],
		"lunch": ["BioRindfleischbrot", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack"]
	},
	"2022-05-04": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["LandwurstBratwurstGrobBrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack", "BodyAttackIsoWheyShake"]
	},
	"2022-05-05": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220505"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake"]
	},
	"2022-05-06": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["FrostaHaehnchenCurry"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack", "BodyAttackIsoWheyShake"]
	},
	"2022-05-07": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["RuehreiV2"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinAvocadoBrot", "EinFerreroRocher", "EinFerreroRocher", "BodyAttackIsoWheyShake"]
	},
	"2022-05-10": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["RuehreiV2"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack", "SkyrHeidelbeeren"]
	},
	"2022-05-11": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["BioBratwurstbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["ReisBohnenPaprikaDoppeltGarnelen"]
	},
	"2022-05-12": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["ReisBohnenPaprikaDoppeltGarnelen", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-05-13": {
		"breakfast": ["MuesliOatlyHaferCalcium", "FageHeidelbeeren"],
		"lunch": ["ReisBohnenPaprikaDoppeltGarnelen", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-05-14": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinBodyAttackProteinCoffee"],
		"lunch": ["BioRindfleischbrot", "EinDuplo"],
		"snack": ["BodyAttackIsoWheyShake"],
		"dinner": ["EinYfoodClassicChoco", "FageHeidelbeeren", "EinDuplo"]
	},
	"2022-05-15": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "RuehreiV2"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch", "EinDuplo"],
		"dinner": ["BodyAttackIsoWheyShake", "SkyrHeidelbeeren", "EinDuplo"]
	},
	"2022-05-17": {
		"breakfast": ["HalbesiMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": ["EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BodyAttackIsoWheyShake", "EinAvocadoBrot"]
	},
	"2022-05-18": {
		"breakfast": ["HalbesiMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["EinFrostaGemuesePfanneCurryKokos", "BodyAttackIsoWheyShake"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["Piccolinis", "EineKleinePopcorn"]
	},
	"2022-05-19": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": ["BodyAttackIsoWheyShake", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["SkyrHeidelbeeren"]
	},
	"2022-05-20": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["ReisBohnenPaprikaDoppeltGarnelen"],
		"snack": ["BodyAttackIsoWheyShake", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinAvocadoBrot", "SkyrHeidelbeeren"]
	},
	"2022-05-21": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": ["BodyAttackIsoWheyShake", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2022-05-22": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust"],
		"snack": ["BodyAttackIsoWheyShake"],
		"dinner": ["Piccolinis", "SkyrHeidelbeeren"]
	},
	"2022-05-24": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EinSeitenbacherProteinRiegelVanille", "EineBanane"],
		"lunch": ["SAPCanteen20220524"],
		"snack": ["BodyAttackIsoWheyShake"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust", "SkyrHeidelbeeren"]
	},
	"2022-05-25": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["EinAvocadoBrot", "BodyAttackIsoWheyShake"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2022-05-26": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren", "EinBodyAttackProteinCoffee"],
		"lunch": ["RuehreiV2Raw", "BodyAttackIsoWheyShake"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake"]
	},
	"2022-05-31": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["EinFrostaGemuesePfanneCurryKokos", "BodyAttackIsoWheyShake"]
	},
	"2022-06-01": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake"]
	},
	"2022-06-02": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["EinYfoodClassicChoco"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "BodyAttackIsoWheyShake"],
		"dinner": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack"]
	},
	"2022-06-04": {
		"breakfast": ["MuesliDaheim", "SkyrHeidelbeeren"],
		"lunch": ["RuehreiV2Raw", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "ReisBohnenPaprikaDoppeltHaenchenbrust"]
	},
	"2022-06-07": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["KartoffelGratinKaisergemueseLaktosefreiDoppeltHack"],
		"snack": ["BodyAttackIsoWheyShake", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["SkyrHeidelbeeren", "EinAvocadoBrot"]
	},
	"2022-06-08": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["ReisBohnenPaprikaDoppeltHaenchenbrust"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BodyAttackIsoWheyShake", "EinFrostaGemuesePfanneAsiaCurry"]
	},
	"2022-06-09": {
		"breakfast": ["KleinePortionWalnuss", "SkyrHeidelbeeren"],
		"lunch": ["EinYfoodClassicChoco"],
		"snack": ["BodyAttackIsoWheyShake", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "KleinePortionWalnuss"]
	},
	"2022-06-11": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinBodyAttackProteinCoffee"],
		"lunch": ["BodyAttackIsoWheyShake"],
		"snack": ["BodyAttackIsoWheyShake", "PfannenFischMitOfenKartoffeln"],
		"dinner": ["EinHalberLiterCola", "SehrKleinePortionWalnuss"]
	},
	"2022-06-12": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["SkyrHeidelbeeren"],
		"snack": ["ReisBohnenPaprikaDoppeltHaenchenbrust"],
		"dinner": ["BodyAttackIsoWheyShake", "EinKaesebrot"]
	},
	"2022-06-13": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["BackofenFischMitOfenKartoffeln"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BodyAttackIsoWheyShake", "EinKaesebrot"]
	},
	"2022-06-14": {
		"breakfast": ["KleinePortionWalnuss", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["SkyrHeidelbeeren"],
		"snack": ["ReisBohnenPaprikaDoppeltHaenchenbrust"],
		"dinner": ["BodyAttackIsoWheyShake", "EinAvocadoBrot"]
	},
	"2022-06-15": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["EinYfoodClassicChoco"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BodyAttackIsoWheyShake", "KleinePortionWalnuss", "FrootLoopsArlaMilch"]
	},
	"2022-06-19": {
		"breakfast": ["RuehreiV2", "Marmeladenbrot"],
		"lunch": ["BodyAttackIsoWheyShake", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"snack": ["EinFuzeTeaZitrone", "EinGenericEiscafe"],
		"dinner": ["BackofenFischMitOfenKartoffeln"]
	},
	"2022-06-20": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "HalberSkyr"],
		"lunch": ["BackofenFischMitOfenKartoffeln", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-06-21": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["RuehreiV2"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake"]
	},
	"2022-06-22": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220622"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake"]
	},
	"2022-06-23": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["EinYfoodClassicChoco"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinDuplo"],
		"dinner": ["EinFrostaGemuesePfanneCurryKokos", "BodyAttackIsoWheyShake"]
	},
	"2022-06-24": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220624"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinDuplo"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-06-26": {
		"breakfast": ["BodyAttackIsoWheyShake", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EineTasseCappuccino"],
		"lunch": ["RuehreiV2"],
		"snack": ["FrootLoopsArlaMilch", "EinDuplo"],
		"dinner": ["EinFrostaMexicanStyleChicken"]
	},
	"2022-06-27": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "HalberSkyr"],
		"lunch": ["BioBierschinkenbrot", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinHalberLiterCola"]
	},
	"2022-06-28": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["BioRindswurstbrot", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-06-29": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220629"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake", "Marmeladenbrot"]
	},
	"2022-06-30": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["EinYfoodClassicChoco", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinDuplo", "FrootLoopsArlaMilch", "FrootLoopsArlaMilch"]
	},
	"2022-07-02": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EinBodyAttackProteinCoffee", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["RuehreiV2"],
		"snack": ["BodyAttackIsoWheyShake"],
		"dinner": ["EinKaesebrot", "EinDuplo", "SkyrHeidelbeeren"]
	},
	"2022-07-03": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"lunch": ["MuesliOatlyHaferCalcium", "BodyAttackIsoWheyShake", "SkyrHeidelbeeren"],
		"snack": ["EinGenericEiscafe", "EineKleinePopcorn"],
		"dinner": ["Piccolinis", "EinHalberLiterCola"]
	},
	"2022-07-04": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220704", "EinMagnumCollectionChocolateCrunchyCookies"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["KleinePortionWalnuss", "EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-05": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["RuehreiV2"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-06": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220706"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "KleinePortionWalnuss"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-07": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220707"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-08": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220708", "EinMagnumCollectionChocolateCrunchyCookies"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-09": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["BodyAttackIsoWheyShake", "SpirelliBologneseV2DoppeltHackfleisch"],
		"dinner": ["SkyrHeidelbeeren", "EinDuplo"]
	},
	"2022-07-11": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220711"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BioRindswurstbrot"]
	},
	"2022-07-12": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-13": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["EinYfoodClassicChoco", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "FrootLoopsArlaMilch"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-14": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220714"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-15": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220715", "EinMagnumCollectionChocolateCrunchyCookies"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinYfoodClassicChoco", "BodyAttackIsoWheyShake"]
	},
	"2022-07-17": {
		"breakfast": ["MuesliDaheimHmilch", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["SpirelliBologneseV2DoppeltHackfleisch"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake"]
	},
	"2022-07-18": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220718"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["KleinePortionWalnuss", "EinDuplo", "BodyAttackIsoWheyShake"]
	},
	"2022-07-19": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220719"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "EinGlasSauerkirschNektar", "BodyAttackIsoWheyShake"]
	},
	"2022-07-20": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220720", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinYfoodClassicChoco", "BodyAttackIsoWheyShake"]
	},
	"2022-07-21": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220721"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-22": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220722", "EinMagnumCollectionChocolateCrunchyCookies"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "KleinePortionWalnuss", "BodyAttackIsoWheyShake"]
	},
	"2022-07-23": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["EinYfoodClassicChoco", "BodyAttackIsoWheyShake"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["SpirelliBologneseV2DoppeltHackfleisch"]
	},
	"2022-07-24": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "MuesliOatlyHaferCalcium", "BodyAttackIsoWheyShake"],
		"lunch": ["KleinePortionWalnuss", "EinKaesebrot"],
		"snack": ["EinGenericFlammkuchen"],
		"dinner": ["RuehreiV2"]
	},
	"2022-07-25": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220725"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["MyFiveGuysBigBaconeBarBQCheeseBurger", "BodyAttackIsoWheyShake"]
	},
	"2022-07-26": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220726"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-27": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220727"],
		"dinner": ["Piccolinis", "EineKleinePopcorn", "BodyAttackIsoWheyShake"]
	},
	"2022-07-28": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220728"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-07-29": {
		"breakfast": ["MuesliOatlyHaferCalcium", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"lunch": ["EinAvocadoBrot"],
		"snack": ["SkyrHeidelbeeren"],
		"dinner": ["EinChickenCaesarSalad", "BodyAttackIsoWheyShake"]
	},
	"2022-08-01": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220801", "TwoPiecesKajuKatli"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinDuplo", "EinGlasSauerkirschNektar"]
	},
	"2022-08-02": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220802"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinAvocadoBrot", "EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-08-03": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220803"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["RuehreiV2", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-08-04": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220804"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BioBratwurstbrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-08-05": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220805", "EinOreoIcrecreamBites", "OnePieceMandyBananaBred", "OnePieceMandyBananaBred"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-08-08": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220808"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-08-09": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220809"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-08-10": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220810"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinDuplo", "EinGlasSauerkirschNektar"]
	},
	"2022-08-12": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220812", "EinOreoIcrecreamBites"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinChickenCaesarSalad"]
	},
	"2022-08-14": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["RuehreiV2"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinDuplo", "EinGlasSauerkirschNektar"]
	},
	"2022-08-15": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220815"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake", "EinDuplo", "EinGlasSauerkirschNektar"]
	},
	"2022-08-16": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220816"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-08-17": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220817"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-08-18": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220818"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinDuplo", "FrootLoopsArlaMilch"]
	},
	"2022-08-22": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220822"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-08-23": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220823"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinAvocadoBrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-08-24": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220824"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-08-25": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220825"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-08-27": {
		"breakfast": ["MuesliOatlyHaferCalcium", "JaSkyrHeidelbeeren"],
		"lunch": ["RuehreiV2", "ChocoKrispiesFettarmeMilch"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-08-29": {
		"breakfast": ["MuesliOatlyHaferCalcium"],
		"lunch": ["SAPCanteen20220829"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "SkyrHeidelbeeren"],
		"dinner": ["MyFiveGuysBigBaconeBarBQCheeseBurger"]
	},
	"2022-08-30": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220830"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["AvocadoPumpernikel", "BodyAttackIsoWheyShake", "EinDuplo"]
	},
	"2022-08-31": {
		"breakfast": ["MuesliAlproNotMilk", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220831"],
		"snack": ["EineBanane", "EinSchokoKirschMuffin"],
		"dinner": ["Piccolinis", "BodyAttackIsoWheyShake"]
	},
	"2022-09-01": {
		"breakfast": ["MuesliAlproNotMilk", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220901"],
		"snack": ["EinMandysZwetschgenkuchen", "EineBanane", "EinDuplo"],
		"dinner": ["EinKaesebrot", "BodyAttackIsoWheyShake"]
	},
	"2022-09-02": {
		"breakfast": ["MuesliOatlyHaferCalcium", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220902"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["Piccolinis", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-09-05": {
		"breakfast": ["MuesliAlproNotMilk", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220905SpinachGnocchi", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["MyFiveGuysBigBaconeBarBQCheeseBurger"]
	},
	"2022-09-06": {
		"breakfast": ["MuesliAlproNotMilk", "SehrKleinePortionWalnuss", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220906TurkeySteak"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackWheyDeluxeShake", "EinDuplo", "ChocoKrispiesFettarmeMilch"]
	},
	"2022-09-07": {
		"breakfast": ["MuesliAlproNotMilk", "SehrKleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220907BeefGoulash", "SAPCanteen20220907PumpkinSoup", "SAPCanteenEspressoCream"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "BodyAttackWheyDeluxeShakeWater", "EinDuplo"]
	},
	"2022-09-08": {
		"breakfast": ["MuesliAlproNotMilk", "SehrKleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220908NewYorkerGrilledChicken", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BodyAttackWheyDeluxeShakeWater", "ChocoKrispiesFettarmeMilch"]
	},
	"2022-09-09": {
		"breakfast": ["MuesliAlproNotMilk", "SehrKleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220909TunaPizza"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado130", "BodyAttackWheyDeluxeShakeWater", "EinWhiteRussian"]
	},
	"2022-09-12": {
		"breakfast": ["MuesliOatlyHaferCalcium", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220912YunaYuuki"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinBodyAttackProteinCoffee"],
		"dinner": ["Piccolinis", "EinDuplo"]
	},
	"2022-09-13": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "EdekaBioSkyrHeidelbeeren"],
		"lunch": ["Piccolinis"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinAndechserNaturBioGoudaBrot", "BodyAttackWheyDeluxeShakeWater", "EinDuplo"]
	},
	"2022-09-14": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "EdekaBioSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220914ButterChicken", "SAPCanteen20220914CheesecakeCreme"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinAndechserNaturBioGoudaBrot", "BodyAttackWheyDeluxeShakeWater", "EinDuplo"]
	},
	"2022-09-15": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "EdekaBioSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220915Madurai"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["KnaeckebrotFrischkaese200Avocado130", "BodyAttackWheyDeluxeShakeWater", "EinDuplo"]
	},
	"2022-09-16": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "EdekaBioSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220916NewOrleansPlants"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado200", "BodyAttackWheyDeluxeShakeWater", "EinDuplo"]
	},
	"2022-09-19": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220919RoastedChickenBreast"],
		"dinner": ["MyFiveGuysBigBaconeBarBQCheeseBurger", "EinFiveGuysLittleFries"]
	},
	"2022-09-20": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220920Paella"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["Avocado200Brot", "BodyAttackWheyDeluxeShakeWater", "EinGlasSauerkirschNektar", "EinDuplo"]
	},
	"2022-09-21": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220921Meatball"],
		"dinner": ["Piccolinis", "EineKleinePopcorn", "BodyAttackWheyDeluxeShake"]
	},
	"2022-09-22": {
		"breakfast": ["MuesliOatlyHaferCalcium", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220922YellowThaiCurry", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["Avocado200Brot", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-09-23": {
		"breakfast": ["HalbesMuesliOatlyHaferCalcium", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20220923PorkLoinSteak", "ParkkaffeeKaesekuchen"],
		"dinner": ["EinKaesebrot", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-09-24": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss"],
		"lunch": ["EinFrostaMexicanStyleChicken", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-10-04": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss"],
		"lunch": ["SAPCanteen202201004"],
		"snack": ["EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinFrostaMexicanStyleChicken", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-10-05": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221005Lasagne"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese200Avocado200"]
	},
	"2022-10-06": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221006"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot"]
	},
	"2022-10-07": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221007PollackFillet", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinSalami30Brot", "EinKaesebrot", "BodyAttackWheyDeluxeShakeWater", "EinDuplo"]
	},
	"2022-10-08": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["EinFrostaMexicanStyleChicken"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinYfoodClassicChoco", "BodyAttackWheyDeluxeShakeWater", "EinDuplo"]
	},
	"2022-10-10": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss"],
		"lunch": ["SAPCanteen20221010"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "JaSkyrHeidelbeeren"],
		"dinner": ["EinKaesebrot", "BodyAttackWheyDeluxeShakeWater", "OnePieceDanaApfelWalnussKuchen"]
	},
	"2022-10-11": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221011PlantsLandshut", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["RuehreiV2", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-10-12": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221012SwabianSouerbraten"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese100Avocado200", "BodyAttackWheyDeluxeShakeWater", "EinDuplo"]
	},
	"2022-10-13": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "JaSkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221013AsianSalad", "SAPCanteenLegumes", "SAPCanteen20221013CoffeeCream", "SAPCanteen20221013CoffeeCream"],
		"dinner": ["EinKaesebrot", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-10-15": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss"],
		"lunch": ["JaSkyrHeidelbeeren", "EinKaesebrot", "EinDuplo", "EinEmmiCaffeLatteHighProtein"],
		"dinner": ["GeneralTsoHaehnchenbrust", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-10-18": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221018PolloConMole", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaesebrot", "EinDuplo", "EinDuplo"]
	},
	"2022-10-19": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221019"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado200", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-10-20": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221020ConCarne", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["PeanutButter30Jelly20Brot", "EinDuplo", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-10-21": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221021Fajita", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EineKleinePopcorn", "EinDuplo", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2022-10-25": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221025"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "EinDuplo", "EsnProteinShakeCinnamonCerealWater"]
	},
	"2022-10-26": {
		"breakfast": ["MuesliAlproNotMilk", "KleinePortionWalnuss", "SkyrHeidelbeeren"],
		"lunch": ["SAPCanteen20221026MediterraneanBowl"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["PeanutButter40Jelly10Brot", "EinDuplo", "EsnProteinShakeRichChocolateWater"]
	},
	"2022-11-02": {
		"breakfast": ["OvernightOatsSkyrWalnussHeidelbeerenWheyDeluxe"],
		"lunch": ["SAPCanteen20221102"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["PeanutButter40Jelly10Sandwich", "NutellaButterSandwich", "EinDuplo"]
	},
	"2022-11-03": {
		"breakfast": ["OvernightOatsSkyrWalnussHeidelbeerenWheyDeluxe"],
		"lunch": ["SAPCanteen20221103"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinDuplo"],
		"dinner": ["NutellaButterSandwich", "NutellaButterSandwich", "EinWhiteRussian"]
	},
	"2022-11-04": {
		"breakfast": ["OvernightOatsSkyrWalnussHeidelbeerenWheyDeluxe"],
		"lunch": ["SAPCanteen20221104"],
		"snack": ["EinSeitenbacherProteinRiegelVanille", "EinDuplo"],
		"dinner": ["EinKaesebrot", "NutellaButterSandwich"]
	},
	"2022-11-05": {
		"breakfast": ["OvernightOatsSkyrWalnussHeidelbeerenWheyDeluxe"],
		"lunch": ["RuehreiV2", "HalbesDanaNutellaSandwich", "EinFuzeTeaPfirsich"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinHelloFreshSeehechtOrzoNudelnGurkenSalat", "EinDuplo"]
	},
	"2022-11-07": {
		"breakfast": ["OvernightOatsSkyrWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221107Cannelloni", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["HansImGlueckGeselleSauerteigbroetchen", "EinDuplo"]
	},
	"2022-11-08": {
		"breakfast": ["OvernightOatsSkyrWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221108PotatoAndPeaCurry", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinFrostaGemuesePfanneCurryKokos", "FrootLoopsArlaMilch"]
	},
	"2022-11-09": {
		"breakfast": ["OvernightOatsSkyrWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221109"],
		"dinner": ["KnaeckebrotFrischkaese200Avocado200", "EinKaesebrot"]
	},
	"2022-11-10": {
		"breakfast": ["OvernightOatsSkyrWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221110Szegediner", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EinHanutaBrownieStyle", "EinDuplo"]
	},
	"2022-11-14": {
		"breakfast": ["OvernightOatsSkyrWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221114Tokio"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MyFiveGuysBigBaconeBarBQCheeseBurger"]
	},
	"2022-11-15": {
		"breakfast": ["OvernightDinkelOatsSkyrWalnussHimbeeren"],
		"lunch": ["SAPCanteen20221115BeefRissole", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2022-11-16": {
		"breakfast": ["OvernightDinkelOatsSkyrWalnussHeidelbeerenPamSchoco"],
		"lunch": ["SAPCanteen20221116Palikaria", "SAPCanteen20221116VeganChocolatePudding", "SAPCanteen20221116Cheesecake"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado200", "EinGlasApfelsaftSchorle"]
	},
	"2022-11-17": {
		"breakfast": ["OvernightDinkelOatsSkyrWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221117HappyBowl", "SAPCanteenLegumes", "SAPCanteen20221117VanillaCurdVerrine"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["HansImGlueckGeselleSauerteigbroetchen", "EinDuplo"]
	},
	"2022-11-18": {
		"breakfast": ["OvernightDinkelOatsSkyrWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221118BordelaiseFishFillet", "SAPCanteenLegumes", "SAPCanteen20221118AppleQuarkDessert"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EinDuplo", "EinHanutaBrownieStyle"]
	},
	"2022-11-23": {
		"breakfast": ["OvernightDinkelOatsSkyrWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221123HungaryBeefGoulash", "SAPCanteen20221123SteamPotatos"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado200", "EinDuplo", "EinHanutaBrownieStyle"]
	},
	"2022-11-24": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221124HuhnMitHaltung", "SAPCanteenLegumes"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot"]
	},
	"2022-11-25": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221125"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EinDuplo", "EinEmmiCaffeLatteHighProtein"]
	},
	"2022-11-29": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221129PoweredByPlants", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BioRindfleischbrot", "EinDuplo"]
	},
	"2022-11-30": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221130PeruPulledPork"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado200", "EinDuplo", "EinHanutaBrownieStyle"]
	},
	"2022-12-01": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221201PoweredByPlants", "SAPCanteenLegumes", "SAPCanteen20221201AppleChocolateCrossies", "SAPCanteen20221201MangoCurdDessert"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EinDuplo", "EinHanutaBrownieStyle"]
	},
	"2022-12-05": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221205TurkeyCurry", "SAPCanteenLegumes", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado200", "EinDuplo"]
	},
	"2022-12-06": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221206PoweredByPlants", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2022-12-07": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221207"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EinDuplo"]
	},
	"2022-12-08": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221208PoweredByPlants", "SAPCanteenLegumes", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EinDuplo"]
	},
	"2022-12-09": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221209FishBurger", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "EinEmmiCaffeLatteHighProtein"]
	},
	"2022-12-12": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221212TurkeyEscalope", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado200", "EinCremeBrulee"]
	},
	"2022-12-13": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221213PoweredByPlants", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BioRindfleischbrot"]
	},
	"2022-12-14": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221214BeefRoulade", "SAPCanteen20221214SwabianNoodles", "SAPCanteen20221214Turnips", "SAPCanteen20221214SkyrCerealMangoHoney"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EinDuplo"]
	},
	"2022-12-15": {
		"breakfast": ["OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221215Tortellini", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinFrostaGemuesePfanneAsiaCurry", "EinDuplo", "EinEmmiCaffeLatteHighProtein"]
	},
	"2022-12-16": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221216Pizza"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EineKleinePopcorn"]
	},
	"2022-12-27": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221227"],
		"snack": ["EineTasseCappuccino"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2022-12-28": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221228SwabianStew", "SAPCanteen20221228ChocolatePudding"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EineKleinePopcorn"]
	},
	"2022-12-30": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkWalnussHeidelbeeren"],
		"lunch": ["SAPCanteen20221230IndiaCurry"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EinDuplo"]
	},
	"2023-01-11": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusHeidelbeeren"],
		"lunch": ["SAPCanteen20230111"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado200", "EinDuplo"]
	},
	"2023-01-18": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusHeidelbeeren"],
		"lunch": ["SAPCanteen20230118"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaesebrot", "EinePortionSmarties", "EinDuplo"]
	},
	"2023-02-08": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230208"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado100", "EinDuplo"]
	},
	"2023-02-09": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "SAPCanteen20230209"],
		"snack": ["EineTasseCappuccino", "200Skyr20Krispies"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EinDuplo"]
	},
	"2023-02-15": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230215"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado100", "EinDuplo", "BodyAttackWheyDeluxeShakeWater", "EinHanuta"]
	},
	"2023-02-16": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230216"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "BodyAttackWheyDeluxeShakeWater", "EinHanuta", "EinHanuta"]
	},
	"2023-02-17": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230217"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2023-02-21": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230221PadThai"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["SpirelliAlBronzoBologneseV3DoppeltHackfleisch", "EinDuplo", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2023-02-22": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230222CoalfishInStock", "SAPCanteen20230222CornAndBellPeppers"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado100", "EinDuplo", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2023-02-23": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230223PastaTaleggioRahm", "SAPCanteen20230223VeganChocolatePudding", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2023-02-24": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230224ArtichokeCreamSoup", "SAPCanteen20230224ChickenFricass"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BioRindfleischbrot", "200Skyr40Krispies"]
	},
	"2023-02-27": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230227"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinKaeseSchinkenbrot", "EinDuplo", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2023-02-28": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230228BraisedBeef", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2023-03-01": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkHaselnussmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230301"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado80", "EinDuplo", "BodyAttackWheyDeluxeShakeWater", "EinDuplo"]
	},
	"2023-03-02": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkHaselnussmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230302PastaSalmon", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "BodyAttackWheyDeluxeShakeWater", "EinDuplo"]
	},
	"2023-03-03": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkHaselnussmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230303CodMashedPotatoes", "SAPCanteen20230303GriessbreiApfelragout"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["Piccolinis", "EinGlasCola", "BodyAttackWheyDeluxeShakeWater"]
	},
	"2023-03-06": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkHaselnussmusBodyAttackHeidelbeeren"],
		"lunch": ["SAPCanteen20230306", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["Piccolinis", "EsnProteinShakeVanillaWater"]
	},
	"2023-03-07": {
		"breakfast": ["BiggerOvernightDinkelOatsSpeisequarkHaselnussmusEsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230307SaffronChicken", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioRindfleischbrot", "EinDuplo"]
	},
	"2023-03-08": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230308PoppyChickenBowl", "SAPCanteen20230308Broccoli", "SAPCanteen20230308PannaCotta", "SAPCanteen20230308PannaCotta"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinDuplo"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado80", "EinDuplo", "EsnProteinShakeVanillaWater"]
	},
	"2023-03-09": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230309HoisinPlantedBurger", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinDuplo"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaWater"]
	},
	"2023-03-10": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230310"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinDuplo"],
		"dinner": ["KnaeckebrotFrischkaese150Avocado80", "EinDuplo", "EsnProteinShakeVanillaWater"]
	},
	"2023-03-14": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230314FriedRedfish", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinDuplo"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2023-03-15": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230315"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinDuplo"],
		"dinner": ["KnaeckebrotFrischkaese150Olivenoel", "EinDuplo", "EineSchwarzwaldhofLandjaeger", "EsnProteinShakeVanillaWater"]
	},
	"2023-03-16": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230316AfricanPeanutStew", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinDuplo"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EineSchwarzwaldhofLandjaeger", "EsnProteinShakeVanillaWater"]
	},
	"2023-03-17": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230317"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko", "EinDuplo"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EineSchwarzwaldhofLandjaeger", "EinKnoppers", "EsnProteinShakeVanillaWater"]
	},
	"2023-03-20": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230320"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["Piccolinis", "EsnProteinShakeVanillaWater"]
	},
	"2023-03-21": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230321"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioRindfleischbrot", "EinDuplo", "EsnProteinShakeVanillaWater"]
	},
	"2023-03-22": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230322CeasarBurger", "SAPCanteen20230322CreamySavoy", "SAPCanteen20230322CreamCaramel"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-03-23": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230323ShrimpSkewer", "SAPCanteen20230323CottageCheeseAndBerryCream", "SAPCanteen20230323CottageCheeseAndBerryCream"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EinKnoppers", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-03-24": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230324"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["ZweiPortionenIgloSchlemmerFiletAlaBordelaiseClassic", "EisbergsalatMitCroutonsUndHonigSenfDressing"]
	},
	"2023-03-27": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230327"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinDuplo", "EineKleinePopcorn", "EsnProteinShakeVanillaWater"]
	},
	"2023-03-28": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230328"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-03-29": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyMango"],
		"lunch": ["SAPCanteen20230329FilletOfHoki", "SAPCanteen20230329RedQuinoa", "SAPCanteen20230329Potato", "SAPCanteen20230329Kohlrabi", "SAPCanteen20230329VeganChocolateCream", "SAPCanteen20230329VeganChocolateCream"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-03-30": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230330FilledWrapChicken", "SAPCanteen20230330CreamySpinach", "SAPCanteen20230330Lentils", "SAPCanteen20230330VeganRicePudding", "SAPCanteen20230330VeganRicePudding"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinKnoppers", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-03-31": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230331"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["HalbesRuehreiV2", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-04-03": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230403"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinNutritionxPulledPorkSandwich", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-04-04": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230404ChickenSchnitzel"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioRindfleischbrot", "EinDuplo", "EinKnoppers"]
	},
	"2023-04-05": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230405"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EineKleinePopcorn", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-04-06": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230406"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo"]
	},
	"2023-04-11": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230411"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo"]
	},
	"2023-04-13": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230413"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-04-14": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230414"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-04-17": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230417BraisedBalsamicoChicken", "SAPCanteen20230417Tagliatelle", "SAPCanteen20230417SweetPotatoQuinoa", "SAPCanteen20230417Passionsfrucht", "SAPCanteen20230417Passionsfrucht"],
		"snack": ["EineTasseCappuccino"],
		"dinner": ["EinYfoodClassicChoco", "EinYfoodClassicChoco" /*Guestimation: Mandys Down-Under Burger + Mozarella-Sticks*/]
	},
	"2023-04-18": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230418ArtichokesFreeRangeChicken"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioRindfleischbrot", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-04-19": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230419Calamares"],
		"snack": ["EineTasseCappuccino", "ParkkaffeeKaesekuchen"],
		"dinner": ["EinKaeseSchinkenbrot", "EsnProteinShakeVanillaWater"]
	},
	"2023-04-20": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230420PastaGroundMeatRagout", "SAPCanteen20230420VeganChocolatePudding"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-04-21": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230421PadThai", "SAPCanteen20230421PlumCrumble", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "SAPCanteen20230421PlumCrumble"],
		"dinner": ["EinKaeseSchinkenbrot", "EsnProteinShakeCinnamonCerealHVollmilch"]
	},
	"2023-04-24": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230424ChickenFajita", "SAPCanteen20230424WheatRisotto", "SAPCanteen20230424Johannisbeerquark", "SAPCanteen20230424Johannisbeerquark"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinChickenCaesarSalad" /*Guestimation: Doktor Flotte Crispy-Chicken*/, "EsnProteinShakeCinnamonCerealHVollmilch"]
	},
	"2023-04-25": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230425BeyondBurger", "SAPCanteen20230425SourCreamStrawberryChia"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioBierschinkenbrot", "EsnProteinShakeCinnamonCerealHVollmilch"]
	},
	"2023-04-26": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230426ChickenBreastViennaArt", "SAPCanteen20230426Lentils"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["ProteinWrapSandwichKaeseKochschinkenAvocadoHummus", "ProteinWrapSandwichKaeseKochschinkenAvocadoHummus", "EsnProteinShakeCinnamonCerealHVollmilch"]
	},
	"2023-05-02": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230502"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinDuplo", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioRindfleischbrot", "EinDuplo", "EinDuplo", "EinDuplo" /*Guestimation: ein Stueck Nusszopf*/]
	},
	"2023-05-03": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230503ItalyButterfishsteak", "SAPCanteen20230503RedLentils", "SAPCanteen20230503VeganSemolinaPudding"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinDuplo", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeCinnamonCerealHVollmilch", "EinDuplo", "EinDuplo" /*Guestimation: ein Stueck Nusszopf*/]
	},
	"2023-05-04": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230504HariraChickpeaSoup", "SAPCanteen20230504SaffronChicken", "SAPCanteenLegumes"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["ProteinWrapGoudaKochschinkenHummus", "ProteinWrapGoudaKochschinkenHummus", "EinDuplo"]
	},
	"2023-05-05": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230505LimeRisottoChickenBreast", "SAPCanteen20230505RedQuinoa", "SAPCanteen20230505VanilleQuarkErdbeer", "SAPCanteen20230505VanilleQuarkErdbeer", "SAPCanteen20230505VanilleQuarkErdbeer"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["ProteinWrapGoudaKochschinkenHummus", "ProteinWrapGoudaKochschinkenHummus", "EinDuplo"]
	},
	"2023-05-09": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230509ChickenThighsCarrotFries"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2023-05-10": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230510"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeCinnamonCerealHVollmilch"]
	},
	"2023-05-11": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230511WildBoarStew", "SAPCanteen20230511AlmondPudding"],
		"snack": ["EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeCinnamonCerealHVollmilch"]
	},
	"2023-05-12": {
		"breakfast": ["OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230512"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EsnProteinShakeVanillaWater"]
	},
	"2023-05-16": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230516IndiaChicken", "SAPCanteen20230516KidneyBeans"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioRindfleischbrot", "EinDuplo", "EinDuplo", "EinDuplo"/*Guestimation: Danas Mango-Nachtisch*/, "EsnProteinShakeVanillaWater"]
	},
	"2023-05-23": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230523ItalyPastaAsparagus", "SAPCanteen20230523ButteredPeas"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo", "EsnProteinShakeCinnamonCerealHVollmilch"]
	},
	"2023-05-24": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230524HungaryBeefGoulash", "SAPCanteen20230524RedQuinoa", "SAPCanteen20230524PotatoWedges", "SAPCanteen20230524SuesserBulgur", "SAPCanteen20230524MaispolentaHonig"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinYfoodClassicChoco", "EinDuplo", "EsnProteinShakeCinnamonCerealHVollmilch"]
	},
	"2023-05-25": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230525RavioliSpinatCottageCheese", "SAPCanteen20230525RaspberryCream"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EinDuplo", "EsnProteinShakeCinnamonCerealHVollmilch"]
	},
	"2023-05-26": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230526FranceFilletOfAlaskaPollock"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EinBecherEhrmannHighProteinChocolateMousse", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["ProteinWrapSandwichKaeseKochschinkenAvocadoHummus", "ProteinWrapSandwichKaeseKochschinkenAvocadoHummus", "EsnProteinShakeCinnamonCerealHVollmilch"]
	},
	"2023-05-30": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230530PiccataTurkey"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioBierschinkenbrot", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-05-31": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230531"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinKaeseSchinkenbrot", "EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-06-01": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230601ChickenFricass", "SAPCanteen20230601SourCreamPassionFruitChia"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["HalbesRuehreiV2", "EinBecherEhrmannHighProteinChocolateMousse", "Marmeladenbrot", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-06-02": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230602FilletOfTilapia", "SAPCanteen20230602Cauliflower", "SAPCanteen20230602Lentils", "SAPCanteen20230602Lentils", "SAPCanteen20230602PannaCotta"],
		"snack": ["EineTasseCappuccino"],
		"dinner": ["EinKaeseSchinkenbrot", "Marmeladenbrot", "EinDuplo", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-06-05": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230605GroundBeefVegetablesRice", "SAPCanteen20230605Quinoa", "SAPCanteen20230605Broccoli"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["EinFeineLeberwurstBrot", "EinFeineLeberwurstBrot", "SkyrHeidelbeeren", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-06-06": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230606Schaeufele"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["BioLammbratwurstbrot", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-06-07": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230607ItalyBraisedChicken"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelSchoko"],
		"dinner": ["Piccolinis", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-06-08": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkReweBioWeissesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["ProteinWrapEinEiSandwichKaeseKochschinkenAvocado", "ProteinWrapEinEiSandwichKaeseKochschinkenAvocado"],
		"snack": ["EinSelbstgemachterEiskaffee", "EsnProteinShakeVanillaHVollmilch"],
		"dinner": ["SalatBrokkoliPaprikaHaehnchenbrustSylterOriginal", "EinStueckLindtEiscafeSchokolade"]
	},
	"2023-06-09": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkReweBioWeissesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230609AlgeriaFriedFish"],
		"dinner": ["Piccolinis", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-06-12": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkReweBioWeissesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230612GratinatedChickenBreastRice", "SAPCanteen20230612GreekYoghurtHoneyWalnuts"],
		"dinner": ["MyFiveGuysBigBaconeBarBQCheeseBurger", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-06-13": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkReweBioWeissesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230613ChickenBreastStalida", "SAPCanteen20230613BananaPudding"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BioRindfleischbrot", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-06-14": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkReweBioWeissesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230614LikamaHloaBraisedChicken", "SAPCanteen20230614BakedBeans", "SAPCanteen20230613BananaPudding"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-06-15": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkReweBioWeissesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230615CrispyPrawnSushiBowl", "SAPCanteen20230615ChocolatePudding"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EinDuplo", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-06-16": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkReweBioWeissesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230616RedfishColorfulLentils", "SAPCanteen20230616ApricotDumplings"],
		"snack": ["EineTasseCappuccino", "EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaeseSchinkenbrot", "200Skyr40Krispies", "EsnProteinShakeVanillaHVollmilch"]
	},
	"2023-06-17": {
		"breakfast": ["20230617WarmPorridge"],
		"lunch": ["HansImGlueckGeselleSauerteigbroetchen", "HansImGlueckKrautknolle"],
		"snack": ["EinDuplo", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["RicePudding50EsnDesignerWheyProteinVanilla30", "200Skyr40Krispies"]
	},
	"2023-06-19": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkReweBioWeissesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230619BraisedTurkeyMeatball", "SAPCanteen20230619Broccoli", "SAPCanteen20230619CreamyCurd"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["20230619Abendessen", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-06-20": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230620ChickenSaltimbocca"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BioRindfleischbrot", "EinDuplo"]
	},
	"2023-06-21": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230621TurkeySteak"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["WrapEinEiSandwichKaeseKochschinkenAvocado", "WrapEinEiSandwichKaeseKochschinkenAvocado", "EinDuplo"]
	},
	"2023-06-22": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230622VegetableDumplings", "SAPCanteen20230622Mandelpudding"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaeseSchinkenbrot", "EinKaeseSchinkenbrot", "EsnProteinShakeVanillaFettarmeHMlich", "EinDuplo"]
	},
	"2023-06-23": {
		"breakfast": ["OvernightDinkelOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230623ItalyFish", "SAPCanteen20230623Quinoa"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["WrapEinEiSandwichKaeseKochschinkenAvocado", "WrapEinEiSandwichKaeseKochschinkenAvocado", "EinDuplo", "FakeIceCoffeeWithVanillaProtein"]
	},
	"2023-06-24": {
		"breakfast": ["20230614WarmPorridge"],
		"lunch": ["BioBierschinkenbrot"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "200Skyr40Krispies"],
		"dinner": ["20230624Abendessen"]
	},
	"2023-06-26": {
		"breakfast": ["20230626Breakfast"],
		"lunch": ["SAPCanteen20230626ChickenDoner"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["WrapEinEiSandwichKaeseKochschinkenAvocado", "WrapEinEiSandwichKaeseKochschinkenAvocado", "EsnProteinShakeVanillaFettarmeHMlich", "EinDuplo"]
	},
	"2023-06-27": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230627BreadedChickenSchnitzel"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BioBierschinkenbrot", "EsnProteinShakeIsoclearWater"]
	},
	"2023-06-28": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230628BeefBrisket", "SAPCanteen20230628Chickpeas"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinYfoodClassicChoco", "EsnProteinShakeIsoclearWater"]
	},
	"2023-06-30": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230630FilletOfRedfish", "SAPCanteen20230630Lentils"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["WrapEinEiSandwichKaeseKochschinkenAvocado", "WrapEinEiSandwichKaeseKochschinkenAvocado"]
	},
	"2023-07-01": {
		"breakfast": ["20230701WarmPorridge"],
		"lunch": ["RicePudding50EsnDesignerWheyProteinVanilla30"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EinDuplo"],
		"dinner": ["GeneralTsoTofu", "EinDuplo", "EsnProteinShakeIsoclearWater"]
	},
	"2023-07-03": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230703MexicoBurritoBowl"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaeseSchinkenbrot", "EsnProteinShakeVanillaFettarmeHMlich", "EinDuplo"]
	},
	"2023-07-04": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230704MexicoAlambreDeBistec"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2023-07-06": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230706ItaliaChickenSaltimbocca", "SAPCanteen20230706BakedBeans", "SAPCanteen20230706Broccoli"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["ProteinWrapEinEiSandwichKaeseKochschinkenAvocado", "ProteinWrapEinEiSandwichKaeseKochschinkenAvocado"]
	},
	"2023-07-10": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230710SaltimboccaOfChicken", "SAPCanteen20230710BroccoliWithAlmonds", "SAPCanteen20230710Erdbeerquark"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "SehrKleinePortionWalnuss", "EinDuplo", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-07-11": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230711RedFisch"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["BioBierschinkenbrot", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-07-12": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["EinYfoodClassicChoco", "EinHanuta"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["ProteinWrapEinEiSandwichKaeseKochschinkenAvocado", "ProteinWrapEinEiSandwichKaeseKochschinkenAvocado", "EsnProteinShakeIsoclearWater"]
	},
	"2023-07-13": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230713ChickenBreastViennaArt", "SAPCanteen20230713PotatoAndChickpeaMash"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino", "EineTasseCappuccino"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-07-14": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230714SaitheBouillabaiseBrew"],
		"snack": ["EineTasseCappuccino"],
		"dinner": ["ProteinWrapEinEiSandwichKaeseKochschinkenAvocado", "ProteinWrapEinEiSandwichKaeseKochschinkenAvocado", "EinDuplo"]
	},
	"2023-07-15": {
		"breakfast": ["20230715WarmPorridge"],
		"lunch": ["SmallPiccolinis"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["20230715AnanasCurryHaehnchenbrust", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-07-17": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyAnanas"],
		"lunch": ["SAPCanteen20230717ChickenFricass"],
		"snack": ["EineTasseCappuccino", "ParkkaffeeKaesekuchen", "EineBanane", "EinSeitenbacherProteinRiegelCappuccino"],
		"dinner": ["RicePudding50EsnDesignerWheyProteinVanilla30", "EsnProteinShakeIsoclearWater"]
	},
	"2023-07-18": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230718GrilledChickenBreast"],
		"snack": ["ParkkaffeeKaesekuchen", "EineBanane"],
		"dinner": ["BioRindfleischbrot"]
	},
	"2023-07-19": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230719GreenGoddessBowl"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["WrapEinEiSandwichKaeseKochschinkenAvocado", "WrapEinEiSandwichKaeseKochschinkenAvocado", "EsnProteinShakeIsoclearWater"]
	},
	"2023-07-20": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230720AfricanPeanutStew", "SAPCanteen20230720PeachAndCurrantCompote"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-07-21": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230721FriedTrout", "SAPCanteen20230721ApricotDumplings"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["WrapEinEiSandwichKaeseKochschinkenAvocado", "WrapEinEiSandwichKaeseKochschinkenAvocado", "EinDuplo", "EsnProteinShakeIsoclearWater"]
	},
	"2023-07-24": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230724FriedSquid"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["MyFiveGuysBigBaconeBarBQCheeseBurger", "EsnProteinShakeIsoclearWater"]
	},
	"2023-07-25": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230725FishThaiCurry", "EinDuplo", "EinDuplo"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinDuplo", "EinDuplo"],
		"dinner": ["BioLammbratwurstbrot", "EsnProteinShakeIsoclearWater"]
	},
	"2023-07-26": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230726JapanBeefTeriyaki", "SAPCanteen20230726Strawberrycream"],
		"snack": ["EineTasseCappuccino", "EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ProteinWrapEinEiSandwichKaeseKochschinkenAvocadoOel", "ProteinWrapEinEiSandwichKaeseKochschinkenAvocadoOel", "EinStueckLindtEiscafeSchokolade"]
	},
	"2023-07-27": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren"],
		"lunch": ["SAPCanteen20230727BeefLambCevapcici", "EineTasseCappuccino", "EinDuplo", "EinDuplo"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinDuplo", "EinDuplo"],
		"dinner": ["EinKaeseSchinkenbrot", "EsnProteinShakeIsoclearWater"]
	},
	"2023-07-31": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230731ChickenBreastStalida", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ProteinWrapEinSandwichKaeseKochschinken", "ProteinWrapEinSandwichKaeseKochschinken", "EsnProteinShakeIsoclearWater"]
	},
	"2023-08-01": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230801SteakFromTurkey", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BioLammbratwurstbrot"]
	},
	"2023-08-02": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230802FilletOfHoki", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ProteinWrapEinSandwichKaeseKochschinken", "ProteinWrapEinSandwichKaeseKochschinken", "EinDuplo", "EsnProteinShakeIsoclearWater"]
	},
	"2023-08-03": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230803BeefRedCurry", "SAPCanteen20230619CreamyCurd", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ProteinWrapEinSandwichKaeseKochschinken", "ProteinWrapEinSandwichKaeseKochschinken", "EsnProteinShakeIsoclearWater"]
	},
	"2023-08-04": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230804TuerkeiKebabRindBulgur", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-08-05": {
		"breakfast": ["20230805WarmPorridge"],
		"lunch": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "EinYfoodClassicChoco"],
		"snack": ["200Skyr20Krispies"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
	"2023-08-07": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230807ChickenSaltimbocca", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaeseSchinkenbrot", "EinDuplo", "EinDuplo", "EsnProteinShakeVanillaFettarmeHMlich"]
	},
	"2023-08-08": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230808JapanMieNoodlesTurkeyMeat", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["BioBierschinkenbrot"]
	},
	"2023-08-09": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230809TilapiaCressSauceAsparagus", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ProteinWrapEinSandwichKaeseKochschinken", "ProteinWrapEinSandwichKaeseKochschinken", "EsnProteinShakeIsoclearWater"]
	},
	"2023-08-10": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230810LambTagine", "EinZitronenKaesekuchen", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["EinKaeseSchinkenKornkraftbrot", "200Skyr20SojaCrispies", "EsnProteinShakeIsoclearWater"]
	},
	"2023-08-11": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230811IndiaFishCurry", "EinZitronenKaesekuchen", "EineTasseCappuccino"],
		"dinner": ["EinKaeseSchinkenKornkraftbrot", "200Skyr20SojaCrispies", "EsnProteinShakeIsoclearWater"]
	},
	"2023-08-12": {
		"breakfast": ["20230812WarmPorridge"],
		"lunch": ["EinYfoodCherryBanana"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ProteinWrapEinSandwichKaeseKochschinken", "ProteinWrapEinSandwichKaeseKochschinken", "EsnProteinShakeIsoclearWater"]
	},
	"2023-08-14": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230814SwabianPorkRoastWithOnions", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ProteinWrapEinSandwichKaeseKochschinken", "ProteinWrapEinSandwichKaeseKochschinken", "EsnProteinShakeIsoclearWater"]
	},
	"2023-08-15": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230815ChopSueyBasmatiRice", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["HalbesRuehreiV2Kornkraft"]
	},
	"2023-08-16": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230816", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ProteinWrapEinSandwichKaeseKochschinken", "ProteinWrapEinSandwichKaeseKochschinken", "EsnProteinShakeIsoclearWater", "EinBecherEhrmannHighProteinDoubleChoc"]
	},
	"2023-09-11": {
		"breakfast": ["OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia"],
		"lunch": ["SAPCanteen20230911", "EineTasseCappuccino"],
		"snack": ["EineBanane", "EinSeitenbacherProteinRiegelVanille"],
		"dinner": ["ProteinWrapEinEiKaeseKochschinkenV2", "ProteinWrapEinEiKaeseKochschinkenV2", "EinBecherEhrmannHighProteinChocolateMousse"]
	},
	"2023-10-15": {
		"breakfast": ["EineBanane", "EinSeitenbacherProteinRiegelVanille", "20231015Breakfast", "EinEmmiCaffeLatteHighProtein"],
		"lunch": ["200Skyr20Krispies"],
		"dinner": ["FrostaHaehnchenCurry"]
	},
};
