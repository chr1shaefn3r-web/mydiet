export type Price = number;

export type Prices = { [key: string]: Price; };

// prices per 100g, ~2021
export const prices: Prices = {
    "Karotten": 0.159,
    "Banane": 0.229,
	"Haehnchenbrust": 2.99,
	"Tomate": 0.499,
	"Gurke": 0.189,
	"Paprika": 0.399,
	"Ingwer": 0.49,
    "ReweBioRinderHackfleisch": (6.49/400) * 100,
    "ReweMagerQuark": (0.4/250) * 100,
    "BarillaFusilli": (1.65/500) * 100,
	"BioRinderFond": (2.99/400) * 100,
	"Ei": (4.39/(50*10)) * 100, //10 Eier pro Packung
	"JasminReis": (2.99/500) * 100,
	"bonduelleKirchererbsen": (1.99/265) * 100,
	"fairtradeKokosmilch": (2.99/400) * 100,
	"reweGrueneCurryPaste": (1.29/110) * 100,
	"OroDiParmaTomatenmark": (1.49/200) * 100,
	"arlaLaktoseFrei": (1.59/1000) * 100,
	"PatrosNatur": (2.19/180) * 100,
	"BeliefMandelMilch": (3.29/1000) * 100,
	"TkMango": (2.49/500) * 100,
	"BioRindfleisch": (3.69/200) * 100,
	"BioLyoner": (3.69/200) * 100,
	"Cornichons": (1.99/190) * 100,
	"ReweSauerKirschNektar": (1.19/1000) * 100,
	"WagnerPiccolinis": (2.69/(3*90)) * 100,
	"SojaSauce": (2.49/150) * 100,
	"MaggiKartoffelGratin": (0.85/37) * 100,
	"cnTkKaisergemuese": (1.99/1000) * 100,
	"ReweBesteWahlSchlagsahne": (0.63/200) * 100,
	"Zwiebel": (1.89/750) * 100,
	"Kartoffel": (1.99/1500) * 100,
	"JaGratinKaese": (1.43/200) * 100,
	"Bohnen": (2.49/200) * 100,
	"BioBratwurstGrob": (2.89/200) * 100,
	"kinderRiegel": (1.99/210) * 100,
	"CocaCola": (1.09/1000) * 100,
	"Parmesan": (1.79 / 100) * 100,
	"Spinat": (1.29 / 100) * 100,
	"KerrygoldButter": (2.39 / 100) * 100,
	"SeitenbacherDinoMuesli": (5.79 / 750) * 100,
	"FerreroDuplo": (3.49 / (18.2 * 18)) * 100,
	"CaffeFreddoCappuccino": (0.99/200) * 100,
};
