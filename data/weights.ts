export type Bodyweight = number;
export type BodyfatInPercent = number;
export type BodyfatInKg = number;
export type MuscleInPercent = number;
export type MuscleInKg = number;

export interface BodyComposition {
	weight: Bodyweight;
	bodyfatInPercent?: BodyfatInPercent;
	bodyfatInKg?: BodyfatInKg;
	muscleInPercent?: MuscleInPercent;
	muscleInKg?: MuscleInKg;
}

export type BodyCompositionPerDay = { [key: string]: BodyComposition; };

export const bodyCompositionPerDay: BodyCompositionPerDay = {
	"2018-10-26": {
		weight: 74.7,
		bodyfatInPercent: 19.4,
		bodyfatInKg: 14.5,
		muscleInPercent: 102.1,
		muscleInKg: 55.8
	},
	"2019-03-01": {
		weight: 72.1,
		bodyfatInPercent: 18.3,
		bodyfatInKg: 13.2,
		muscleInPercent: 100.1,
		muscleInKg: 54.7
	},
	"2020-04-25":  {
		weight: 66
	},
	"2020-05-02":  {
		weight: 65.7
	},
	"2020-05-09":  {
		weight: 67
	},
	"2020-05-30":  {
		weight: 65.5
	},
	"2020-06-05": {
		weight: 66
	},
	"2020-06-07": {
		weight: 65.7
	},
	"2020-06-09": {
		weight: 67.1
	},
	"2020-06-14": {
		weight: 67
	},
	"2020-06-19": {
		weight: 67.4,
		bodyfatInPercent: 13.9,
		bodyfatInKg: 9.4,
		muscleInPercent: 99,
		muscleInKg: 54.1
	},
	"2020-06-23": {
		weight: 67
	},
	"2020-06-26": {
		weight: 67
	},
	"2020-06-28": {
		weight: 67
	},
	"2020-06-30": {
		weight: 68
	},
	"2020-07-04": {
		weight: 66.8
	},
	"2020-07-07": {
		weight: 68.5
	},
	"2020-07-17": {
		weight: 69.1,
		bodyfatInPercent: 14.5,
		bodyfatInKg: 10,
		muscleInPercent: 100.9,
		muscleInKg: 55.1
	},
	"2020-07-19": {
		weight: 68
	},
	"2020-07-24": {
		weight: 68.5
	},
	"2020-07-25": {
		weight: 67
	},
	"2020-07-28": {
		weight: 69.8,
		bodyfatInPercent: 15,
		bodyfatInKg: 10.5,
		muscleInPercent: 100.1,
		muscleInKg: 55.2
	},
	"2020-07-31": {
		weight: 69.5
	},
	"2020-08-02": {
		weight: 69
	},
	"2020-08-04": {
		weight: 68.5
	},
	"2020-08-07": {
		weight: 69.5
	},
	"2020-08-11": {
		weight: 68.6,
		bodyfatInPercent: 13.1,
		bodyfatInKg: 9,
		muscleInPercent: 101.8,
		muscleInKg: 55.6
	},
	"2020-08-16": {
		weight: 69.5
	},
	"2020-08-18": {
		weight: 70.5
	},
	"2020-08-21": {
		weight: 70
	},
	"2020-08-23": {
		weight: 69.5
	},
	"2020-08-28": {
		weight: 68
	},
	"2020-08-30": {
		weight: 70.3,
		bodyfatInPercent: 15.5,
		bodyfatInKg: 10.9,
		muscleInPercent: 101.2,
		muscleInKg: 55.3
	},
	"2020-09-04": {
		weight: 69
	},
	"2020-09-08": {
		weight: 69
	},
	"2020-09-11": {
		weight: 69.5
	},
	"2020-09-13": {
		weight: 70
	},
	"2020-09-18": {
		weight: 69.5
	},
	"2020-09-25": {
		weight: 70.5
	},
	"2020-09-29": {
		weight: 71.3,
		bodyfatInPercent: 15.3,
		bodyfatInKg: 10.9,
		muscleInPercent: 102.7,
		muscleInKg: 56.1
	},
	"2020-10-04": {
		weight: 70
	},
	"2020-10-09": {
		weight: 71
	},
	"2020-10-13": {
		weight: 70.5
	},
	"2020-10-15": {
		weight: 70.8,
		bodyfatInPercent: 15.5,
		bodyfatInKg: 11,
		muscleInPercent: 102,
		muscleInKg: 55.7
	},
	"2020-10-23": {
		weight: 71.5
	},
	"2020-10-25": {
		weight: 71.5
	},
	"2020-10-27": {
		weight: 71
	},
	"2020-10-30": {
		weight: 71.6,
		bodyfatInPercent: 16.5,
		bodyfatInKg: 11.8,
		muscleInPercent: 102,
		muscleInKg: 55.7
	},
	"2020-11-01": {
		weight: 72
	},
	"2020-11-02": {
		weight: 72
	},
	"2020-11-07": {
		weight: 70
	},
	"2020-11-14": {
		weight: 70
	},
	"2020-12-06": {
		weight: 70
	},
	"2020-12-24": {
		weight: 69.5
	},
	"2021-01-04": {
		weight: 69
	},
	"2021-01-24": {
		weight: 69
	},
	"2021-06-08": {
		weight: 67.3,
		bodyfatInPercent: 12.2,
		bodyfatInKg: 8.2,
		muscleInPercent: 101,
		muscleInKg: 55.2
	},
	"2021-06-13": {
		weight: 66.7,
		bodyfatInPercent: 12.7,
		bodyfatInKg: 8.5,
		muscleInPercent: 99.4,
		muscleInKg: 54.3
	},
	"2021-06-19": {
		weight: 66.7
	},
	"2021-06-20": {
		weight: 68.2
	},
	"2021-06-22": {
		weight: 68.4,
		bodyfatInPercent: 13.6,
		bodyfatInKg: 9.3,
		muscleInPercent: 100.9,
		muscleInKg: 55.1
	},
	"2021-06-29": {
		weight: 67.5,
		bodyfatInPercent: 13.5,
		bodyfatInKg: 9.1,
		muscleInPercent: 99.6,
		muscleInKg: 54.4
	},
	"2021-07-06": {
		weight: 67.9,
		bodyfatInPercent: 14.1,
		bodyfatInKg: 9.6,
		muscleInPercent: 99.4,
		muscleInKg: 54.3
	},
	"2021-07-13": {
		weight: 68.8,
		bodyfatInPercent: 14.2,
		bodyfatInKg: 9.8,
		muscleInPercent: 100.7,
		muscleInKg: 55
	},
	"2021-07-20": {
		weight: 68.6,
		bodyfatInPercent: 13.3,
		bodyfatInKg: 9.1,
		muscleInPercent: 101.4,
		muscleInKg: 55.4
	},
	"2021-07-23": {
		weight: 66.9,
		bodyfatInPercent: 12.3,
		bodyfatInKg: 8.2,
		muscleInPercent: 100.3,
		muscleInKg: 54.0
	},
	"2021-07-30": {
		weight: 69.2,
		bodyfatInPercent: 14.7,
		bodyfatInKg: 10.2,
		muscleInPercent: 100.7,
		muscleInKg: 55.0
	},
	"2021-08-03": {
		weight: 70.1,
		bodyfatInPercent: 15,
		bodyfatInKg: 10.5,
		muscleInPercent: 101.6,
		muscleInKg: 55.5
	},
	"2021-08-10": {
		weight: 70,
		bodyfatInPercent: 14.9,
		bodyfatInKg: 10.4,
		muscleInPercent: 101.6,
		muscleInKg: 55.5
	},
	"2021-08-13": {
		weight: 70.5,
		bodyfatInPercent: 11.8,
		bodyfatInKg: 8.3,
		muscleInPercent: 106.4,
		muscleInKg: 58.1
	},
	"2021-08-14": {
		weight: 70.5,
		bodyfatInPercent: 14.9,
		bodyfatInKg: 10.5,
		muscleInPercent: 102.3,
		muscleInKg: 55.9
	},
	"2021-08-17": {
		weight: 71.1,
		bodyfatInPercent: 15.6,
		bodyfatInKg: 11.1,
		muscleInPercent: 102.1,
		muscleInKg: 55.8
	},
	"2021-08-22": {
		weight: 70.3,
		bodyfatInPercent: 15.6,
		bodyfatInKg: 11.0,
		muscleInPercent: 101,
		muscleInKg: 55.2
	},
	"2021-08-24": {
		weight: 70.9,
		bodyfatInPercent: 15.8,
		bodyfatInKg: 11.2,
		muscleInPercent: 101.8,
		muscleInKg: 55.6
	},
	"2021-08-31": {
		weight: 71,
		bodyfatInPercent: 15.8,
		bodyfatInKg: 11.2,
		muscleInPercent: 102,
		muscleInKg: 55.7
	},
	"2021-09-07": {
		weight: 72,
		bodyfatInPercent: 16.4,
		bodyfatInKg: 11.8,
		muscleInPercent: 102.5,
		muscleInKg: 56
	},
	"2021-09-14": {
		weight: 72.5,
		bodyfatInPercent: 16.3,
		bodyfatInKg: 11.8,
		muscleInPercent: 103.4,
		muscleInKg: 56.5
	},
	"2021-09-18": {
		weight: 71.7,
		bodyfatInPercent: 16,
		bodyfatInKg: 11.5,
		muscleInPercent: 102.5,
		muscleInKg: 56
	},
	"2021-09-21": {
		weight: 72,
		bodyfatInPercent: 16.1,
		bodyfatInKg: 11.6,
		muscleInPercent: 102.9,
		muscleInKg: 56.2
	},
	"2021-09-28": {
		weight: 72.9,
		bodyfatInPercent: 16.3,
		bodyfatInKg: 11.9,
		muscleInPercent: 103.8,
		muscleInKg: 56.7
	},
	"2021-10-08": {
		weight: 72.5,
		bodyfatInPercent: 16.6,
		bodyfatInKg: 12,
		muscleInPercent: 103.1,
		muscleInKg: 56.3
	},
	"2021-10-12": {
		weight: 73.9,
		bodyfatInPercent: 15,
		bodyfatInKg: 11.1,
		muscleInPercent: 107.1,
		muscleInKg: 58.5
	},
	"2021-10-15": {
		weight: 73.2,
		bodyfatInPercent: 15.6,
		bodyfatInKg: 11.4,
		muscleInPercent: 105.3,
		muscleInKg: 57.5
	},
	"2021-10-19": {
		weight: 73.6,
		bodyfatInPercent: 15.6,
		bodyfatInKg: 11.5,
		muscleInPercent: 105.8,
		muscleInKg: 57.8
	},
	"2021-10-23": {
		weight: 71.9,
		bodyfatInPercent: 16.8,
		bodyfatInKg: 12.1,
		muscleInPercent: 102.0,
		muscleInKg: 55.7
	},
	"2021-10-24": {
		weight: 72.9,
		bodyfatInPercent: 17.4,
		bodyfatInKg: 12.7,
		muscleInPercent: 102.3,
		muscleInKg: 55.9
	},
	"2021-10-26": {
		weight: 73.7,
		bodyfatInPercent: 14.1,
		bodyfatInKg: 10.4,
		muscleInPercent: 108,
		muscleInKg: 59
	},
	"2021-10-29": {
		weight: 73.1,
		bodyfatInPercent: 15.6,
		bodyfatInKg: 11.4,
		muscleInPercent: 105.1,
		muscleInKg: 57.4
	},
	"2021-11-02": {
		weight: 73.3,
		bodyfatInPercent: 17.3,
		bodyfatInKg: 12.7,
		muscleInPercent: 103.1,
		muscleInKg: 56.3
	},
	"2021-11-05": {
		weight: 73.9,
		bodyfatInPercent: 16.2,
		bodyfatInKg: 12,
		muscleInPercent: 105.4,
		muscleInKg: 57.6
	},
	"2021-11-09": {
		weight: 72.4,
		bodyfatInPercent: 15.9,
		bodyfatInKg: 11.5,
		muscleInPercent: 103.6,
		muscleInKg: 56.6
	},
	"2021-11-16": {
		weight: 71.2,
		bodyfatInPercent: 15.4,
		bodyfatInKg: 11,
		muscleInPercent: 102.5,
		muscleInKg: 56
	},
	"2021-11-20": {
		weight: 70.6,
		bodyfatInPercent: 15.2,
		bodyfatInKg: 10.7,
		muscleInPercent: 102,
		muscleInKg: 55.7
	},
	"2021-11-23": {
		weight: 70.8,
		bodyfatInPercent: 15.5,
		bodyfatInKg: 11,
		muscleInPercent: 102,
		muscleInKg: 55.7
	},
	"2021-11-26": {
		weight: 70.3,
		bodyfatInPercent: 12.4,
		bodyfatInKg: 8.7,
		muscleInPercent: 105.3,
		muscleInKg: 57.5
	},
	"2021-11-30": {
		weight: 69.5
	},
	"2021-12-03": {
		weight: 69.9,
		bodyfatInPercent: 14.7,
		bodyfatInKg: 10.3,
		muscleInPercent: 101.6,
		muscleInKg: 55.5
	},
	"2021-12-05": {
		weight: 69.4,
		bodyfatInPercent: 14.3,
		bodyfatInKg: 9.9,
		muscleInPercent: 101.4,
		muscleInKg: 55.4
	},
	"2021-12-10": {
		weight: 70.5,
		bodyfatInPercent: 15,
		bodyfatInKg: 10.6,
		muscleInPercent: 102,
		muscleInKg: 55.7
	},
	"2021-12-16": {
		weight: 70.3,
		bodyfatInPercent: 15.1,
		bodyfatInKg: 10.6,
		muscleInPercent: 101.8,
		muscleInKg: 55.6
	},
	"2021-12-19": {
		weight: 71.5,
		bodyfatInPercent: 15.2,
		bodyfatInKg: 10.9,
		muscleInPercent: 103.2,
		muscleInKg: 56.4
	},
	"2021-12-23": {
		weight: 71.2,
		bodyfatInPercent: 15.3,
		bodyfatInKg: 10.9,
		muscleInPercent: 102.7,
		muscleInKg: 56.1
	},
	"2021-12-28": {
		weight: 71.8,
		bodyfatInPercent: 13.8,
		bodyfatInKg: 9.9,
		muscleInPercent: 105.6,
		muscleInKg: 57.7
	},
	"2022-01-06": {
		weight: 73,
		bodyfatInPercent: 16.7,
		bodyfatInKg: 12.2,
		muscleInPercent: 103.6,
		muscleInKg: 56.6
	},
	"2022-01-11": {
		weight: 72.8,
		bodyfatInPercent: 16.5,
		bodyfatInKg: 12,
		muscleInPercent: 103.6,
		muscleInKg: 56.6
	},
	"2022-01-14": {
		weight: 72.7,
		bodyfatInPercent: 16.5,
		bodyfatInKg: 12,
		muscleInPercent: 103.4,
		muscleInKg: 56.5
	},
	"2022-01-21": {
		weight: 72.4,
		bodyfatInPercent: 16.6,
		bodyfatInKg: 12,
		muscleInPercent: 102.9,
		muscleInKg: 56.3
	},
	"2022-01-31": {
		weight: 72.1,
		bodyfatInPercent: 16.8,
		bodyfatInKg: 12.1,
		muscleInPercent: 102.1,
		muscleInKg: 55
	},
	"2022-02-10": {
		weight: 74.1,
		bodyfatInPercent: 15.4,
		bodyfatInKg: 11.4,
		muscleInPercent: 106.7,
		muscleInKg: 58.3
	},
	"2022-02-12": {
		weight: 73.6,
		bodyfatInPercent: 14.9,
		bodyfatInKg: 11,
		muscleInPercent: 106.7,
		muscleInKg: 58.3
	},
	"2022-02-17": {
		weight: 73.8,
		bodyfatInPercent: 14.9,
		bodyfatInKg: 11,
		muscleInPercent: 107.1,
		muscleInKg: 58.5
	},
	"2022-02-19": {
		weight: 73.1,
		bodyfatInPercent: 16.8,
		bodyfatInKg: 12.3,
		muscleInPercent: 103.6,
		muscleInKg: 56.6
	},
	"2022-02-24": {
		weight: 74.1,
		bodyfatInPercent: 14.6,
		bodyfatInKg: 10.8,
		muscleInPercent: 108,
		muscleInKg: 59
	},
	"2022-02-26": {
		weight: 73.5,
		bodyfatInPercent: 15.8,
		bodyfatInKg: 11.6,
		muscleInPercent: 105.4,
		muscleInKg: 57.6
	},
	"2022-03-03": {
		weight: 74.1,
		bodyfatInPercent: 15.1,
		bodyfatInKg: 11.2,
		muscleInPercent: 107.3,
		muscleInKg: 58.6
	},
	"2022-03-06": {
		weight: 74.4,
		bodyfatInPercent: 16.5,
		bodyfatInKg: 12.3,
		muscleInPercent: 105.6,
		muscleInKg: 57.7
	},
	"2022-03-17": {
		weight: 74,
	},
	"2022-03-18": {
		weight: 74.5,
	},
	"2022-03-24": {
		weight: 75,
	},
	"2022-03-26": {
		weight: 74,
	},
	"2022-03-29": {
		weight: 75.5,
	},
	"2022-03-31": {
		weight: 74.5,
	},
	"2022-04-06": {
		weight: 77.7,
		bodyfatInPercent: 18.8,
		bodyfatInKg: 14.6,
		muscleInPercent: 107.3,
		muscleInKg: 58.6
	},
	"2022-04-10": {
		weight: 76,
	},
	"2022-04-12": {
		weight: 77,
	},
	"2022-04-14": {
		weight: 77.5,
	},
	"2022-04-18": {
		weight: 77,
	},
	"2022-05-01": { // dated back to CW28 to end phase, actual date: 2022-05-03
		weight: 77.3,
		bodyfatInPercent: 17.1,
		bodyfatInKg: 13.2,
		muscleInPercent: 109.1,
		muscleInKg: 59.6
	},
	"2022-05-02": {
		weight: 77.5,
	},
	"2022-05-08": {
		weight: 77.5,
	},
	"2022-05-10": {
		weight: 76.6,
		bodyfatInPercent: 16,
		bodyfatInKg: 12.3,
		muscleInKg: 36.6
	},
	"2022-05-12": {
		weight: 75.5,
	},
	"2022-05-14": {
		weight: 76,
	},
	"2022-05-17": {
		weight: 76,
	},
	"2022-05-19": {
		weight: 76,
	},
	"2022-05-22": {
		weight: 75,
	},
	"2022-05-26": {
		weight: 74,
	},
	"2022-05-29": {
		weight: 75.5,
	},
	"2022-06-02": {
		weight: 75,
	},
	"2022-06-11": {
		weight: 75,
	},
	"2022-06-16": {
		weight: 74,
	},
	"2022-06-19": {
		weight: 75,
	},
	"2022-06-21": {
		weight: 74,
	},
	"2022-06-23": {
		weight: 74,
	},
	"2022-06-24": {
		weight: 74.4,
		bodyfatInPercent: 14.1,
		bodyfatInKg: 10.5,
		muscleInKg: 36.4
	},
	"2022-06-28": {
		weight: 73,
	},
	"2022-07-02": {
		weight: 73.5,
	},
	"2022-07-03": {
		weight: 73,
	},
	"2022-07-05": {
		weight: 74.5,
	},
	"2022-07-10": {
		weight: 73,
	},
	"2022-07-15": {
		weight: 73.5,
	},
	"2022-07-16": {
		weight: 73,
	},
	"2022-07-19": {
		weight: 74,
	},
	"2022-07-23": {
		weight: 73.5,
	},
	"2022-07-24": {
		weight: 73,
	},
	"2022-07-28": {
		weight: 73.5,
	},
	"2022-07-29": {
		weight: 73,
	},
	"2022-08-02": {
		weight: 73.5,
	},
	"2022-08-04": {
		weight: 73.5,
	},
	"2022-08-07": {
		weight: 72.5,
	},
	"2022-08-12": {
		weight: 72,
	},
	"2022-08-15": {
		weight: 72,
	},
	"2022-08-19": {
		weight: 72.8,
		bodyfatInPercent: 13,
		bodyfatInKg: 9.5,
		muscleInKg: 36.3
	},
	"2022-08-22": { // dated into the future to CW34 to start phase, actual date: 2022-08-19
		weight: 72.8,
		bodyfatInPercent: 13,
		bodyfatInKg: 9.5,
		muscleInKg: 36.3
	},
	"2022-08-26": {
		weight: 72.5,
	},
	"2022-08-27": {
		weight: 72,
	},
	"2022-09-02": {
		weight: 72.5,
	},
	"2022-09-06": {
		weight: 72.5,
	},
	"2022-09-14": {
		weight: 73,
	},
	"2022-09-20": {
		weight: 74,
	},
	"2022-09-22": {
		weight: 72.5,
	},
	"2022-09-23": {
		weight: 73.5,
	},
	"2022-10-04": {
		weight: 74.5,
	},
	"2022-10-06": {
		weight: 74,
	},
	"2022-10-11": {
		weight: 74.5,
	},
	"2022-10-18": {
		weight: 75,
	},
	"2022-10-25": {
		weight: 76.3,
		bodyfatInPercent: 14.6,
		bodyfatInKg: 11.2,
		muscleInKg: 37
	},
	"2022-11-04": {
		weight: 75,
	},
	"2022-11-08": {
		weight: 75.5,
	},
	"2022-11-10": {
		weight: 74,
	},
	"2022-11-15": {
		weight: 75,
	},
	"2022-11-18": {
		weight: 75,
	},
	"2022-11-24": {
		weight: 75.5,
	},
	"2022-11-29": {
		weight: 76,
	},
	"2022-12-13": {
		weight: 77.6,
		bodyfatInPercent: 16.5,
		bodyfatInKg: 12.8,
		muscleInKg: 36.8
	},
	"2022-12-20": {
		weight: 76.5,
	},
	"2022-12-29": {
		weight: 76.5,
	},
	"2022-12-30": {
		weight: 76,
	},
	"2023-01-09": {
		weight: 76.5,
	},
	"2023-01-10": {
		weight: 77.7,
		bodyfatInPercent: 16.4,
		bodyfatInKg: 12.7,
		muscleInKg: 36.9
	},
	"2023-01-13": {
		weight: 75.5,
	},
	"2023-01-14": {
		weight: 76,
	},
	"2023-02-14": {
		weight: 77,
	},
	"2023-02-17": {
		weight: 76,
	},
	"2023-03-07": {
		weight: 77.5,
	},
	"2023-03-12": {
		weight: 78,
	},
	"2023-03-15": {
		weight: 79,
	},
	"2023-03-21": {
		weight: 78,
	},
	"2023-03-24": {
		weight: 79,
	},
	"2023-03-25": {
		weight: 78.5,
	},
	"2023-03-28": {
		weight: 79.3,
		bodyfatInPercent: 17.1,
		bodyfatInKg: 13.5,
		muscleInKg: 37.5
	},
	"2023-04-01": {
		weight: 79,
	},
	"2023-04-04": {
		weight: 80,
	},
	"2023-04-08": {
		weight: 79.5,
	},
	"2023-04-14": {
		weight: 80,
	},
	"2023-04-20": {
		weight: 79,
	},
	"2023-04-24": {
		weight: 80.5,
	},
	"2023-05-05": {
		weight: 81,
	},
	"2023-05-09": {
		weight: 81.5,
	},
	"2023-05-11": {
		weight: 80.5,
	},
	"2023-05-14": { // dated from the future to CW19 to end phase, actual date: 2023-05-16
		weight: 81.1,
		bodyfatInPercent: 18.8,
		bodyfatInKg: 15.2,
		muscleInKg: 37.5
	},
	"2023-05-16": {
		weight: 81.1,
		bodyfatInPercent: 18.8,
		bodyfatInKg: 15.2,
		muscleInKg: 37.5
	},
	"2023-05-23": { // dated from the past to CW21 to start phase, actual date: 2023-05-16
		weight: 81.1,
		bodyfatInPercent: 18.8,
		bodyfatInKg: 15.2,
		muscleInKg: 37.5
	},
	"2023-05-30": {
		weight: 81,
	},
	"2023-06-04": {
		weight: 81,
	},
	"2023-06-06": {
		weight: 80,
	},
	"2023-06-13": {
		weight: 79,
	},
	"2023-06-20": {
		weight: 80,
	},
	"2023-06-29": {
		weight: 79.5,
	},
	"2023-07-04": {
		weight: 79,
	},
	"2023-07-11": {
		weight: 79.1,
		bodyfatInPercent: 18.1,
		bodyfatInKg: 14.3,
		muscleInKg: 36.9
	},
	"2023-07-13": {
		weight: 78,
	},
	"2023-07-15": {
		weight: 78.5,
	},
	"2023-07-18": {
		weight: 78,
	},
	"2023-07-25": {
		weight: 78,
	},
	"2023-08-01": {
		weight: 78,
	},
	"2023-08-13": { // dated back from 2023-08-15 to finish phase
		weight: 77.6,
		bodyfatInPercent: 17.2,
		bodyfatInKg: 13.4,
		muscleInKg: 36.5
	},
	"2023-08-15": {
		weight: 77.6,
		bodyfatInPercent: 17.2,
		bodyfatInKg: 13.4,
		muscleInKg: 36.5
	},
	"2023-09-11": {
		weight: 78.2,
		bodyfatInPercent: 19.2,
		bodyfatInKg: 15,
		muscleInKg: 35.9
	},
	"2023-10-21": {
		weight: 78,
	},
	"2023-11-07": {
		weight: 78.4,
		bodyfatInPercent: 18.9,
		bodyfatInKg: 14.8,
		muscleInKg: 36.3
	},
	"2023-12-19": {
		weight: 77.4,
		bodyfatInPercent: 16.3,
		bodyfatInKg: 12.6,
		muscleInKg: 36.9
	},
	"2024-03-04": {
		weight: 76.2,
		bodyfatInPercent: 16.6,
		bodyfatInKg: 12.6,
		muscleInKg: 36.2
	},
	"2024-04-02": {
		weight: 76.3,
		bodyfatInPercent: 16.9,
		bodyfatInKg: 12.9,
		muscleInKg: 35.9
	},
	"2024-04-03": {
		weight: 75.5,
	},
	"2024-04-04": {
		weight: 75.4,
	},
	"2024-04-05": {
		weight: 74.6,
	},
	"2024-04-06": {
		weight: 74.4,
	},
	"2024-04-07": {
		weight: 75,
	},
	"2024-04-08": {
		weight: 75.1,
	},
	"2024-04-09": {
		weight: 75,
	},
	"2024-04-10": {
		weight: 75.4,
	},
	"2024-04-11": {
		weight: 74.6,
	},
	"2024-04-12": {
		weight: 74.8,
	},
	"2024-04-13": {
		weight: 75,
	},
	"2024-04-14": {
		weight: 74.5,
	},
	"2024-04-15": {
		weight: 74.3,
	},
	"2024-04-16": {
		weight: 74.8,
	},
	"2024-04-17": {
		weight: 74.4,
	},
	"2024-04-18": {
		weight: 74.9,
	},
	"2024-04-19": {
		weight: 74.4,
	},
	"2024-04-20": {
		weight: 74.4,
	},
	"2024-04-21": {
		weight: 75,
	},
	"2024-04-22": {
		weight: 75.2,
	},
	"2024-04-23": {
		weight: 75.2,
	},
	"2024-04-30": {
		weight: 76.7,
	},
	"2024-05-01": {
		weight: 76,
	},
	"2024-05-02": {
		weight: 76.3,
	},
	"2024-05-03": {
		weight: 75.5,
	},
	"2024-05-04": {
		weight: 76.1,
	},
	"2024-05-05": {
		weight: 74.5,
	},
	"2024-05-06": {
		weight: 74.6,
	},
	"2024-05-07": {
		weight: 75.7,
		bodyfatInPercent: 16.4,
		bodyfatInKg: 12.4,
		muscleInKg: 35.9
	},
	"2024-05-09": {
		weight: 74.5,
	},
	"2024-05-13": {
		weight: 75,
	},
	"2024-05-14": {
		weight: 74.7,
	},
	"2024-05-15": {
		weight: 75,
	},
	"2024-05-16": {
		weight: 74.5,
	},
	"2024-05-17": {
		weight: 74.4,
	},
	"2024-05-18": {
		weight: 74.1,
	},
	"2024-05-19": {
		weight: 74.5,
	},
	"2024-05-20": {
		weight: 75,
	},
	"2024-05-21": {
		weight: 75.1,
	},
	"2024-05-23": {
		weight: 74.2,
	},
	"2024-05-25": {
		weight: 74.8,
	},
	"2024-05-26": {
		weight: 74.7,
	},
	"2024-05-27": {
		weight: 74.1,
	},
	"2024-05-28": {
		weight: 74.4,
	},
	"2024-05-29": {
		weight: 74.4,
	},
	"2024-05-30": {
		weight: 74.1,
	},
	"2024-05-31": {
		weight: 75,
	},
	"2024-06-03": {
		weight: 74.1,
	},
	"2024-06-04": {
		weight: 73.8,
	},
	"2024-06-05": {
		weight: 74.3,
	},
	"2024-06-06": {
		weight: 74.2,
	},
	"2024-06-07": {
		weight: 74.1,
	},
	"2024-06-08": {
		weight: 74,
	},
	"2024-06-09": {
		weight: 74.5,
	},
	"2024-06-10": {
		weight: 74.8,
	},
	"2024-06-11": {
		weight: 74,
	},
	"2024-06-12": {
		weight: 73.9,
	},
	"2024-06-13": {
		weight: 73.5,
	},
	"2024-06-14": {
		weight: 73.7,
	},
	"2024-06-15": {
		weight: 74,
	},
	"2024-06-18": {
		weight: 75.7,
		bodyfatInPercent: 15.1,
		bodyfatInKg: 11.4,
		muscleInKg: 36.6
	},
	"2024-06-19": {
		weight: 74.5,
	},
	"2024-06-20": {
		weight: 73.7,
	},
	"2024-06-21": {
		weight: 74,
	},
	"2024-06-29": {
		weight: 76.8,
	},
	"2024-06-30": {
		weight: 75.6,
	},
	"2024-07-01": {
		weight: 75.7,
	},
	"2024-07-02": {
		weight: 75.4,
	},
	"2024-07-03": {
		weight: 75.7,
	},
	"2024-07-04": {
		weight: 75,
	},
	"2024-07-05": {
		weight: 75,
	},
	"2024-07-06": {
		weight: 75.1,
	},
	"2024-07-08": {
		weight: 75,
	},
	"2024-07-09": {
		weight: 74.2,
	},
	"2024-07-10": {
		weight: 74.6,
	},
	"2024-07-11": {
		weight: 74.2,
	},
	"2024-07-12": {
		weight: 74.5,
	},
	"2024-07-13": {
		weight: 73.9,
	},
	"2024-07-14": {
		weight: 74,
	},
	"2024-07-15": {
		weight: 74.6,
	},
	"2024-07-18": {
		weight: 74.3,
	},
	"2024-07-19": {
		weight: 74.3,
	},
	"2024-07-20": {
		weight: 74.4,
	},
	"2024-07-21": {
		weight: 75.3,
	},
	"2024-07-22": {
		weight: 75.2,
	},
	"2024-07-23": {
		weight: 74.4,
	},
	"2024-07-24": {
		weight: 74.1,
	},
	"2024-07-25": {
		weight: 74.3,
	},
	"2024-07-26": {
		weight: 73.7,
	},
	"2024-07-27": {
		weight: 74.2,
	},
	"2024-07-28": {
		weight: 74.4,
	},
	"2024-07-30": {
		weight: 74.3,
	},
	"2024-07-31": {
		weight: 74,
	},
	"2024-08-01": {
		weight: 73.9,
	},
	"2024-08-02": {
		weight: 74.2,
	},
	"2024-08-05": {
		weight: 75.3,
	},
	"2024-08-06": {
		weight: 74.8,
	},
	"2024-08-07": {
		weight: 74.1,
	},
	"2024-08-09": {
		weight: 74.1,
	},
	"2024-08-10": {
		weight: 74.1,
	},
	"2024-08-13": {
		weight: 73.8,
	},
	"2024-08-14": {
		weight: 74.2,
	},
	"2024-08-15": {
		weight: 73.8,
	},
	"2024-08-16": {
		weight: 73.2,
	},
	"2024-08-17": {
		weight: 74,
	},
	"2024-08-19": {
		weight: 74.5,
	},
	"2024-08-20": {
		weight: 74,
	},
	"2024-08-21": {
		weight: 74,
	},
	"2024-08-23": {
		weight: 73.6,
	},
	"2024-08-24": {
		weight: 73.4,
	},
	"2024-08-25": {
		weight: 73.7,
	},
	"2024-08-26": {
		weight: 73.7,
	},
	"2024-08-27": {
		weight: 73.4,
	},
	"2024-08-28": {
		weight: 73.1,
	},
	"2024-08-29": {
		weight: 73.3,
	},
	"2024-08-30": {
		weight: 73.8,
	},
	"2024-09-03": {
		weight: 74,
	},
	"2024-09-04": {
		weight: 73.5,
	},
	"2024-09-05": {
		weight: 73.1,
	},
	"2024-09-10": {
		weight: 73.4,
		bodyfatInPercent: 13.9,
		bodyfatInKg: 10.2,
		muscleInKg: 36.1
	},
	"2024-10-29": {
		weight: 75.2,
		bodyfatInPercent: 16.2,
		bodyfatInKg: 12.2,
		muscleInKg: 35.7
	},
	"2024-12-11": {
		weight: 74.6,
	},
	"2024-12-14": {
		weight: 75.2,
	},
	"2024-12-15": {
		weight: 75.1,
	},
	"2024-12-16": {
		weight: 75.2,
	},
	"2024-12-18": {
		weight: 74.2,
	},
	"2024-12-21": {
		weight: 74.2,
	},
}
