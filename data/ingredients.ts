export type Macro = number;

export type Cal = Macro;
export type Fat = Macro;
export type Saturated = Macro;
export type Carbs = Macro;
export type Sugar = Macro;
export type Protein = Macro;
export type Salt = Macro;

export type Name = string;

export interface Macros {
    cal: Cal;
    fat: Fat;
    saturated: Saturated;
    carbs: Carbs;
    sugar: Sugar;
    protein: Protein;
    salt: Salt;
}

export interface NamedMacros extends Macros {
    name: Name;
}

export type NamedMacrosMap = { [key: string]: NamedMacros; }

export type Ingredients = NamedMacrosMap;

export const ingredients: Ingredients = {
    "SeitenbacherDinoMuesli": {
        "name": "Seitenbacher Dino Müsli",
        "cal": 413,
        "fat": 11,
        "saturated": 4,
        "carbs": 65,
        "sugar": 28,
        "protein": 10,
        "salt": 0
    },
    "BeliefMandelMilch": {
        "name": "Belief Mandel Drink",
        "cal": 35,
        "fat": 3.3,
        "saturated": 0.3,
        "carbs": 0.5,
        "sugar": 0.5,
        "protein": 1.1,
        "salt": 0.14
    },
    "BeriefHafterMilch": {
        "name": "Belief Bio Hafter Glutenfrei",
        "cal": 46,
        "fat": 1.4,
        "saturated": 0.2,
        "carbs": 7.6,
        "sugar": 5.2,
        "protein": 0.7,
        "salt": 0.13
    },
	"ReweMagerQuark": {
        "name": "Rewe Mager Quark",
		"cal": 68,
		"fat": 0.2,
		"saturated": 0.1,
		"carbs": 4.1,
		"sugar": 4.1,
		"protein": 12,
		"salt": 0.1
	},
	"ReweFreiVonMagerQuark": {
        "name": "Rewe frei von Mager Quark",
		"cal": 70,
		"fat": 0.3,
		"saturated": 0.2,
		"carbs": 4.3,
		"sugar": 4.3,
		"protein": 12.5,
		"salt": 0.1
	},
	"BerchtesgadenerLandVollmilch": {
        "name": "BerchtesgadenerLand Vollmilch",
		"cal": 66,
		"fat": 3.6,
		"saturated": 2.4,
		"carbs": 5,
		"sugar": 5,
		"protein": 3.3,
		"salt": 0.11
	},
	"BerchtesgadenerLandFettarmemilch": {
        "name": "BerchtesgadenerLand Milch fettarm",
		"cal": 48,
		"fat": 1.6,
		"saturated": 1,
		"carbs": 5.1,
		"sugar": 5.1,
		"protein": 3.4,
		"salt": 0.12
	},
	"EdekaBioHVollmilch": {
        "name": "EDEKA Bio H-Vollmilch",
		"cal": 67,
		"fat": 3.8,
		"saturated": 2.4,
		"carbs": 4.9,
		"sugar": 4.9,
		"protein": 3.4,
		"salt": 0.13
	},
	"EdekaBioFettarmeHMilch": {
        "name": "EDEKA Bio Fettarme H-Milch",
		"cal": 48,
		"fat": 1.5,
		"saturated": 1,
		"carbs": 5,
		"sugar": 5,
		"protein": 3.5,
		"salt": 0.13
	},
	"SchwarzwaldmilchHaltbareProteinMilch": {
        "name": "Schwarzwaldmilch Haltbare Protein Milch",
		"cal": 58,
		"fat": 0.9,
		"saturated": 0.6,
		"carbs": 4.8,
		"sugar": 4.8,
		"protein": 7.5,
		"salt": 0.12
	},
	"SeitenbacherBioLeinsaat": {
        "name": "Seitenbacher Bio-Leinsaat",
		"cal": 473,
		"fat": 31,
		"saturated": 3,
		"carbs": 0,
		"sugar": 0,
		"protein": 29,
		"salt": 0
	},
	"BarillaFusilli": {
        "name": "Barilla Fusilli",
		"cal": 359,
		"fat": 2,
		"saturated": 0.5,
		"carbs": 71.2,
		"sugar": 3.5,
		"protein": 12.5,
		"salt": 0.013
	},
	"BarillaAlBronzoFusilli": {
        "name": "Barilla Al Bronzo Fusilli",
		"cal": 359,
		"fat": 2,
		"saturated": 0.4,
		"carbs": 69.7,
		"sugar": 3.5,
		"protein": 14,
		"salt": 0.013
	},
	"ReweBesteWahlSpiralen": {
        "name": "REWE Beste Wahl Spiralen",
		"cal": 365,
		"fat": 3.7,
		"saturated": 1.2,
		"carbs": 68,
		"sugar": 3,
		"protein": 15,
		"salt": 0.08
	},
	"jaPassierteTomaten": {
        "name": "ja Passierte Tomaten",
		"cal": 27,
		"fat": 0.1,
		"saturated": 0,
		"carbs": 3.8,
		"sugar": 3.8,
		"protein": 1.8,
		"salt": 0.3
	},
	"pomitoPassierteTomaten": {
        "name": "Pomito Passierte Tomaten",
		"cal": 27,
		"fat": 0.5,
		"saturated": 0,
		"carbs": 4,
		"sugar": 4,
		"protein": 1.3,
		"salt": 0.55
	},
	"muttiGeschaelteTomaten": {
        "name": "Mutti Geschälte Tomaten",
		"cal": 22,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 3.6,
		"sugar": 2.9,
		"protein": 1.1,
		"salt": 0
	},
	"OroDiParmaTomatenmark": {
        "name": "OroDiParma Tomatenmark",
		"cal": 113,
		"fat": 0.6,
		"saturated": 0.1,
		"carbs": 18,
		"sugar": 18,
		"protein": 5.9,
		"salt": 1
	},
	"FerreroRocher": {
        "name": "Ferrero Rocher",
		"cal": 603,
		"fat": 42.7,
		"saturated": 14.1,
		"carbs": 44.4,
		"sugar": 39.9,
		"protein": 8.2,
		"salt": 0
	},
	"FerreroDuplo": {
        "name": "Ferrero Duplo",
		"cal": 555,
		"fat": 33.5,
		"saturated": 19.1,
		"carbs": 56,
		"sugar": 50.4,
		"protein": 6.1,
		"salt": 0.2
	},
	"FerreroNutella": {
        "name": "Ferrero Nutella",
		"cal": 539,
		"fat": 30.9,
		"saturated": 10.6,
		"carbs": 57.5,
		"sugar": 56.3,
		"protein": 6.3,
		"salt": 0.1
	},
	"FerreroHanuta": {
        "name": "Ferrero Hanuta",
		"cal": 542,
		"fat": 31.9,
		"saturated": 13.7,
		"carbs": 54,
		"sugar": 42.6,
		"protein": 7.6,
		"salt": 0.432
	},
	"FerreroHanutaBrownieStyle": {
        "name": "Ferrero Hanuta Brownie Style",
		"cal": 512,
		"fat": 26.9,
		"saturated": 14.2,
		"carbs": 55.7,
		"sugar": 42.7,
		"protein": 9.2,
		"salt": 0.405
	},
	"GoertzHerzschlag": {
        "name": "Goertz Herzschlag",
		"cal": 252,
		"fat": 3.2,
		"saturated": 0.5,
		"carbs": 45,
		"sugar": 0.6,
		"protein": 8.5,
		"salt": 1.2
	},
	"GoertzDinkel": {
        "name": "Goertz Dinkel",
		"cal": 282,
		"fat": 12,
		"saturated": 1.5,
		"carbs": 32,
		"sugar": 2.2,
		"protein": 8.4,
		"salt": 1.1
	},
	"GoertzKornkraft": {
        "name": "Goertz Kornkraft",
		"cal": 267,
		"fat": 10,
		"saturated": 1.5,
		"carbs": 31,
		"sugar": 1.3,
		"protein": 9.3,
		"salt": 1.2
	},
	"GoertzLaugenbroetchen": {
        "name": "Görtz Laugenbrötchen",
		"cal": 261,
		"fat": 3.5,
		"saturated": 0.5,
		"carbs": 47,
		"sugar": 1.3,
		"protein": 7.6,
		"salt": 4.7
	},
	"ReweSauerKirschNektar": {
        "name": "Rewe Sauerkirsch Nektar",
		"cal": 54,
		"fat": 0,
		"saturated": 0,
		"carbs": 12.3,
		"sugar": 11.8,
		"protein": 0,
		"salt": 0
	},
	"Ei": {
        "name": "Ei",
		"cal": 143,
		"fat": 9.5,
		"saturated": 3.1,
		"carbs": 0,
		"sugar": 0.4,
		"protein": 13,
		"salt": 0.142
	},
	"Banane": {
        "name": "Banane",
		"cal": 89,
		"fat": 0.33,
		"saturated": 0.112,
		"carbs": 22.84,
		"sugar": 12.23,
		"protein": 1,
		"salt": 0
	},
	"Olivenoel": {
        "name": "Olivenöl",
		"cal": 900,
		"fat": 100,
		"saturated": 14,
		"carbs": 0,
		"sugar": 0,
		"protein": 0,
		"salt": 0
	},
	"MazolaKeimOel": {
        "name": "Mazola Keim Öl",
		"cal": 828,
		"fat": 92,
		"saturated": 13,
		"carbs": 0,
		"sugar": 0,
		"protein": 0,
		"salt": 0
	},
	"KerrygoldButter": {
        "name": "Kerrygold Butter",
		"cal": 748,
		"fat": 82.5,
		"saturated": 56,
		"carbs": 0.7,
		"sugar": 0.7,
		"protein": 0.7,
		"salt": 0.02
	},
	"AltmeisterKlassikEssig": {
        "name": "Altmeister Klassik Essig",
		"cal": 161,
		"fat": 0,
		"saturated": 0,
		"carbs": 0,
		"sugar": 0,
		"protein": 0,
		"salt": 0.2
	},
	"Maggi": {
        "name": "Maggi",
		"cal": 116,
		"fat": 0,
		"saturated": 0,
		"carbs": 9.3,
		"sugar": 6.2,
		"protein": 18.6,
		"salt": 24.1
	},
	"PatrosNatur": {
        "name": "Patros Natur",
		"cal": 279,
		"fat": 24,
		"saturated": 16,
		"carbs": 0.7,
		"sugar": 0.7,
		"protein": 15,
		"salt": 3
	},
	"PatrosFeta": {
        "name": "Patros Feta",
		"cal": 287,
		"fat": 24,
		"saturated": 17,
		"carbs": 0.5,
		"sugar": 0.5,
		"protein": 18,
		"salt": 2.4
	},
	"Tomate": {
        "name": "Tomate",
		"cal": 18,
		"fat": 0.2,
		"saturated": 0,
		"carbs": 3.9,
		"sugar": 2.6,
		"protein": 0.9,
		"salt": 0
	},
	"Gurke": {
        "name": "Gurke",
		"cal": 16,
		"fat": 0.11,
		"saturated": 0,
		"carbs": 3.63,
		"sugar": 1.67,
		"protein": 0.65,
		"salt": 0
	},
	"Zwiebel": {
        "name": "Zwiebel",
		"cal": 40,
		"fat": 0.1,
		"saturated": 0,
		"carbs": 9.34,
		"sugar": 4.24,
		"protein": 1.1,
		"salt": 0
	},
	"TkMango": {
        "name": "TK Mango",
		"cal": 60,
		"fat": 0.1,
		"saturated": 0.1,
		"carbs": 14.2,
		"sugar": 11.3,
		"protein": 0.5,
		"salt": 0.1
	},
	"TkHeidelbeeren": {
        "name": "TK Heidelbeeren",
		"cal": 59,
		"fat": 0.6,
		"saturated": 0.1,
		"carbs": 8.7,
		"sugar": 8.6,
		"protein": 1.3,
		"salt": 0.01
	},
	"BioLeberwurst": {
        "name": "Bio-Leberwurst",
		"cal": 277,
		"fat": 24.9,
		"saturated": 10.4,
		"carbs": 1.2,
		"sugar": 0.8,
		"protein": 12.1,
		"salt": 1.8
	},
	"DemeterLeberwurst": {
        "name": "Demeter Leberwurst",
		"cal": 399,
		"fat": 38.4,
		"saturated": 16.4,
		"carbs": 1.3,
		"sugar": 0.9,
		"protein": 12,
		"salt": 2
	},
	"FeineLeberwurst": {
        "name": "Feine Leberwurst",
		"cal": 335,
		"fat": 31,
		"saturated": 12.7,
		"carbs": 1,
		"sugar": 1,
		"protein": 13,
		"salt": 2.1
	},
	"Cornichons": {
        "name": "Cornichons",
		"cal": 22,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 2.9,
		"sugar": 2.8,
		"protein": 1.2,
		"salt": 1.3
	},
	"Honig": {
        "name": "Honig",
		"cal": 305,
		"fat": 0.5,
		"saturated": 0,
		"carbs": 72,
		"sugar": 10.6,
		"protein": 1.27,
		"salt": 0
	},
	"FrischeSchlagsahne": {
        "name": "Frische Schlagsahne",
		"cal": 292,
		"fat": 30,
		"saturated": 20.7,
		"carbs": 3.2,
		"sugar": 3.2,
		"protein": 2.4,
		"salt": 0
	},
	"CocaCola": {
        "name": "CocaCola",
		"cal": 42,
		"fat": 0,
		"saturated": 0,
		"carbs": 10.6,
		"sugar": 10.6,
		"protein": 0,
		"salt": 0
	},
	"BioBierschinken": {
        "name": "Bio-Bierschinken",
		"cal": 185,
		"fat": 12.3,
		"saturated": 5.2,
		"carbs": 0.7,
		"sugar": 0.5,
		"protein": 17.9,
		"salt": 2
	},
	"BioBierwurst": {
        "name": "Bio-Bierwurst",
		"cal": 184,
		"fat": 13.6,
		"saturated": 6,
		"carbs": 1,
		"sugar": 0.5,
		"protein": 14.4,
		"salt": 2.1
	},
	"BioLyoner": {
        "name": "Bio-Lyoner",
		"cal": 284,
		"fat": 26.3,
		"saturated": 11.5,
		"carbs": 0.7,
		"sugar": 0.5,
		"protein": 11.2,
		"salt": 2.2
	},
	"LandwurstBratwurstGrob": {
        "name": "Landwurst Bratwurst Grob",
		"cal": 252,
		"fat": 20,
		"saturated": 8.7,
		"carbs": 0.5,
		"sugar": 0.5,
		"protein": 18,
		"salt": 2.2
	},
	"Marmelade": {
        "name": "Marmelade",
		"cal": 280,
		"fat": 0.1,
		"saturated": 0,
		"carbs": 70,
		"sugar": 48.5,
		"protein": 0.4,
		"salt": 0
	},
	"SeitenbacherBioChiasamen": {
        "name": "Seitenbacher Bio-Chiasamen",
		"cal": 475,
		"fat": 33,
		"saturated": 4,
		"carbs": 7,
		"sugar": 1,
		"protein": 24,
		"salt": 0
	},
	"SeitenbacherProteinPorridge": {
        "name": "Seitenbacher Protein Porridge",
		"cal": 372,
		"fat": 6,
		"saturated": 1,
		"carbs": 48,
		"sugar": 10,
		"protein": 28,
		"salt": 0.1
	},
	"SeitenbacherProteinPorridgeSchokolade": {
        "name": "Seitenbacher Protein Porridge Schokolade",
		"cal": 389,
		"fat": 7,
		"saturated": 2,
		"carbs": 49,
		"sugar": 19,
		"protein": 30,
		"salt": 0.2
	},
	"TkHimbeeren": {
        "name": "Tk Himbeeren",
		"cal": 49,
		"fat": 0.8,
		"saturated": 0.1,
		"carbs": 7.3,
		"sugar": 6,
		"protein": 1.1,
		"salt": 0.1
	},
	"BioRindfleisch": {
        "name": "Bio-Rindfleisch",
		"cal": 137,
		"fat": 6.1,
		"saturated": 2.6,
		"carbs": 1,
		"sugar": 0.5,
		"protein": 19.4,
		"salt": 1.2
	},
	"BioLammbratwurstMitRindfleisch": {
        "name": "Bio-Lammbratwurst mit Rindfleisch",
		"cal": 186,
		"fat": 11.5,
		"saturated": 6.4,
		"carbs": 1,
		"sugar": 0.6,
		"protein": 19.2,
		"salt": 2
	},
	"BioRindswurst": {
        "name": "Bio-Rindswurst",
		"cal": 191,
		"fat": 14.2,
		"saturated": 7.2,
		"carbs": 0.5,
		"sugar": 0.5,
		"protein": 15.4,
		"salt": 2.2
	},
	"BioBratwurstGrob": {
        "name": "Bio-Bratwurst, grob",
		"cal": 255,
		"fat": 20.7,
		"saturated": 9.4,
		"carbs": 0.5,
		"sugar": 0.5,
		"protein": 17.2,
		"salt": 2.2
	},
	"Lammbratwurst": {
        "name": "Lammbratwurst",
		"cal": 177,
		"fat": 10.7,
		"saturated": 5.8,
		"carbs": 1,
		"sugar": 0.6,
		"protein": 19.2,
		"salt": 2
	},
	"ReweBioRinderHackfleisch": {
        "name": "REWE Bio Rinder-Hackfleisch",
		"cal": 165,
		"fat": 11,
		"saturated": 5.2,
		"carbs": 0,
		"sugar": 0,
		"protein": 19,
		"salt": 0.2
	},
	"ReweBioHackfleischGemischt": {
        "name": "REWE Bio Hackfleisch Gemischt",
		"cal": 244,
		"fat": 18,
		"saturated": 7.2,
		"carbs": 0.5,
		"sugar": 0.3,
		"protein": 20,
		"salt": 0
	},
	"WagnerPiccolinis": {
        "name": "Wagner Piccolinis",
		"cal": 264,
		"fat": 11.8,
		"saturated": 4.6,
		"carbs": 27.6,
		"sugar": 3.3,
		"protein": 10.8,
		"salt": 1.4
	},
	"KelloggsFrootLoops": {
        "name": "Kelloggs FrootLoops",
		"cal": 385,
		"fat": 3,
		"saturated": 0.9,
		"carbs": 79,
		"sugar": 25,
		"protein": 8.3,
		"salt": 1.13
	},
	"BiolandFrischmilch": {
        "name": "Bioland Frischmilch 3,8%",
		"cal": 67,
		"fat": 3.8,
		"saturated": 2.5,
		"carbs": 4.8,
		"sugar": 4.8,
		"protein": 3.3,
		"salt": 0.13
	},
	"MagnumDoubleChocolate": {
        "name": "Magnum Double Chocolate",
		"cal": 324,
		"fat": 22,
		"saturated": 15,
		"carbs": 25,
		"sugar": 22,
		"protein": 4.4,
		"salt": 0.1
	},
	"MagnumMandel": {
        "name": "Magnum Mandel",
		"cal": 332,
		"fat": 21,
		"saturated": 13,
		"carbs": 30,
		"sugar": 29,
		"protein": 5,
		"salt": 0.1
	},
	"MagnumCollectionChocolateCrunchyCookies": {
        "name": "Magnum Collection Chocolate & Crunchy Cookies",
		"cal": 253,
		"fat": 15,
		"saturated": 9.6,
		"carbs": 26,
		"sugar": 20,
		"protein": 2.8,
		"salt": 0.12
	},
	"MoevenpickWalnussWaffel": {
        "name": "Mövenpick Walnuss Waffeleis",
		"cal": 323,
		"fat": 18,
		"saturated": 10,
		"carbs": 35,
		"sugar": 25,
		"protein": 3.9,
		"salt": 0.1
	},
	"MoevenpickVanilleEis": {
        "name": "Mövenpick Bourbon-Vanille Eis",
		"cal": 208,
		"fat": 11,
		"saturated": 9.5,
		"carbs": 26,
		"sugar": 22,
		"protein": 1.8,
		"salt": 0.12
	},
	"Karotten": {
        "name": "Karotten",
		"cal": 41,
		"fat": 0.24,
		"saturated": 0,
		"carbs": 9.6,
		"sugar": 4.7,
		"protein": 0.93,
		"salt": 0
	},
	"Kartoffel": {
        "name": "Kartoffel",
		"cal": 77,
		"fat": 0.1,
		"saturated": 0,
		"carbs": 17,
		"sugar": 0.8,
		"protein": 2,
		"salt": 0
	},
	"BioSchinkenwurst": {
        "name": "Bio-Schinkenwurst",
		"cal": 275,
		"fat": 24.6,
		"saturated": 10.8,
		"carbs": 0.9,
		"sugar": 0.5,
		"protein": 12.6,
		"salt": 2.1
	},
	"jaMandeln": {
        "name": "ja! Mandeln",
		"cal": 612,
		"fat": 51.1,
		"saturated": 4,
		"carbs": 4.5,
		"sugar": 4.5,
		"protein": 29.1,
		"salt": 0
	},
	"FrostaHaehnchenCurry": {
        "name": "Frosta Hähnchen Curry",
		"cal": 104,
		"fat": 2.5,
		"saturated": 0.6,
		"carbs": 14.4,
		"sugar": 3.4,
		"protein": 5.5,
		"salt": 0.8
	},
	"FrostaMexicanStyleChicken": {
        "name": "Frosta Mexican Style Chicken",
		"cal": 97,
		"fat": 1.4,
		"saturated": 0.2,
		"carbs": 14.6,
		"sugar": 2.1,
		"protein": 5.7,
		"salt": 0.93
	},
	"FrostaGemuesePfanneCurryKokos": {
        "name": "Frosta Gemüsepfanne Curry Kokos",
		"cal": 44,
		"fat": 1.8,
		"saturated": 1.5,
		"carbs": 4.7,
		"sugar": 3.6,
		"protein": 1.3,
		"salt": 0.86
	},
	"FrostaGemuesePfanneAsiaCurry": {
        "name": "Frosta Gemüsepfanne Asia Curry",
		"cal": 57,
		"fat": 2.4,
		"saturated": 1.3,
		"carbs": 5.8,
		"sugar": 4.3,
		"protein": 2.0,
		"salt": 0.85
	},
	"FrostaGemuesePfanneSommergarten": {
        "name": "Frosta Gemüsepfanne Sommergarten",
		"cal": 61,
		"fat": 3,
		"saturated": 1.9,
		"carbs": 5.2,
		"sugar": 3.3,
		"protein": 1.7,
		"salt": 0.98
	},
	"FrostaGemuesePfanneProvence": {
        "name": "Frosta Gemüsepfanne Provence",
		"cal": 50,
		"fat": 1.5,
		"saturated": 0.3,
		"carbs": 6.3,
		"sugar": 3.6,
		"protein": 1.7,
		"salt": 0.81
	},
	"FrostaGemuesePfanneMediterranea": {
        "name": "Frosta Gemüsepfanne Mediterranea",
		"cal": 43,
		"fat": 0.6,
		"saturated": 0.1,
		"carbs": 6.9,
		"sugar": 3.5,
		"protein": 1.6,
		"salt": 0.68
	},
	"FrostaGemuesePfanneToscana": {
        "name": "Frosta Gemüsepfanne Toscana",
		"cal": 41,
		"fat": 1.9,
		"saturated": 0.3,
		"carbs": 3.3,
		"sugar": 3,
		"protein": 1.5,
		"salt": 0.97
	},
	"LindtHelloCrunchyNougat": {
        "name": "Lindt Hello Crunchy Nougat",
		"cal": 556,
		"fat": 33,
		"saturated": 16,
		"carbs": 55,
		"sugar": 51,
		"protein": 8.3,
		"salt": 0.14
	},
	"ReweBesteWahlSchlagsahne": {
        "name": "REWE Beste Wahl Schlagsahne",
		"cal": 311,
		"fat": 32,
		"saturated": 20.2,
		"carbs": 3.2,
		"sugar": 3.2,
		"protein": 2.5,
		"salt": 0
	},
	"LacSchlagsahne": {
        "name": "LAC Schlagsahne",
		"cal": 311,
		"fat": 32,
		"saturated": 20.2,
		"carbs": 3.3,
		"sugar": 3.3,
		"protein": 2.4,
		"salt": 0.08
	},
	"FinelloPastakaese": {
        "name": "Finello Pastakäse",
		"cal": 402,
		"fat": 32,
		"saturated": 20,
		"carbs": 2.4,
		"sugar": 0.4,
		"protein": 24,
		"salt": 2.3
	},
	"FinelloGratinkaese": {
        "name": "Finello Gratinkäse",
		"cal": 314,
		"fat": 23,
		"saturated": 14,
		"carbs": 2.8,
		"sugar": 0.6,
		"protein": 24,
		"salt": 1.5
	},
	"GutUndGuenstigTkBroccoli": {
        "name": "Gut&Günstig Tk Broccoli",
		"cal": 38,
		"fat": 0.9,
		"saturated": 0.2,
		"carbs": 1.7,
		"sugar": 1.4,
		"protein": 4.4,
		"salt": 0
	},
	"jaTkBroccoli": {
        "name": "JA! Tk Broccoli",
		"cal": 40,
		"fat": 0.6,
		"saturated": 0.2,
		"carbs": 3.2,
		"sugar": 1.9,
		"protein": 4.3,
		"salt": 0.02
	},
	"cnTkBroccoli": {
        "name": "cn congelados de navarra Tk Broccoli",
		"cal": 34,
		"fat": 0,
		"saturated": 0,
		"carbs": 3.5,
		"sugar": 1.5,
		"protein": 3,
		"salt": 0
	},
	"cnTkKaisergemuese": {
        "name": "Tk Kaisergemüse",
		"cal": 36,
		"fat": 0.5,
		"saturated": 0,
		"carbs": 5,
		"sugar": 1.8,
		"protein": 1.6,
		"salt": 0.07
	},
	"Broccoli": {
        "name": "Broccoli", // https://en.wikipedia.org/wiki/Broccoli
		"cal": 34,
		"fat": 0.37,
		"saturated": 0,
		"carbs": 6.64,
		"sugar": 1.7,
		"protein": 2.82,
		"salt": 0.07
	},
	"jaTkKaisergemuese": {
        "name": "ja Kaisergemüse",
		"cal": 39,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 5.1,
		"sugar": 4,
		"protein": 2.5,
		"salt": 0
	},
	"Rindfleisch": {
        "name": "Rindfleisch",
		"cal": 250,
		"fat": 15,
		"saturated": 6,
		"carbs": 0,
		"sugar": 0,
		"protein": 26,
		"salt": 0
	},
	"Cappuccino": {
        "name": "Cappuccino",
		"cal": 31,
		"fat": 1.7,
		"saturated": 1.5,
		"carbs": 2.5,
		"sugar": 2.3,
		"protein": 1.7,
		"salt": 0
	},
	"Zucker": {
        "name": "Zucker",
		"cal": 387,
		"fat": 0,
		"saturated": 0,
		"carbs": 100,
		"sugar": 100,
		"protein": 0,
		"salt": 0
	},
	"BadLiebenzellerApfelsaftSchorle": {
        "name": "Bad Liebenzeller Apfelsaft Schorle Naturtrüb",
		"cal": 30,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 7.7,
		"sugar": 6.1,
		"protein": 0.1,
		"salt": 0.1
	},
	"SubwayChickenTeriyaki": {
		"name": "Subway Chicken Teriyaki",
		// taken from https://www.subway.com/en-US/MenuNutrition/Menu/Product?ProductId=4258&MenuCategoryId=1
		// Bread: Italian; Cheese: Swiss; Veggies: Red Onions, Lettuce, Green Peppers, Cucumbers; Sauces: Sweet Onion Sauce
		"cal": (710/430)*100,
		"fat": (17/430)*100,
		"saturated": (7/430)*100,
		"carbs": (89/430)*100,
		"sugar": (21/430)*100,
		"protein": (54/430)*100,
		"salt": (1.2/430)*100
	},
	"SeitenbacherProteinRiegelVanille": {
        "name": "Seitenbacher Protein-Riegel Vanille",
		"cal": 359,
		"fat": 13,
		"saturated": 4,
		"carbs": 23,
		"sugar": 21,
		"protein": 27,
		"salt": 0.1
	},
	"SeitenbacherProteinRiegelCappuccino": {
        "name": "Seitenbacher Protein-Riegel Cappuccino",
		"cal": 362,
		"fat": 14,
		"saturated": 4,
		"carbs": 19,
		"sugar": 14,
		"protein": 27,
		"salt": 0.1
	},
	"SeitenbacherProteinRiegelSchoko": {
        "name": "Seitenbacher Protein-Riegel Schoko",
		"cal": 350,
		"fat": 12,
		"saturated": 5,
		"carbs": 24,
		"sugar": 23,
		"protein": 26,
		"salt": 0.1
	},
	"SeitenbacherProteinRiegelKakao": {
        "name": "Seitenbacher Protein-Riegel Kakao",
		"cal": 302,
		"fat": 8,
		"saturated": 1,
		"carbs": 12,
		"sugar": 7,
		"protein": 30,
		"salt": 0.1
	},
	"SeitenbacherEnergieRiegel": {
        "name": "Seitenbacher Energize",
		"cal": 434,
		"fat": 19,
		"saturated": 8,
		"carbs": 47,
		"sugar": 44,
		"protein": 12,
		"salt": 0.1
	},
	"Vodka": {
        "name": "Vodka",
		"cal": 200,
		"fat": 0,
		"saturated": 0,
		"carbs": 0,
		"sugar": 0,
		"protein": 0,
		"salt": 0
	},
	"Kahlua": {
        "name": "Kahlua",
		"cal": 303,
		"fat": 0,
		"saturated": 0,
		"carbs": 49,
		"sugar": 49,
		"protein": 0,
		"salt": 0
	},
	"MuffinMarenKirschSchoko": {
        "name": "Marens Kirsch-Schoko-Muffin",
		"cal": 204,
		"fat": 8.2,
		"saturated": 2,
		"carbs": 29,
		"sugar": 15,
		"protein": 3.3,
		"salt": 0
	},
	"LandliebeVollmilchSchokoladenEis": {
        "name": "Landliebe Vollmilch Schokoladen Eis",
		"cal": 235,
		"fat": 12,
		"saturated": 8.2,
		"carbs": 28,
		"sugar": 25,
		"protein": 3,
		"salt": 0.1
	},
	"GelatelliEiswaffel": {
        "name": "Gelatelli Eiswaffel",
		"cal": 163,
		"fat": 11.4,
		"saturated": 7.3,
		"carbs": 13.5,
		"sugar": 11.4,
		"protein": 1.8,
		"salt": 0
	},
	"AirPoppedPopcorn": {
        "name": "Generic Air Popped Popcorn",
		"cal": 387,
		"fat": 4.5,
		"saturated": 0.6,
		"carbs": 77.8,
		"sugar": 0.9,
		"protein": 12.9,
		"salt": 0.1
	},
	"MuellermilchProteinVanille": {
        "name": "Müllermilch Protein Vanille",
		"cal": 79,
		"fat": 1.4,
		"saturated": 1,
		"carbs": 10,
		"sugar": 10,
		"protein": 6.6,
		"salt": 0.13
	},
	"MuellermilchProteinSchoko": {
        "name": "Müllermilch Protein Schoko",
		"cal": 84,
		"fat": 1.6,
		"saturated": 1.1,
		"carbs": 10.7,
		"sugar": 10.2,
		"protein": 6.5,
		"salt": 0.14
	},
	"MuellermilchProteinBanane": {
        "name": "Müllermilch Protein Banane",
		"cal": 78,
		"fat": 1.4,
		"saturated": 1,
		"carbs": 9.9,
		"sugar": 9.9,
		"protein": 6.5,
		"salt": 0.13
	},
	"CaffeFreddoCappuccino": {
        "name": "Caffè Freddo Cappuccino",
		"cal": 66,
		"fat": 1.5,
		"saturated": 1,
		"carbs": 9.4,
		"sugar": 8.9,
		"protein": 3.6,
		"salt": 0.1
	},
	"EmmiCaffeLatteHighProtein": {
        "name": "Emmi Caffè Latte High Protein",
		"cal": 55,
		"fat": 1.1,
		"saturated": 0.6,
		"carbs": 3.7,
		"sugar": 3.2,
		"protein": 7.7,
		"salt": 0.07
	},
	"Kiwi": {
        "name": "Kiwi",
		"cal": 61,
		"fat": 1,
		"saturated": 1.1,
		"carbs": 15,
		"sugar": 9,
		"protein": 1,
		"salt": 0
	},
	"Pfirsich": {
        "name": "Pfirsich",
		"cal": 39,
		"fat": 0,
		"saturated": 0,
		"carbs": 9.5,
		"sugar": 8.4,
		"protein": 0.9,
		"salt": 0
	},
	"Erdbeere": {
        "name": "Erdbeere",
		"cal": 32,
		"fat": 0.3,
		"saturated": 0,
		"carbs": 7.6,
		"sugar": 4.8,
		"protein": 0.7,
		"salt": 0.001
	},
	"ActivePro80Chocolate": {
        "name": "Active Pro80 Schokolade",
		"cal": 362,
		"fat": 1.8,
		"saturated": 1.1,
		"carbs": 5,
		"sugar": 1,
		"protein": 80,
		"salt": 0.24
	},
	"Haehnchenbrust": {
        "name": "Haehnchenbrust",
		"cal": 165,
		"fat": 3.6,
		"saturated": 1,
		"carbs": 0,
		"sugar": 0,
		"protein": 31,
		"salt": 0
	},
	"Putenschnitzel": {
        "name": "Putenschnitzel",
		"cal": 133,
		"fat": 5,
		"saturated": 1,
		"carbs": 0,
		"sugar": 0,
		"protein": 22,
		"salt": 0
	},
	"Paprika": {
        "name": "Paprika",
		"cal": 289,
		"fat": 12.95,
		"saturated": 2.1,
		"carbs": 55.74,
		"sugar": 10.34,
		"protein": 14.76,
		"salt": 0
	},
	"Brokkoli": {
        "name": "Brokkoli",
		"cal": 34,
		"fat": 0.4,
		"saturated": 0,
		"carbs": 7,
		"sugar": 1.7,
		"protein": 2.8,
		"salt": 0.033
	},
	"Bohnen": {
        "name": "Bohnen",
		"cal": 31,
		"fat": 0.1,
		"saturated": 0,
		"carbs": 7.13,
		"sugar": 1.4,
		"protein": 1.82,
		"salt": 0
	},
	"JasminReis": {
        "name": "Oryza Jasmin Reis",
		"cal": 349,
		"fat": 1,
		"saturated": 0.3,
		"carbs": 76,
		"sugar": 0.5,
		"protein": 7.5,
		"salt": 0
	},
	"SojaSauce": {
        "name": "Bamboo Garden Bio Soja Sauce",
		"cal": 74,
		"fat": 0.7,
		"saturated": 0.1,
		"carbs": 7,
		"sugar": 0.5,
		"protein": 9.4,
		"salt": 15.5
	},
	"fairtradeTeriyaki": {
        "name": "Fairtrade Japanische Teriyaki",
		"cal": 176,
		"fat": 0.1,
		"saturated": 0,
		"carbs": 42,
		"sugar": 38,
		"protein": 1.7,
		"salt": 4.9
	},
	"Ingwer": {
        "name": "Ingwer",
		"cal": 80,
		"fat": 0.75,
		"saturated": 0.2,
		"carbs": 17.77,
		"sugar": 1.7,
		"protein": 1.82,
		"salt": 0
	},
	"ChickenCaesarSalad": {
        "name": "Chicken Caesar Salad",
		"cal": 125,
		"fat": 4.8,
		"saturated": 1.5,
		"carbs": 9,
		"sugar": 0,
		"protein": 12,
		"salt": 0
	},
	"SushiboxAyami": {
        "name": "Sushi-Box AYAMI",
		"cal": 160,
		"fat": 3.5,
		"saturated": 0.4,
		"carbs": 27,
		"sugar": 4.4,
		"protein": 4.6,
		"salt": 1.4
	},
	"SushiboxAyaka": {
        "name": "Sushi-Box AYAKA",
		"cal": 153,
		"fat": 3.6,
		"saturated": 0.5,
		"carbs": 24.3,
		"sugar": 3.8,
		"protein": 5.3,
		"salt": 1.2
	},
	"Garnelen": {
        "name": "Garnelen",
		"cal": 119,
		"fat": 1.5,
		"saturated": 0.5,
		"carbs": 1.5,
		"sugar": 0,
		"protein": 23,
		"salt": 2.6
	},
	"RiesenGarnelen": {
        "name": "Riesen-Garnelen (Deutsche See)",
		"cal": 84,
		"fat": 1.7,
		"saturated": 0.6,
		"carbs": 0,
		"sugar": 0,
		"protein": 17.1,
		"salt": 0.9
	},
	"Spinat": {
        "name": "Spinat",
		"cal": 23,
		"fat": 0.4,
		"saturated": 0.1,
		"carbs": 3.63,
		"sugar": 0.42,
		"protein": 2.86,
		"salt": 0.1
	},
	"Parmesan": {
        "name": "Parmesan",
		"cal": 402,
		"fat": 30,
		"saturated": 20,
		"carbs": 0,
		"sugar": 0,
		"protein": 32,
		"salt": 1.6
	},
	"GranaPadano": {
        "name": "Grana Padano Grattugiato Fresco",
		"cal": 398,
		"fat": 29,
		"saturated": 18,
		"carbs": 0,
		"sugar": 0,
		"protein": 33,
		"salt": 1.5
	},
	"ParmigianoReggiano": {
        "name": "Parmigiano Reggiano Grattugiato Fresco",
		"cal": 402,
		"fat": 30,
		"saturated": 20,
		"carbs": 0,
		"sugar": 0,
		"protein": 32,
		"salt": 1.6
	},
	"BioRinderFond": {
        "name": "BIO Rinder Fond",
		"cal": 6,
		"fat": 0.1,
		"saturated": 0,
		"carbs": 0.7,
		"sugar": 0.6,
		"protein": 0.5,
		"salt": 1.1
	},
	"BalsamicoEssig": {
        "name": "Hengstenberg Aceto Balsamico di Modena",
		"cal": 104,
		"fat": 0,
		"saturated": 0,
		"carbs": 21,
		"sugar": 20,
		"protein": 0.5,
		"salt": 0.1
	},
	"Apfel": {
        "name": "Apfel",
		"cal": 52,
		"fat": 0,
		"saturated": 0,
		"carbs": 14,
		"sugar": 10,
		"protein": 0.3,
		"salt": 0
	},
	"MaggiKartoffelGratin": {
		"name": "Maggi Kartoffel Gratin",
		"cal": 450,
		"fat": 27.7,
		"saturated": 2.6,
		"carbs": 44,
		"sugar": 7.1,
		"protein": 5,
		"salt": 15.5
	},
	"JaGratinKaese": {
		"name": "ja! Gratin Käse",
		"cal": 339,
		"fat": 26.3,
		"saturated": 17.6,
		"carbs": 2.4,
		"sugar": 0.5,
		"protein": 23.2,
		"salt": 1.6
	},
	"MinusLEmmentaler": {
		"name": "MinusL Emmentaler",
		"cal": 364,
		"fat": 28,
		"saturated": 21,
		"carbs": 0.1,
		"sugar": 0.1,
		"protein": 28,
		"salt": 1
	},
	"fairtradeKokosmilch": {
		"name": "fairtrade Original Kokosmilch",
		"cal": 162,
		"fat": 17,
		"saturated": 16,
		"carbs": 1.6,
		"sugar": 1,
		"protein": 0.2,
		"salt": 0.05
	},
	"bonduelleKirchererbsen": {
		"name": "Bonduelle Kircherbsen",
		"cal": 120,
		"fat": 2.2,
		"saturated": 0.2,
		"carbs": 15,
		"sugar": 0.5,
		"protein": 6.4,
		"salt": 0.3
	},
	"reweGrueneCurryPaste": {
		"name": "REWE Beste Wahl Grüne Curry Paste",
		"cal": 96,
		"fat": 0.4,
		"saturated": 0.1,
		"carbs": 19.7,
		"sugar": 9.1,
		"protein": 2.3,
		"salt": 12
	},
	"arlaLaktoseFrei": {
		"name": "Arla Laktose Frei",
		"cal": 38,
		"fat": 1.5,
		"saturated": 1,
		"carbs": 2.6,
		"sugar": 2.6,
		"protein": 3.4,
		"salt": 0.07
	},
	"kinderRiegel": {
		"name": "Kinder Riegel",
		"cal": 566,
		"fat": 35,
		"saturated": 22.6,
		"carbs": 53.5,
		"sugar": 53.3,
		"protein": 8.7,
		"salt": 0.313
	},
	"bodyAttackProteinPancakeVanilla": {
		"name": "Body Attack Protein Pancake",
		"cal": 353,
		"fat": 3,
		"saturated": 1.4,
		"carbs": 43.9,
		"sugar": 4.1,
		"protein": 35,
		"salt": 1.6
	},
	"bodyAttackWheyDeluxe": {
		"name": "Body Attack Whey Deluxe",
		"cal": 401,
		"fat": 4.4,
		"saturated": 2.7,
		"carbs": 6.4,
		"sugar": 4.1,
		"protein": 84,
		"salt": 0.79
	},
	"bodyAttackIsoWhey": {
		"name": "Body Attack Iso Deluxe",
		"cal": 381,
		"fat": 1.1,
		"saturated": 0.5,
		"carbs": 3.1,
		"sugar": 2,
		"protein": 89.4,
		"salt": 0.72
	},
	"Apfelbrei": {
		"name": "Apfelbrei",
		"cal": 43,
		"fat": 0.05,
		"saturated": 0.008,
		"carbs": 11.29,
		"sugar": 10.09,
		"protein": 0.17,
		"salt": 0.002
	},
	"SahneMinusL": {
		"name": "Sahne Minus L",
		"cal": 319,
		"fat": 33,
		"saturated": 21.7,
		"carbs": 3.1,
		"sugar": 3.1,
		"protein": 2.4,
		"salt": 0.1
	},
	"TressBandnudeln": {
		"name": "Tres Bandnudeln",
		"cal": 373,
		"fat": 3.3,
		"saturated": 0.8,
		"carbs": 68.6,
		"sugar": 3.3,
		"protein": 15.3,
		"salt": 0.1
	},
	"Avocado": {
		"name": "Avocado",
		"cal": 161,
		"fat": 14.5,
		"saturated": 2.1,
		"carbs": 8.5,
		"sugar": 0.7,
		"protein": 2,
		"salt": 0
	},
	"StarbucksCappuccino": {
		"name": "Starbucks Capuccino",
		"cal": 69,
		"fat": 2.5,
		"saturated": 1.6,
		"carbs": 8.8,
		"sugar": 8.5,
		"protein": 2.7,
		"salt": 0.08
	},
	"LandmetzgereiSpringerBierschinken": {
		"name": "Landmetzgerei Springer Bierschinken",
		"cal": 190,
		"fat": 14,
		"saturated": 5.6,
		"carbs": 0.9,
		"sugar": 0.3,
		"protein": 15,
		"salt": 1.7
	},
	"RehmSchinkenwurst": {
		"name": "REHM Schinkenwurst",
		"cal": 224,
		"fat": 18,
		"saturated": 7,
		"carbs": 0.5,
		"sugar": 0.5,
		"protein": 15,
		"salt": 2
	},
	"RehmBierschinken": {
		"name": "REHM Bierschinken",
		"cal": 185,
		"fat": 13,
		"saturated": 5.1,
		"carbs": 1,
		"sugar": 0.5,
		"protein": 16,
		"salt": 2
	},
	"WiltmannSalamissimoRind": {
		"name": "Wiltmann Salamissimo Rind",
		"cal": 476,
		"fat": 38,
		"saturated": 15,
		"carbs": 1,
		"sugar": 1,
		"protein": 32,
		"salt": 4
	},
	"WiltmannSalamissimoPikant": {
		"name": "Wiltmann Salamissimo Pikant",
		"cal": 496,
		"fat": 39,
		"saturated": 15.8,
		"carbs": 1,
		"sugar": 1,
		"protein": 34.8,
		"salt": 4
	},
	"WiltmannSalamissimoPur": {
		"name": "Wiltmann Salamissimo Pur",
		"cal": 491,
		"fat": 39,
		"saturated": 16,
		"carbs": 1,
		"sugar": 1,
		"protein": 33.4,
		"salt": 4
	},
	"ZentisErdnussButter": {
		"name": "Zentis Erdnuss Butter",
		"cal": 623,
		"fat": 50,
		"saturated": 10,
		"carbs": 15,
		"sugar": 10,
		"protein": 25,
		"salt": 1.1
	},
	"GlueckPassiertHimbeerMarmelade": {
		"name": "Glück Passiert Aus Himbeeren",
		"cal": 214,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 49,
		"sugar": 49,
		"protein": 0.7,
		"salt": 0.02
	},
	"RealThaiMassamanCurryCookingSauce": {
		"name": "Real THAI Massaman Curry Cooking Sauce",
		"cal": 145,
		"fat": 12,
		"saturated": 8.1,
		"carbs": 7.7,
		"sugar": 6.6,
		"protein": 1.1,
		"salt": 1.4
	},
	"ZitronenKaesekuchen": {
		"name": "Kochbar.de Zitronen Kaeskuchen", // https://www.kochbar.de/rezept/466323/Zitronen-Kaesekuchen.html
		"cal": 478,
		"fat": 24.5,
		"saturated": 20, // guestimation
		"carbs": 61.6,
		"sugar": 40, // guestimation
		"protein": 2.8,
		"salt": 1 // guestimation
	},
	"FeineKuecheRinderFond": {
		"name": "Feine Küche Jürgen Langbein Rinder-Fond",
		"cal": 8,
		"fat": 0,
		"saturated": 0,
		"carbs": 1,
		"sugar": 1,
		"protein": 0.9,
		"salt": 1
	},
	"JackLinksBeefJerkyOriginal": {
		"name": "Jack Link's Beef Jerky Original",
		"cal": 255,
		"fat": 3.9,
		"saturated": 1.4,
		"carbs": 16,
		"sugar": 16,
		"protein": 39,
		"salt": 4.9
	},
	"BodyAttackProteinCoffee": {
		"name": "Body Attack Protein Coffee",
		"cal": 58,
		"fat": 0.2,
		"saturated": 0.1,
		"carbs": 4,
		"sugar": 4,
		"protein": 10,
		"salt": 0.1
	},
	"ReweBesteWahlSteinofenBurgerBuns": {
		"name": "Steinofen Burger Buns",
		"cal": 241,
		"fat": 3.9,
		"saturated": 0.4,
		"carbs": 41.9,
		"sugar": 3.2,
		"protein": 8.3,
		"salt": 1.23
	},
	"ButchersBurgerCheddarCheese": {
		"name": "Burger Cheddar Cheese",
		"cal": 390,
		"fat": 32,
		"saturated": 21,
		"carbs": 0.5,
		"sugar": 0.5,
		"protein": 25,
		"salt": 1.8
	},
	"HellmannsMayoMitKnoblauchNote": {
		"name": "Mayo mit Knoblauch",
		"cal": 271,
		"fat": 27,
		"saturated": 2.7,
		"carbs": 6.1,
		"sugar": 2.3,
		"protein": 0.7,
		"salt": 1.8
	},
	"PemaEiweissBrot": {
		"name": "PEMA Eiweiss Brot",
		"cal": 225,
		"fat": 8.6,
		"saturated": 1.1,
		"carbs": 14,
		"sugar": 1.9,
		"protein": 18,
		"salt": 1.1
	},
	"yfoodThisIsFoodClassicChoco": {
		"name": "yfood THIS IS FOOD classic choco",
		"cal": 100,
		"fat": 4.3,
		"saturated": 1.2,
		"carbs": 8.2,
		"sugar": 4.3,
		"protein": 6.5,
		"salt": 0.15
	},
	"yfoodThisIsFoodCherryBanana": {
		"name": "yfood THIS IS FOOD cherry Banana",
		"cal": 100,
		"fat": 4.6,
		"saturated": 1,
		"carbs": 6.9,
		"sugar": 4.5,
		"protein": 7,
		"salt": 0.16
	},
	"McDonaldsMcChickenClassic": {
		"name": "McChicken Classic",
		"cal": 258,
		"fat": 11,
		"saturated": 1.9,
		"carbs": 28,
		"sugar": 2.3,
		"protein": 12,
		"salt": 1.4
	},
	"McDonaldsPommesFrites": {
		"name": "McDonalds Pommes Frites",
		"cal": 289,
		"fat": 14,
		"saturated": 1.3,
		"carbs": 36,
		"sugar": 0.3,
		"protein": 3.4,
		"salt": 0.47
	},
	"McDonaldsMcNuggets": {
		"name": "McDonalds McNuggets",
		"cal": 243,
		"fat": 12,
		"saturated": 1.4,
		"carbs": 19,
		"sugar": 0.7,
		"protein": 15,
		"salt": 1.1
	},
	"BurgerKingVeganLongChicken": {
		"name": "BurgerKing Vegan Long Chicken",
		"cal": 276,
		"fat": 15,
		"saturated": 1.5,
		"carbs": 25,
		"sugar": 2.7,
		"protein": 7.8,
		"salt": 1.5
	},
	"BurgerKingPommes": {
		"name": "BurgerKing Vegan Long Chicken",
		"cal": 274,
		"fat": 13.8,
		"saturated": 4.9,
		"carbs": 32.2,
		"sugar": 0.2,
		"protein": 3.3,
		"salt": 1.2
	},
	"OatlyHafer": {
		"name": "The Original Oatly Hafer",
		"cal": 37,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 6.7,
		"sugar": 4.1,
		"protein": 1,
		"salt": 0.1
	},
	"OatlyHaferCalcium": {
		"name": "The Original Oatly Hafer Calcium",
		"cal": 46,
		"fat": 1.5,
		"saturated": 0.2,
		"carbs": 6.7,
		"sugar": 4.1,
		"protein": 1,
		"salt": 0.1
	},
	"ReweBioGouda": {
		"name": "Rewe Bio Gouda",
		"cal": 357,
		"fat": 29,
		"saturated": 22,
		"carbs": 0,
		"sugar": 0,
		"protein": 24,
		"salt": 1.3
	},
	"ReweBioGefluegelSalami": {
		"name": "Rewe Bio Geflügel-Salami",
		"cal": 277,
		"fat": 19,
		"saturated": 8.7,
		"carbs": 1,
		"sugar": 1,
		"protein": 25,
		"salt": 3.9
	},
	"AndechserNaturBioGouda": {
		"name": "Andechser Natur Bio-Gouda",
		"cal": 348,
		"fat": 28,
		"saturated": 18,
		"carbs": 0,
		"sugar": 0,
		"protein": 24,
		"salt": 1.3
	},
	"ClasenBioStudentenfutter": {
		"name": "Clasen Bio Studentenfutter",
		"cal": 451,
		"fat": 25.2,
		"saturated": 3,
		"carbs": 43.2,
		"sugar": 38.8,
		"protein": 9.9,
		"salt": 0.01
	},
	"ClasenBioMangostreifen": {
		"name": "Clasen Bio Mangostreifen",
		"cal": 320,
		"fat": 1.3,
		"saturated": 0.3,
		"carbs": 70.3,
		"sugar": 59.6,
		"protein": 2.6,
		"salt": 0.1
	},
	"ClasenNussMix": {
		"name": "Clasen Bio Nuss Mix",
		"cal": 652,
		"fat": 57.9,
		"saturated": 7.8,
		"carbs": 11,
		"sugar": 3.6,
		"protein": 17.9,
		"salt": 0.02
	},
	"Heidelbeeren": {
		"name": "Heidelbeeren",
		"cal": 57,
		"fat": 0,
		"saturated": 0,
		"carbs": 14.5,
		"sugar": 10,
		"protein": 0.74,
		"salt": 0.01
	},
	"Fage02": {
		"name": "Fage Total 0,2%",
		"cal": 55,
		"fat": 0.2,
		"saturated": 0,
		"carbs": 3,
		"sugar": 3,
		"protein": 10.3,
		"salt": 0.1
	},
	"ExquisaSkyrMild": {
		"name": "Exquisa Milder Skyr",
		"cal": 64,
		"fat": 0.2,
		"saturated": 0.1,
		"carbs": 4,
		"sugar": 4,
		"protein": 11,
		"salt": 0.08
	},
	"JaSkyrNatur": {
		"name": "ja! Skyr Natur",
		"cal": 64,
		"fat": 0.2,
		"saturated": 0.1,
		"carbs": 4,
		"sugar": 4,
		"protein": 11,
		"salt": 0.08
	},
	"EdekaBioSkyr": {
		"name": "EDEKA Bio Skyr",
		"cal": 65,
		"fat": 0.2,
		"saturated": 0.1,
		"carbs": 4.4,
		"sugar": 4.4,
		"protein": 10.6,
		"salt": 0.08
	},
	"LindtGoldhase": {
		"name": "Lindt Goldhase",
		"cal": 544,
		"fat": 32,
		"saturated": 19,
		"carbs": 56,
		"sugar": 54,
		"protein": 7.2,
		"salt": 0.32
	},
	"SAPCanteen20220505SignoraItaly": {
		"name": "SAP Canteen Signora Italy",
		"cal": 918,
		"fat": 71.29,
		"saturated": 23.75,
		"carbs": 8.29,
		"sugar": 0.97,
		"protein": 61.20,
		"salt": 0.97
	},
	"SAPCanteen20220506CalamaresAlaRomana": {
		"name": "SAP Canteen Calamares a la Romana",
		"cal": 971,
		"fat": 85.3,
		"saturated": 13.1,
		"carbs": 12.7,
		"sugar": 7.7,
		"protein": 39.8,
		"salt": 1.6
	},
	"SAPCanteen20220524PeppersStuffedGroundBeef": {
		"name": "SAP Canteen Peppers Stuffed Ground Beef",
		"cal": 561,
		"fat": 26.73,
		"saturated": 7.18,
		"carbs": 48.6,
		"sugar": 17.98,
		"protein": 26.79,
		"salt": 0.24
	},
	"SAPCanteen20220622TurkeySteak": {
		"name": "SAP Canteen Turkey Steak",
		"cal": 507,
		"fat": 14.6,
		"saturated": 2.5,
		"carbs": 40.6,
		"sugar": 6.1,
		"protein": 50.8,
		"salt": 2.2
	},
	"SAPCanteen20220622Broccoli": {
		"name": "SAP Canteen Broccoli",
		"cal": 50,
		"fat": 0.4,
		"saturated": 0.1,
		"carbs": 4.5,
		"sugar": 3.7,
		"protein": 6.7,
		"salt": 1.5
	},
	"SAPCanteen20220622Bananapudding": {
		"name": "SAP Canteen Bananapudding",
		"cal": 70,
		"fat": 0.9,
		"saturated": 0.5,
		"carbs": 12.6,
		"sugar": 9.3,
		"protein": 2.3,
		"salt": 0.1
	},
	"SAPCanteen20220624EuropaBowl": {
		"name": "SAP Canteen Turkey Steak",
		"cal": 507,
		"fat": 14.6,
		"saturated": 2.5,
		"carbs": 40.6,
		"sugar": 6.1,
		"protein": 50.8,
		"salt": 2.2
	},
	"SAPCanteen20220629BeefHip": {
		"name": "SAP Canteen Beef Hip",
		"cal": 460,
		"fat": 11.9,
		"saturated": 2.6,
		"carbs": 45.8,
		"sugar": 15.6,
		"protein": 40.2,
		"salt": 3.9
	},
	"SAPCanteen20220629ChocolatePudding": {
		"name": "SAP Canteen Vegan Chocolate Pudding",
		"cal": 210,
		"fat": 12.5,
		"saturated": 6.8,
		"carbs": 21.8,
		"sugar": 17.6,
		"protein": 2.6,
		"salt": 0.1
	},
	"SAPCanteen20220629Vegetables": {
		"name": "SAP Canteen Vegetables",
		"cal": 127,
		"fat": 3.8,
		"saturated": 0.6,
		"carbs": 16.9,
		"sugar": 5.5,
		"protein": 5.8,
		"salt": 2.5
	},
	"SAPCanteen20220704Bowl": {
		"name": "SAP Canteen Salad Bowl",
		"cal": 516,
		"fat": 25.91,
		"saturated": 9.11,
		"carbs": 15.35,
		"sugar": 14.45,
		"protein": 53.30,
		"salt": 2.79
	},
	"SAPCanteen20220704Peas": {
		"name": "SAP Canteen Peas",
		"cal": 96,
		"fat": 0.52,
		"saturated": 0.1,
		"carbs": 12.71,
		"sugar": 1.28,
		"protein": 7.12,
		"salt": 0.01
	},
	"SAPCanteen20220704Shake": {
		"name": "SAP Canteen Shake",
		"cal": 65,
		"fat": 1.48,
		"saturated": 0.98,
		"carbs": 8.8,
		"sugar": 8.26,
		"protein": 3.67,
		"salt": 0.07
	},
	"SAPCanteen20220706Turkey": {
		"name": "SAP Canteen Turkey",
		"cal": 817,
		"fat": 46.5,
		"saturated": 24.2,
		"carbs": 44.1,
		"sugar": 5.9,
		"protein": 54.3,
		"salt": 4.5
	},
	"SAPCanteen20220707TexBowl": {
		"name": "SAP Canteen Tex Bowl",
		"cal": 794,
		"fat": 43,
		"saturated": 14.17,
		"carbs": 64.99,
		"sugar": 6.47,
		"protein": 31.15,
		"salt": 0.7
	},
	"SAPCanteen20220708AmericanStyleBowl": { // zwei Pute (+80g Putenschnitzel)
		"name": "SAP Canteen American Style Bowl",
		"cal": 663 + 106,
		"fat": 37.8 + 4,
		"saturated": 9.57 + 0.8,
		"carbs": 29.08,
		"sugar": 14.12,
		"protein": 46.56 + 17.6,
		"salt": 1.14
	},
	"SAPCanteen20220711SZEGED": {
		"name": "SAP Canteen SZEGED",
		"cal": 571,
		"fat": 22.6,
		"saturated": 2.74,
		"carbs": 43.93,
		"sugar": 15.16,
		"protein": 37.87,
		"salt": 3.94
	},
	"SAPCanteen20220714Poultry": {
		"name": "SAP Canteen Poultry",
		"cal": 459,
		"fat": 15.69,
		"saturated": 4.51,
		"carbs": 43.36,
		"sugar": 12.84,
		"protein": 30.12,
		"salt": 2.25
	},
	"SAPCanteen20220715Wraps": {
		"name": "SAP Canteen Wraps",
		"cal": 664,
		"fat": 34.57,
		"saturated": 9.02,
		"carbs": 63.06,
		"sugar": 13.98,
		"protein": 24.59,
		"salt": 3.35
	},
	"SAPCanteen20220718Coqauvin": {
		"name": "SAP Canteen Coq au vin",
		"cal": 612,
		"fat": 28.46,
		"saturated": 9.45,
		"carbs": 34.01,
		"sugar": 9.38,
		"protein": 48.04,
		"salt": 0.36
	},
	"SAPCanteen20220719PorkTenderloin": {
		"name": "SAP Canteen Pork Tenderloin",
		"cal": 546,
		"fat": 11.24,
		"saturated": 4.41,
		"carbs": 44.09,
		"sugar": 6.98,
		"protein": 64.03,
		"salt": 0.98
	},
	"SAPCanteen20220720SharkCatfish": {
		"name": "SAP Canteen Shark Catfish",
		"cal": 268,
		"fat": 8.5,
		"saturated": 2.4,
		"carbs": 11.8,
		"sugar": 7.7,
		"protein": 35.2,
		"salt": 0.2
	},
	"SAPCanteen20220720DillRice": {
		"name": "SAP Canteen Dill Rice",
		"cal": 304,
		"fat": 1,
		"saturated": 0.2,
		"carbs": 66.2,
		"sugar": 0.3,
		"protein": 6.5,
		"salt": 0.2
	},
	"SAPCanteen20220720PeasCarottsVegetables": {
		"name": "SAP Canteen Peas Carotts Vegetables",
		"cal": 144,
		"fat": 0.9,
		"saturated": 0.2,
		"carbs": 21.2,
		"sugar": 2.2,
		"protein": 11.9,
		"salt": 0.9
	},
	"SAPCanteen20220721PotatoFrittata": {
		"name": "SAP Canteen Potato Frittata",
		"cal": 459,
		"fat": 26.43,
		"saturated": 3.98,
		"carbs": 37.22,
		"sugar": 15.76,
		"protein": 13.73,
		"salt": 2.31
	},
	"SAPCanteen20220722Curry": {
		"name": "SAP Canteen Curry",
		"cal": 593,
		"fat": 26.49,
		"saturated": 6.79,
		"carbs": 70.7,
		"sugar": 23.84,
		"protein": 18.88,
		"salt": 0.53
	},
	"SAPCanteen20220725BeefHash": {
		"name": "SAP Canteen Hash of Beef",
		"cal": 520,
		"fat": 15.19,
		"saturated": 6.46,
		"carbs": 58.52,
		"sugar": 4.62,
		"protein": 33.7,
		"salt": 0.15
	},
	"SAPCanteen20220726PorkTenderloin": {
		"name": "SAP Canteen Pork Tenderloin",
		"cal": 445,
		"fat": 7.42,
		"saturated": 1.84,
		"carbs": 39.68,
		"sugar": 10.11,
		"protein": 50.34,
		"salt": 0.3
	},
	"SAPCanteen20220727SaladNicoise": {
		"name": "SAP Canteen Salad Nicoise",
		"cal": 685,
		"fat": 30.6,
		"saturated": 5.7,
		"carbs": 69.9,
		"sugar": 18.8,
		"protein": 30.3,
		"salt": 3.9
	},
	"SAPCanteen20220728GrilledChickenPaella": {
		"name": "SAP Canteen Grilled Chicken Paella",
		"cal": 877,
		"fat": 45.21,
		"saturated": 13.78,
		"carbs": 49.64,
		"sugar": 4.9,
		"protein": 65.77,
		"salt": 0.69
	},
	"SAPCanteen20220801WholemealSpaghetti": {
		"name": "SAP Canteen Wholemeal Spaghetti",
		"cal": 435,
		"fat": 12.52,
		"saturated": 1.8,
		"carbs": 53.31,
		"sugar": 10.09,
		"protein": 19.6,
		"salt": 0.21
	},
	"SAPCanteen20220802PastaStrozzapreti": {
		"name": "SAP Canteen Pasta Strozzapreti",
		"cal": 335,
		"fat": 27.29,
		"saturated": 5.67,
		"carbs": 15.13,
		"sugar": 16.38,
		"protein": 6.35,
		"salt": 0.76
	},
	"SAPCanteen20220803MushroomRagout": {
		"name": "SAP Canteen Mushroom Ragout",
		"cal": 417,
		"fat": 16.1,
		"saturated": 7.4,
		"carbs": 46.3,
		"sugar": 10.8,
		"protein": 20.9,
		"salt": 2.3
	},
	"SAPCanteen20220804FriedPastaPockets": {
		"name": "SAP Canteen Fried Pasta Pockets",
		"cal": 544,
		"fat": 16.59,
		"saturated": 1.68,
		"carbs": 67.31,
		"sugar": 11.88,
		"protein": 25.57,
		"salt": 2.44
	},
	"SAPCanteen20220805AsianSalad": {
		"name": "SAP Canteen Asian Salad",
		"cal": 278,
		"fat": 4.66,
		"saturated": 0.98,
		"carbs": 12,
		"sugar": 5.58,
		"protein": 43.24,
		"salt": 2.83
	},
	"SAPCanteen20220808OsloScandicBurger": {
		"name": "SAP Canteen Oslo Scandic Burger",
		"cal": 791,
		"fat": 30.31,
		"saturated": 5.94,
		"carbs": 85.04,
		"sugar": 33.3,
		"protein": 34.57,
		"salt": 2.47
	},
	"SAPCanteen20220809PenneChicken": {
		"name": "SAP Canteen Penne Fried Chicken Breast",
		"cal": 729,
		"fat": 28.52,
		"saturated": 9.26,
		"carbs": 59.42,
		"sugar": 6.77,
		"protein": 53.54,
		"salt": 0.33
	},
	"SAPCanteen20220810UmamiOvenEggplant": {
		"name": "SAP Canteen Umami Oven Eggplant",
		"cal": 707,
		"fat": 10.2,
		"saturated": 1.7,
		"carbs": 111.1,
		"sugar": 26.7,
		"protein": 35.9,
		"salt": 0.8
	},
	"SAPCanteen20220812GreenPaella": {
		"name": "SAP Canteen Green Paella",
		"cal": 547,
		"fat": 17.96,
		"saturated": 2.56,
		"carbs": 58.13,
		"sugar": 10.53,
		"protein": 32.23,
		"salt": 2.36
	},
	"SAPCanteen20220815ChickenFricassee": {
		"name": "SAP Canteen Chicken Fricassee",
		"cal": 553,
		"fat": 20.49,
		"saturated": 7.62,
		"carbs": 46,
		"sugar": 4.02,
		"protein": 40.08,
		"salt": 0.24
	},
	"SAPCanteen20220816Sauerbraten": {
		"name": "SAP Canteen Sauerbraten",
		"cal": 424,
		"fat": 9.11,
		"saturated": 3.9,
		"carbs": 31.67,
		"sugar": 7.23,
		"protein": 48.38,
		"salt": 0.26
	},
	"SAPCanteen20220817PastaRocketGranaPadano": {
		"name": "SAP Canteen Pasta Rocket Grana Padano",
		"cal": 705,
		"fat": 10.1,
		"saturated": 4.1,
		"carbs": 123.9,
		"sugar": 8.4,
		"protein": 27.1,
		"salt": 1.5
	},
	"SAPCanteen20220818TandoriChicken": {
		"name": "SAP Canteen Tandori Chicken",
		"cal": 349,
		"fat": 2.32,
		"saturated": 0.45,
		"carbs": 35.69,
		"sugar": 5.96,
		"protein": 44.9,
		"salt": 0.3
	},
	"SAPCanteen20220822PorkTenderloin": {
		"name": "SAP Canteen Pork Tenderloin",
		"cal": 452,
		"fat": 14.62,
		"saturated": 5.52,
		"carbs": 27.15,
		"sugar": 3.97,
		"protein": 49.37,
		"salt": 5.52
	},
	"SAPCanteen20220823EuropaBowl": {
		"name": "SAP Canteen Europa Bowl",
		"cal": 338,
		"fat": 8.47,
		"saturated": 1.02,
		"carbs": 16.66,
		"sugar": 14.9,
		"protein": 44.2,
		"salt": 1.56
	},
	"SAPCanteen20220824TortelliniCarne": {
		"name": "SAP Canteen Tortellini Carne",
		"cal": 742,
		"fat": 31.6,
		"saturated": 14.4,
		"carbs": 69.8,
		"sugar": 6.5,
		"protein": 43.7,
		"salt": 3.5
	},
	"SAPCanteen20220825BakedPotato": {
		"name": "SAP Canteen Baked Potato",
		"cal": 586,
		"fat": 26.5,
		"saturated": 11.37,
		"carbs": 56.74,
		"sugar": 5.94,
		"protein": 29.62,
		"salt": 2.01
	},
	"SAPCanteen20220829CesarSalad": {
		"name": "SAP Canteen Cesar Salad",
		"cal": 375,
		"fat": 15.52,
		"saturated": 5.06,
		"carbs": 18.05,
		"sugar": 7.09,
		"protein": 37.2,
		"salt": 0.56
	},
	"SAPCanteen20220830PiccataChickenBreast": {
		"name": "SAP Canteen Piccata Chicken Breast",
		"cal": 452,
		"fat": 18.59,
		"saturated": 3.53,
		"carbs": 29.3,
		"sugar": 5.97,
		"protein": 39.43,
		"salt": 2.59
	},
	"SAPCanteen20220831Palikaria": {
		"name": "SAP Canteen Palikaria",
		"cal": 443,
		"fat": 18.4,
		"saturated": 2,
		"carbs": 48.9,
		"sugar": 5,
		"protein": 19.8,
		"salt": 4.2
	},
	"SAPCanteen20220831RicePudding": {
		"name": "SAP Canteen Rice Pudding",
		"cal": 120,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 24.7,
		"sugar": 5.8,
		"protein": 3.6,
		"salt": 0
	},
	"SAPCanteen20220901BeefRissole": {
		"name": "SAP Canteen Beef Rissole",
		"cal": 520,
		"fat": 21.75,
		"saturated": 9.49,
		"carbs": 48.32,
		"sugar": 11.1,
		"protein": 27.12,
		"salt": 2.86
	},
	"SAPCanteen20220902LaCucinaAllora": {
		"name": "SAP Canteen La Cucina Allora",
		"cal": 667,
		"fat": 27.6,
		"saturated": 11.77,
		"carbs": 81.34,
		"sugar": 7.58,
		"protein": 19.54,
		"salt": 2.31
	},
	"SAPCanteen20220905SpinachGnocchi": {
		"name": "SAP Canteen Spinach Gnocchi",
		"cal": 510,
		"fat": 11.12,
		"saturated": 6.52,
		"carbs": 47.56,
		"sugar": 11.66,
		"protein": 49.1,
		"salt": 0.39
	},
	"SAPCanteen20220906TurkeySteak": {
		"name": "SAP Canteen Turkey Steak",
		"cal": 678,
		"fat": 19.45,
		"saturated": 5.99,
		"carbs": 60.68,
		"sugar": 7.21,
		"protein": 59.92,
		"salt": 0.61
	},
	"SAPCanteen20220907BeefGoulash": {
		"name": "SAP Canteen Beef Goulash",
		"cal": 807,
		"fat": 29.2,
		"saturated": 8.3,
		"carbs": 82.7,
		"sugar": 11.3,
		"protein": 52.2,
		"salt": 2.7
	},
	"SAPCanteen20220907PumpkinSoup": {
		"name": "SAP Canteen Pumpkin Cream Soup",
		"cal": 65,
		"fat": 3.4,
		"saturated": 2,
		"carbs": 6.4,
		"sugar": 3.8,
		"protein": 2.2,
		"salt": 0.8
	},
	"SAPCanteen20220908NewYorkerGrilledChicken": {
		"name": "SAP Canteen New Yorker Grilled Chicken",
		"cal": 931,
		"fat": 60.81,
		"saturated": 19.73,
		"carbs": 32.13,
		"sugar": 9.42,
		"protein": 60.45,
		"salt": 5.04
	},
	"SAPCanteen20220909TunaPizza": {
		"name": "SAP Canteen Tuna Pizza",
		"cal": 887,
		"fat": 21.26,
		"saturated": 9.22,
		"carbs": 124.96,
		"sugar": 1.25,
		"protein": 43.81,
		"salt": 7.71
	},
	"SAPCanteen20220912YunaYuuki": {
		"name": "SAP Canteen Yuna und Huuki",
		"cal": 618,
		"fat": 17.83,
		"saturated": 3.66,
		"carbs": 60.01,
		"sugar": 6.48,
		"protein": 53,
		"salt": 4.29
	},
	"SAPCanteen20220914ButterChicken": {
		"name": "SAP Canteen Butter Chicken",
		"cal": 476,
		"fat": 14.6,
		"saturated": 4.4,
		"carbs": 60.3,
		"sugar": 11.5,
		"protein": 24.5,
		"salt": 3.3
	},
	"SAPCanteen20220914CheesecakeCreme": {
		"name": "SAP Canteen Cheesecake-Creme",
		"cal": 243,
		"fat": 15,
		"saturated": 9.2,
		"carbs": 22.1,
		"sugar": 21,
		"protein": 4.8,
		"salt": 0.3
	},
	"SAPCanteen20220916NewOrleansPlants": {
		"name": "SAP Canteen Powered by Plants New Orleans",
		"cal": 601,
		"fat": 22.07,
		"saturated": 2,
		"carbs": 59.4,
		"sugar": 25.05,
		"protein": 31.73,
		"salt": 4
	},
	"SAPCanteen20220915Madurai": {
		"name": "SAP Canteen Madurai",
		"cal": 707,
		"fat": 10.74,
		"saturated": 3.17,
		"carbs": 120.77,
		"sugar": 16.62,
		"protein": 23.98,
		"salt": 1.01
	},
	"SAPCanteen20220919RoastedChickenBreast": {
		"name": "SAP Canteen Roasted Chicken Breast",
		"cal": 475,
		"fat": 20.29,
		"saturated": 6.96,
		"carbs": 20.94,
		"sugar": 10.04,
		"protein": 48.74,
		"salt": 0.39
	},
	"SAPCanteen20220920Paella": {
		"name": "SAP Canteen Paella",
		"cal": 536,
		"fat": 12.06,
		"saturated": 4.48,
		"carbs": 51.27,
		"sugar": 5.57,
		"protein": 50.93,
		"salt": 2.3
	},
	"SAPCanteen20220921Meatball": {
		"name": "SAP Canteen Meatball",
		"cal": 697,
		"fat": 46,
		"saturated": 19.7,
		"carbs": 41.5,
		"sugar": 8.8,
		"protein": 28.9,
		"salt": 3.8
	},
	"SAPCanteen20220922YellowThaiCurry": {
		"name": "SAP Canteen Yellow Thai Curry",
		"cal": 515,
		"fat": 19.97,
		"saturated": 7.82,
		"carbs": 56.46,
		"sugar": 16.66,
		"protein": 30.8,
		"salt": 0.86
	},
	"SAPCanteen20220923PorkLoinSteak": {
		"name": "SAP Canteen Pork Loin Steak",
		"cal": 717,
		"fat": 44.25,
		"saturated": 16.78,
		"carbs": 35.66,
		"sugar": 12.13,
		"protein": 40.1,
		"salt": 0.29
	},
	"SAPCanteen20221004ApplePumpkinSoup": {
		"name": "SAP Canteen Apple Pumpkin Soup",
		"cal": 30,
		"fat": 0.22,
		"saturated": 0.05,
		"carbs": 5.37,
		"sugar": 5,
		"protein": 0.68,
		"salt": 0.02
	},
	"SAPCanteen20221004ChickenFricassee": {
		"name": "SAP Canteen Chicken Fricassee",
		"cal": 553,
		"fat": 20.48,
		"saturated": 7.62,
		"carbs": 45.99,
		"sugar": 4.02,
		"protein": 40.08,
		"salt": 0.24
	},
	"SAPCanteen20221004Smoothie": {
		"name": "SAP Canteen Smoothie",
		"cal": 57,
		"fat": 1.16,
		"saturated": 0.14,
		"carbs": 9.96,
		"sugar": 7.05,
		"protein": 0.98,
		"salt": 0.05
	},
	"SAPCanteen20221005Lasagne": {
		"name": "SAP Canteen Lasagne",
		"cal": 804,
		"fat": 16,
		"saturated": 4.2,
		"carbs": 129.4,
		"sugar": 7.1,
		"protein": 26.7,
		"salt": 0.5
	},
	"SAPCanteen20221006RisoniPan": {
		"name": "SAP Canteen Risoni Pan",
		"cal": 775,
		"fat": 26.61,
		"saturated": 7.6,
		"carbs": 92.65,
		"sugar": 9.28,
		"protein": 35.81,
		"salt": 2.48
	},
	"SAPCanteen20221006Dessert": {
		"name": "SAP Canteen Dessert",
		"cal": 93,
		"fat": 3.47,
		"saturated": 2.6,
		"carbs": 12.31,
		"sugar": 11.7,
		"protein": 1.35,
		"salt": 0.06
	},
	"SAPCanteen20221007PollackFillet": {
		"name": "SAP Canteen Pollack Fillet",
		"cal": 321,
		"fat": 5.22,
		"saturated": 2.47,
		"carbs": 27.91,
		"sugar": 8.73,
		"protein": 34.74,
		"salt": 0.31
	},
	"SAPCanteen20221010CoqAuVin": {
		"name": "SAP Canteen Coq au vin",
		"cal": 611,
		"fat": 28.47,
		"saturated": 9.45,
		"carbs": 33.83,
		"sugar": 9.25,
		"protein": 48.03,
		"salt": 0.37
	},
	"SAPCanteen20221011PlantsLandshut": {
		"name": "SAP Canteen Landshut Vegetable Stew",
		"cal": 336,
		"fat": 8.95,
		"saturated": 1.19,
		"carbs": 27.63,
		"sugar": 13.44,
		"protein": 27.34,
		"salt": 3.07
	},
	"SAPCanteen20221012SwabianSouerbraten": {
		"name": "SAP Canteen Swabian Souerbraten",
		"cal": 971,
		"fat": 63.1,
		"saturated": 19.6,
		"carbs": 36.6,
		"sugar": 10.4,
		"protein": 62.1,
		"salt": 3.9
	},
	"SAPCanteen20221013AsianSalad": {
		"name": "SAP Canteen Asian Salad",
		"cal": 278,
		"fat": 4.66,
		"saturated": 0.98,
		"carbs": 12,
		"sugar": 5.58,
		"protein": 43.24,
		"salt": 2.83
	},
	"SAPCanteen20221013CoffeeCream": {
		"name": "SAP Canteen Coffee Cream",
		"cal": 134,
		"fat": 8.15,
		"saturated": 6.45,
		"carbs": 12.29,
		"sugar": 10.7,
		"protein": 2.13,
		"salt": 0.15
	},
	"SAPCanteen20221018PolloConMole": {
		"name": "SAP Canteen Pollo con Mole",
		"cal": 536,
		"fat": 10.33,
		"saturated": 1.94,
		"carbs": 58.74,
		"sugar": 5.58,
		"protein": 47.98,
		"salt": 0.23
	},
	"SAPCanteen20221019SwabianStew": {
		"name": "SAP Canteen Swabian Stew",
		"cal": 598,
		"fat": 19.5,
		"saturated": 8.7,
		"carbs": 68.8,
		"sugar": 5.4,
		"protein": 35.5,
		"salt": 5.1
	},
	"SAPCanteen20221019PassionfruitDessert": {
		"name": "SAP Canteen Passionfruit Dessert",
		"cal": 187,
		"fat": 8.9,
		"saturated": 7.9,
		"carbs": 22,
		"sugar": 22,
		"protein": 2,
		"salt": 0.1
	},
	"SAPCanteen20221020ConCarne": {
		"name": "SAP Canteen Con Carne",
		"cal": 557,
		"fat": 15.34,
		"saturated": 4.02,
		"carbs": 60.31,
		"sugar": 5.74,
		"protein": 40.78,
		"salt": 0.34
	},
	"SAPCanteen20221021Fajita": {
		"name": "SAP Canteen Fajita",
		"cal": 512,
		"fat": 17.89,
		"saturated": 6.14,
		"carbs": 57.57,
		"sugar": 13.15,
		"protein": 28.14,
		"salt": 3.05
	},
	"SAPCanteen20221025BetterGreen": {
		"name": "SAP Canteen Better Green",
		"cal": 368,
		"fat": 24.17,
		"saturated": 6.4,
		"carbs": 18.39,
		"sugar": 10.9,
		"protein": 16.24,
		"salt": 2.8
	},
	"SAPCanteen20221025AppleCrumble": {
		"name": "SAP Canteen Apple Crumble",
		"cal": 188,
		"fat": 6.45,
		"saturated": 3.17,
		"carbs": 29.39,
		"sugar": 16.35,
		"protein": 1.75,
		"salt": 0.03
	},
	"SAPCanteen20221026MediterraneanBowl": {
		"name": "SAP Canteen Mediterranean Bowl",
		"cal": 712,
		"fat": 28,
		"saturated": 4,
		"carbs": 83.2,
		"sugar": 16.9,
		"protein": 29.1,
		"salt": 0.2
	},
	"SAPCanteen20221102StuffedEggplant": {
		"name": "SAP Canteen Stuffed Eggplant",
		"cal": 493,
		"fat": 16.3,
		"saturated": 2.4,
		"carbs": 74.4,
		"sugar": 10,
		"protein": 10.8,
		"salt": 3.3
	},
	"SAPCanteen20221102PeachSmoothie": {
		"name": "SAP Canteen Peach Smoothie",
		"cal": 123,
		"fat": 4.3,
		"saturated": 0.4,
		"carbs": 16.7,
		"sugar": 11.4,
		"protein": 13.8,
		"salt": 0.1
	},
	"SAPCanteen20221102PlumCrumble": {
		"name": "SAP Canteen Plum Crumble",
		"cal": 207,
		"fat": 7.7,
		"saturated": 4.5,
		"carbs": 31.7,
		"sugar": 19.8,
		"protein": 2.4,
		"salt": 0.2
	},
	"SAPCanteen20221103RutabagaStew": {
		"name": "SAP Canteen Rutabaga Stew",
		"cal": 314,
		"fat": 9.84,
		"saturated": 2.24,
		"carbs": 27.09,
		"sugar": 18.1,
		"protein": 23.2,
		"salt": 0.22
	},
	"SAPCanteen20221103ChocolateMousse": {
		"name": "SAP Canteen Chocolate Mousse",
		"cal": 225,
		"fat": 14.2,
		"saturated": 9.03,
		"carbs": 20.62,
		"sugar": 20.62,
		"protein": 2.65,
		"salt": 0.13
	},
	"SAPCanteen20221104Jaipur": {
		"name": "SAP Canteen Jaipur",
		"cal": 453,
		"fat": 11.15,
		"saturated": 2,
		"carbs": 61.8,
		"sugar": 10.88,
		"protein": 19.88,
		"salt": 1
	},
	"SAPCanteen20221104Stracciatella": {
		"name": "SAP Canteen Stracciatella",
		"cal": 125,
		"fat": 3.07,
		"saturated": 1.93,
		"carbs": 11.13,
		"sugar": 15.74,
		"protein": 7,
		"salt": 0.08
	},
	"SAPCanteen20221107Cannelloni": {
		"name": "SAP Canteen Cannelloni",
		"cal": 476,
		"fat": 16.12,
		"saturated": 6.83,
		"carbs": 56.26,
		"sugar": 20.74,
		"protein": 24.3,
		"salt": 1.08
	},
	"SAPCanteen20221108PotatoAndPeaCurry": {
		"name": "SAP Canteen Potato and pea curry",
		"cal": 685,
		"fat": 10.69,
		"saturated": 3.15,
		"carbs": 116.93,
		"sugar": 13.43,
		"protein": 23.04,
		"salt": 0.98
	},
	"SAPCanteen20221109BurritoDeEspinacha": {
		"name": "SAP Canteen Burrito De Espinacha",
		"cal": 759,
		"fat": 23.3,
		"saturated": 10.5,
		"carbs": 107.4,
		"sugar": 15.1,
		"protein": 27.3,
		"salt": 4.3
	},
	"SAPCanteen20221109Desert": {
		"name": "SAP Canteen Ananas-Kakao-Rum-Creme",
		"cal": 184,
		"fat": 5.7,
		"saturated": 3.9,
		"carbs": 26.7,
		"sugar": 26.5,
		"protein": 2.1,
		"salt": 0.1
	},
	"SAPCanteen20221109Smoothie": {
		"name": "SAP Canteen Mango-Pfirsich Smoothie",
		"cal": 48,
		"fat": 0.2,
		"saturated": 0,
		"carbs": 10.6,
		"sugar": 10.1,
		"protein": 0.5,
		"salt": 0
	},
	"SAPCanteen20221110Szegediner": {
		"name": "SAP Canteen Szegediner",
		"cal": 569,
		"fat": 22.58,
		"saturated": 2.74,
		"carbs": 43.66,
		"sugar": 14.89,
		"protein": 37.76,
		"salt": 3.91
	},
	"SAPCanteen20221114Tokio": {
		"name": "SAP Canteen Tokio",
		"cal": 715,
		"fat": 23.41,
		"saturated": 5.91,
		"carbs": 95,
		"sugar": 17.75,
		"protein": 28.54,
		"salt": 1.21
	},
	"SAPCanteen20221115BeefRissole": {
		"name": "SAP Canteen Beef Rissole",
		"cal": 520,
		"fat": 21.75,
		"saturated": 9.49,
		"carbs": 48.32,
		"sugar": 11.10,
		"protein": 27.12,
		"salt": 2.86
	},
	"SAPCanteen20221116Palikaria": {
		"name": "SAP Canteen Palikaria",
		"cal": 443,
		"fat": 18.4,
		"saturated": 2,
		"carbs": 48.9,
		"sugar": 5,
		"protein": 19.8,
		"salt": 4.2
	},
	"SAPCanteen20221116VeganChocolatePudding": {
		"name": "SAP Canteen Vegan Chocolate Pudding",
		"cal": 210,
		"fat": 12.5,
		"saturated": 6.8,
		"carbs": 21.8,
		"sugar": 17.6,
		"protein": 2.6,
		"salt": 0.1
	},
	"SAPCanteen20221116Cheesecake": {
		"name": "SAP Canteen Cheesecake",
		"cal": 298,
		"fat": 9,
		"saturated": 5.1,
		"carbs": 40.3,
		"sugar": 25.5,
		"protein": 12.9,
		"salt": 0.1
	},
	"SAPCanteen20221117HappyBowl": {
		"name": "SAP Canteen Happy Bowl",
		"cal": 377,
		"fat": 15.1,
		"saturated": 1.49,
		"carbs": 40.07,
		"sugar": 7.99,
		"protein": 16.09,
		"salt": 2.1
	},
	"SAPCanteen20221117VanillaCurdVerrine": {
		"name": "SAP Canteen Vanilla Curd Verrine",
		"cal": 119,
		"fat": 4.52,
		"saturated": 2.92,
		"carbs": 13.15,
		"sugar": 13.15,
		"protein": 5.94,
		"salt": 0.1
	},
	"SAPCanteen20221118BordelaiseFishFillet": {
		"name": "SAP Canteen Bordelaise Fish Fillet",
		"cal": 467,
		"fat": 21,
		"saturated": 7.4,
		"carbs": 35,
		"sugar": 6.4,
		"protein": 31.2,
		"salt": 3.3
	},
	"SAPCanteen20221118AppleQuarkDessert": {
		"name": "SAP Canteen Apple Quark Dessert",
		"cal": 105,
		"fat": 2.28,
		"saturated": 1.43,
		"carbs": 15.84,
		"sugar": 15.54,
		"protein": 4.72,
		"salt": 0.06
	},
	"SAPCanteen20221123HungaryBeefGoulash": {
		"name": "SAP Canteen Hungary Beef Goulash",
		"cal": 807,
		"fat": 29.2,
		"saturated": 8.3,
		"carbs": 82.7,
		"sugar": 11.3,
		"protein": 52.2,
		"salt": 2.7
	},
	"SAPCanteen20221123SteamPotatos": {
		"name": "SAP Canteen Steam Potatos",
		"cal": 146,
		"fat": 0,
		"saturated": 0,
		"carbs": 31.3,
		"sugar": 3.9,
		"protein": 0.5,
		"salt": 0.4
	},
	"SAPCanteen20221124HuhnMitHaltung": {
		"name": "SAP Canteen Huhn mit Haltung",
		"cal": 1098,
		"fat": 60.8,
		"saturated": 23.94,
		"carbs": 63.05,
		"sugar": 7.01,
		"protein": 70.6,
		"salt": 0.97
	},
	"SAPCanteen20221125TortillaWrap": {
		"name": "SAP Canteen Tortilla Wrap",
		"cal": 684,
		"fat": 32.6,
		"saturated": 17.1,
		"carbs": 67.3,
		"sugar": 17.6,
		"protein": 29.2,
		"salt": 3.2
	},
	"SAPCanteen20221125Erbsen": {
		"name": "SAP Canteen Halbes Erbsengemüse",
		"cal": 611/2,
		"fat": 0.9/2,
		"saturated": 0.2/2,
		"carbs": 21.5/2,
		"sugar": 2.4/2,
		"protein": 12.1/2,
		"salt": 0.5/2
	},
	"SAPCanteen20221125OatsCurdApplesSorrel": {
		"name": "SAP Canteen Oats Curd Apples Sorrel",
		"cal": 137,
		"fat": 1.6,
		"saturated": 0.4,
		"carbs": 22,
		"sugar": 11.2,
		"protein": 8,
		"salt": 0.1
	},
	"SAPCanteen20221129PoweredByPlants": {
		"name": "SAP Canteen Powered by Plants",
		"cal": 689,
		"fat": 25.85,
		"saturated": 8.65,
		"carbs": 99.81,
		"sugar": 31.36,
		"protein": 15.4,
		"salt": 0.46
	},
	"SAPCanteen20221130PeruPulledPork": {
		"name": "SAP Canteen Peru Pulled Pork",
		"cal": 913,
		"fat": 46.2,
		"saturated": 19.4,
		"carbs": 77.5,
		"sugar": 28.7,
		"protein": 44.5,
		"salt": 5.7
	},
	"SAPCanteen20221201PoweredByPlants": {
		"name": "SAP Canteen Powered by Plants",
		"cal": 489,
		"fat": 17.43,
		"saturated": 2.06,
		"carbs": 55.71,
		"sugar": 7.33,
		"protein": 17.67,
		"salt": 0.26
	},
	"SAPCanteen20221201AppleChocolateCrossies": {
		"name": "SAP Canteen Apple compote with chocolate crossies",
		"cal": 162,
		"fat": 2.34,
		"saturated": 1.06,
		"carbs": 32.62,
		"sugar": 18.98,
		"protein": 1.52,
		"salt": 0.19
	},
	"SAPCanteen20221201MangoCurdDessert": {
		"name": "SAP Canteen MangoCurdDessert",
		"cal": 110,
		"fat": 2.48,
		"saturated": 1.57,
		"carbs": 16.03,
		"sugar": 15.95,
		"protein": 5.24,
		"salt": 0.06
	},
	"SAPCanteen20221205TurkeyCurry": {
		"name": "SAP Canteen Turkey Curry",
		"cal": 403,
		"fat": 4.05,
		"saturated": 0.77,
		"carbs": 48.57,
		"sugar": 11.47,
		"protein": 40.45,
		"salt": 0.31
	},
	"SAPCanteen20221206PoweredByPlants": {
		"name": "SAP Canteen Powered By Plants",
		"cal": 675,
		"fat": 34.63,
		"saturated": 3.43,
		"carbs": 69.6,
		"sugar": 12.68,
		"protein": 13.76,
		"salt": 3.98
	},
	"SAPCanteen20221207BeefBrisket": {
		"name": "SAP Canteen Beef Brisket",
		"cal": 886,
		"fat": 53.4,
		"saturated": 14.6,
		"carbs": 62,
		"sugar": 25.8,
		"protein": 37.3,
		"salt": 4.1
	},
	"SAPCanteen20221207Bandnudeln": {
		"name": "SAP Canteen Beef Bandnudeln",
		"cal": 327,
		"fat": 8.4,
		"saturated": 0.8,
		"carbs": 52.8,
		"sugar": 0.3,
		"protein": 9.4,
		"salt": 1
	},
	"SAPCanteen20221207ErbsenKarotten": {
		"name": "SAP Canteen Beef Erbsen-Karottengemüse",
		"cal": 163,
		"fat": 8.9,
		"saturated": 5.5,
		"carbs": 14.7,
		"sugar": 5.9,
		"protein": 6,
		"salt": 1
	},
	"SAPCanteen20221207Gluehweinmousse": {
		"name": "SAP Canteen Beef Glühweinmousse",
		"cal": 67,
		"fat": 1.6,
		"saturated": 1,
		"carbs": 5.6,
		"sugar": 5.5,
		"protein": 1.8,
		"salt": 0
	},
	"SAPCanteen20221208PoweredByPlants": {
		"name": "SAP Canteen Powered By Plants",
		"cal": 551,
		"fat": 17.36,
		"saturated": 1.88,
		"carbs": 66.88,
		"sugar": 12.02,
		"protein": 26.11,
		"salt": 2.84
	},
	"SAPCanteen20221209FishBurger": {
		"name": "SAP Canteen Fish Burger",
		"cal": 826,
		"fat": 46.5,
		"saturated": 4.87,
		"carbs": 79.1,
		"sugar": 9.8,
		"protein": 21.49,
		"salt": 2.59
	},
	"SAPCanteen20221212TurkeyEscalope": {
		"name": "SAP Canteen Turkey Escalope",
		"cal": 702,
		"fat": 42.15,
		"saturated": 4.46,
		"carbs": 34.53,
		"sugar": 3.75,
		"protein": 44.54,
		"salt": 1.01
	},
	"SAPCanteen20221213PoweredByPlants": {
		"name": "SAP Canteen Powered By Plants",
		"cal": 446,
		"fat": 13,
		"saturated": 1.45,
		"carbs": 46.29,
		"sugar": 9.54,
		"protein": 26.17,
		"salt": 0.62
	},
	"SAPCanteen20221214BeefRoulade": {
		"name": "SAP Canteen Beef Roulade",
		"cal": 767,
		"fat": 35,
		"saturated": 16.8,
		"carbs": 47.2,
		"sugar": 7,
		"protein": 63.6,
		"salt": 3.9
	},
	"SAPCanteen20221214SwabianNoodles": {
		"name": "SAP Canteen Swabian Noodles",
		"cal": 217,
		"fat": 3.4,
		"saturated": 0.9,
		"carbs": 37.4,
		"sugar": 0.5,
		"protein": 8.7,
		"salt": 0.7
	},
	"SAPCanteen20221214Turnips": {
		"name": "SAP Canteen Turnips",
		"cal": 109,
		"fat": 5.3,
		"saturated": 3.1,
		"carbs": 12.6,
		"sugar": 10,
		"protein": 2.4,
		"salt": 0.4
	},
	"SAPCanteen20221214SkyrCerealMangoHoney": {
		"name": "SAP Canteen Skyr&Cereal Mango Honey",
		"cal": 140,
		"fat": 1.6,
		"saturated": 0.3,
		"carbs": 21.2,
		"sugar": 10.2,
		"protein": 9.5,
		"salt": 0.1
	},
	"SAPCanteen20221215Tortellini": {
		"name": "SAP Canteen Tortellini Verdi",
		"cal": 651,
		"fat": 29.34,
		"saturated": 4.18,
		"carbs": 79.49,
		"sugar": 6.8,
		"protein": 15.18,
		"salt": 1.38
	},
	"SAPCanteen20221216Pizza": {
		"name": "SAP Canteen Pizza",
		"cal": 825,
		"fat": 20.29,
		"saturated": 10.21,
		"carbs": 124.18,
		"sugar": 0.82,
		"protein": 32.47,
		"salt": 5.8
	},
	"SAPCanteen20221227ChiliConCarne": {
		"name": "SAP Canteen Chili Con Carne",
		"cal": 586,
		"fat": 20.5,
		"saturated": 9.9,
		"carbs": 68.1,
		"sugar": 7,
		"protein": 31.3,
		"salt": 2
	},
	"SAPCanteen20221227FrenchFries": {
		"name": "SAP Canteen French Fries",
		"cal": 373,
		"fat": 13.9,
		"saturated": 1.6,
		"carbs": 53.3,
		"sugar": 2.4,
		"protein": 6.6,
		"salt": 2.9
	},
	"SAPCanteen20221227Dessert": {
		"name": "SAP Canteen Greek yoghurt Walnut Honey",
		"cal": 130,
		"fat": 10.9,
		"saturated": 5.5,
		"carbs": 4.5,
		"sugar": 4.4,
		"protein": 3.2,
		"salt": 0.1
	},
	"SAPCanteen20221228SwabianStew": {
		"name": "SAP Canteen Swabian Stew",
		"cal": 598,
		"fat": 19.5,
		"saturated": 8.7,
		"carbs": 68.8,
		"sugar": 5.4,
		"protein": 35.5,
		"salt": 5.1
	},
	"SAPCanteen20221228ChocolatePudding": {
		"name": "SAP Canteen Chocolate Pudding",
		"cal": 210,
		"fat": 12.5,
		"saturated": 6.8,
		"carbs": 21.8,
		"sugar": 17.6,
		"protein": 2.6,
		"salt": 0.1
	},
	"SAPCanteen20221229TurkeySteak": {
		"name": "SAP Canteen Turkey Steak",
		"cal": 797,
		"fat": 27.9,
		"saturated": 8.2,
		"carbs": 80.8,
		"sugar": 6.1,
		"protein": 54.6,
		"salt": 1.4
	},
	"SAPCanteen20221230IndiaCurry": {
		"name": "SAP Canteen India Curry",
		"cal": 810,
		"fat": 32,
		"saturated": 3.2,
		"carbs": 100.7,
		"sugar": 22.2,
		"protein": 25.4,
		"salt": 0.9
	},
	"SAPCanteen20230111Goulash": {
		"name": "SAP Canteen Hirschgulasch",
		"cal": 415,
		"fat": 10.7,
		"saturated": 3.1,
		"carbs": 40.3,
		"sugar": 14.8,
		"protein": 38.3,
		"salt": 1.1
	},
	"SAPCanteen20230111LeipzigerAllerlei": {
		"name": "SAP Canteen Leizpiger Allerlei",
		"cal": 98,
		"fat": 4.5,
		"saturated": 1.6,
		"carbs": 9.8,
		"sugar": 4.9,
		"protein": 4.4,
		"salt": 0.9
	},
	"SAPCanteen20230118VeganKebabPlate": {
		"name": "SAP Canteen Vegan Kebab Plate, Soy, Garlic-Herb Sauce, Lettuce, Pita bread",
		"cal": 725,
		"fat": 36.4,
		"saturated": 3.4,
		"carbs": 71.9,
		"sugar": 11.1,
		"protein": 26,
		"salt": 3.3
	},
	"SAPCanteen20230118KohlrabiPeaVegetables": {
		"name": "SAP Canteen Kohlrabi, Pea",
		"cal": 135,
		"fat": 5.6,
		"saturated": 0.6,
		"carbs": 13.3,
		"sugar": 7,
		"protein": 7.4,
		"salt": 0.9
	},
	"SAPCanteen20230118Churros": {
		"name": "SAP Canteen Churros with cinnamon and sugar",
		"cal": 158,
		"fat": 3.6,
		"saturated": 0.6,
		"carbs": 30.2,
		"sugar": 6.5,
		"protein": 4.1,
		"salt": 0.8
	},
	"SAPCanteen20230207VeganChocolatePudding": {
		"name": "SAP Canteen Vegan Chocolate Pudding",
		"cal": 191,
		"fat": 5.6,
		"saturated": 1.8,
		"carbs": 31,
		"sugar": 28,
		"protein": 3.4,
		"salt": 0.06
	},
	"SAPCanteen20230208SalmonSteak": {
		"name": "SAP Canteen Rosted Salmon Steak",
		"cal": 559,
		"fat": 35,
		"saturated": 13,
		"carbs": 16,
		"sugar": 16,
		"protein": 44,
		"salt": 0.29
	},
	"SAPCanteen20230209ThailandBeef": {
		"name": "SAP Canteen Thailand Beef",
		"cal": 546,
		"fat": 23.5,
		"saturated": 5.3,
		"carbs": 52.1,
		"sugar": 16.1,
		"protein": 34.5,
		"salt": 4.4
	},
	"SAPCanteen20230209VeganChocolateCream": {
		"name": "SAP Canteen Vegan Chocolate Cream",
		"cal": 131,
		"fat": 7.3,
		"saturated": 6.1,
		"carbs": 14.5,
		"sugar": 14.5,
		"protein": 1.7,
		"salt": 0.0
	},
	"SAPCanteen20230215SpaghettiGroundBeef": {
		"name": "SAP Canteen Wholegrain spaghetti ground beef",
		"cal": 610,
		"fat": 19.9,
		"saturated": 4.3,
		"carbs": 79.5,
		"sugar": 7.3,
		"protein": 27.4,
		"salt": 2.3
	},
	"SAPCanteen20230215Cauliflower": {
		"name": "SAP Canteen Cauliflower",
		"cal": 78,
		"fat": 3.4,
		"saturated": 1.8,
		"carbs": 7.8,
		"sugar": 4.6,
		"protein": 4.1,
		"salt": 1.2
	},
	"SAPCanteen20230215PeachCouscous": {
		"name": "SAP Canteen Peach Couscous",
		"cal": 61,
		"fat": 2.1,
		"saturated": 0.2,
		"carbs": 8.3,
		"sugar": 5.6,
		"protein": 1.9,
		"salt": 0.0
	},
	"SAPCanteen20230216NoMeatBallsSpaghetti": {
		"name": "SAP Canteen No Meat Balls Spaghetti",
		"cal": 939.5,
		"fat": 25,
		"saturated": 10,
		"carbs": 135,
		"sugar": 64,
		"protein": 38,
		"salt": 2.3
	},
	"SAPCanteen20230217FilletOfHoki": {
		"name": "SAP Canteen Fillet of Hoki",
		"cal": 613,
		"fat": 35.9,
		"saturated": 5.3,
		"carbs": 21.1,
		"sugar": 5.8,
		"protein": 51.2,
		"salt": 3.9
	},
	"SAPCanteen20230217SteamPotatos": {
		"name": "SAP Canteen Steam Potatos",
		"cal": 122,
		"fat": 0,
		"saturated": 0,
		"carbs": 26,
		"sugar": 1.2,
		"protein": 3.2,
		"salt": 0.4
	},
	"SAPCanteen20230217Cheesecake": {
		"name": "SAP Canteen Cheesecake",
		"cal": 162,
		"fat": 9.2,
		"saturated": 5.8,
		"carbs": 16.4,
		"sugar": 15.6,
		"protein": 3.1,
		"salt": 0.1
	},
	"SAPCanteen20230221PadThai": {
		"name": "SAP Canteen Pad Thai",
		"cal": 424,
		"fat": 15,
		"saturated": 3.9,
		"carbs": 42,
		"sugar": 20,
		"protein": 25,
		"salt": 3.3
	},
	"SAPCanteen20230222CoalfishInStock": {
		"name": "SAP Canteen Coalfish in Stock",
		"cal": 658,
		"fat": 15.2,
		"saturated": 5.6,
		"carbs": 78.4,
		"sugar": 6.6,
		"protein": 49.2,
		"salt": 1.9
	},
	"SAPCanteen20230222CornAndBellPeppers": {
		"name": "SAP Canteen Corn and Bell Peppers",
		"cal": 150,
		"fat": 15.6,
		"saturated": 0.6,
		"carbs": 20.8,
		"sugar": 11.4,
		"protein": 3.7,
		"salt": 1.1
	},
	"SAPCanteen20230223PastaTaleggioRahm": {
		"name": "SAP Canteen Pasta Taleggio Rahm",
		"cal": 616,
		"fat": 19,
		"saturated": 11,
		"carbs": 87,
		"sugar": 8.1,
		"protein": 23,
		"salt": 0.48
	},
	"SAPCanteen20230223VeganChocolatePudding": {
		"name": "SAP Canteen Vegan Chocolate Pudding",
		"cal": 164,
		"fat": 4.3,
		"saturated": 2.2,
		"carbs": 27,
		"sugar": 24,
		"protein": 3.6,
		"salt": 0.07
	},
	"SAPCanteen20230224ChickenFricass": {
		"name": "SAP Canteen Chicken Fricass",
		"cal": 807,
		"fat": 50,
		"saturated": 22,
		"carbs": 53,
		"sugar": 7.2,
		"protein": 36,
		"salt": 2.6
	},
	"SAPCanteen20230224ArtichokeCreamSoup": {
		"name": "SAP Canteen Artichoke Cream Soup",
		"cal": 222,
		"fat": 16,
		"saturated": 8.1,
		"carbs": 11,
		"sugar": 11,
		"protein": 8.1,
		"salt": 0.14
	},
	"SAPCanteen20230227StuffedPeppers": {
		"name": "SAP Canteen Stuffed Peppers",
		"cal": 814,
		"fat": 41,
		"saturated": 10,
		"carbs": 72,
		"sugar": 14,
		"protein": 39,
		"salt": 1.8
	},
	"SAPCanteen20230227ChaiYoghurt": {
		"name": "SAP Canteen Stuffed Peppers",
		"cal": 190,
		"fat": 13,
		"saturated": 2.5,
		"carbs": 6.5,
		"sugar": 6.1,
		"protein": 10,
		"salt": 0.1
	},
	"SAPCanteen20230228BraisedBeef": {
		"name": "SAP Canteen Braised Beef",
		"cal": 557,
		"fat": 17,
		"saturated": 6.5,
		"carbs": 66,
		"sugar": 9.4,
		"protein": 33,
		"salt": 1.5
	},
	"SAPCanteen20230301ChickenBreastVienna": {
		"name": "SAP Canteen Chicken Breast Vienna Art",
		"cal": 760,
		"fat": 15.3,
		"saturated": 3.3,
		"carbs": 95,
		"sugar": 4.1,
		"protein": 57.5,
		"salt": 1.4
	},
	"SAPCanteen20230301Vanillecreme": {
		"name": "SAP Canteen Vanillecreme Vegetarisch",
		"cal": 129,
		"fat": 7,
		"saturated": 4.4,
		"carbs": 13.7,
		"sugar": 13.7,
		"protein": 2.4,
		"salt": 0.1
	},
	"SAPCanteen20230301Apfelkuchen": {
		"name": "SAP Canteen Apfelkuchen Vegetarisch",
		"cal": 144,
		"fat": 7.5,
		"saturated": 4.1,
		"carbs": 17.4,
		"sugar": 16.3,
		"protein": 1.6,
		"salt": 0.0
	},
	"SAPCanteen20230302PastaSalmon": {
		"name": "SAP Canteen Pasta Salmon",
		"cal": 764,
		"fat": 31,
		"saturated": 16,
		"carbs": 78,
		"sugar": 7.8,
		"protein": 42,
		"salt": 0.72
	},
	"SAPCanteen20230303CodMashedPotatoes": {
		"name": "SAP Canteen Cod Mashed Potatoes",
		"cal": 428,
		"fat": 23,
		"saturated": 14,
		"carbs": 24,
		"sugar": 8,
		"protein": 30,
		"salt": 0.77
	},
	"SAPCanteen20230303GriessbreiApfelragout": {
		"name": "SAP Canteen Griessbrei Apfelragout",
		"cal": 112,
		"fat": 4.2,
		"saturated": 1.3,
		"carbs": 15,
		"sugar": 8.5,
		"protein": 3.1,
		"salt": 0.07
	},
	"SAPCanteen20230306Bibimbap": {
		"name": "SAP Canteen BIBIMBAP",
		"cal": 452,
		"fat": 14,
		"saturated": 2.8,
		"carbs": 59,
		"sugar": 13,
		"protein": 19,
		"salt": 1.4
	},
	"SAPCanteen20230307SaffronChicken": {
		"name": "SAP Canteen Saffron Chicken",
		"cal": 630,
		"fat": 33,
		"saturated": 6.9,
		"carbs": 54,
		"sugar": 6.6,
		"protein": 29,
		"salt": 0.99
	},
	"SAPCanteen20230308PoppyChickenBowl": {
		"name": "SAP Canteen Poppy Chicken Bowl",
		"cal": 654,
		"fat": 28.1,
		"saturated": 5.4,
		"carbs": 55.1,
		"sugar": 13.2,
		"protein": 43.7,
		"salt": 3.5
	},
	"SAPCanteen20230308Broccoli": {
		"name": "SAP Canteen Broccoli",
		"cal": 42,
		"fat": 0.4,
		"saturated": 0.1,
		"carbs": 4.1,
		"sugar": 3.9,
		"protein": 5.3,
		"salt": 0.1
	},
	"SAPCanteen20230308PannaCotta": {
		"name": "SAP Canteen Panna Cotta",
		"cal": 141,
		"fat": 11.1,
		"saturated": 6.9,
		"carbs": 8.4,
		"sugar": 8.4,
		"protein": 2,
		"salt": 0.1
	},
	"SAPCanteen20230309HoisinPlantedBurger": {
		"name": "SAP Canteen Hoisin Planted Burger",
		"cal": 882,
		"fat": 48,
		"saturated": 5.5,
		"carbs": 73,
		"sugar": 28,
		"protein": 37,
		"salt": 7.8
	},
	"SAPCanteen20230310JapanMieNoodles": {
		"name": "SAP Canteen Japan Mie Noodles",
		"cal": 771,
		"fat": 38.1,
		"saturated": 8.3,
		"carbs": 52.9,
		"sugar": 14.4,
		"protein": 49.2,
		"salt": 4.3
	},
	"SAPCanteen20230310GlazedCarrots": {
		"name": "SAP Canteen Glazed Carrots",
		"cal": 88,
		"fat": 3.5,
		"saturated": 2.2,
		"carbs": 12.8,
		"sugar": 12.3,
		"protein": 1.1,
		"salt": 1.1
	},
	"SAPCanteen20230310VeganChocolateCream": {
		"name": "SAP Canteen Vegan Chocolate Cream",
		"cal": 131,
		"fat": 7.3,
		"saturated": 6.1,
		"carbs": 14.5,
		"sugar": 14.5,
		"protein": 1.7,
		"salt": 0
	},
	"SAPCanteen20230310Milchreis": {
		"name": "SAP Canteen Milchreis",
		"cal": 120,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 27.7,
		"sugar": 5.8,
		"protein": 3.6,
		"salt": 0
	},
	"SAPCanteen20230314FriedRedfish": {
		"name": "SAP Canteen Fried redfish",
		"cal": 608,
		"fat": 19,
		"saturated": 2.4,
		"carbs": 56,
		"sugar": 14,
		"protein": 52,
		"salt": 0.44
	},
	"SAPCanteen20230315IndiaButterChicken": {
		"name": "SAP Canteen India Butter Chicken",
		"cal": 528,
		"fat": 17.7,
		"saturated": 10,
		"carbs": 59,
		"sugar": 9.2,
		"protein": 32,
		"salt": 2
	},
	"SAPCanteen20230315ChickenBreastSweetPotato": {
		"name": "SAP Canteen Chicken Breast Sweet Potato",
		"cal": 426,
		"fat": 22.8,
		"saturated": 3.9,
		"carbs": 20.5,
		"sugar": 5.6,
		"protein": 34.6,
		"salt": 0.6
	},
	"SAPCanteen20230315Courgette": {
		"name": "SAP Canteen Courgette",
		"cal": 56,
		"fat": 4.3,
		"saturated": 1.6,
		"carbs": 2.4,
		"sugar": 2.4,
		"protein": 1.8,
		"salt": 0.2
	},
	"SAPCanteen20230315PeachYoghurt": {
		"name": "SAP Canteen Peach Yoghurt",
		"cal": 154,
		"fat": 9.6,
		"saturated": 5.3,
		"carbs": 15.1,
		"sugar": 15.1,
		"protein": 1.5,
		"salt": 0.1
	},
	"SAPCanteen20230316AfricanPeanutStew": {
		"name": "SAP Canteen African Peanut Stew",
		"cal": 695,
		"fat": 44,
		"saturated": 8.4,
		"carbs": 37,
		"sugar": 12,
		"protein": 36,
		"salt": 1.2
	},
	"SAPCanteen20230316AlmondPudding": {
		"name": "SAP Canteen Almond Pudding",
		"cal": 141,
		"fat": 6.4,
		"saturated": 0.5,
		"carbs": 17,
		"sugar": 13,
		"protein": 2.7,
		"salt": 0.17
	},
	"SAPCanteen20230317LinsenBolognese": {
		"name": "SAP Canteen Noodles Linsen-Bolognese",
		"cal": 733,
		"fat": 7.6,
		"saturated": 1,
		"carbs": 124,
		"sugar": 18,
		"protein": 39,
		"salt": 0.6
	},
	"SAPCanteen20230320MexicoChickenStew": {
		"name": "SAP Canteen Mexico Chicken Stew",
		"cal": 526,
		"fat": 26.2,
		"saturated": 9.5,
		"carbs": 41.3,
		"sugar": 7.2,
		"protein": 30.5,
		"salt": 3
	},
	"SAPCanteen20230320KohlrabiVegetables": {
		"name": "SAP Canteen Kohlrabi Vegetables",
		"cal": 119,
		"fat": 8.7,
		"saturated": 5.4,
		"carbs": 6.5,
		"sugar": 6.2,
		"protein": 3.6,
		"salt": 1.1
	},
	"SAPCanteen20230320ApricotCurd": {
		"name": "SAP Canteen Apricot Curd",
		"cal": 89,
		"fat": 0.2,
		"saturated": 0.1,
		"carbs": 11.7,
		"sugar": 11.7,
		"protein": 9,
		"salt": 0.1
	},
	"SAPCanteen20230321RoastedStripsOfBeef": {
		"name": "SAP Canteen Roasted Duck Breast",
		"cal": 750,
		"fat": 38,
		"saturated": 10,
		"carbs": 64,
		"sugar": 18,
		"protein": 38,
		"salt": 3.1
	},
	"SAPCanteen20230321HazelnutCream": {
		"name": "SAP Canteen Hazelnut Cream",
		"cal": 239,
		"fat": 18,
		"saturated": 9.6,
		"carbs": 13,
		"sugar": 13,
		"protein": 5.8,
		"salt": 0.31
	},
	"SAPCanteen20230322CeasarBurger": {
		"name": "SAP Canteen Ceasar Burger",
		"cal": 691,
		"fat": 32.9,
		"saturated": 9.4,
		"carbs": 51.6,
		"sugar": 4.1,
		"protein": 45.5,
		"salt": 2.65
	},
	"SAPCanteen20230322CreamySavoy": {
		"name": "SAP Canteen Creamy Savoy",
		"cal": 200,
		"fat": 17.2,
		"saturated": 10.4,
		"carbs": 7.1,
		"sugar": 4.8,
		"protein": 4.4,
		"salt": 1.5
	},
	"SAPCanteen20230322CreamCaramel": {
		"name": "SAP Canteen Cream caramel",
		"cal": 209,
		"fat": 3.6,
		"saturated": 2.2,
		"carbs": 39.9,
		"sugar": 30.7,
		"protein": 3.5,
		"salt": 0.2
	},
	"SAPCanteen20230323ShrimpSkewer": {
		"name": "SAP Canteen Shrimp Skewer",
		"cal": 689,
		"fat": 47,
		"saturated": 17,
		"carbs": 33,
		"sugar": 6.5,
		"protein": 32,
		"salt": 4.3
	},
	"SAPCanteen20230323CottageCheeseAndBerryCream": {
		"name": "SAP Canteen Cottage Cheese and Berry Cream",
		"cal": 125,
		"fat": 2.1,
		"saturated": 1,
		"carbs": 19,
		"sugar": 18,
		"protein": 6.7,
		"salt": 0.06
	},
	"SAPCanteen20230324Zander": {
		"name": "SAP Canteen Zander",
		"cal": 628,
		"fat": 29,
		"saturated": 11,
		"carbs": 47,
		"sugar": 10,
		"protein": 45,
		"salt": 0.46
	},
	"SAPCanteen20230324BananaYogurt": {
		"name": "SAP Canteen Banana Yogurt",
		"cal": 188,
		"fat": 15,
		"saturated": 5.4,
		"carbs": 8.6,
		"sugar": 8.1,
		"protein": 3.8,
		"salt": 0.13
	},
	"SAPCanteen20230324CarrotSoup": {
		"name": "SAP Canteen Banana Yogurt",
		"cal": 174,
		"fat": 9,
		"saturated": 4.8,
		"carbs": 20,
		"sugar": 13,
		"protein": 3.2,
		"salt": 0.08
	},
	"SAPCanteen20230327Harira": {
		"name": "SAP Canteen Harira",
		"cal": 64.25,
		"fat": 0.8,
		"saturated": 0.1,
		"carbs": 9.6,
		"sugar": 1.8,
		"protein": 4.3,
		"salt": 0.22
	},
	"SAPCanteen20230327Gulasch": {
		"name": "SAP Canteen Ungarisches Paprikagulasch",
		"cal": 545.05,
		"fat": 17,
		"saturated": 8.1,
		"carbs": 59,
		"sugar": 21,
		"protein": 34,
		"salt": 2.9
	},
	"SAPCanteen20230327KaffeeRicottaCreme": {
		"name": "SAP Canteen Kaffee-Ricotta-Creme",
		"cal": 156.92,
		"fat": 6,
		"saturated": 3.9,
		"carbs": 18,
		"sugar": 17,
		"protein": 7.4,
		"salt": 0.25
	},
	"SAPCanteen20230328Maishaehnchenbrust": {
		"name": "SAP Canteen Maishähnchenbrust",
		"cal": 540.99,
		"fat": 37,
		"saturated": 8.9,
		"carbs": 9.9,
		"sugar": 9,
		"protein": 40,
		"salt": 0.5
	},
	"SAPCanteen20230328SourCreamBlueberry": {
		"name": "SAP Canteen Sour Cream Blueberry",
		"cal": 174,
		"fat": 12,
		"saturated": 6.8,
		"carbs": 15,
		"sugar": 14,
		"protein": 2.1,
		"salt": 0.06
	},
	"SAPCanteen20230329FilletOfHoki": {
		"name": "SAP Canteen Fillet of Hoki",
		"cal": 445,
		"fat": 8.4,
		"saturated": 3.2,
		"carbs": 55.4,
		"sugar": 4.7,
		"protein": 35.4,
		"salt": 3
	},
	"SAPCanteen20230329RedQuinoa": {
		"name": "SAP Canteen Red Quinoa",
		"cal": 142,
		"fat": 2.4,
		"saturated": 0.2,
		"carbs": 25,
		"sugar": 0.7,
		"protein": 4.9,
		"salt": 1
	},
	"SAPCanteen20230329Potato": {
		"name": "SAP Canteen Potato Parsley",
		"cal": 133,
		"fat": 0,
		"saturated": 0,
		"carbs": 28.3,
		"sugar": 1.4,
		"protein": 3.6,
		"salt": 0.9
	},
	"SAPCanteen20230329Kohlrabi": {
		"name": "SAP Canteen Kohlrabi cream",
		"cal": 162,
		"fat": 14,
		"saturated": 8.1,
		"carbs": 5.8,
		"sugar": 5.6,
		"protein": 3.3,
		"salt": 1.1
	},
	"SAPCanteen20230329VeganChocolateCream": {
		"name": "SAP Canteen Vegan Chocolate Cream",
		"cal": 131,
		"fat": 7.3,
		"saturated": 6.1,
		"carbs": 14.5,
		"sugar": 14.5,
		"protein": 1.7,
		"salt": 0
	},
	"SAPCanteen20230330FilledWrapChicken": {
		"name": "SAP Canteen Filled Wrap Chicken",
		"cal": 656,
		"fat": 24.8,
		"saturated": 7,
		"carbs": 62.6,
		"sugar": 14.4,
		"protein": 43.5,
		"salt": 2.2
	},
	"SAPCanteen20230330CreamySpinach": {
		"name": "SAP Canteen Creamy Spinach",
		"cal": 55,
		"fat": 2.3,
		"saturated": 1.1,
		"carbs": 2.1,
		"sugar": 1.8,
		"protein": 5.3,
		"salt": 1.2
	},
	"SAPCanteen20230330Lentils": {
		"name": "SAP Canteen Lentils",
		"cal": 111,
		"fat": 2.1,
		"saturated": 0.2,
		"carbs": 15.5,
		"sugar": 0.9,
		"protein": 7,
		"salt": 0.7
	},
	"SAPCanteen20230330VeganRicePudding": {
		"name": "SAP Canteen Vegan Rice-Pudding",
		"cal": 120,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 24.7,
		"sugar": 5.8,
		"protein": 3.6,
		"salt": 0
	},
	"SAPCanteen20230331Redfish": {
		"name": "SAP Canteen Redfish",
		"cal": 561,
		"fat": 25,
		"saturated": 9.3,
		"carbs": 55.7,
		"sugar": 7.4,
		"protein": 27.8,
		"salt": 1.9
	},
	"SAPCanteen20230331MashedPotatoes": {
		"name": "SAP Canteen Mashed Potatoes",
		"cal": 188,
		"fat": 8.9,
		"saturated": 5.7,
		"carbs": 22.6,
		"sugar": 2.7,
		"protein": 3.8,
		"salt": 1
	},
	"SAPCanteen20230331Beans": {
		"name": "SAP Canteen Beans",
		"cal": 133,
		"fat": 4.1,
		"saturated": 1.3,
		"carbs": 12.2,
		"sugar": 1.4,
		"protein": 11.6,
		"salt": 1
	},
	"SAPCanteen20230403GroundBeef": {
		"name": "SAP Canteen Ground Beef",
		"cal": 619,
		"fat": 19.4,
		"saturated": 5.9,
		"carbs": 80.5,
		"sugar": 12.5,
		"protein": 29.4,
		"salt": 2.1
	},
	"SAPCanteen20230403Chickpeas": {
		"name": "SAP Canteen Chickpeas",
		"cal": 210,
		"fat": 6.8,
		"saturated": 2,
		"carbs": 24.4,
		"sugar": 3.1,
		"protein": 12,
		"salt": 0.4
	},
	"SAPCanteen20230403CarrotsWithOnions": {
		"name": "SAP Canteen Carrots with Onions",
		"cal": 62,
		"fat": 2.6,
		"saturated": 0.3,
		"carbs": 8.4,
		"sugar": 8.1,
		"protein": 1.1,
		"salt": 1
	},
	"SAPCanteen20230403GreekYoghurt": {
		"name": "SAP Canteen Greek Yoghurt",
		"cal": 130,
		"fat": 10.9,
		"saturated": 5.5,
		"carbs": 4.5,
		"sugar": 4.4,
		"protein": 3.2,
		"salt": 0.1
	},
	"SAPCanteen20230404ChickenSchnitzel": {
		"name": "SAP Canteen Chicken Schnitzel",
		"cal": 1007,
		"fat": 36,
		"saturated": 8.3,
		"carbs": 110,
		"sugar": 10,
		"protein": 57,
		"salt": 1.8
	},
	"SAPCanteen20230405PorkLoinSteak": {
		"name": "SAP Canteen Pork Loin Steak",
		"cal": 715,
		"fat": 27.3,
		"saturated": 7.5,
		"carbs": 63.1,
		"sugar": 7.8,
		"protein": 51.3,
		"salt": 4.7
	},
	"SAPCanteen20230405GreenBeans": {
		"name": "SAP Canteen Green Beans",
		"cal": 55,
		"fat": 0.4,
		"saturated": 0.1,
		"carbs": 8.4,
		"sugar": 4.1,
		"protein": 3.9,
		"salt": 0.9
	},
	"SAPCanteen20230405Broccoli": {
		"name": "SAP Canteen Broccoli",
		"cal": 43,
		"fat": 3,
		"saturated": 0.3,
		"carbs": 3.2,
		"sugar": 2.6,
		"protein": 4.9,
		"salt": 0.4
	},
	"SAPCanteen20230405CouscousMango": {
		"name": "SAP Canteen Couscous Mango",
		"cal": 202,
		"fat": 13,
		"saturated": 12,
		"carbs": 18,
		"sugar": 4.5,
		"protein": 3.8,
		"salt": 0
	},
	"SAPCanteen20230406BurritoBowl": {
		"name": "SAP Canteen Mexico Burrito Bowl",
		"cal": 766,
		"fat": 37.4,
		"saturated": 9.2,
		"carbs": 39.5,
		"sugar": 18.4,
		"protein": 66.6,
		"salt": 0.9
	},
	"SAPCanteen20230406GratinatedPotatoes": {
		"name": "SAP Canteen Gratinated Potatoes",
		"cal": 106,
		"fat": 5.5,
		"saturated": 3.4,
		"carbs": 10.1,
		"sugar": 1.9,
		"protein": 3.7,
		"salt": 0.2
	},
	"SAPCanteen20230406Beans": {
		"name": "SAP Canteen Beans",
		"cal": 213,
		"fat": 5,
		"saturated": 1.8,
		"carbs": 24.8,
		"sugar": 2,
		"protein": 16.8,
		"salt": 2
	},
	"SAPCanteen20230406MediterranesGemuese": {
		"name": "SAP Canteen Mediterranes Gemüse",
		"cal": 168,
		"fat": 13.6,
		"saturated": 3.6,
		"carbs": 8.4,
		"sugar": 4.4,
		"protein": 1.6,
		"salt": 0.5
	},
	"SAPCanteen20230406PannaCotta": {
		"name": "SAP Canteen Panna Cotta Aprikose",
		"cal": 228,
		"fat": 18.3,
		"saturated": 9.8,
		"carbs": 12.3,
		"sugar": 12.1,
		"protein": 4.1,
		"salt": 0.1
	},
	"SAPCanteen20230411AdanaKebap": {
		"name": "SAP Canteen Turkey Adana Kebap",
		"cal": 817,
		"fat": 46.5,
		"saturated": 24.2,
		"carbs": 44.1,
		"sugar": 5.9,
		"protein": 54.3,
		"salt": 4.5
	},
	"SAPCanteen20230411CurdBlueBerries": {
		"name": "SAP Canteen Curd with Blue Berries",
		"cal": 94,
		"fat": 0.3,
		"saturated": 0.1,
		"carbs": 12.2,
		"sugar": 12.2,
		"protein": 9.5,
		"salt": 0.1
	},
	"SAPCanteen20230413TunisiaChickenBreast": {
		"name": "SAP Canteen Tunisia Chicken Breast",
		"cal": 521,
		"fat": 11.6,
		"saturated": 2.9,
		"carbs": 57,
		"sugar": 24.8,
		"protein": 45.2,
		"salt": 0.6
	},
	"SAPCanteen20230413KidneyBeans": {
		"name": "SAP Canteen Kidney Beans",
		"cal": 120,
		"fat": 2.8,
		"saturated": 1,
		"carbs": 14,
		"sugar": 1.2,
		"protein": 9.4,
		"salt": 1.6
	},
	"SAPCanteen20230413Pasta": {
		"name": "SAP Canteen Pasta",
		"cal": 206,
		"fat": 0.7,
		"saturated": 0.1,
		"carbs": 41.8,
		"sugar": 0.3,
		"protein": 7.4,
		"salt": 0.4
	},
	"SAPCanteen20230413BananaCoconutPudding": {
		"name": "SAP Canteen Banana Coconut Pudding",
		"cal": 71,
		"fat": 1.9,
		"saturated": 0.4,
		"carbs": 12,
		"sugar": 8.9,
		"protein": 1.3,
		"salt": 0.1
	},
	"SAPCanteen20230413SchokoladenCreme": {
		"name": "SAP Canteen Weiße Schokoladen-Creme mit Kirschen",
		"cal": 196,
		"fat": 9.1,
		"saturated": 5.7,
		"carbs": 25.1,
		"sugar": 24.7,
		"protein": 2.7,
		"salt": 0.1
	},
	"SAPCanteen20230414WildSalmon": {
		"name": "SAP Canteen Wild Salmon",
		"cal": 433,
		"fat": 15.8,
		"saturated": 4,
		"carbs": 36.2,
		"sugar": 8.9,
		"protein": 34.6,
		"salt": 2.7
	},
	"SAPCanteen20230414Gemuesereis": {
		"name": "SAP Canteen Gemüsereis",
		"cal": 205,
		"fat": 2,
		"saturated": 0.2,
		"carbs": 41.8,
		"sugar": 2.3,
		"protein": 4.1,
		"salt": 0.1
	},
	"SAPCanteen20230414Kichererbsen": {
		"name": "SAP Canteen Kichererbsen",
		"cal": 210,
		"fat": 6.8,
		"saturated": 2,
		"carbs": 24.4,
		"sugar": 3.1,
		"protein": 12,
		"salt": 0.4
	},
	"SAPCanteen20230414ApfelZimtKuchen": {
		"name": "SAP Canteen Apfel-Zimt Kuchen",
		"cal": 144,
		"fat": 8,
		"saturated": 0.5,
		"carbs": 15.6,
		"sugar": 9.7,
		"protein": 2.4,
		"salt": 0.1
	},
	"SAPCanteen20230417BraisedBalsamicoChicken": {
		"name": "SAP Canteen Braised Balsamico Chicken",
		"cal": 563,
		"fat": 10.3,
		"saturated": 2.7,
		"carbs": 69.1,
		"sugar": 14.2,
		"protein": 45.2,
		"salt": 3.2
	},
	"SAPCanteen20230417Tagliatelle": {
		"name": "SAP Canteen Tagliatelle",
		"cal": 174,
		"fat": 0.6,
		"saturated": 0.1,
		"carbs": 35.2,
		"sugar": 0.2,
		"protein": 6.2,
		"salt": 0.5
	},
	"SAPCanteen20230417SweetPotatoQuinoa": {
		"name": "SAP Canteen Sweet Potato Quinoa",
		"cal": 129,
		"fat": 3.5,
		"saturated": 0.5,
		"carbs": 20.6,
		"sugar": 1.6,
		"protein": 3.4,
		"salt": 0.4
	},
	"SAPCanteen20230417Passionsfrucht": {
		"name": "SAP Canteen Passionsfrucht Joghurtcreme",
		"cal": 154,
		"fat": 9.6,
		"saturated": 5.3,
		"carbs": 15.1,
		"sugar": 15.1,
		"protein": 1.5,
		"salt": 0.1
	},
	"SAPCanteen20230418ArtichokesFreeRangeChicken": {
		"name": "SAP Canteen Artichokes & Free Range Chicken",
		"cal": 901,
		"fat": 82,
		"saturated": 22,
		"carbs": 9,
		"sugar": 7.1,
		"protein": 34,
		"salt": 2.2
	},
	"SAPCanteen20230419Calamares": {
		"name": "SAP Canteen Calamares a la Romana",
		"cal": 971,
		"fat": 85.3,
		"saturated": 13.1,
		"carbs": 12.7,
		"sugar": 7.7,
		"protein": 39.8,
		"salt": 1.6
	},
	"SAPCanteen20230420PastaGroundMeatRagout": {
		"name": "SAP Canteen Pasta Ground Meat Ragout",
		"cal": 945,
		"fat": 21.9,
		"saturated": 9.4,
		"carbs": 135.4,
		"sugar": 8.1,
		"protein": 49.1,
		"salt": 3
	},
	"SAPCanteen20230420VeganChocolatePudding": {
		"name": "SAP Canteen Vegan Chocolate Pudding",
		"cal": 247,
		"fat": 12,
		"saturated": 9.7,
		"carbs": 31.5,
		"sugar": 25.9,
		"protein": 2.6,
		"salt": 0.1
	},
	"SAPCanteen20230421PadThai": {
		"name": "SAP Canteen Pad Thai",
		"cal": 929,
		"fat": 11,
		"saturated": 1.7,
		"carbs": 175,
		"sugar": 22,
		"protein": 28,
		"salt": 4.3
	},
	"SAPCanteen20230421PlumCrumble": {
		"name": "SAP Canteen Plum Crumble",
		"cal": 152,
		"fat": 5.6,
		"saturated": 3,
		"carbs": 22,
		"sugar": 13,
		"protein": 2.1,
		"salt": 0.23
	},
	"SAPCanteen20230424ChickenFajita": {
		"name": "SAP Canteen Chicken Fajita",
		"cal": 571,
		"fat": 19.3,
		"saturated": 3.8,
		"carbs": 68,
		"sugar": 19.6,
		"protein": 29.4,
		"salt": 3.4
	},
	"SAPCanteen20230424WheatRisotto": {
		"name": "SAP Canteen Wheat Risotto",
		"cal": 245,
		"fat": 6,
		"saturated": 0.6,
		"carbs": 39.7,
		"sugar": 0.5,
		"protein": 7.6,
		"salt": 0
	},
	"SAPCanteen20230424Johannisbeerquark": {
		"name": "SAP Canteen Johannisbeerquark",
		"cal": 95,
		"fat": 0.4,
		"saturated": 0.3,
		"carbs": 12.7,
		"sugar": 12.7,
		"protein": 9.1,
		"salt": 0.1
	},
	"SAPCanteen20230425BeyondBurger": {
		"name": "SAP Canteen Beyond Burgers",
		"cal": 784,
		"fat": 42,
		"saturated": 10,
		"carbs": 75,
		"sugar": 16,
		"protein": 27,
		"salt": 5.2
	},
	"SAPCanteen20230425SourCreamStrawberryChia": {
		"name": "SAP Canteen Sour Cream Strawberry Chia",
		"cal": 155,
		"fat": 12,
		"saturated": 6.8,
		"carbs": 10,
		"sugar": 10,
		"protein": 2.2,
		"salt": 0.06
	},
	"SAPCanteen20230426ChickenBreastViennaArt": {
		"name": "SAP Canteen Chicken Breast Vienna Art",
		"cal": 760,
		"fat": 15.3,
		"saturated": 3.3,
		"carbs": 95,
		"sugar": 4.1,
		"protein": 57.5,
		"salt": 1.4
	},
	"SAPCanteen20230426Lentils": {
		"name": "SAP Canteen Lentils",
		"cal": 111,
		"fat": 2.1,
		"saturated": 0.2,
		"carbs": 15.5,
		"sugar": 0.9,
		"protein": 7,
		"salt": 0.7
	},
	"SAPCanteen20230502StuffedCabbage": {
		"name": "SAP Canteen Stuffed Cabbage",
		"cal": 686,
		"fat": 50,
		"saturated": 27,
		"carbs": 23,
		"sugar": 20,
		"protein": 37,
		"salt": 0.74
	},
	"SAPCanteen20230502ApricotDumplings": {
		"name": "SAP Canteen Apricot dumplings",
		"cal": 174,
		"fat": 7,
		"saturated": 2.7,
		"carbs": 22,
		"sugar": 10,
		"protein": 5.5,
		"salt": 0.43
	},
	"SAPCanteen20230503ItalyButterfishsteak": {
		"name": "SAP Canteen Italy Butterfishsteak",
		"cal": 458,
		"fat": 16.6,
		"saturated": 2.5,
		"carbs": 33.4,
		"sugar": 6.5,
		"protein": 42,
		"salt": 3.2
	},
	"SAPCanteen20230503RedLentils": {
		"name": "SAP Canteen Red Lentils",
		"cal": 289,
		"fat": 3.1,
		"saturated": 1.5,
		"carbs": 39.7,
		"sugar": 1.2,
		"protein": 20.9,
		"salt": 0.5
	},
	"SAPCanteen20230503VeganSemolinaPudding": {
		"name": "SAP Canteen Vegan Semolina Pudding",
		"cal": 100,
		"fat": 0.6,
		"saturated": 0.1,
		"carbs": 19.4,
		"sugar": 7.6,
		"protein": 3.6,
		"salt": 0.2
	},
	"SAPCanteen20230504HariraChickpeaSoup": {
		"name": "SAP Canteen Harira Chickpea Soup",
		"cal": 106,
		"fat": 5.7,
		"saturated": 0.8,
		"carbs": 9.3,
		"sugar": 1.5,
		"protein": 4.1,
		"salt": 0.22
	},
	"SAPCanteen20230504SaffronChicken": {
		"name": "SAP Canteen Saffron Chicken",
		"cal": 630,
		"fat": 33,
		"saturated": 6.9,
		"carbs": 54,
		"sugar": 6.6,
		"protein": 29,
		"salt": 0.99
	},
	"SAPCanteen20230504ChocolatePudding": {
		"name": "SAP Canteen Chocolate Pudding",
		"cal": 175,
		"fat": 6.3,
		"saturated": 2.7,
		"carbs": 26,
		"sugar": 18,
		"protein": 3.2,
		"salt": 0.06
	},
	"SAPCanteen20230505LimeRisottoChickenBreast": {
		"name": "SAP Canteen Lime Risotto Chicken Breast",
		"cal": 557,
		"fat": 18.1,
		"saturated": 6.1,
		"carbs": 67.7,
		"sugar": 5.3,
		"protein": 29.5,
		"salt": 2.2
	},
	"SAPCanteen20230505RedQuinoa": {
		"name": "SAP Canteen Red Quinoa",
		"cal": 88,
		"fat": 1.5,
		"saturated": 0.1,
		"carbs": 15.5,
		"sugar": 0.5,
		"protein": 3,
		"salt": 0.6
	},
	"SAPCanteen20230505VanilleQuarkErdbeer": {
		"name": "SAP Canteen Vanillequark Erdbeer",
		"cal": 105,
		"fat": 1.1,
		"saturated": 0.2,
		"carbs": 15,
		"sugar": 14.1,
		"protein": 7.8,
		"salt": 0.1
	},
	"SAPCanteen20230509ChickenThighsCarrotFries": {
		"name": "SAP Canteen Chicken Thighs Carrot Fries",
		"cal": 1036,
		"fat": 76,
		"saturated": 14,
		"carbs": 30,
		"sugar": 28,
		"protein": 60,
		"salt": 0.9
	},
	"SAPCanteen20230510JamaicaGrilledChickenBreast": {
		"name": "SAP Canteen Jamaica Grilled Chicken Breast",
		"cal": 455,
		"fat": 3.8,
		"saturated": 1.3,
		"carbs": 68.8,
		"sugar": 43.1,
		"protein": 32.4,
		"salt": 2.1
	},
	"SAPCanteen20230510Reis": {
		"name": "SAP Canteen Reis",
		"cal": 125,
		"fat": 1.2,
		"saturated": 0.1,
		"carbs": 25.5,
		"sugar": 1.4,
		"protein": 2.5,
		"salt": 0
	},
	"SAPCanteen20230510BrokkoliBlumenkohl": {
		"name": "SAP Canteen Brokkoli Blumenkohl",
		"cal": 31,
		"fat": 0.2,
		"saturated": 0,
		"carbs": 2.8,
		"sugar": 2.3,
		"protein": 4.1,
		"salt": 0.8
	},
	"SAPCanteen20230510Erbsen": {
		"name": "SAP Canteen Erbsen",
		"cal": 97,
		"fat": 0.6,
		"saturated": 0.1,
		"carbs": 14.3,
		"sugar": 1.8,
		"protein": 8,
		"salt": 0.3
	},
	"SAPCanteen20230510PannaCottaKirsch": {
		"name": "SAP Canteen Panna Cotta Kirsch",
		"cal": 176,
		"fat": 7.2,
		"saturated": 4.5,
		"carbs": 25.4,
		"sugar": 25.2,
		"protein": 2.1,
		"salt": 0.1
	},
	"SAPCanteen20230511WildBoarStew": {
		"name": "SAP Canteen Wild Boar Stew",
		"cal": 865,
		"fat": 40,
		"saturated": 8.5,
		"carbs": 64,
		"sugar": 14,
		"protein": 62,
		"salt": 2.9
	},
	"SAPCanteen20230511AlmondPudding": {
		"name": "SAP Canteen Almond Pudding",
		"cal": 141,
		"fat": 6.4,
		"saturated": 0.5,
		"carbs": 17,
		"sugar": 13,
		"protein": 2.7,
		"salt": 0.17
	},
	"SAPCanteen20230512PollockFilletBroccoli": {
		"name": "SAP Canteen Pollock Fillet Broccoli",
		"cal": 689,
		"fat": 21.5,
		"saturated": 7.9,
		"carbs": 70.6,
		"sugar": 6.8,
		"protein": 50.3,
		"salt": 2.4
	},
	"SAPCanteen20230512RedQuinoa": {
		"name": "SAP Canteen Red Quinoa",
		"cal": 88,
		"fat": 1.5,
		"saturated": 0.1,
		"carbs": 15.5,
		"sugar": 0.5,
		"protein": 3,
		"salt": 0.6
	},
	"SAPCanteen20230512SchokoladenPudding": {
		"name": "SAP Canteen Schokoladen Pudding",
		"cal": 247,
		"fat": 12,
		"saturated": 9.7,
		"carbs": 31.5,
		"sugar": 25.9,
		"protein": 2.6,
		"salt": 0.1
	},
	"SAPCanteen20230515MexicoChiliConCarne": {
		"name": "SAP Canteen Chili Con Carne",
		"cal": 488,
		"fat": 20.1,
		"saturated": 9.3,
		"carbs": 44.9,
		"sugar": 9.9,
		"protein": 31.2,
		"salt": 2.1
	},
	"SAPCanteen20230515PumpkinAndChickpeaMash": {
		"name": "SAP Canteen Pumpkin Chickepea Mash",
		"cal": 111,
		"fat": 3.8,
		"saturated": 2,
		"carbs": 14.8,
		"sugar": 1.7,
		"protein": 4,
		"salt": 0.5
	},
	"SAPCanteen20230515StrawberryMilkshake": {
		"name": "SAP Canteen Strawberry Milkshake",
		"cal": 72,
		"fat": 1.2,
		"saturated": 0.8,
		"carbs": 12.1,
		"sugar": 12.1,
		"protein": 2.6,
		"salt": 0.1
	},
	"SAPCanteen20230516IndiaChicken": {
		"name": "SAP Canteen India Chicken",
		"cal": 515,
		"fat": 14.4,
		"saturated": 2.2,
		"carbs": 66.3,
		"sugar": 14.9,
		"protein": 28.6,
		"salt": 7.2
	},
	"SAPCanteen20230516KidneyBeans": {
		"name": "SAP Canteen Kidney Beans",
		"cal": 120,
		"fat": 2.8,
		"saturated": 1,
		"carbs": 14,
		"sugar": 1.2,
		"protein": 9.4,
		"salt": 1.6
	},
	"SAPCanteen20230523ItalyPastaAsparagus": {
		"name": "SAP Canteen Italy Pasta Asparagus",
		"cal": 633,
		"fat": 9.6,
		"saturated": 5.4,
		"carbs": 105.9,
		"sugar": 8.3,
		"protein": 28.5,
		"salt": 1.1
	},
	"SAPCanteen20230523ButteredPeas": {
		"name": "SAP Canteen Buttered Peas",
		"cal": 113,
		"fat": 3.2,
		"saturated": 1.8,
		"carbs": 13.4,
		"sugar": 1.7,
		"protein": 7.3,
		"salt": 0.6
	},
	"SAPCanteen20230524HungaryBeefGoulash": {
		"name": "SAP Canteen Hungary Beef Goulash",
		"cal": 486,
		"fat": 16.8,
		"saturated": 6.5,
		"carbs": 42.9,
		"sugar": 11.2,
		"protein": 39.3,
		"salt": 2.5
	},
	"SAPCanteen20230524RedQuinoa": {
		"name": "SAP Canteen Red Quinoa",
		"cal": 88,
		"fat": 1.5,
		"saturated": 0.1,
		"carbs": 15.5,
		"sugar": 0.5,
		"protein": 3,
		"salt": 0.6
	},
	"SAPCanteen20230524PotatoWedges": {
		"name": "SAP Canteen Potato Wedges",
		"cal": 60,
		"fat": 0.6,
		"saturated": 0.1,
		"carbs": 11.9,
		"sugar": 0.5,
		"protein": 1.5,
		"salt": 0.3
	},
	"SAPCanteen20230524SuesserBulgur": {
		"name": "SAP Canteen Süßer Bulgur",
		"cal": 122,
		"fat": 2.9,
		"saturated": 0.3,
		"carbs": 18.5,
		"sugar": 7.5,
		"protein": 5,
		"salt": 0.3
	},
	"SAPCanteen20230524MaispolentaHonig": {
		"name": "SAP Canteen Maispolenta Honig",
		"cal": 109,
		"fat": 3.2,
		"saturated": 2,
		"carbs": 16.2,
		"sugar": 9.4,
		"protein": 3.5,
		"salt": 0.2
	},
	"SAPCanteen20230525RavioliSpinatCottageCheese": {
		"name": "SAP Canteen Ravioli Spinat Cottage Cheese",
		"cal": 983,
		"fat": 37,
		"saturated": 19,
		"carbs": 120,
		"sugar": 14,
		"protein": 40,
		"salt": 1.2
	},
	"SAPCanteen20230525RaspberryCream": {
		"name": "SAP Canteen Raspberry Cream",
		"cal": 114,
		"fat": 2,
		"saturated": 0.9,
		"carbs": 15,
		"sugar": 9.6,
		"protein": 7.8,
		"salt": 0.06
	},
	"SAPCanteen20230526FranceFilletOfAlaskaPollock": {
		"name": "SAP Canteen Fillet of Alaska Pollock",
		"cal": 481,
		"fat": 14.4,
		"saturated": 2.6,
		"carbs": 50.5,
		"sugar": 5.5,
		"protein": 36.3,
		"salt": 2.7
	},
	"SAPCanteen20230530PiccataTurkey": {
		"name": "SAP Canteen Piccata Turkey",
		"cal": 694,
		"fat": 6.5,
		"saturated": 2.6,
		"carbs": 100.4,
		"sugar": 5.9,
		"protein": 55.8,
		"salt": 2.8
	},
	"SAPCanteen20230531SteamedWildSalmon": {
		"name": "SAP Canteen Steamed Wild Salmon",
		"cal": 616,
		"fat": 20.8,
		"saturated": 6.8,
		"carbs": 67.9,
		"sugar": 8.5,
		"protein": 38.4,
		"salt": 3.2
	},
	"SAPCanteen20230531MixedQuinoa": {
		"name": "SAP Canteen Mixed Quinoa",
		"cal": 88,
		"fat": 1.5,
		"saturated": 0.1,
		"carbs": 15.5,
		"sugar": 0.5,
		"protein": 3,
		"salt": 0.6
	},
	"SAPCanteen20230531BananaPudding": {
		"name": "SAP Canteen Banana Pudding",
		"cal": 77,
		"fat": 1,
		"saturated": 0.6,
		"carbs": 14,
		"sugar": 10.3,
		"protein": 2.6,
		"salt": 0.1
	},
	"SAPCanteen20230601ChickenFricass": {
		"name": "SAP Canteen Chicken Fricass",
		"cal": 528,
		"fat": 22,
		"saturated": 9.3,
		"carbs": 47,
		"sugar": 4,
		"protein": 35,
		"salt": 0.23
	},
	"SAPCanteen20230601SourCreamPassionFruitChia": {
		"name": "SAP Canteen Sour Cream Passion Fruit Chia",
		"cal": 164,
		"fat": 12,
		"saturated": 6.8,
		"carbs": 11,
		"sugar": 11,
		"protein": 2.8,
		"salt": 0.08
	},
	"SAPCanteen20230602FilletOfTilapia": {
		"name": "SAP Canteen Fillet of Tilapia",
		"cal": 568,
		"fat": 10.1,
		"saturated": 3.5,
		"carbs": 77,
		"sugar": 12.3,
		"protein": 40.2,
		"salt": 1.3
	},
	"SAPCanteen20230602Cauliflower": {
		"name": "SAP Canteen Cauliflower",
		"cal": 63,
		"fat": 3.2,
		"saturated": 1.8,
		"carbs": 4,
		"sugar": 3.5,
		"protein": 4.4,
		"salt": 0.7
	},
	"SAPCanteen20230602Lentils": {
		"name": "SAP Canteen Lentils",
		"cal": 160,
		"fat": 1.3,
		"saturated": 0.1,
		"carbs": 27.1,
		"sugar": 5.8,
		"protein": 10,
		"salt": 2.9
	},
	"SAPCanteen20230602PannaCotta": {
		"name": "SAP Canteen Panna Cotta",
		"cal": 218,
		"fat": 18.4,
		"saturated": 10.9,
		"carbs": 11.2,
		"sugar": 11.1,
		"protein": 2.6,
		"salt": 0.1
	},
	"SAPCanteen20230605GroundBeefVegetablesRice": {
		"name": "SAP Canteen Ground Beef Vegetables Rice",
		"cal": 532,
		"fat": 19.2,
		"saturated": 5.8,
		"carbs": 61.1,
		"sugar": 12.4,
		"protein": 27.5,
		"salt": 2.1
	},
	"SAPCanteen20230605Quinoa": {
		"name": "SAP Canteen Quinoa",
		"cal": 88,
		"fat": 1.5,
		"saturated": 0.1,
		"carbs": 15.5,
		"sugar": 0.5,
		"protein": 3,
		"salt": 0.6
	},
	"SAPCanteen20230605Broccoli": {
		"name": "SAP Canteen Broccoli",
		"cal": 70,
		"fat": 3.7,
		"saturated": 1.5,
		"carbs": 3.5,
		"sugar": 2.9,
		"protein": 5.6,
		"salt": 0.5
	},
	"SAPCanteen20230606Schaeufele": {
		"name": "SAP Canteen Schaeufele",
		"cal": 742,
		"fat": 47,
		"saturated": 18,
		"carbs": 22,
		"sugar": 19,
		"protein": 53,
		"salt": 0.48
	},
	"SAPCanteen20230607ItalyBraisedChicken": {
		"name": "SAP Canteen Italy Braised Chicken Glazed Vegetables",
		"cal": 534,
		"fat": 14.9,
		"saturated": 3.3,
		"carbs": 53.7,
		"sugar": 15.3,
		"protein": 43.8,
		"salt": 3.8
	},
	"SAPCanteen20230609AlgeriaFriedFish": {
		"name": "SAP Canteen Algeria Fried fish",
		"cal": 845,
		"fat": 22.4,
		"saturated": 4.7,
		"carbs": 109,
		"sugar": 7.6,
		"protein": 49.9,
		"salt": 2.7
	},
	"SAPCanteen20230612GratinatedChickenBreastRice": {
		"name": "SAP Canteen Gratinated Chicken Breast Rice",
		"cal": 546,
		"fat": 10.4,
		"saturated": 5.2,
		"carbs": 62,
		"sugar": 2.7,
		"protein": 48.9,
		"salt": 1.9
	},
	"SAPCanteen20230612GreekYoghurtHoneyWalnuts": {
		"name": "SAP Canteen Greek Yoghurt Honey Walnuts",
		"cal": 130,
		"fat": 10.9,
		"saturated": 5.5,
		"carbs": 4.5,
		"sugar": 4.4,
		"protein": 3.2,
		"salt": 0.1
	},
	"SAPCanteen20230613ChickenBreastStalida": {
		"name": "SAP Canteen Chicken Breast Stalida",
		"cal": 578,
		"fat": 16.6,
		"saturated": 4.4,
		"carbs": 52.8,
		"sugar": 3.8,
		"protein": 53.1,
		"salt": 2.6
	},
	"SAPCanteen20230613BananaPudding": {
		"name": "SAP Canteen Banana Pudding",
		"cal": 132,
		"fat": 4,
		"saturated": 2.4,
		"carbs": 21.3,
		"sugar": 17.2,
		"protein": 2.3,
		"salt": 0.1
	},
	"SAPCanteen20230614LikamaHloaBraisedChicken": {
		"name": "SAP Canteen Likama Hloa Braised Chicken",
		"cal": 608,
		"fat": 32.7,
		"saturated": 9.3,
		"carbs": 35.7,
		"sugar": 11.4,
		"protein": 42.2,
		"salt": 2
	},
	"SAPCanteen20230614BakedBeans": {
		"name": "SAP Canteen Baked Beans",
		"cal": 149,
		"fat": 8,
		"saturated": 1.2,
		"carbs": 10.4,
		"sugar": 1,
		"protein": 9,
		"salt": 0.7
	},
	"SAPCanteen20230615CrispyPrawnSushiBowl": {
		"name": "SAP Canteen Crispy Prawn Sushi Bowl",
		"cal": 658,
		"fat": 24,
		"saturated": 3.2,
		"carbs": 67,
		"sugar": 20,
		"protein": 44,
		"salt": 3.1
	},
	"SAPCanteen20230615ChocolatePudding": {
		"name": "SAP Canteen Chocolate Pudding",
		"cal": 181,
		"fat": 6.2,
		"saturated": 2.7,
		"carbs": 28,
		"sugar": 17,
		"protein": 3.1,
		"salt": 0.05
	},
	"SAPCanteen20230616RedfishColorfulLentils": {
		"name": "SAP Canteen Redfish Colorful Lentils",
		"cal": 465,
		"fat": 23,
		"saturated": 2.9,
		"carbs": 26,
		"sugar": 15,
		"protein": 39,
		"salt": 0.99
	},
	"SAPCanteen20230616ApricotDumplings": {
		"name": "SAP Canteen Apricot Dumplings",
		"cal": 174,
		"fat": 7,
		"saturated": 2.7,
		"carbs": 22,
		"sugar": 10,
		"protein": 5.5,
		"salt": 0.43
	},
	"SAPCanteen20230619BraisedTurkeyMeatball": {
		"name": "SAP Canteen Braised Turkey Meatball",
		"cal": 622,
		"fat": 19.6,
		"saturated": 7.9,
		"carbs": 64.8,
		"sugar": 8.9,
		"protein": 45.1,
		"salt": 4.6
	},
	"SAPCanteen20230619Broccoli": {
		"name": "SAP Canteen Broccoli",
		"cal": 31,
		"fat": 0.2,
		"saturated": 0,
		"carbs": 2.8,
		"sugar": 2.3,
		"protein": 4.1,
		"salt": 0.6
	},
	"SAPCanteen20230619CreamyCurd": {
		"name": "SAP Canteen Creamy Curd Popcorn",
		"cal": 114,
		"fat": 4.1,
		"saturated": 2.3,
		"carbs": 12.7,
		"sugar": 12.7,
		"protein": 6.1,
		"salt": 0.1
	},
	"SAPCanteen20230620ChickenSaltimbocca": {
		"name": "SAP Canteen Chicken Saltimbocca",
		"cal": 862,
		"fat": 28,
		"saturated": 4.2,
		"carbs": 90,
		"sugar": 4.9,
		"protein": 62,
		"salt": 1.2
	},
	"SAPCanteen20230621TurkeySteak": {
		"name": "SAP Canteen Turkey Steak",
		"cal": 692,
		"fat": 14.6,
		"saturated": 6.1,
		"carbs": 84,
		"sugar": 6.3,
		"protein": 54.3,
		"salt": 1.7
	},
	"SAPCanteen20230622VegetableDumplings": {
		"name": "SAP Canteen Vegetable Dumplings",
		"cal": 635,
		"fat": 24,
		"saturated": 10,
		"carbs": 81,
		"sugar": 18,
		"protein": 25,
		"salt": 3.5
	},
	"SAPCanteen20230622Mandelpudding": {
		"name": "SAP Canteen Mandelpudding",
		"cal": 122,
		"fat": 4.2,
		"saturated": 1.2,
		"carbs": 19,
		"sugar": 8.8,
		"protein": 1.4,
		"salt": 0.17
	},
	"SAPCanteen20230623ItalyFish": {
		"name": "SAP Canteen Fish Vegetables Potatoes",
		"cal": 454,
		"fat": 16.2,
		"saturated": 2.4,
		"carbs": 33.3,
		"sugar": 6.5,
		"protein": 41.9,
		"salt": 3
	},
	"SAPCanteen20230623Quinoa": {
		"name": "SAP Canteen Quinoa",
		"cal": 88,
		"fat": 1.5,
		"saturated": 0.1,
		"carbs": 15.5,
		"sugar": 0.5,
		"protein": 3,
		"salt": 0.6
	},
	"SAPCanteen20230626ChickenDoner": {
		"name": "SAP Canteen Chicken Doner",
		"cal": 646,
		"fat": 25.1,
		"saturated": 10.1,
		"carbs": 58,
		"sugar": 9.4,
		"protein": 44.7,
		"salt": 5.7
	},
	"SAPCanteen20230627BreadedChickenSchnitzel": {
		"name": "SAP Canteen Breaded Chicken Schnitzel",
		"cal": 810,
		"fat": 47.3,
		"saturated": 22.3,
		"carbs": 62,
		"sugar": 6.9,
		"protein": 33.8,
		"salt": 2.3
	},
	"SAPCanteen20230628BeefBrisket": {
		"name": "SAP Canteen Beef Brisket",
		"cal": 673,
		"fat": 43.9,
		"saturated": 18.5,
		"carbs": 29.5,
		"sugar": 5,
		"protein": 36.1,
		"salt": 3.4
	},
	"SAPCanteen20230628Chickpeas": {
		"name": "SAP Canteen Chickpeas",
		"cal": 211,
		"fat": 6.3,
		"saturated": 1,
		"carbs": 26.8,
		"sugar": 2.3,
		"protein": 11.1,
		"salt": 1.1
	},
	"SAPCanteen20230630FilletOfRedfish": {
		"name": "SAP Canteen Fillet of redfish",
		"cal": 655,
		"fat": 30.1,
		"saturated": 4,
		"carbs": 57,
		"sugar": 5.2,
		"protein": 38.7,
		"salt": 3.7
	},
	"SAPCanteen20230630Lentils": {
		"name": "SAP Canteen Lentils",
		"cal": 160,
		"fat": 1.3,
		"saturated": 0.1,
		"carbs": 27.1,
		"sugar": 5.8,
		"protein": 10,
		"salt": 2.9
	},
	"SAPCanteen20230703MexicoBurritoBowl": {
		"name": "SAP Canteen Mexico Burrito Bowl",
		"cal": 766,
		"fat": 37.4,
		"saturated": 9.2,
		"carbs": 39.5,
		"sugar": 18.4,
		"protein": 66.6,
		"salt": 0.9
	},
	"SAPCanteen20230704MexicoAlambreDeBistec": {
		"name": "SAP Canteen Mexico Alambre De Bistec",
		"cal": 580,
		"fat": 35.2,
		"saturated": 14.9,
		"carbs": 35.2,
		"sugar": 5.9,
		"protein": 30.4,
		"salt": 2.2
	},
	"SAPCanteen20230706ItaliaChickenSaltimbocca": {
		"name": "SAP Canteen Italia Chicken Saltimbocca",
		"cal": 414,
		"fat": 10.4,
		"saturated": 4,
		"carbs": 33.5,
		"sugar": 3,
		"protein": 44,
		"salt": 2.9
	},
	"SAPCanteen20230706BakedBeans": {
		"name": "SAP Canteen Baked Beans",
		"cal": 149,
		"fat": 8,
		"saturated": 1.2,
		"carbs": 10.4,
		"sugar": 1,
		"protein": 9,
		"salt": 0.7
	},
	"SAPCanteen20230706Broccoli": {
		"name": "SAP Canteen Broccoli",
		"cal": 34,
		"fat": 0.2,
		"saturated": 0,
		"carbs": 3,
		"sugar": 2.5,
		"protein": 4.5,
		"salt": 0.4
	},
	"SAPCanteen20230710SaltimboccaOfChicken": {
		"name": "SAP Canteen Saltimbocca of Chicken",
		"cal": 510,
		"fat": 11.5,
		"saturated": 6,
		"carbs": 67.8,
		"sugar": 4.5,
		"protein": 27.9,
		"salt": 1.6
	},
	"SAPCanteen20230710BroccoliWithAlmonds": {
		"name": "SAP Canteen Broccoli with almonds",
		"cal": 70,
		"fat": 3.7,
		"saturated": 1.5,
		"carbs": 3.5,
		"sugar": 2.9,
		"protein": 5.6,
		"salt": 0.5
	},
	"SAPCanteen20230710Erdbeerquark": {
		"name": "SAP Canteen Erdbeerquark",
		"cal": 84,
		"fat": 1.1,
		"saturated": 0.6,
		"carbs": 9.5,
		"sugar": 9.5,
		"protein": 8.3,
		"salt": 0.1
	},
	"SAPCanteen20230711RedFisch": {
		"name": "SAP Canteen Red Fisch",
		"cal": 556,
		"fat": 24.9,
		"saturated": 9.3,
		"carbs": 54.7,
		"sugar": 6.9,
		"protein": 27.7,
		"salt": 1.2
	},
	"SAPCanteen20230713ChickenBreastViennaArt": {
		"name": "SAP Canteen Chicken Breast Vienna Art",
		"cal": 784,
		"fat": 23,
		"saturated": 5.5,
		"carbs": 103.1,
		"sugar": 4,
		"protein": 39.5,
		"salt": 2.2
	},
	"SAPCanteen20230713PotatoAndChickpeaMash": {
		"name": "SAP Canteen Potato and Chickpea Mash",
		"cal": 113,
		"fat": 3.8,
		"saturated": 2,
		"carbs": 15.2,
		"sugar": 1.7,
		"protein": 4,
		"salt": 0.5
	},
	"SAPCanteen20230714SaitheBouillabaiseBrew": {
		"name": "SAP Canteen Saithe Bouillabaise Brew",
		"cal": 529,
		"fat": 20,
		"saturated": 6.4,
		"carbs": 40,
		"sugar": 4.1,
		"protein": 48,
		"salt": 1.2
	},
	"SAPCanteen20230717ChickenFricass": {
		"name": "SAP Canteen Chicken Fricass",
		"cal": 535,
		"fat": 22,
		"saturated": 9.3,
		"carbs": 49,
		"sugar": 5.3,
		"protein": 35,
		"salt": 0.23
	},
	"SAPCanteen20230718GrilledChickenBreast": {
		"name": "SAP Canteen Grilled Chicken Breast",
		"cal": 494,
		"fat": 8.9,
		"saturated": 4,
		"carbs": 46.3,
		"sugar": 5.8,
		"protein": 53.6,
		"salt": 4
	},
	"SAPCanteen20230719GreenGoddessBowl": {
		"name": "SAP Canteen Green Goddess Bowl",
		"cal": 621,
		"fat": 38.6,
		"saturated": 4.9,
		"carbs": 39.8,
		"sugar": 14.8,
		"protein": 28.5,
		"salt": 3
	},
	"SAPCanteen20230720AfricanPeanutStew": {
		"name": "SAP Canteen African Peanut Stew",
		"cal": 771,
		"fat": 51,
		"saturated": 9.2,
		"carbs": 40,
		"sugar": 13,
		"protein": 38,
		"salt": 1.2
	},
	"SAPCanteen20230720PeachAndCurrantCompote": {
		"name": "SAP Canteen Peach and Currant",
		"cal": 71,
		"fat": 1.7,
		"saturated": 0.4,
		"carbs": 11,
		"sugar": 6.2,
		"protein": 1.6,
		"salt": 0.03
	},
	"SAPCanteen20230721FriedTrout": {
		"name": "SAP Canteen Fried Trout",
		"cal": 371,
		"fat": 15,
		"saturated": 6.1,
		"carbs": 19,
		"sugar": 12,
		"protein": 40,
		"salt": 0.35
	},
	"SAPCanteen20230721ApricotDumplings": {
		"name": "SAP Canteen Apricot Dumplings",
		"cal": 174,
		"fat": 7,
		"saturated": 2.7,
		"carbs": 22,
		"sugar": 10,
		"protein": 5.5,
		"salt": 0.43
	},
	"SAPCanteen20230724FriedSquid": {
		"name": "SAP Canteen Fried Squid",
		"cal": 560,
		"fat": 43.3,
		"saturated": 4.5,
		"carbs": 15.5,
		"sugar": 12.2,
		"protein": 27,
		"salt": 1.7
	},
	"SAPCanteen20230725FishThaiCurry": {
		"name": "SAP Canteen Fish Thai Curry",
		"cal": 317,
		"fat": 4.7,
		"saturated": 1.3,
		"carbs": 47,
		"sugar": 9.5,
		"protein": 20,
		"salt": 0.91
	},
	"SAPCanteen20230725MelonYoghurt": {
		"name": "SAP Canteen Melon Yoghurt",
		"cal": 128,
		"fat": 4.7,
		"saturated": 2.3,
		"carbs": 14,
		"sugar": 9.6,
		"protein": 6.1,
		"salt": 0.1
	},
	"SAPCanteen20230726JapanBeefTeriyaki": {
		"name": "SAP Canteen Japan Beef Teriyaki",
		"cal": 413,
		"fat": 5,
		"saturated": 0.1,
		"carbs": 60.9,
		"sugar": 5,
		"protein": 28.6,
		"salt": 1.3
	},
	"SAPCanteen20230726Strawberrycream": {
		"name": "SAP Canteen Strawberrycream",
		"cal": 173,
		"fat": 11.3,
		"saturated": 3.9,
		"carbs": 13.5,
		"sugar": 12.3,
		"protein": 3.9,
		"salt": 0.2
	},
	"SAPCanteen20230727BeefLambCevapcici": {
		"name": "SAP Canteen Beef & Lamb Cevapcici",
		"cal": 681,
		"fat": 43,
		"saturated": 11,
		"carbs": 41,
		"sugar": 13,
		"protein": 34,
		"salt": 0.97
	},
	"SAPCanteen20230728PastaTomatoCreamSauceSalmon": {
		"name": "SAP Canteen Pasta Tomatt Cream Sauce Salmon",
		"cal": 878,
		"fat": 28,
		"saturated": 6.9,
		"carbs": 114,
		"sugar": 7.8,
		"protein": 41,
		"salt": 1.7
	},
	"SAPCanteen20230731ChickenBreastStalida": {
		"name": "SAP Canteen Chicken Breast Stalida",
		"cal": 578,
		"fat": 16.6,
		"saturated": 4.4,
		"carbs": 52.8,
		"sugar": 3.8,
		"protein": 53.1,
		"salt": 2.6
	},
	"SAPCanteen20230801SteakFromTurkey": {
		"name": "SAP Canteen Steak from Turkey",
		"cal": 616,
		"fat": 26,
		"saturated": 7.1,
		"carbs": 43,
		"sugar": 8.4,
		"protein": 53,
		"salt": 0.38
	},
	"SAPCanteen20230802FilletOfHoki": {
		"name": "SAP Canteen Fillet of Hoki",
		"cal": 473,
		"fat": 9.5,
		"saturated": 4,
		"carbs": 59.9,
		"sugar": 6.2,
		"protein": 35.4,
		"salt": 3.6
	},
	"SAPCanteen20230803BeefRedCurry": {
		"name": "SAP Canteen Beef Red Curry",
		"cal": 573,
		"fat": 16.4,
		"saturated": 6.6,
		"carbs": 64.3,
		"sugar": 8.2,
		"protein": 38.4,
		"salt": 1.7
	},
	"SAPCanteen20230804TuerkeiKebabRindBulgur": {
		"name": "SAP Canteen Kebab Rind Bulgur",
		"cal": 702,
		"fat": 40.2,
		"saturated": 0.2,
		"carbs": 36.8,
		"sugar": 5.3,
		"protein": 47.3,
		"salt": 1
	},
	"SAPCanteen20230807ChickenSaltimbocca": {
		"name": "SAP Canteen Chicken Saltimbocca",
		"cal": 492,
		"fat": 25,
		"saturated": 7.7,
		"carbs": 19,
		"sugar": 4,
		"protein": 48,
		"salt": 1.3
	},
	"SAPCanteen20230808JapanMieNoodlesTurkeyMeat": {
		"name": "SAP Canteen Japan Mie Noodles Turkey Meat",
		"cal": 486,
		"fat": 8,
		"saturated": 2.6,
		"carbs": 57.5,
		"sugar": 13.4,
		"protein": 44,
		"salt": 2
	},
	"SAPCanteen20230809TilapiaCressSauceAsparagus": {
		"name": "SAP Canteen Tilapia Cress-Sauce Asparagus",
		"cal": 466,
		"fat": 8.4,
		"saturated": 4.5,
		"carbs": 57.8,
		"sugar": 4.9,
		"protein": 37.8,
		"salt": 2.8
	},
	"SAPCanteen20230810LambTagine": {
		"name": "SAP Canteen Lamb Tagine",
		"cal": 489,
		"fat": 13,
		"saturated": 2.3,
		"carbs": 67,
		"sugar": 14,
		"protein": 25,
		"salt": 0.39
	},
	"SAPCanteen20230811IndiaFishCurry": {
		"name": "SAP Canteen India Fish-Curry",
		"cal": 654,
		"fat": 26,
		"saturated": 20.7,
		"carbs": 72,
		"sugar": 10.5,
		"protein": 28,
		"salt": 2.3
	},
	"SAPCanteen20230814SwabianPorkRoastWithOnions": {
		"name": "SAP Canteen Swabian Pork Roast With Onions",
		"cal": 556,
		"fat": 15.1,
		"saturated": 4.8,
		"carbs": 54.3,
		"sugar": 7.8,
		"protein": 49.6,
		"salt": 2.2
	},
	"SAPCanteen20230815ChopSueyBasmatiRice": {
		"name": "SAP Canteen Chop Suey Basmati Rice",
		"cal": 520,
		"fat": 17.4,
		"saturated": 4,
		"carbs": 57.1,
		"sugar": 9.1,
		"protein": 32.8,
		"salt": 3
	},
	"SAPCanteen20230816TurkeyChilliBeansCorn": {
		"name": "SAP Canteen Turkey Chilli Beans Corn",
		"cal": 553,
		"fat": 28.8,
		"saturated": 9.8,
		"carbs": 50.8,
		"sugar": 8,
		"protein": 43.6,
		"salt": 3.9
	},
	"SAPCanteen20230816Broccoli": {
		"name": "SAP Canteen Broccoli",
		"cal": 34,
		"fat": 0.2,
		"saturated": 0,
		"carbs": 3,
		"sugar": 2.5,
		"protein": 4.5,
		"salt": 0.4
	},
	"SAPCanteen20230816RaspberryCurd": {
		"name": "SAP Canteen Raspberry Curd",
		"cal": 102,
		"fat": 1.9,
		"saturated": 1,
		"carbs": 12,
		"sugar": 9.6,
		"protein": 7.8,
		"salt": 0.06
	},
	"SAPCanteen20230911HungarySzegedGoulash": {
		"name": "SAP Canteen Hungary Szeged Goulash",
		"cal": 551,
		"fat": 28.2,
		"saturated": 10.5,
		"carbs": 34.2,
		"sugar": 4.2,
		"protein": 38.1,
		"salt": 2.2
	},
	"SAPCanteen20230911KidneyBeans": {
		"name": "SAP Canteen Kidney Beans",
		"cal": 103,
		"fat": 0.6,
		"saturated": 0.1,
		"carbs": 15.1,
		"sugar": 1.3,
		"protein": 8.9,
		"salt": 1.8
	},
	"SAPCanteen20230911ApricotCurd": {
		"name": "SAP Canteen Apricot Curd",
		"cal": 111,
		"fat": 3.1,
		"saturated": 0.3,
		"carbs": 11,
		"sugar": 10.9,
		"protein": 9,
		"salt": 0.1
	},
	"SAPCanteenLegumes": {
		"name": "SAP Canteen Legumes",
		"cal": 336,
		"fat": 1.6,
		"saturated": 0.4,
		"carbs": 50,
		"sugar": 1.1,
		"protein": 23,
		"salt": 0.01
	},
	"SAPCanteenChocolateMousse": {
		"name": "SAP Canteen Chocolate Mousse",
		"cal": 253,
		"fat": 19.22,
		"saturated": 11.07,
		"carbs": 16.52,
		"sugar": 16.17,
		"protein": 3.18,
		"salt": 0.07
	},
	"SAPCanteenEspressoCream": {
		"name": "SAP Canteen Espresso Cream",
		"cal": 118,
		"fat": 6.8,
		"saturated": 6,
		"carbs": 13.1,
		"sugar": 12.8,
		"protein": 0.8,
		"salt": 0.1
	},
	"SeitenbacheriMuesli": {
		"name": "Seitenbacher iMüsli",
		"cal": 394,
		"fat": 10,
		"saturated": 3,
		"carbs": 58,
		"sugar": 16,
		"protein": 13,
		"salt": 0
	},
	"Walnuss": {
		"name": "Walnuss",
		"cal": 654,
		"fat": 65,
		"saturated": 6.1,
		"carbs": 14,
		"sugar": 2.6,
		"protein": 15.2,
		"salt": 0
	},
	"FrostaPfannenFischMuellerinArt": {
		"name": "Frosta Pfannenfisch Müllerin Art",
		"cal": 114,
		"fat": 0.8,
		"saturated": 0.2,
		"carbs": 11.4,
		"sugar": 0.4,
		"protein": 15,
		"salt": 1.39
	},
	"FrostaBackofenFischKnusprigKross": {
		"name": "Frosta Packofen Fisch Knusprig Kriss",
		"cal": 174,
		"fat": 6.7,
		"saturated": 0.9,
		"carbs": 15.2,
		"sugar": 0.7,
		"protein": 13,
		"salt": 1.23
	},
	"FuzeTeaZitrone": {
		"name": "fuzetea Schwarzer Tee Zitrone",
		"cal": 30,
		"fat": 0,
		"saturated": 0,
		"carbs": 7.2,
		"sugar": 7.2,
		"protein": 0,
		"salt": 0.03
	},
	"FuzeTeaPfirsich": {
		"name": "fuzetea Schwarzer Tee Pfirsich",
		"cal": 28,
		"fat": 0,
		"saturated": 0,
		"carbs": 6.7,
		"sugar": 6.5,
		"protein": 0,
		"salt": 0.04
	},
	"GenericEiscafe": {
		"name": "Generischer Eiscafe",
		"cal": 205,
		"fat": 10,
		"saturated": 6,
		"carbs": 25,
		"sugar": 23,
		"protein": 5,
		"salt": 0
	},
	"GenericFlammkuchen": {
		"name": "Generischer Flammkuchen",
		"cal": 202,
		"fat": 10,
		"saturated": 4.2,
		"carbs": 21,
		"sugar": 1.3,
		"protein": 7,
		"salt": 1.5
	},
	"FiveGuysLittleFries": {
		"name": "Five Guys Fries Little",
		"cal": 526,
		"fat": 23,
		"saturated": 4,
		"carbs": 72,
		"sugar": 2,
		"protein": 8,
		"salt": 0.5
	},
	"FiveGuysBun": {
		"name": "Five Guys Bun",
		"cal": 260,
		"fat": 9,
		"saturated": 3.5,
		"carbs": 39,
		"sugar": 8,
		"protein": 7,
		"salt": 0.33
	},
	"FiveGuysHamburgerPatty": {
		"name": "Five Guys Hamburger Patty",
		"cal": 220,
		"fat": 17,
		"saturated": 8,
		"carbs": 0,
		"sugar": 0,
		"protein": 16,
		"salt": 0.05
	},
	"FiveGuysHamburgerCheese": {
		"name": "Five Guys Cheese",
		"cal": 70,
		"fat": 6,
		"saturated": 4,
		"carbs": 0,
		"sugar": 0,
		"protein": 4,
		"salt": 0.36
	},
	"FiveGuysHamburgerLettuce": {
		"name": "Five Guys Lettuce",
		"cal": 4,
		"fat": 0,
		"saturated": 0,
		"carbs": 1,
		"sugar": 0,
		"protein": 4,
		"salt": 0.03
	},
	"FiveGuysHamburgerBacon": {
		"name": "Five Guys Bacon",
		"cal": 80,
		"fat": 7,
		"saturated": 3,
		"carbs": 0,
		"sugar": 0,
		"protein": 4,
		"salt": 0.26
	},
	"FiveGuysHamburgerBarBQSauce": {
		"name": "Five Guys Bar-B-Q Sauce",
		"cal": 60,
		"fat": 0,
		"saturated": 0,
		"carbs": 15,
		"sugar": 10,
		"protein": 0,
		"salt": 0.4
	},
	"FiveGuysHamburgerGrilledOnions": {
		"name": "Five Guys GrilledOnions",
		"cal": 10,
		"fat": 0,
		"saturated": 0,
		"carbs": 2,
		"sugar": 1,
		"protein": 0,
		"salt": 0.01
	},
	"HansImGlueckGeselleSauerteigbroetchen": {
		"name": "Hans im Glück Geselle (Burger, Sauerteigbrötchen, Huhn)",
		"cal": 746.9,
		"fat": 38.7,
		"saturated": 6.8,
		"carbs": 50.8,
		"sugar": 9.7,
		"protein": 47.3,
		"salt": 4.3
	},
	"HansImGlueckKrautknolle": {
		"name": "Hans im Glück Krautknolle (Krautsalat mit Hanfdressing)",
		"cal": 184,
		"fat": 13.8,
		"saturated": 1.2,
		"carbs": 11.7,
		"sugar": 10.4,
		"protein": 2.4,
		"salt": 1.3
	},
	"KajuKatli": {
		"name": "Kaju Katli",
		"cal": 480,
		"fat": 23,
		"saturated": 6,
		"carbs": 58,
		"sugar": 49,
		"protein": 10,
		"salt": 0.02
	},
	"MandysBananaBred": {
		"name": "Banana Bred by Mandy",
		"cal": 29,
		"fat": 1.49,
		"saturated": 0.5, // guestimation
		"carbs": 3.21,
		"sugar": 1.5, // guestimation
		"protein": 0.48,
		"salt": 0
	},
	"MandysZwetschgenkuchen": {
		"name": "Zwetschgenkuchen by Mandy",
		"cal": 306,
		"fat": 13,
		"saturated": 6, // guestimation
		"carbs": 36.9,
		"sugar": 15.4, // guestimation
		"protein": 6.2,
		"salt": 0.5 // guestimation
	},
	"OreoIcrecreamBites": {
		"name": "OREO Ice Cream Bites",
		"cal": 347,
		"fat": 21,
		"saturated": 14,
		"carbs": 35,
		"sugar": 28,
		"protein": 2.9,
		"salt": 0.25
	},
	"KellogsChocoKrispies": {
		"name": "Kellogs Choco Krispies",
		"cal": 377,
		"fat": 2.5,
		"saturated": 0.9,
		"carbs": 75,
		"sugar": 27,
		"protein": 9,
		"salt": 0.78
	},
	"LandliebeFrischeLandmilchFettarm": {
		"name": "Landliebe Frische Landmilch Fettarm",
		"cal": 46,
		"fat": 1.5,
		"saturated": 1,
		"carbs": 4.7,
		"sugar": 4.7,
		"protein": 3.3,
		"salt": 0.1
	},
	"KruemelkandisBraun": {
		"name": "Krümelkandis Braun",
		"cal": 396,
		"fat": 0,
		"saturated": 0,
		"carbs": 99,
		"sugar": 99,
		"protein": 0,
		"salt": 0
	},
	"WeizenmehlTyp405": {
		"name": "Weizenmehl Typ 405",
		"cal": 340,
		"fat": 1,
		"saturated": 0.2,
		"carbs": 71,
		"sugar": 0.5,
		"protein": 9.8,
		"salt": 0.01
	},
	"BensdorfKakao": {
		"name": "Bensdorf Kako",
		"cal": 366,
		"fat": 21,
		"saturated": 13,
		"carbs": 8.9,
		"sugar": 0.6,
		"protein": 20,
		"salt": 0.08
	},
	"ThomyReinesSonnenblumenoel": {
		"name": "Thomy Reines Sonnenblumenöl",
		"cal": 828,
		"fat": 92,
		"saturated": 11.5,
		"carbs": 0,
		"sugar": 0,
		"protein": 0,
		"salt": 0
	},
	"OdenwaldSchattenmorellen": {
		"name": "Odenwald Schattenmorellen",
		"cal": 69,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 15,
		"sugar": 14,
		"protein": 0.6,
		"salt": 0.01
	},
	"AlproNotMilk": {
		"name": "alpro Not Milk",
		"cal": 44,
		"fat": 1.8,
		"saturated": 0.2,
		"carbs": 5.7,
		"sugar": 0,
		"protein": 0.7,
		"salt": 0.12
	},
	"ClassicSchokoChunks": {
		"name": "Classic Schoko Chunks",
		"cal": 522,
		"fat": 37,
		"saturated": 19,
		"carbs": 50,
		"sugar": 47,
		"protein": 6,
		"salt": 0.01
	},
	"NaturallyPamChocolateChips": {
		"name": "naturally PAM Chocolate Chips",
		"cal": 589,
		"fat": 46,
		"saturated": 29,
		"carbs": 27,
		"sugar": 23,
		"protein": 9.2,
		"salt": 0.11
	},
	"MestemacherWestfaelischerPumpernickel": {
		"name": "Mestemacher Westfälischer Pumpernickel",
		"cal": 199,
		"fat": 1.4,
		"saturated": 0.3,
		"carbs": 35.6,
		"sugar": 8.1,
		"protein": 5.2,
		"salt": 1.2
	},
	"WasaRustikalKnaeckebrot": {
		"name": "Wasa Rustikal Knäckebrot",
		"cal": 327,
		"fat": 1.5,
		"saturated": 0.4,
		"carbs": 59.3,
		"sugar": 1.5,
		"protein": 9,
		"salt": 1.25
	},
	"EdekaBioKoernigerFrischkaese": {
		"name": "EDEKA Bio Körniger Frischkäse",
		"cal": 103,
		"fat": 4.5,
		"saturated": 2.9,
		"carbs": 3,
		"sugar": 3,
		"protein": 12.5,
		"salt": 0.7
	},
	"ReweBioKoernigerFrischkaese": {
		"name": "Rewe Bio Körniger Frischkäse",
		"cal": 91,
		"fat": 4,
		"saturated": 2.6,
		"carbs": 1,
		"sugar": 1,
		"protein": 12.7,
		"salt": 0.7
	},
	"HelloFreshPfannkuchenAuflauf": {
		"name": "Hello Fresh Pfannkuchen Auflauf",
		"cal": 129,
		"fat": 6.99,
		"saturated": 3.09,
		"carbs": 9.68,
		"sugar": 2.85,
		"protein": 6.49,
		"salt": 0.598
	},
	"HelloFreshSeehechtOrzoNudelnGurkenSalat": {
		"name": "Hello Fresh Seehecht Orzo Nudeln Gurken Salat",
		"cal": 87,
		"fat": 2.82,
		"saturated": 0.85,
		"carbs": 9.79,
		"sugar": 1.67,
		"protein": 5.44,
		"salt": 0.137
	},
	"MingChuSesamoel": {
		"name": "Ming Chu Sesamöl",
		"cal": 828,
		"fat": 92,
		"saturated": 15.4,
		"carbs": 0,
		"sugar": 0,
		"protein": 0,
		"salt": 0
	},
	"ThomySonnenblumenoel": {
		"name": "Thomy Sonnenblumenöl",
		"cal": 828,
		"fat": 92,
		"saturated": 11.5,
		"carbs": 0,
		"sugar": 0,
		"protein": 0,
		"salt": 0
	},
	"BambooGardenReisessig": {
		"name": "Bamboo Garden Reisessig",
		"cal": 15,
		"fat": 0,
		"saturated": 0,
		"carbs": 0,
		"sugar": 0,
		"protein": 0,
		"salt": 0
	},
	"bioZentraleAhornsirup": {
		"name": "Bio Zentrale Ahornsirup",
		"cal": 270,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 67,
		"sugar": 59,
		"protein": 0.5,
		"salt": 0.03
	},
	"BambooGardenHoisinSauce": {
		"name": "Bamboo Garden Hoisin Sauce",
		"cal": 160,
		"fat": 0.9,
		"saturated": 0.1,
		"carbs": 32.4,
		"sugar": 28.8,
		"protein": 4.1,
		"salt": 4.4
	},
	"Staerke": {
		"name": "Stärke", // https://www.naehrwert-index.de/lebensmittel/staerke/
		"cal": 351,
		"fat": 0.1,
		"saturated": 0,
		"carbs": 85.9,
		"sugar": 0,
		"protein": 0.43,
		"salt": 0
	},
	"EsnDesignerWheyProteinRichChocolate": {
		"name": "ESN Designer Whey Protein Rich Chocolate",
		"cal": 370,
		"fat": 3.6,
		"saturated": 1.8,
		"carbs": 5,
		"sugar": 3.2,
		"protein": 78,
		"salt": 1.4
	},
	"EsnDesignerWheyProteinCinnamonCereal": {
		"name": "ESN Designer Whey Protein Cinnamon Cereal",
		"cal": 375,
		"fat": 5.2,
		"saturated": 1.7,
		"carbs": 9.2,
		"sugar": 5.9,
		"protein": 73,
		"salt": 1.3
	},
	"EsnDesignerWheyProteinVanilla": {
		"name": "ESN Designer Whey Protein Vanilla",
		"cal": 372,
		"fat": 3.7,
		"saturated": 1.7,
		"carbs": 6.7,
		"sugar": 4.1,
		"protein": 78,
		"salt": 1.4
	},
	"EsnIsoclearWheyIsolate": {
		"name": "ESN Isoclear Whey Isolate",
		"cal": 348,
		"fat": 0,
		"saturated": 0,
		"carbs": 2.3,
		"sugar": 0,
		"protein": 83,
		"salt": 0.11
	},
	"KoellnMultikornFlocken": {
		"name": "Kölln Multikorn Flocken",
		"cal": 351,
		"fat": 4.7,
		"saturated": 1,
		"carbs": 60,
		"sugar": 2.1,
		"protein": 11,
		"salt": 0
	},
	"KoellnBioDinkelflocken": {
		"name": "Kölln Bio-Dinkelflocken",
		"cal": 346,
		"fat": 2.5,
		"saturated": 0.2,
		"carbs": 63,
		"sugar": 1.7,
		"protein": 14,
		"salt": 0
	},
	"KoroBioHaferflocken": {
		"name": "Koro Bio-Haferflocken",
		"cal": 372,
		"fat": 7,
		"saturated": 1.3,
		"carbs": 59,
		"sugar": 0.7,
		"protein": 14,
		"salt": 0.02
	},
	"KoroBioEiweiss": {
		"name": "Koro Bio Flüssiges Eiweiß",
		"cal": 48,
		"fat": 0.2,
		"saturated": 0,
		"carbs": 0.7,
		"sugar": 0.7,
		"protein": 10.9,
		"salt": 0.42
	},
	"KoroBioDattelHaselnussCreme": {
		"name": "Koro Bio Dattel Haselnuss Creme",
		"cal": 547,
		"fat": 2.0,
		"saturated": 2,
		"carbs": 44,
		"sugar": 40,
		"protein": 7,
		"salt": 0.02
	},
	"TkBioHeidelbeeren": {
		"name": "TK Bio Heidelbereen",
		"cal": 62,
		"fat": 0.6,
		"saturated": 0.2,
		"carbs": 12,
		"sugar": 8.5,
		"protein": 0.4,
		"salt": 0.01
	},
	"TkBioHimbeeren": {
		"name": "TK Bio Himbeeren",
		"cal": 76,
		"fat": 0.7,
		"saturated": 0.2,
		"carbs": 12,
		"sugar": 4.4,
		"protein": 1.2,
		"salt": 0.03
	},
	"KornMuehleButtertoast": {
		"name": "Korn Mühle Buttertoast",
		"cal": 269,
		"fat": 3.8,
		"saturated": 2.5,
		"carbs": 48,
		"sugar": 2.5,
		"protein": 9,
		"salt": 1
	},
	"Apfelsaft": {
		"name": "Apfelsaft",
		"cal": 44,
		"fat": 0,
		"saturated": 0,
		"carbs": 11,
		"sugar": 11,
		"protein": 0,
		"salt": 0
	},
	"PamSchoco": {
		"name": "Pam Schoco",
		"cal": 569,
		"fat": 46,
		"saturated": 29,
		"carbs": 27,
		"sugar": 23,
		"protein": 9.2,
		"salt": 0.11
	},
	"GutUndGuenstigSpeisequark": {
		"name": "Gut & Günstig Speisequark Magerstufe",
		"cal": 68,
		"fat": 0.2,
		"saturated": 0.1,
		"carbs": 4.1,
		"sugar": 4.1,
		"protein": 12,
		"salt": 0.1
	},
	"KoroMandelmusZimtVanille": {
		"name": "Koro Mandelmus Zimt & Vanille",
		"cal": 613,
		"fat": 52,
		"saturated": 4.9,
		"carbs": 9.3,
		"sugar": 9,
		"protein": 24,
		"salt": 0
	},
	"KoroHaselnussmus": {
		"name": "Koro Haselnussmus",
		"cal": 670,
		"fat": 62,
		"saturated": 4.5,
		"carbs": 11,
		"sugar": 7.4,
		"protein": 14,
		"salt": 0
	},
	"KoroBraunesMandelmus": {
		"name": "Koro Braunes Mandelmus",
		"cal": 639,
		"fat": 56,
		"saturated": 5.2,
		"carbs": 4.6,
		"sugar": 4.6,
		"protein": 27,
		"salt": 0
	},
	"ReweBioWeissesMandelmus": {
		"name": "Rewe Bio weißes Mandelmus",
		"cal": 634,
		"fat": 54.7,
		"saturated": 5.1,
		"carbs": 7.3,
		"sugar": 5.2,
		"protein": 23.2,
		"salt": 0.04
	},
	"KoroErdnussmusHaselnussKakao": {
		"name": "Koro Erdnussmus Haselnuss-Kakao",
		"cal": 605,
		"fat": 48,
		"saturated": 5.6,
		"carbs": 16,
		"sugar": 13,
		"protein": 23,
		"salt": 0
	},
	"Smarties": {
		"name": "Smarties",
		"cal": 471,
		"fat": 18.8,
		"saturated": 11.4,
		"carbs": 67.8,
		"sugar": 62.5,
		"protein": 6.3,
		"salt": 0.12
	},
	"EdekaBioMyVeggieMandelmus": {
		"name": "EDEKA Bio My Veggie Mandelmus",
		"cal": 657,
		"fat": 58.2,
		"saturated": 5.5,
		"carbs": 5.1,
		"sugar": 4.9,
		"protein": 24.2,
		"salt": 0.008
	},
	"ReweBioKochschinken": {
		"name": "Rewe Bio Kochschinken",
		"cal": 115,
		"fat": 3,
		"saturated": 1,
		"carbs": 1,
		"sugar": 1,
		"protein": 21,
		"salt": 2
	},
	"EdekaBioGouda": {
		"name": "Edeka Bio Gouda",
		"cal": 353,
		"fat": 28.5,
		"saturated": 19.1,
		"carbs": 0.1,
		"sugar": 0.1,
		"protein": 24,
		"salt": 1.5
	},
	"KnorrCroutinosMitSonnenblumenkernen": {
		"name": "Knorr Croutinos mit Sonnenblumenkernen",
		"cal": 575,
		"fat": 42,
		"saturated": 15,
		"carbs": 36,
		"sugar": 1.3,
		"protein": 11,
		"salt": 1.7
	},
	"FloretteEisbergStreifen": {
		"name": "Florette Eisberg-Streifen Knackig-Süß",
		"cal": 17,
		"fat": 0.5,
		"saturated": 0.3,
		"carbs": 1.6,
		"sugar": 1.6,
		"protein": 0.7,
		"salt": 0.03
	},
	"LowCoJohannisbeerDressing": {
		"name": "Mr. Low & Co Johannisbeer Dressing",
		"cal": 8,
		"fat": 0,
		"saturated": 0,
		"carbs": 0,
		"sugar": 0,
		"protein": 0.5,
		"salt": 1
	},
	"LowCoHonigSenfDressing": {
		"name": "Mr. Low & Co Honig Senf Dressing",
		"cal": 13,
		"fat": 0,
		"saturated": 0,
		"carbs": 1.8,
		"sugar": 0,
		"protein": 0.5,
		"salt": 2.2
	},
	"SchwarzwaldhofBioLandjaeger": {
		"name": "Schwarzwaldhof Bio Landjäger",
		"cal": 495,
		"fat": 43,
		"saturated": 17.5,
		"carbs": 1,
		"sugar": 0.5,
		"protein": 26,
		"salt": 4.2
	},
	"StorckKnoppers": {
		"name": "Storck Knoppers",
		"cal": 548,
		"fat": 33.2,
		"saturated": 18.5,
		"carbs": 51.3,
		"sugar": 34.6,
		"protein": 9.2,
		"salt": 0.56
	},
	"IgloSchlemmerFiletAlaBordelaiseClassic": { // eine Portion zubereitet nach Anleitung im Backofen
		"name": "Iglo Schlemmer Filet ala Bordelaise Classic",
		"cal": 279,
		"fat": 16,
		"saturated": 1.3,
		"carbs": 9.4,
		"sugar": 0.8,
		"protein": 24,
		"salt": 2.2
	},
	"EdekaTkMango": {
		"name": "Edeka Tk Mango",
		"cal": 65,
		"fat": 0.2,
		"saturated": 0.1,
		"carbs": 13.9,
		"sugar": 11.3,
		"protein": 0.5,
		"salt": 0.03
	},
	"NutritionxPulledPorkSandwich": { // https://www.nutritionix.com/food/pulled-pork-sandwiches
		"name": "Nutritionx Pulled Pork Sandwich",
		"cal": 532,
		"fat": 7.5,
		"saturated": 2.2,
		"carbs": 81,
		"sugar": 54,
		"protein": 34,
		"salt": 2.077
	},
	"EdekaProteinWraps": {
		"name": "Edeka Protein Wraps",
		"cal": 319,
		"fat": 8.8,
		"saturated": 1.1,
		"carbs": 38.5,
		"sugar": 3,
		"protein": 18.4,
		"salt": 1.3
	},
	"ItzaProteinWraps": {
		"name": "it za Plain Wraps High Protein",
		"cal": 310,
		"fat": 7.9,
		"saturated": 1.5,
		"carbs": 38,
		"sugar": 2.5,
		"protein": 18,
		"salt": 1
	},
	"HochlandSandwichScheibenCheddar": {
		"name": "HochlandSandwichScheibenCheddar",
		"cal": 337,
		"fat": 28,
		"saturated": 18,
		"carbs": 1.7,
		"sugar": 1.6,
		"protein": 20,
		"salt": 3.2
	},
	"EdekaBioFrischkaese": {
		"name": "EDKEA Bio Frischkäse",
		"cal": 245,
		"fat": 24,
		"saturated": 16,
		"carbs": 2.3,
		"sugar": 2.3,
		"protein": 5,
		"salt": 0.8
	},
	"HummusKichererbsenOlivenoel": {
		"name": "Hummus Kichererbsen Olivenöl",
		"cal": 190,
		"fat": 12,
		"saturated": 2.2,
		"carbs": 8.6,
		"sugar": 1.2,
		"protein": 8.4,
		"salt": 1.1
	},
	"EhrmannHighProteinChocolateMousse": {
		"name": "Ehrmann High Protein Chocolate Mousse",
		"cal": 76,
		"fat": 1.6,
		"saturated": 1.1,
		"carbs": 5.0,
		"sugar": 4.0,
		"protein": 10.0,
		"salt": 0.1
	},
	"EhrmannHighProteinDoubleChoc": {
		"name": "Ehrmann High Protein Double Choc",
		"cal": 81,
		"fat": 2.3,
		"saturated": 2.5,
		"carbs": 5.0,
		"sugar": 4.0,
		"protein": 10.0,
		"salt": 0.12
	},
	"SylterOriginal": {
		"name": "Sylter Salatfrische Das Original",
		"cal": 305,
		"fat": 29,
		"saturated": 2.2,
		"carbs": 9.9,
		"sugar": 8.3,
		"protein": 0.5,
		"salt": 1.4
	},
	"RotePaprika": { // According to 0.9 https://www.nutritionix.com/de/food/rote-paprika
		"name": "Rote Paprika",
		"cal": 28.8,
		"fat": 0.2,
		"saturated": 0,
		"carbs": 6.8,
		"sugar": 4.5,
		"protein": 0.9,
		"salt": 0
	},
	"Kirschen": { // According to 12.2 https://www.nutritionix.com/food/cherry
		"name": "Kirschen",
		"cal": 63.4,
		"fat": 0.2,
		"saturated": 0,
		"carbs": 15.9,
		"sugar": 13.4,
		"protein": 1.1,
		"salt": 0
	},
	"Nektarine": { // According to 0.68 https://www.nutritionix.com/food/nectarine
		"name": "Nektarine",
		"cal": 44.9,
		"fat": 0.3,
		"saturated": 0,
		"carbs": 10.4,
		"sugar": 7.8,
		"protein": 1.1,
		"salt": 0
	},
	"Ananas": { // According to 0.6 https://www.nutritionix.com/food/pineapple
		"name": "Ananas",
		"cal": 49.2,
		"fat": 0.1,
		"saturated": 0,
		"carbs": 13.2,
		"sugar": 9.7,
		"protein": 0.5,
		"salt": 0.001
	},
	"ReweBioFalafelBaellchen": {
		"name": "REWE Bio +vegan Falafel-Bällchen",
		"cal": 213,
		"fat": 10.6,
		"saturated": 1.3,
		"carbs": 17.1,
		"sugar": 1.0,
		"protein": 8.8,
		"salt": 1.32
	},
	"LindtEiscafeSchokolade": {
		"name": "Lindt Eiscafe Schokolade",
		"cal": 579,
		"fat": 38,
		"saturated": 24,
		"carbs": 53,
		"sugar": 53,
		"protein": 6.3,
		"salt": 0.21
	},
	"VayuInstantRicePudding": {
		"name": "VAYU Instant Rice Pudding",
		"cal": 373,
		"fat": 0.7,
		"saturated": 0.1,
		"carbs": 83.1,
		"sugar": 0.2,
		"protein": 7.7,
		"salt": 0.01
	},
	"KoroSojaProteinCrispies": {
		"name": "KoRo Soja Protein Crispies",
		"cal": 362,
		"fat": 1.9,
		"saturated": 0.4,
		"carbs": 28,
		"sugar": 9.1,
		"protein": 58,
		"salt": 2.7
	},
	"Lamm": { // https://www.nutritionix.com/food/lamb 3.5oz 99.2g
		"name": "Lamm",
		"cal": 291.7,
		"fat": 21,
		"saturated": 8.8,
		"carbs": 0,
		"sugar": 0,
		"protein": 24.5,
		"salt": 0.0712
	},
	"TortillaCornWheat": {
		"name": "Tortilla Corn&Wheat",
		"cal": 293,
		"fat": 5.2,
		"saturated": 1.2,
		"carbs": 50,
		"sugar": 3.6,
		"protein": 9.4,
		"salt": 1.1
	},
	"RaeucherTofuSesamMandel": {
		"name": "Räuchertofu Sesam-Mandel",
		"cal": 198,
		"fat": 12,
		"saturated": 1.9,
		"carbs": 1.5,
		"sugar": 0.5,
		"protein": 19,
		"salt": 1.6
	},
	"AlnaturTofuNatur": {
		"name": "Alnatur Tofu Natur",
		"cal": 116,
		"fat": 7.1,
		"saturated": 1.2,
		"carbs": 0.5,
		"sugar": 0.5,
		"protein": 12,
		"salt": 0.01
	},
	"WanKwaiMihoenReisnudeln": {
		"name": "Wan Kwai Mihoen Reisnudeln",
		"cal": 366,
		"fat": 0.8,
		"saturated": 0.2,
		"carbs": 83.4,
		"sugar": 0,
		"protein": 5.8,
		"salt": 0.05
	},
	"RicefieldCuChiReispapier": {
		"name": "Ricefield Cu Chi Reispapier",
		"cal": 336,
		"fat": 0.5,
		"saturated": 0.1,
		"carbs": 82,
		"sugar": 1.5,
		"protein": 0.6,
		"salt": 2.2
	},
	"EdekaBioAhornSirup": {
		"name": "EDEKA Bio Ahornsirup",
		"cal": 350,
		"fat": 0,
		"saturated": 0,
		"carbs": 87.6,
		"sugar": 86.3,
		"protein": 0,
		"salt": 0
	},
	"EdekaRaspelSchokolade": {
		"name": "EDEKA Raspel Schokolade",
		"cal": 518,
		"fat": 32,
		"saturated": 20.2,
		"carbs": 43.2,
		"sugar": 39.2,
		"protein": 8.4,
		"salt": 0.01
	},
	"RamaCremefine": {
		"name": "Rama Cremefine zum Kochen 7% Fett",
		"cal": 90,
		"fat": 7,
		"saturated": 2.5,
		"carbs": 4.5,
		"sugar": 2.5,
		"protein": 1,
		"salt": 0.08
	},
	"DemeterCampoVerdeWeizenMehl550": {
		"name": "demeter CampoVerde Weizenmehl Type 550",
		"cal": 352,
		"fat": 1.1,
		"saturated": 0.17,
		"carbs": 70,
		"sugar": 1.1,
		"protein": 11.6,
		"salt": 0.01
	},
	"AlnaturaRoggenVollkornMehl": {
		"name": "Alnatura Roggen Vollkorn Mehl",
		"cal": 352,
		"fat": 1.7,
		"saturated": 0.2,
		"carbs": 62,
		"sugar": 1.1,
		"protein": 8.4,
		"salt": 0.01
	},
	// HACK
	// Copied manually from recipe "WakeAndBakeWeizenmischbrotMitWalnuessen"
	"WakeAndBakeWeizenmischbrotMitWalnuessen": {
		"name": "Wake & Bake Weizenmischbrot mit Walnüssen",
		"cal": 329.10,
		"fat": 11.84,
		"saturated": 1.15,
		"carbs": 44.46,
		"sugar": 1.12,
		"protein": 9.27,
		"salt": 0.062
	},
}
