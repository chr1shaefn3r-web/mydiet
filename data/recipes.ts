export type Amount = number;
export type RecipeName = string;
export type Recipe = { [key: string]: Amount; };
export type Recipes = { [key: string]: Recipe; };

const nudelnMitTomatensosseV2V3Portions = 4;
const nudelnMitGarnelen = 4;
const griechischerSalatPortions = 3;
const kartoffelGraintPortionen = 4;
const kartoffelGratinHackfleischPortionen = 4;
const massamanCurryPortionen = 4;
const cremeBrulee = 4;

const reisBohnenPaprikaHaenchenbrustPortion = 4;
const reisBohnenPaprikaGarnelenPortion = 4;

const oneSliceOfHerzschlag = 750 / 18;
const twoSliceOfHerzschlag = 2 * oneSliceOfHerzschlag;
const threeSliceOfHerzschlag = 3 * oneSliceOfHerzschlag;
const oneSliceOfKornkraft = 54;

const oneSliceOfToast = 25;

const oneSliceOfGouda = 150 / 8;
const twoSliceOfGouda = 2 * oneSliceOfGouda;
const fourSliceOfGouda = 2 * twoSliceOfGouda;

const oneSliceOfKochschinken = 80 / 4;
const twoSliceOfKochschinken = 2 * oneSliceOfKochschinken;

const oneSliceOfEdekaBioGouda = 150 / 5;

const oneSliceOfPemaEiweissbrot = 500 / 8;

const spirelliBolognese = 4;
const kirchererbsenCurry = 4;
const papaSpezial = 4;
const zwiebelBurger = 2;
const generalTso = 2;

const twentySixMuffins = 26;
const oneCakeSlice = 12;

const oneTablespoon = 15;
const twoTablespoon = oneTablespoon * 2;
const threeTablespoon = oneTablespoon * 3;
const oneTeaspoon = 5;
const twoTeaspoon = oneTeaspoon * 2;

const einEdekaProteinWrap = 40;
const eineScheibeHochlandSandwichScheiben = 150/9;
const einTortillaCornWheat = 336/8;
const einItzaProteinWrap = 320/8;

export const recipes: Recipes = {
	"Muesli": {
		"SeitenbacherDinoMuesli": 100,
		"BeliefMandelMilch": 200
	},
	"MuesliWithSeeds": {
		"SeitenbacherDinoMuesli": 100,
		"BeliefMandelMilch": 200,
		"SeitenbacherBioLeinsaat": 25,
		"SeitenbacherBioChiasamen": 40
	},
	"MuesliDaheim": {
		"SeitenbacherDinoMuesli": 100,
		"BiolandFrischmilch": 200
	},
	"MuesliDaheimHmilch": {
		"SeitenbacherDinoMuesli": 100,
		"EdekaBioHVollmilch": 200
	},
	"MuesliHafterMilch": {
		"SeitenbacherDinoMuesli": 100,
		"BeriefHafterMilch": 200
	},
	"MuesliOatlyHafer": {
		"SeitenbacherDinoMuesli": 100,
		"OatlyHafer": 200
	},
	"MuesliOatlyHaferCalcium": {
		"SeitenbacherDinoMuesli": 100,
		"OatlyHaferCalcium": 200
	},
	"HalbesMuesliOatlyHaferCalcium": {
		"SeitenbacherDinoMuesli": 50,
		"OatlyHaferCalcium": 100
	},
	"HalbesiMuesliOatlyHaferCalcium": {
		"SeitenbacheriMuesli": 50,
		"OatlyHaferCalcium": 100
	},
	"MuesliAlproNotMilk": {
		"SeitenbacherDinoMuesli": 100,
		"AlproNotMilk": 200,
	},
	"NuddelnMitTomatensosse": {
		"BarillaFusilli": 160,
		"jaPassierteTomaten": 160,
		"OroDiParmaTomatenmark": 16,
		"KerrygoldButter": 5
	},
	"NudelnMitTomatensosseV2": {
		"BarillaFusilli": 500/nudelnMitTomatensosseV2V3Portions,
		"OroDiParmaTomatenmark": (200/3)/nudelnMitTomatensosseV2V3Portions,
		"pomitoPassierteTomaten": 500/nudelnMitTomatensosseV2V3Portions,
		"muttiGeschaelteTomaten": 260/nudelnMitTomatensosseV2V3Portions,
		"Olivenoel": 25/nudelnMitTomatensosseV2V3Portions,
		"Karotten": 200/nudelnMitTomatensosseV2V3Portions,
		"Zwiebel": 50/nudelnMitTomatensosseV2V3Portions,
		"KerrygoldButter": 25/nudelnMitTomatensosseV2V3Portions
	},
	"SpiralenNudelnMitTomatensosseV2": {
		"ReweBesteWahlSpiralen": 500/nudelnMitTomatensosseV2V3Portions,
		"OroDiParmaTomatenmark": (200/3)/nudelnMitTomatensosseV2V3Portions,
		"pomitoPassierteTomaten": 500/nudelnMitTomatensosseV2V3Portions,
		"muttiGeschaelteTomaten": 260/nudelnMitTomatensosseV2V3Portions,
		"Olivenoel": 25/nudelnMitTomatensosseV2V3Portions,
		"Karotten": 200/nudelnMitTomatensosseV2V3Portions,
		"Zwiebel": 70/nudelnMitTomatensosseV2V3Portions,
		"KerrygoldButter": 25/nudelnMitTomatensosseV2V3Portions
	},
	"SpiralenNudelnMitTomatensosseV3": {
		"ReweBesteWahlSpiralen": 500/nudelnMitTomatensosseV2V3Portions,
		"OroDiParmaTomatenmark": (200/3)/nudelnMitTomatensosseV2V3Portions,
		"pomitoPassierteTomaten": 500/nudelnMitTomatensosseV2V3Portions,
		"muttiGeschaelteTomaten": 260/nudelnMitTomatensosseV2V3Portions,
		"Olivenoel": 25/nudelnMitTomatensosseV2V3Portions,
		"Karotten": 200/nudelnMitTomatensosseV2V3Portions,
		"Zwiebel": 70/nudelnMitTomatensosseV2V3Portions,
		"ReweBioRinderHackfleisch": 280/nudelnMitTomatensosseV2V3Portions,
		"ReweBesteWahlSchlagsahne": 200/nudelnMitTomatensosseV2V3Portions,
		"KerrygoldButter": 25/nudelnMitTomatensosseV2V3Portions
	},
	"SpiralenNudelnMitTomatensosseV1GemischtesHack": {
		"ReweBesteWahlSpiralen": 500/nudelnMitTomatensosseV2V3Portions,
		"OroDiParmaTomatenmark": (200/3)/nudelnMitTomatensosseV2V3Portions,
		"pomitoPassierteTomaten": 500/nudelnMitTomatensosseV2V3Portions,
		"muttiGeschaelteTomaten": 260/nudelnMitTomatensosseV2V3Portions,
		"Olivenoel": 25/nudelnMitTomatensosseV2V3Portions,
		"Zwiebel": 70/nudelnMitTomatensosseV2V3Portions,
		"ReweBioHackfleischGemischt": 280/nudelnMitTomatensosseV2V3Portions,
		"KerrygoldButter": 25/nudelnMitTomatensosseV2V3Portions
	},
	"NudelnMitGarnelen": {
		"BarillaFusilli": 500/nudelnMitGarnelen,
		"Garnelen": 200/nudelnMitGarnelen,
		"ReweBesteWahlSchlagsahne": 200/nudelnMitGarnelen,
		"Tomate": 200/nudelnMitGarnelen,
		"Spinat": 400/nudelnMitGarnelen,
		"Parmesan": 120/nudelnMitGarnelen,
		"KerrygoldButter": 40/nudelnMitGarnelen
	},
	"NudelnMitRiesenGarnelen": {
		"BarillaFusilli": 500/nudelnMitGarnelen,
		"RiesenGarnelen": 220/nudelnMitGarnelen,
		"LacSchlagsahne": 200/nudelnMitGarnelen,
		"Tomate": 200/nudelnMitGarnelen,
		"Spinat": 400/nudelnMitGarnelen,
		"GranaPadano": 60/nudelnMitGarnelen,
		"ParmigianoReggiano": 60/nudelnMitGarnelen,
		"KerrygoldButter": 40/nudelnMitGarnelen
	},
	"NudelnMitGarnelenLaktosefrei": {
		"BarillaFusilli": 500/nudelnMitGarnelen,
		"Garnelen": 200/nudelnMitGarnelen,
		"SahneMinusL": 200/nudelnMitGarnelen,
		"Tomate": 200/nudelnMitGarnelen,
		"Spinat": 400/nudelnMitGarnelen,
		"Parmesan": 120/nudelnMitGarnelen,
		"KerrygoldButter": 40/nudelnMitGarnelen
	},
	"FrostaHaehnchenCurry": {
		"FrostaHaehnchenCurry": 500,
		"MazolaKeimOel": 20
	},
	"EinFrostaMexicanStyleChicken": {
		"FrostaMexicanStyleChicken": 500,
		"MazolaKeimOel": 20
	},
	"EinFrostaGemuesePfanneCurryKokos": {
		"FrostaGemuesePfanneCurryKokos": 480,
		"MazolaKeimOel": 20
	},
	"EinFrostaGemuesePfanneAsiaCurry": {
		"FrostaGemuesePfanneAsiaCurry": 480,
		"MazolaKeimOel": 20
	},
	"EinFrostaGemuesePfanneSommergarten": {
		"FrostaGemuesePfanneSommergarten": 480,
		"MazolaKeimOel": 20
	},
	"EinFrostaGemuesePfanneProvence": {
		"FrostaGemuesePfanneProvence": 480,
		"MazolaKeimOel": 20
	},
	"EinFrostaGemuesePfanneMediterranea": {
		"FrostaGemuesePfanneMediterranea": 480,
		"MazolaKeimOel": 20
	},
	"EinFrostaGemuesePfanneToscana": {
		"FrostaGemuesePfanneToscana": 480,
		"MazolaKeimOel": 20
	},
	"BananeMagerQuarkEiweissSmoothy": {
		"ReweMagerQuark": 250,
		"BerchtesgadenerLandVollmilch": 150,
		"ReweSauerKirschNektar": 100,
		"SeitenbacherBioLeinsaat": 50,
		"Ei": 50,
		"Banane": 80
	},
	"BananeMagerQuarkEiweissSmoothyArlaMilch": {
		"ReweMagerQuark": 250,
		"arlaLaktoseFrei": 150,
		"ReweSauerKirschNektar": 100,
		"SeitenbacherBioLeinsaat": 50,
		"Ei": 50,
		"Banane": 80
	},
	"BananeMagerQuarkEiweissSmoothyLaktoseFrei": {
		"ReweFreiVonMagerQuark": 250,
		"arlaLaktoseFrei": 150,
		"ReweSauerKirschNektar": 100,
		"SeitenbacherBioLeinsaat": 50,
		"Ei": 50,
		"Banane": 80
	},
	"MangoLeinsamenMagerQuarkSmoothy": {
		"ReweMagerQuark": 250,
		"BerchtesgadenerLandVollmilch": 250,
		"SeitenbacherBioLeinsaat": 50,
		"Ei": 50,
		"TkMango": 80
	},
	"MangoLeinsamenMagerQuarkSmoothyArlaMilch": {
		"ReweMagerQuark": 250,
		"arlaLaktoseFrei": 250,
		"SeitenbacherBioLeinsaat": 50,
		"Ei": 50,
		"TkMango": 80
	},
	"MangoLeinsamenMagerQuarkSmoothyLaktoseFrei": {
		"ReweFreiVonMagerQuark": 250,
		"arlaLaktoseFrei": 250,
		"SeitenbacherBioLeinsaat": 50,
		"Ei": 50,
		"TkMango": 80
	},
	"HeidelbeerenLeinsamenMagerQuarkSmoothy": {
		"ReweMagerQuark": 250,
		"BerchtesgadenerLandVollmilch": 250,
		"SeitenbacherBioLeinsaat": 50,
		"Ei": 50,
		"TkHeidelbeeren": 80
	},
	"ApfelZimtLeinsamenMagerQuarkSmoothy": {
		"ReweMagerQuark": 250,
		"BerchtesgadenerLandVollmilch": 250,
		"SeitenbacherBioLeinsaat": 50,
		"Ei": 50,
		"Apfel": 100
	},
	"PfirsichLeinsamenMagerQuarkSmoothy": {
		"ReweMagerQuark": 250,
		"BerchtesgadenerLandVollmilch": 250,
		"SeitenbacherBioLeinsaat": 50,
		"Ei": 50,
		"Pfirsich": 120
	},
	"MangoLeinsamenMagerQuarkFettarmeMilchSmoothy": {
		"ReweMagerQuark": 250,
		"BerchtesgadenerLandFettarmemilch": 250,
		"SeitenbacherBioLeinsaat": 50,
		"Ei": 50,
		"TkMango": 80
	},
	"ZAPProteinShakePro80": {
		"ActivePro80Chocolate": 50,
		"BerchtesgadenerLandFettarmemilch": 500
	},
	"Ruehrei": {
		"Olivenoel": 5,
		"GoertzHerzschlag": twoSliceOfHerzschlag,
		"Ei": 220,
		"BerchtesgadenerLandVollmilch": 100,
		"KerrygoldButter": 25
	},
	"RuehreiArlaMilch": {
		"Olivenoel": 5,
		"GoertzHerzschlag": twoSliceOfHerzschlag,
		"Ei": 220,
		"arlaLaktoseFrei": 100,
		"KerrygoldButter": 25
	},
	"HalbesRuehreiArlaMilch": {
		"Olivenoel": 5,
		"GoertzHerzschlag": oneSliceOfHerzschlag,
		"Ei": 110,
		"arlaLaktoseFrei": 100,
		"KerrygoldButter": 15
	},
	"RuehreiRaw": {
		"Olivenoel": 5,
		"Ei": 220,
		"BerchtesgadenerLandVollmilch": 100,
		"KerrygoldButter": 10
	},
	"RuehreiV2": {
		"Olivenoel": 5,
		"GoertzHerzschlag": twoSliceOfHerzschlag,
		"Ei": 210,
		"KerrygoldButter": 25
	},
	"HalbesRuehreiV2Kornkraft": {
		"Olivenoel": 5,
		"GoertzKornkraft": oneSliceOfKornkraft + oneSliceOfKornkraft,
		"Ei": 105,
		"KerrygoldButter": 25
	},
	"HalbesRuehreiV2": {
		"Olivenoel": 5,
		"GoertzHerzschlag": oneSliceOfHerzschlag,
		"Ei": 105,
		"KerrygoldButter": 10
	},
	"RuehreiV2Raw": {
		"Olivenoel": 5,
		"Ei": 210,
		"KerrygoldButter": 5
	},
	"GriechischerSalat": {
		"Olivenoel": 20/griechischerSalatPortions,
		"AltmeisterKlassikEssig": 16,
		"Maggi": 2,
		"Tomate": 80,
		"Gurke": 95,
		"Zwiebel": 30,
		"PatrosNatur": 60,
		"PatrosFeta": 50,
		"GoertzHerzschlag": threeSliceOfHerzschlag
	},
	"OatmealHimbeere": {
		"SeitenbacherProteinPorridge": 60,
		"SeitenbacherBioChiasamen": 10,
		"TkHimbeeren": 40
	},
	"SmallOatmealHimbeereV2": {
		"SeitenbacherProteinPorridge": 50,
		"SeitenbacherBioChiasamen": 10,
		"TkHimbeeren": 25,
		"Zucker": 5
	},
	"BigOatmealHimbeere": {
		"SeitenbacherProteinPorridge": 100,
		"SeitenbacherBioChiasamen": 20,
		"TkHimbeeren": 50
	},
	"BigOatmealHimbeereV2": {
		"SeitenbacherProteinPorridge": 100,
		"SeitenbacherBioChiasamen": 20,
		"TkHimbeeren": 50,
		"Zucker": 10
	},
	"SmallOatmealSchokolade": {
		"SeitenbacherProteinPorridgeSchokolade": 50,
		"SeitenbacherBioChiasamen": 10,
		"jaMandeln": 10
	},
	"OatmealSchokolade": {
		"SeitenbacherProteinPorridgeSchokolade": 60,
		"SeitenbacherBioChiasamen": 10,
		"jaMandeln": 10
	},
	"BigOatmealSchokolade": {
		"SeitenbacherProteinPorridgeSchokolade": 100,
		"SeitenbacherBioChiasamen": 20,
		"jaMandeln": 20
	},
	"HalfBioBierschinkenbrot": {
		"GoertzHerzschlag": twoSliceOfHerzschlag,
		"BioBierschinken": 100,
		"Cornichons": 50
	},
	"BioBierschinkenbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"BioBierschinken": 200,
		"Cornichons": 95
	},
	"BioBierwurstbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"BioBierwurst": 200,
		"Cornichons": 95
	},
	"BioRindfleischbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"BioRindfleisch": 200,
		"Cornichons": 95
	},
	"BioLammbratwurstbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"BioLammbratwurstMitRindfleisch": 200,
		"Cornichons": 95
	},
	"BioRindswurstbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"BioRindswurst": 200,
		"Cornichons": 95
	},
	"BioSchinkenwurstbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"BioSchinkenwurst": 200,
		"Cornichons": 95
	},
	"BioLeberwurstbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"BioLeberwurst": 200,
		"Cornichons": 95
	},
	"BioLyonerwurstbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"BioLyoner": 200,
		"Cornichons": 95
	},
	"Lammbratwurstbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"Lammbratwurst": 200,
		"Cornichons": 95
	},
	"BioBratwurstbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"BioBratwurstGrob": 200,
		"Cornichons": 95
	},
	"LandmetzgereiSpringerBierschinkenBrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"LandmetzgereiSpringerBierschinken": 190,
		"Cornichons": 95
	},
	"RehmSchinkenwurstBrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"RehmSchinkenwurst": 200,
		"Cornichons": 95
	},
	"RehmBierschinkenBrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"RehmBierschinken": 200,
		"Cornichons": 95
	},
	"DemeterLeberwurstbrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"DemeterLeberwurst": 200,
		"Cornichons": 95
	},
	"LandwurstBratwurstGrobBrot": {
		"GoertzHerzschlag": threeSliceOfHerzschlag,
		"LandwurstBratwurstGrob": 200,
		"Cornichons": 95
	},
	"Piccolinis": {
		"WagnerPiccolinis": 4*90
	},
	"SmallPiccolinis": {
		"WagnerPiccolinis": 3*90
	},
	"HalfPiccolinis": {
		"WagnerPiccolinis": 2*90
	},
	"FrootLoops": {
		"KelloggsFrootLoops": 50,
		"BerchtesgadenerLandVollmilch": 100
	},
	"FrootLoopsArlaMilch": {
		"KelloggsFrootLoops": 50,
		"arlaLaktoseFrei": 100
	},
	"ChocoKrispiesFettarmeMilch": {
		"KellogsChocoKrispies": 50,
		"LandliebeFrischeLandmilchFettarm": 100
	},
	"ChocoKrispies100FettarmeMilch": {
		"KellogsChocoKrispies": 100,
		"LandliebeFrischeLandmilchFettarm": 100
	},
	"Marmeladenbrot": {
		"GoertzHerzschlag": oneSliceOfHerzschlag,
		"KerrygoldButter": 10,
		"Marmelade": 20
	},
	"MarmeladenEiweissbrot": {
		"PemaEiweissBrot": oneSliceOfPemaEiweissbrot,
		"KerrygoldButter": 10,
		"Marmelade": 20
	},
	"PeanutButter40Jelly10Brot": {
		"GoertzHerzschlag": oneSliceOfHerzschlag,
		"ZentisErdnussButter": 40,
		"GlueckPassiertHimbeerMarmelade": 10
	},
	"PeanutButter40Jelly10Sandwich": {
		"KornMuehleButtertoast": 2 * oneSliceOfToast,
		"ZentisErdnussButter": 40,
		"GlueckPassiertHimbeerMarmelade": 10
	},
	"PeanutButter30Jelly20Brot": {
		"GoertzHerzschlag": oneSliceOfHerzschlag,
		"ZentisErdnussButter": 30,
		"GlueckPassiertHimbeerMarmelade": 20
	},
	"NutellaBrot": {
		"GoertzDinkel": 45,
		"FerreroNutella": 15
	},
	"EinMagnumDoubleChocolate": {
		"MagnumDoubleChocolate": 74
	},
	"EinMagnumMandel": {
		"MagnumMandel": 82
	},
	"EinMagnumCollectionChocolateCrunchyCookies": {
		"MagnumCollectionChocolateCrunchyCookies": 90,
	},
	"EinMoevenpickWalnussWaffel": {
		"MoevenpickWalnussWaffel": 74
	},
	"EinFerreroRocher": {
		"FerreroRocher": 12.5
	},
	"EinDuplo": {
		"FerreroDuplo": 18.2
	},
	"EinKinderRiegel": {
		"kinderRiegel": 21
	},
	"EineBanane": {
		"Banane": 80
	},
	"EineKiwi": {
		"Kiwi": 70
	},
	"EinGlasCola": {
		"CocaCola": 200
	},
	"EineKleineCola": {
		"CocaCola": 300
	},
	"EinLiterCola": {
		"CocaCola": 1000
	},
	"EinHalberLiterCola": {
		"CocaCola": 500
	},
	"EineKleineKarotte": {
		"Karotten": 50
	},
	"EineKarotte": {
		"Karotten": 100
	},
	"EinLindtHelloCrunchyNougat": {
		"LindtHelloCrunchyNougat": 39
	},
	"KartoffelGratinBroccoliDaheim": {
		"Kartoffel": 600/kartoffelGraintPortionen,
		"ReweBesteWahlSchlagsahne": 30/kartoffelGraintPortionen,
		"FinelloPastakaese": 180/kartoffelGraintPortionen,
		"GutUndGuenstigTkBroccoli": 350/kartoffelGraintPortionen
	},
	"EinRinderSteak": {
		"Rindfleisch": 200
	},
	"KartoffelGratinBroccoliDoubleCheese": {
		"Kartoffel": 600/kartoffelGratinHackfleischPortionen,
		"ReweBesteWahlSchlagsahne": 30/kartoffelGratinHackfleischPortionen,
		"FinelloPastakaese": 180/kartoffelGratinHackfleischPortionen,
		"cnTkBroccoli": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 15/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 400/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 80/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 25/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinBroccoli": {
		"Kartoffel": 600/kartoffelGratinHackfleischPortionen,
		"ReweBesteWahlSchlagsahne": 50/kartoffelGratinHackfleischPortionen,
		"FinelloGratinkaese": 90/kartoffelGratinHackfleischPortionen,
		"cnTkBroccoli": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 400/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinBroccoliNoCheese": {
		"Kartoffel": 600/kartoffelGratinHackfleischPortionen,
		"ReweBesteWahlSchlagsahne": 30/kartoffelGratinHackfleischPortionen,
		"cnTkBroccoli": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 400/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinKaisergemuese": {
		"Kartoffel": 600/kartoffelGratinHackfleischPortionen,
		"ReweBesteWahlSchlagsahne": 30/kartoffelGratinHackfleischPortionen,
		"FinelloGratinkaese": 90/kartoffelGratinHackfleischPortionen,
		"cnTkKaisergemuese": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 400/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinKaisergemueseJaKaese": {
		"Kartoffel": 600/kartoffelGratinHackfleischPortionen,
		"ReweBesteWahlSchlagsahne": 50/kartoffelGratinHackfleischPortionen,
		"JaGratinKaese": 100/kartoffelGratinHackfleischPortionen,
		"cnTkKaisergemuese": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 400/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen,
		"MaggiKartoffelGratin": 43/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinKaisergemueseLaktosefrei": {
		"Kartoffel": 850/kartoffelGratinHackfleischPortionen,
		"LacSchlagsahne": 50/kartoffelGratinHackfleischPortionen,
		"MinusLEmmentaler": 75/kartoffelGratinHackfleischPortionen,
		"jaTkKaisergemuese": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 400/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen,
		"MaggiKartoffelGratin": 43/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinKaisergemueseLaktosefreiVielSahne": {
		"Kartoffel": 850/kartoffelGratinHackfleischPortionen, //2kg Sack
		"LacSchlagsahne": 200/kartoffelGratinHackfleischPortionen,
		"MinusLEmmentaler": 75/kartoffelGratinHackfleischPortionen,
		"jaTkKaisergemuese": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 400/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen,
		"MaggiKartoffelGratin": 43/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinKaisergemueseLaktosefreiVielSahneMehrKartoffel": {
		"Kartoffel": 1000/kartoffelGratinHackfleischPortionen, //2.5kg Sack
		"LacSchlagsahne": 200/kartoffelGratinHackfleischPortionen,
		"MinusLEmmentaler": 75/kartoffelGratinHackfleischPortionen,
		"jaTkKaisergemuese": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 400/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen,
		"MaggiKartoffelGratin": 43/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinKaisergemueseLaktosefreiDoppeltHack": {
		"Kartoffel": 850/kartoffelGratinHackfleischPortionen, //2kg Sack
		"LacSchlagsahne": 50/kartoffelGratinHackfleischPortionen,
		"MinusLEmmentaler": 75/kartoffelGratinHackfleischPortionen,
		"jaTkKaisergemuese": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 800/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen,
		"MaggiKartoffelGratin": 43/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinBroccoliLaktosefreiDoppeltHack": {
		"Kartoffel": 850/kartoffelGratinHackfleischPortionen, //2kg Sack
		"LacSchlagsahne": 50/kartoffelGratinHackfleischPortionen,
		"MinusLEmmentaler": 75/kartoffelGratinHackfleischPortionen,
		"jaTkKaisergemuese": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 800/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen,
		"MaggiKartoffelGratin": 43/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinKaisergemueseLaktosefreiDoppeltHackGemischt": {
		"Kartoffel": 850/kartoffelGratinHackfleischPortionen, //2kg Sack
		"LacSchlagsahne": 50/kartoffelGratinHackfleischPortionen,
		"MinusLEmmentaler": 75/kartoffelGratinHackfleischPortionen,
		"jaTkBroccoli": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioHackfleischGemischt": 800/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen,
		"MaggiKartoffelGratin": 43/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinKaisergemueseLaktosefreiHackGemischt": {
		"Kartoffel": 850/kartoffelGratinHackfleischPortionen, //2kg Sack
		"LacSchlagsahne": 50/kartoffelGratinHackfleischPortionen,
		"MinusLEmmentaler": 75/kartoffelGratinHackfleischPortionen,
		"jaTkBroccoli": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioHackfleischGemischt": 400/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen,
		"MaggiKartoffelGratin": 43/kartoffelGratinHackfleischPortionen
	},
	"KartoffelGratinKaisergemueseLaktose": {
		"Kartoffel": 850/kartoffelGratinHackfleischPortionen,
		"LacSchlagsahne": 50/kartoffelGratinHackfleischPortionen,
		"MinusLEmmentaler": 75/kartoffelGratinHackfleischPortionen,
		"jaTkKaisergemuese": 500/kartoffelGratinHackfleischPortionen,
		"KerrygoldButter": 10/kartoffelGratinHackfleischPortionen,
		"ReweBioRinderHackfleisch": 400/kartoffelGratinHackfleischPortionen,
		"Zwiebel": 70/kartoffelGratinHackfleischPortionen,
		"MazolaKeimOel": 15/kartoffelGratinHackfleischPortionen,
		"MaggiKartoffelGratin": 43/kartoffelGratinHackfleischPortionen
	},
	"EineTasseCappuccino": {
		"Cappuccino": 100,
		"Zucker": 15
	},
	"EinBadLiebenzellerApfelsaftSchorle": {
		"BadLiebenzellerApfelsaftSchorle": 250
	},
	"DreiBadLiebenzellerApfelsaftSchorle": {
		"BadLiebenzellerApfelsaftSchorle": 750
	},
	"GanzesSubwayChickenTeriyaki": {
		"SubwayChickenTeriyaki": 430
	},
	"EinSeitenbacherProteinRiegelVanille": {
		"SeitenbacherProteinRiegelVanille": 60
	},
	"EinSeitenbacherProteinRiegelCappuccino": {
		"SeitenbacherProteinRiegelCappuccino": 60
	},
	"EinSeitenbacherProteinRiegelSchoko": {
		"SeitenbacherProteinRiegelSchoko": 60
	},
	"EinSeitenbacherProteinRiegelKakao": {
		"SeitenbacherProteinRiegelKakao": 55
	},
	"EinSeitenbacherEnergieRiegel": {
		"SeitenbacherEnergieRiegel": 50
	},
	"EinWhiteRussian": {
		"BerchtesgadenerLandVollmilch": 150,
		"Vodka": 15,
		"Kahlua": 25
	},
	"EinWhiteRussianArlaMilch": {
		"arlaLaktoseFrei": 150,
		"Vodka": 15,
		"Kahlua": 25
	},
	"EinMuffinMarenKirschSchoko": {
		"MuffinMarenKirschSchoko": 100
	},
	"OnePieceDanaApfelWalnussKuchen": {
		"Walnuss": 150/oneCakeSlice,
		"Apfel": 1000/oneCakeSlice,
		"KerrygoldButter": 205/oneCakeSlice,
		"Zucker": 120/oneCakeSlice,
		"Ei": 100/oneCakeSlice,
		"WeizenmehlTyp405": 320/oneCakeSlice,
		"KruemelkandisBraun": 40/oneCakeSlice,
		"Honig": 50/oneCakeSlice,
		"FrischeSchlagsahne": 80/oneCakeSlice,
	},
	"EineKugelSchokoladenEisInDerWaffel": {
		"LandliebeVollmilchSchokoladenEis": 60,
		"GelatelliEiswaffel": 15
	},
	"EineKleinePopcorn": {
		"AirPoppedPopcorn": 80
	},
	"EineMittlerePopcorn": {
		"AirPoppedPopcorn": 160
	},
	"EineMuellermilchProteinVanille": {
		"MuellermilchProteinVanille": 400
	},
	"EineMuellermilchProteinSchoko": {
		"MuellermilchProteinSchoko": 400
	},
	"EineMuellermilchProteinBanane": {
		"MuellermilchProteinBanane": 400
	},
	"EinCaffeFreddoCappuccino": {
		"CaffeFreddoCappuccino": 200
	},
	"EinStarbucksCappuccino": {
		"StarbucksCappuccino": 220
	},
	"ReisBohnenPaprikaHaenchenbrust": {
		"JasminReis": 250/reisBohnenPaprikaHaenchenbrustPortion,
		"Haehnchenbrust": 350/reisBohnenPaprikaHaenchenbrustPortion,
		"SojaSauce": 75/reisBohnenPaprikaHaenchenbrustPortion,
		"Bohnen": 400/reisBohnenPaprikaHaenchenbrustPortion,
		"Paprika": 200/reisBohnenPaprikaHaenchenbrustPortion,
		"Ingwer": 30/reisBohnenPaprikaHaenchenbrustPortion,
		"MazolaKeimOel": 25/reisBohnenPaprikaHaenchenbrustPortion,
		"Zucker": 15/reisBohnenPaprikaHaenchenbrustPortion
	},
	"ReisBohnenPaprikaDoppeltHaenchenbrust": {
		"JasminReis": 250/reisBohnenPaprikaHaenchenbrustPortion,
		"Haehnchenbrust": 650/reisBohnenPaprikaHaenchenbrustPortion,
		"SojaSauce": 75/reisBohnenPaprikaHaenchenbrustPortion,
		"Bohnen": 400/reisBohnenPaprikaHaenchenbrustPortion,
		"Paprika": 200/reisBohnenPaprikaHaenchenbrustPortion,
		"Ingwer": 30/reisBohnenPaprikaHaenchenbrustPortion,
		"MazolaKeimOel": 25/reisBohnenPaprikaHaenchenbrustPortion,
		"Zucker": 15/reisBohnenPaprikaHaenchenbrustPortion
	},
	"ReisBohnenPaprikaDoppeltPutenschnitzel": {
		"JasminReis": 250/reisBohnenPaprikaHaenchenbrustPortion,
		"Putenschnitzel": 550/reisBohnenPaprikaHaenchenbrustPortion,
		"SojaSauce": 75/reisBohnenPaprikaHaenchenbrustPortion,
		"Bohnen": 400/reisBohnenPaprikaHaenchenbrustPortion,
		"Paprika": 200/reisBohnenPaprikaHaenchenbrustPortion,
		"Ingwer": 30/reisBohnenPaprikaHaenchenbrustPortion,
		"MazolaKeimOel": 25/reisBohnenPaprikaHaenchenbrustPortion,
		"Zucker": 15/reisBohnenPaprikaHaenchenbrustPortion
	},
	"ReisBohnenPaprikaGarnelen": {
		"JasminReis": 250/reisBohnenPaprikaGarnelenPortion,
		"Garnelen": 220/reisBohnenPaprikaGarnelenPortion,
		"SojaSauce": 75/reisBohnenPaprikaGarnelenPortion,
		"fairtradeTeriyaki": 150/reisBohnenPaprikaGarnelenPortion,
		"Bohnen": 400/reisBohnenPaprikaGarnelenPortion,
		"Paprika": 200/reisBohnenPaprikaGarnelenPortion,
		"MazolaKeimOel": 25/reisBohnenPaprikaGarnelenPortion,
		"Karotten": 200/reisBohnenPaprikaGarnelenPortion,
	},
	"ReisBohnenPaprikaDoppeltGarnelen": {
		"JasminReis": 250/reisBohnenPaprikaGarnelenPortion,
		"Garnelen": 440/reisBohnenPaprikaGarnelenPortion,
		"SojaSauce": 75/reisBohnenPaprikaGarnelenPortion,
		"fairtradeTeriyaki": 150/reisBohnenPaprikaGarnelenPortion,
		"Bohnen": 400/reisBohnenPaprikaGarnelenPortion,
		"Paprika": 200/reisBohnenPaprikaGarnelenPortion,
		"MazolaKeimOel": 25/reisBohnenPaprikaGarnelenPortion,
		"Karotten": 200/reisBohnenPaprikaGarnelenPortion,
	},
	"EinChickenCaesarSalad": {
		"ChickenCaesarSalad": 400
	},
	"EineSushiboxAyami": {
		"SushiboxAyami": 340
	},
	"EineSushiboxAyaka": {
		"SushiboxAyaka": 340
	},
	"SpirelliBolognese": {
		"BarillaFusilli": 500/spirelliBolognese,
		"KerrygoldButter": 25/spirelliBolognese,
		"OroDiParmaTomatenmark": 100/spirelliBolognese,
		"pomitoPassierteTomaten": 500/spirelliBolognese,
		"BioRinderFond": 400/spirelliBolognese,
		"BalsamicoEssig": 50/spirelliBolognese,
		"Olivenoel": 50/spirelliBolognese,
		"Karotten": 200/spirelliBolognese,
		"Zwiebel": 60/spirelliBolognese,
		"ReweBioHackfleischGemischt": 300/spirelliBolognese,
		"BerchtesgadenerLandVollmilch": 200/spirelliBolognese,
	},
	"SpirelliBologneseLaktoseFrei": {
		"BarillaFusilli": 500/spirelliBolognese,
		"KerrygoldButter": 25/spirelliBolognese,
		"OroDiParmaTomatenmark": 100/spirelliBolognese,
		"pomitoPassierteTomaten": 500/spirelliBolognese,
		"BioRinderFond": 400/spirelliBolognese,
		"BalsamicoEssig": 50/spirelliBolognese,
		"Olivenoel": 50/spirelliBolognese,
		"Karotten": 200/spirelliBolognese,
		"Zwiebel": 60/spirelliBolognese,
		"ReweBioHackfleischGemischt": 300/spirelliBolognese,
	},
	"SpirelliBologneseLaktoseFreiMehrHackfleisch": {
		"BarillaFusilli": 500/spirelliBolognese,
		"KerrygoldButter": 25/spirelliBolognese,
		"OroDiParmaTomatenmark": 100/spirelliBolognese,
		"pomitoPassierteTomaten": 500/spirelliBolognese,
		"BioRinderFond": 400/spirelliBolognese,
		"BalsamicoEssig": 50/spirelliBolognese,
		"Olivenoel": 50/spirelliBolognese,
		"Karotten": 200/spirelliBolognese,
		"Zwiebel": 60/spirelliBolognese,
		"ReweBioHackfleischGemischt": 400/spirelliBolognese,
	},
	"SpirelliBologneseV2": {
		"BarillaFusilli": 500/spirelliBolognese,
		"KerrygoldButter": 25/spirelliBolognese,
		"OroDiParmaTomatenmark": 100/spirelliBolognese,
		"pomitoPassierteTomaten": 500/spirelliBolognese,
		"BioRinderFond": 400/spirelliBolognese,
		"BalsamicoEssig": 50/spirelliBolognese,
		"Olivenoel": 50/spirelliBolognese,
		"Karotten": 200/spirelliBolognese,
		"Zwiebel": 60/spirelliBolognese,
		"ReweBioRinderHackfleisch": 400/spirelliBolognese,
		"arlaLaktoseFrei": 150/spirelliBolognese,
		"FeineKuecheRinderFond": 500/spirelliBolognese,
	},
	"SpirelliBologneseV2DoppeltHackfleisch": {
		"BarillaFusilli": 500/spirelliBolognese,
		"KerrygoldButter": 25/spirelliBolognese,
		"OroDiParmaTomatenmark": 100/spirelliBolognese,
		"pomitoPassierteTomaten": 500/spirelliBolognese,
		"BalsamicoEssig": 50/spirelliBolognese,
		"Olivenoel": 50/spirelliBolognese,
		"Karotten": 200/spirelliBolognese,
		"Zwiebel": 60/spirelliBolognese,
		"ReweBioRinderHackfleisch": 800/spirelliBolognese,
		"arlaLaktoseFrei": 150/spirelliBolognese,
		"FeineKuecheRinderFond": 500/spirelliBolognese,
	},
	"SpirelliAlBronzoBologneseV3DoppeltHackfleisch": {
		"BarillaAlBronzoFusilli": 400/spirelliBolognese,
		"KerrygoldButter": 25/spirelliBolognese,
		"OroDiParmaTomatenmark": 100/spirelliBolognese,
		"pomitoPassierteTomaten": 500/spirelliBolognese,
		"BalsamicoEssig": 50/spirelliBolognese,
		"Olivenoel": 50/spirelliBolognese,
		"Karotten": 200/spirelliBolognese,
		"Zwiebel": 60/spirelliBolognese,
		"ReweBioRinderHackfleisch": 800/spirelliBolognese,
		"FeineKuecheRinderFond": 500/spirelliBolognese,
	},
	"KirchererbsenCurry": {
		"JasminReis": 250/kirchererbsenCurry,
		"fairtradeKokosmilch": 400/kirchererbsenCurry,
		"Zwiebel": 60/kirchererbsenCurry,
		"Ingwer": 30/kirchererbsenCurry,
		"bonduelleKirchererbsen":(2*265)/kirchererbsenCurry,
		"Haehnchenbrust": 350/kirchererbsenCurry,
	},
	"ProteinPancakes": {
		"bodyAttackProteinPancakeVanilla": 100,
		"arlaLaktoseFrei": 50,
	},
	"ProteinPancakesMitNutella": {
		"bodyAttackProteinPancakeVanilla": 100,
		"arlaLaktoseFrei": 50,
		"FerreroNutella": 100
	},
	"PapaSpezial": {
		"BarillaFusilli": 500/papaSpezial,
		"Ei": (50*(3 * papaSpezial))/papaSpezial,
		"Apfelbrei": 440/papaSpezial,
		"Zucker": (25 * papaSpezial)/papaSpezial,
		"KerrygoldButter": 25/papaSpezial,
		"arlaLaktoseFrei": (50 * papaSpezial)/papaSpezial,
	},
	"PapaSpezialBandnudeln": {
		"TressBandnudeln": 500/papaSpezial,
		"Ei": (50*(3 * papaSpezial))/papaSpezial,
		"Apfelbrei": 440/papaSpezial,
		"Zucker": (25 * papaSpezial)/papaSpezial,
		"KerrygoldButter": 25/papaSpezial,
		"arlaLaktoseFrei": (50 * papaSpezial)/papaSpezial,
	},
	"AvocadoBrot": {
		"GoertzHerzschlag": twoSliceOfHerzschlag,
		"Avocado": 130,
	},
	"Avocado200Brot": {
		"GoertzHerzschlag": twoSliceOfHerzschlag,
		"Avocado": 200,
	},
	"AvocadoPumpernikel": {
		"MestemacherWestfaelischerPumpernickel": 76,
		"Avocado": 130,
	},
	"BodyAttackWheyDeluxeShake": {
		"bodyAttackWheyDeluxe": 30,
		"arlaLaktoseFrei": 300,
	},
	"BodyAttackWheyDeluxeShakeWater": {
		"bodyAttackWheyDeluxe": 30,
	},
	"BodyAttackWheyDeluxeShakeWaterSmall": {
		"bodyAttackWheyDeluxe": 19,
	},
	"BodyAttackIsoWheyShake": {
		"bodyAttackIsoWhey": 30,
		"arlaLaktoseFrei": 300,
	},
	"EineWiltmannSalamissimoMiniSalami": {
		"WiltmannSalamissimoRind": 10,
	},
	"EineWiltmannSalamissimoMiniSalamiPikant": {
		"WiltmannSalamissimoPikant": 10,
	},
	"EineWiltmannSalamissimoMiniSalamiPur": {
		"WiltmannSalamissimoPur": 10,
	},
	"MassamanCurry": {
		"Kartoffel": 850/massamanCurryPortionen,
		"bonduelleKirchererbsen":(2*265)/massamanCurryPortionen,
		"Zwiebel": 50/massamanCurryPortionen,
		"Olivenoel": 20/massamanCurryPortionen,
		"MazolaKeimOel": 5/massamanCurryPortionen,
		"ZentisErdnussButter": (350/2)/massamanCurryPortionen,
		"RealThaiMassamanCurryCookingSauce": (250*2)/massamanCurryPortionen
	},
	"MassamanCurryMehrKartoffel": {
		"Kartoffel": 1000/massamanCurryPortionen,
		"bonduelleKirchererbsen":(2*265)/massamanCurryPortionen,
		"Zwiebel": 50/massamanCurryPortionen,
		"Olivenoel": 20/massamanCurryPortionen,
		"MazolaKeimOel": 5/massamanCurryPortionen,
		"ZentisErdnussButter": (350/2)/massamanCurryPortionen,
		"RealThaiMassamanCurryCookingSauce": (250*2)/massamanCurryPortionen
	},
	"MassamanCurryLight": { // Recipe for two
		"Kartoffel": 100/2,
		"bonduelleKirchererbsen":265/2,
		"Zwiebel": 50/2,
		"Olivenoel": oneTeaspoon/2,
		"MazolaKeimOel": 5/2,
		"ZentisErdnussButter": 40/2,
		"RealThaiMassamanCurryCookingSauce": 250/2
	},
	"ParkkaffeeKaesekuchen": {
		"ZitronenKaesekuchen": 175 // guestimation
	},
	"EinZitronenKaesekuchen": {
		"ZitronenKaesekuchen": 75 // guestimation
	},
	"HalbesJackLinksBeefJerkyOriginal": {
		"JackLinksBeefJerkyOriginal": 35
	},
	"EinBodyAttackProteinCoffee": {
		"BodyAttackProteinCoffee": 250
	},
	"ZwiebelBurger": {
		"ReweBesteWahlSteinofenBurgerBuns": 180/zwiebelBurger,
		"BioRindfleisch": 200/zwiebelBurger,
		"Zwiebel": 50/zwiebelBurger,
		"KerrygoldButter": 40/zwiebelBurger,
		"MazolaKeimOel": 10/zwiebelBurger,
		"ButchersBurgerCheddarCheese": 100/zwiebelBurger,
		"HellmannsMayoMitKnoblauchNote": 40/zwiebelBurger
	},
	"EinYfoodClassicChoco": {
		"yfoodThisIsFoodClassicChoco": 500
	},
	"EinYfoodCherryBanana": {
		"yfoodThisIsFoodCherryBanana": 500
	},
	"EinMcChickenClassic": {
		"McDonaldsMcChickenClassic": 171
	},
	"EineGrosseMcDonaldsPommes": {
		"McDonaldsPommesFrites": 150
	},
	"EineKleineMcDonaldsPommes": {
		"McDonaldsPommesFrites": 80
	},
	"NeunerMcDonaldsMcNuggets": {
		"McDonaldsMcNuggets": 163
	},
	"EinBurgerKingVeganLongChicken": {
		"BurgerKingVeganLongChicken": 210
	},
	"EineKleineBurgerKingPommes": {
		"BurgerKingPommes": 90
	},
	"EinAvocadoBrot": {
		"Avocado": 200,
		"GoertzHerzschlag": twoSliceOfHerzschlag,
	},
	"EinKaesebrot": {
		"ReweBioGouda": fourSliceOfGouda,
		"GoertzHerzschlag": oneSliceOfHerzschlag,
	},
	"EinKaeseSchinkenbrot": {
		"EdekaBioGouda": oneSliceOfEdekaBioGouda,
		"ReweBioKochschinken": twoSliceOfKochschinken,
		"GoertzHerzschlag": oneSliceOfHerzschlag,
	},
	"EinKaeseSchinkenKornkraftbrot": {
		"EdekaBioGouda": oneSliceOfEdekaBioGouda,
		"ReweBioKochschinken": twoSliceOfKochschinken,
		"GoertzKornkraft": oneSliceOfKornkraft,
	},
	"EinFeineLeberwurstBrot": {
		"GoertzHerzschlag": oneSliceOfHerzschlag,
		"FeineLeberwurst": 25,
	},
	"EinSalami30Brot": {
		"ReweBioGefluegelSalami": 30,
		"GoertzHerzschlag": oneSliceOfHerzschlag,
	},
	"EinAndechserNaturBioGoudaBrot": {
		"AndechserNaturBioGouda": 75, // halbe Packung
		"GoertzHerzschlag": oneSliceOfHerzschlag,
	},
	"KleinePortionStudentenfutter": {
		"ClasenBioStudentenfutter": 50
	},
	"KleinePortionMangostreifen": {
		"ClasenBioMangostreifen": 50
	},
	"KleinePortionNussMix": {
		"ClasenNussMix": 50
	},
	"KleinePortionWalnuss": {
		"Walnuss": 50
	},
	"SehrKleinePortionWalnuss": {
		"Walnuss": 25
	},
	"FageHeidelbeeren": {
		"Fage02": 170,
		"Heidelbeeren": 85,
	},
	"SkyrHeidelbeeren": {
		"ExquisaSkyrMild": 200,
		"Heidelbeeren": 85,
	},
	"JaSkyrHeidelbeeren": {
		"JaSkyrNatur": 250,
		"Heidelbeeren": 85,
	},
	"EdekaBioSkyrHeidelbeeren": {
		"EdekaBioSkyr": 250,
		"Heidelbeeren": 85,
	},
	"HalberSkyr": {
		"ExquisaSkyrMild": 200,
	},
	"ViertelLindtGoldhase": {
		"LindtGoldhase": 50,
	},
	"SAPCanteen20220505": {
		"SAPCanteen20220505SignoraItaly": 100,
	},
	"SAPCanteen20220506": {
		"SAPCanteen20220506CalamaresAlaRomana": 100,
	},
	"SAPCanteen20220524": {
		"SAPCanteen20220524PeppersStuffedGroundBeef": 100,
	},
	"SAPCanteen20220622": {
		"SAPCanteen20220622TurkeySteak": 100,
		"SAPCanteen20220622Broccoli": 100,
		"SAPCanteen20220622Bananapudding": 100,
	},
	"SAPCanteen20220624": {
		"SAPCanteen20220624EuropaBowl": 100,
	},
	"SAPCanteen20220629": {
		"SAPCanteen20220629BeefHip": 100,
		"SAPCanteen20220629ChocolatePudding": 100,
		"SAPCanteen20220629Vegetables": 100,
	},
	"SAPCanteen20220704": {
		"SAPCanteen20220704Bowl": 100,
		"SAPCanteen20220704Peas": 100,
		"SAPCanteen20220704Shake": 100,
	},
	"SAPCanteen20220706": {
		"SAPCanteen20220706Turkey": 100,
	},
	"SAPCanteen20220707": {
		"SAPCanteen20220707TexBowl": 100,
	},
	"SAPCanteen20220708": {
		"SAPCanteen20220708AmericanStyleBowl": 100,
	},
	"SAPCanteen20220711": {
		"SAPCanteen20220711SZEGED": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220714": {
		"SAPCanteen20220714Poultry": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220715": {
		"SAPCanteen20220715Wraps": 100,
	},
	"SAPCanteen20220718": {
		"SAPCanteen20220718Coqauvin": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220719": {
		"SAPCanteen20220719PorkTenderloin": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220720": {
		"SAPCanteen20220720SharkCatfish": 100,
		"SAPCanteen20220720DillRice": 100,
		"SAPCanteen20220720PeasCarottsVegetables": 100,
	},
	"SAPCanteen20220721": {
		"SAPCanteen20220721PotatoFrittata": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220722": {
		"SAPCanteen20220722Curry": 100,
	},
	"SAPCanteen20220725": {
		"SAPCanteen20220725BeefHash": 100,
	},
	"SAPCanteen20220726": {
		"SAPCanteen20220726PorkTenderloin": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220727": {
		"SAPCanteen20220727SaladNicoise": 100,
	},
	"SAPCanteen20220728": {
		"SAPCanteen20220728GrilledChickenPaella": 100,
	},
	"SAPCanteen20220801": {
		"SAPCanteen20220801WholemealSpaghetti": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220802": {
		"SAPCanteen20220802PastaStrozzapreti": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220803": {
		"SAPCanteen20220803MushroomRagout": 100,
	},
	"SAPCanteen20220804": {
		"SAPCanteen20220804FriedPastaPockets": 100,
	},
	"SAPCanteen20220805": {
		"SAPCanteen20220805AsianSalad": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220808": {
		"SAPCanteen20220808OsloScandicBurger": 100,
	},
	"SAPCanteen20220809": {
		"SAPCanteen20220809PenneChicken": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220810": {
		"SAPCanteen20220810UmamiOvenEggplant": 100,
	},
	"SAPCanteen20220812": {
		"SAPCanteen20220812GreenPaella": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220815": {
		"SAPCanteen20220815ChickenFricassee": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220816": {
		"SAPCanteen20220816Sauerbraten": 100,
		"SAPCanteenChocolateMousse": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220817": {
		"SAPCanteen20220817PastaRocketGranaPadano": 100,
	},
	"SAPCanteen20220818": {
		"SAPCanteen20220818TandoriChicken": 100,
	},
	"SAPCanteen20220822": {
		"SAPCanteen20220822PorkTenderloin": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220823": {
		"SAPCanteen20220823EuropaBowl": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220824": {
		"SAPCanteen20220824TortelliniCarne": 100,
	},
	"SAPCanteen20220825": {
		"SAPCanteen20220825BakedPotato": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220829": {
		"SAPCanteen20220829CesarSalad": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220830": {
		"SAPCanteen20220830PiccataChickenBreast": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220831": {
		"SAPCanteen20220831Palikaria": 100,
		"SAPCanteen20220831RicePudding": 100,
	},
	"SAPCanteen20220901": {
		"SAPCanteen20220901BeefRissole": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20220902": {
		"SAPCanteen20220902LaCucinaAllora": 100,
	},
	"SAPCanteen202201004": {
		"SAPCanteen20221004ApplePumpkinSoup": 100,
		"SAPCanteen20221004ChickenFricassee": 100,
		"SAPCanteenLegumes": 100,
		"SAPCanteen20221004Smoothie": 100,
	},
	"SAPCanteen20221006": {
		"SAPCanteen20221006RisoniPan": 100,
		"SAPCanteenLegumes": 100,
		"SAPCanteen20221006Dessert": 100,
	},
	"SAPCanteen20221007PollackFillet": {
		"SAPCanteen20221007PollackFillet": 100,
	},
	"SAPCanteen20221010": {
		"SAPCanteen20221010CoqAuVin": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20221011PlantsLandshut": {
		"SAPCanteen20221011PlantsLandshut": 100,
	},
	"SAPCanteen20221012SwabianSouerbraten": {
		"SAPCanteen20221012SwabianSouerbraten": 100,
	},
	"SAPCanteen20221019": {
		"SAPCanteen20221019SwabianStew": 100,
		"SAPCanteen20221019PassionfruitDessert": 100,
	},
	"SAPCanteen20221025": {
		"SAPCanteen20221025BetterGreen": 100,
		"SAPCanteenLegumes": 100,
		"SAPCanteen20221025AppleCrumble": 100,
	},
	"SAPCanteen20221102": {
		"SAPCanteen20221102StuffedEggplant": 100,
		"SAPCanteen20221102PeachSmoothie": 100,
		"SAPCanteen20221102PlumCrumble": 100,
	},
	"SAPCanteen20221103": {
		"SAPCanteen20221103RutabagaStew": 100,
		"SAPCanteen20221103ChocolateMousse": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20221104": {
		"SAPCanteen20221104Jaipur": 100,
		"SAPCanteenLegumes": 100,
		"SAPCanteen20221104Stracciatella": 100,
	},
	"SAPCanteen20221109": {
		"SAPCanteen20221109BurritoDeEspinacha": 100,
		"SAPCanteen20221109Desert": 100,
		"SAPCanteen20221109Smoothie": 100,
	},
	"SAPCanteen20221125": {
		"SAPCanteen20221125TortillaWrap": 100,
		"SAPCanteen20221125OatsCurdApplesSorrel": 100,
		"SAPCanteen20221125Erbsen": 100,
	},
	"SAPCanteen20221207": {
		"SAPCanteen20221207BeefBrisket": 100,
		"SAPCanteen20221207Bandnudeln": 100,
		"SAPCanteen20221207ErbsenKarotten": 100,
		"SAPCanteen20221207Gluehweinmousse": 100,
	},
	"SAPCanteen20221227": {
		"SAPCanteen20221227ChiliConCarne": 100,
		"SAPCanteen20221227FrenchFries": 100,
		"SAPCanteen20221227Dessert": 100,
	},
	"SAPCanteen20230111": {
		"SAPCanteen20221228ChocolatePudding": 100, // estimation for Griess+Kirsche
		"SAPCanteen20230111Goulash": 100,
		"SAPCanteen20230111LeipzigerAllerlei": 100,
	},
	"SAPCanteen20230118": {
		"SAPCanteen20230118VeganKebabPlate": 100,
		"SAPCanteen20230118KohlrabiPeaVegetables": 100,
		"SAPCanteen20230118Churros": 100,
	},
	"SAPCanteen20230208": {
		"SAPCanteen20230208SalmonSteak": 100,
		"SAPCanteen20230207VeganChocolatePudding": 100,
	},
	"SAPCanteen20230209": {
		"SAPCanteen20230209ThailandBeef": 100,
		"SAPCanteen20230209VeganChocolateCream": 100,
	},
	"SAPCanteen20230215": {
		"SAPCanteen20230215SpaghettiGroundBeef": 100,
		"SAPCanteen20230215Cauliflower": 100,
		"SAPCanteen20230215PeachCouscous": 200,
	},
	"SAPCanteen20230216": {
		"SAPCanteen20230216NoMeatBallsSpaghetti": 100,
	},
	"SAPCanteen20230217": {
		"SAPCanteen20230217FilletOfHoki": 100,
		"SAPCanteen20230217SteamPotatos": 200,
		"SAPCanteen20230217Cheesecake": 200,
	},
	"SAPCanteen20230227": {
		"SAPCanteen20230227StuffedPeppers": 100,
		"SAPCanteen20230227ChaiYoghurt": 100,
	},
	"SAPCanteen20230301": {
		"SAPCanteen20230301ChickenBreastVienna": 100,
		"SAPCanteen20230301Vanillecreme": 100,
	},
	"SAPCanteen20230306": {
		"SAPCanteen20230306Bibimbap": 100,
	},
	"SAPCanteen20230310": {
		"SAPCanteen20230310JapanMieNoodles": 100,
		"SAPCanteen20230310GlazedCarrots": 100,
		"SAPCanteen20230310VeganChocolateCream": 100,
		"SAPCanteen20230310Milchreis": 100,
	},
	"SAPCanteen20230315": {
		"SAPCanteen20230315ChickenBreastSweetPotato": 100,
		"SAPCanteen20230315Courgette": 100,
		"SAPCanteen20230315PeachYoghurt": 100,
	},
	"SAPCanteen20230317": {
		"SAPCanteen20230317LinsenBolognese": 100,
		"SAPCanteen20230316AlmondPudding": 100,
	},
	"SAPCanteen20230320": {
		"SAPCanteen20230320MexicoChickenStew": 100,
		"SAPCanteen20230320KohlrabiVegetables": 100,
		"SAPCanteen20230320ApricotCurd": 100,
	},
	"SAPCanteen20230321": {
		"SAPCanteen20230321RoastedStripsOfBeef": 100,
		"SAPCanteen20230321HazelnutCream": 100,
	},
	"SAPCanteen20230324": {
		"SAPCanteen20230324CarrotSoup": 100,
		"SAPCanteen20230324Zander": 100,
		"SAPCanteen20230323CottageCheeseAndBerryCream": 100,
		"SAPCanteen20230324BananaYogurt": 100,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20230327": {
		"SAPCanteen20230327Harira": 100,
		"SAPCanteen20230327Gulasch": 100,
		"SAPCanteen20230327KaffeeRicottaCreme": 200,
		"SAPCanteenLegumes": 100,
	},
	"SAPCanteen20230328": {
		"SAPCanteen20230328Maishaehnchenbrust": 100,
		"SAPCanteen20230327KaffeeRicottaCreme": 100,
		"SAPCanteen20230328SourCreamBlueberry": 100,
	},
	"SAPCanteen20230331": {
		"SAPCanteen20230331Redfish": 100,
		"SAPCanteen20230331MashedPotatoes": 100,
		"SAPCanteen20230331Beans": 100,
		"SAPCanteen20230330VeganRicePudding": 200,
	},
	"SAPCanteen20230403": {
		"SAPCanteen20230403GroundBeef": 100,
		"SAPCanteen20230403CarrotsWithOnions": 100,
		"SAPCanteen20230403Chickpeas": 100,
		"SAPCanteen20230403GreekYoghurt": 100,
	},
	"SAPCanteen20230405": {
		"SAPCanteen20230405PorkLoinSteak": 100,
		"SAPCanteen20230405Broccoli": 100,
		"SAPCanteen20230405GreenBeans": 100,
		"SAPCanteen20230405CouscousMango": 100,
	},
	"SAPCanteen20230406": {
		"SAPCanteen20230406BurritoBowl": 100,
		"SAPCanteen20230406GratinatedPotatoes": 100,
		"SAPCanteen20230406Beans": 100,
		"SAPCanteen20230406MediterranesGemuese": 100,
		"SAPCanteen20230406PannaCotta": 100
	},
	"SAPCanteen20230411": {
		"SAPCanteen20230411AdanaKebap": 100,
		"SAPCanteen20230411CurdBlueBerries": 200,
	},
	"SAPCanteen20230413": {
		"SAPCanteen20230413TunisiaChickenBreast": 100,
		"SAPCanteen20230413KidneyBeans": 100,
		"SAPCanteen20230413Pasta": 100,
		"SAPCanteen20230413BananaCoconutPudding": 100,
		"SAPCanteen20230413SchokoladenCreme": 100,
	},
	"SAPCanteen20230414": {
		"SAPCanteen20230414WildSalmon": 100,
		"SAPCanteen20230414Gemuesereis": 100,
		"SAPCanteen20230414Kichererbsen": 100,
		"SAPCanteen20230414ApfelZimtKuchen": 100,
		"SAPCanteen20230413BananaCoconutPudding": 100,
	},
	"SAPCanteen20230502": {
		"SAPCanteen20230502StuffedCabbage": 100,
		"SAPCanteen20230502ApricotDumplings": 100,
	},
	"SAPCanteen20230510": {
		"SAPCanteen20230510JamaicaGrilledChickenBreast": 100,
		"SAPCanteen20230510Reis": 100,
		"SAPCanteen20230510BrokkoliBlumenkohl": 100,
		"SAPCanteen20230510Erbsen": 100,
		"SAPCanteen20230510PannaCottaKirsch": 200,
	},
	"SAPCanteen20230512": {
		"SAPCanteen20230512PollockFilletBroccoli": 100,
		"SAPCanteen20230512RedQuinoa": 200,
		"SAPCanteen20230512SchokoladenPudding": 200,
	},
	"SAPCanteen20230515": {
		"SAPCanteen20230515MexicoChiliConCarne": 100,
		"SAPCanteen20230515PumpkinAndChickpeaMash": 200,
		"SAPCanteen20230515StrawberryMilkshake": 200,
	},
	"SAPCanteen20230531": {
		"SAPCanteen20230531SteamedWildSalmon": 100,
		"SAPCanteen20230531MixedQuinoa": 100,
		"SAPCanteen20230531BananaPudding": 200,
	},
	"SAPCanteen20230816": {
		"SAPCanteen20230816TurkeyChilliBeansCorn": 100,
		"SAPCanteen20230816Broccoli": 100,
		"SAPCanteen20230816RaspberryCurd": 200,
	},
	"SAPCanteen20230911": {
		"SAPCanteen20230911HungarySzegedGoulash": 100,
		"SAPCanteen20230911KidneyBeans": 100,
		"SAPCanteen20230911ApricotCurd": 200,
	},
	"PfannenFischMitOfenKartoffeln": {
		"FrostaPfannenFischMuellerinArt": 250,
		"MazolaKeimOel": 25,
		"Kartoffel": 250
	},
	"BackofenFischMitOfenKartoffeln": {
		"FrostaBackofenFischKnusprigKross": 240,
		"Kartoffel": 250
	},
	"EinFuzeTeaZitrone": {
		"FuzeTeaZitrone": 300,
	},
	"EinFuzeTeaPfirsich": {
		"FuzeTeaPfirsich": 300,
	},
	"EinGenericEiscafe": {
		"GenericEiscafe": 100,
		"Zucker": 5,
	},
	"EinGenericFlammkuchen": {
		"GenericFlammkuchen": 260,
	},
	"MyFiveGuysBaconeBarBQCheeseBurger": {
		"FiveGuysBun": 100,
		"FiveGuysHamburgerPatty": 100,
		"FiveGuysHamburgerCheese": 100,
		"FiveGuysHamburgerLettuce": 100,
		"FiveGuysHamburgerBacon": 100,
		"FiveGuysHamburgerBarBQSauce": 100,
		"FiveGuysHamburgerGrilledOnions": 100
	},
	"MyFiveGuysBigBaconeBarBQCheeseBurger": {
		"FiveGuysBun": 100,
		"FiveGuysHamburgerPatty": 200,
		"FiveGuysHamburgerCheese": 200,
		"FiveGuysHamburgerLettuce": 100,
		"FiveGuysHamburgerBacon": 100,
		"FiveGuysHamburgerBarBQSauce": 100,
		"FiveGuysHamburgerGrilledOnions": 100
	},
	"EinFiveGuysLittleFries": {
		"FiveGuysLittleFries": 100,
	},
	"EinGlasSauerkirschNektar": {
		"ReweSauerKirschNektar": 200,
	},
	"TwoPiecesKajuKatli": {
		"KajuKatli": 23,
	},
	"OnePieceMandyBananaBred": {
		"MandysBananaBred": 100
	},
	"EinOreoIcrecreamBites": {
		"OreoIcrecreamBites": 54
	},
	"EinSchokoKirschMuffin": {
		"Zucker": 300/twentySixMuffins,
		"WeizenmehlTyp405": 400/twentySixMuffins,
		"Ei": 205/twentySixMuffins,
		"BensdorfKakao": 80/twentySixMuffins,
		"ThomyReinesSonnenblumenoel": 300/twentySixMuffins,
		"OdenwaldSchattenmorellen": 350/twentySixMuffins,
		"AlproNotMilk": 400/twentySixMuffins,
		"ClassicSchokoChunks": 200/twentySixMuffins,
		"NaturallyPamChocolateChips": 100/twentySixMuffins,
	},
	"EinSchokoKirschProteinMuffin": {
		"Zucker": 200/twentySixMuffins,
		"bodyAttackWheyDeluxe": 100/twentySixMuffins,
		"WeizenmehlTyp405": 300/twentySixMuffins,
		"Ei": 205/twentySixMuffins,
		"BensdorfKakao": 80/twentySixMuffins,
		"ThomyReinesSonnenblumenoel": 300/twentySixMuffins,
		"OdenwaldSchattenmorellen": 350/twentySixMuffins,
		"AlproNotMilk": 400/twentySixMuffins,
	},
	"EinCremeBrulee": {
		"FrischeSchlagsahne": 350/cremeBrulee,
		"BerchtesgadenerLandVollmilch": 100/cremeBrulee,
		"Ei": (4*60)/cremeBrulee,
		"Zucker": 40/cremeBrulee,
	},
	"EinMandysZwetschgenkuchen": {
		"MandysZwetschgenkuchen": 100
	},
	"KnaeckebrotFrischkaese100Avocado200": {
		"EdekaBioKoernigerFrischkaese": 100,
		"Avocado": 200,
		"WasaRustikalKnaeckebrot": 45
	},
	"KnaeckebrotFrischkaese150Avocado200": {
		"EdekaBioKoernigerFrischkaese": 150,
		"Avocado": 200,
		"WasaRustikalKnaeckebrot": 45
	},
	"KnaeckebrotFrischkaese150Avocado100": {
		"EdekaBioKoernigerFrischkaese": 150,
		"Avocado": 100,
		"WasaRustikalKnaeckebrot": 45
	},
	"KnaeckebrotFrischkaese150Avocado80": {
		"EdekaBioKoernigerFrischkaese": 150,
		"Avocado": 80,
		"WasaRustikalKnaeckebrot": 45
	},
	"KnaeckebrotFrischkaese200Avocado200": {
		"EdekaBioKoernigerFrischkaese": 200,
		"Avocado": 200,
		"WasaRustikalKnaeckebrot": 45
	},
	"KnaeckebrotFrischkaese200Avocado130": {
		"ReweBioKoernigerFrischkaese": 200,
		"Avocado": 130,
		"WasaRustikalKnaeckebrot": 45
	},
	"KnaeckebrotFrischkaese150Avocado130": {
		"ReweBioKoernigerFrischkaese": 150,
		"Avocado": 130,
		"WasaRustikalKnaeckebrot": 45
	},
	"KnaeckebrotFrischkaese150Olivenoel": {
		"ReweBioKoernigerFrischkaese": 150,
		"Olivenoel": 30,
		"WasaRustikalKnaeckebrot": 45
	},
	"EinHelloFreshPfannkuchenAuflauf": {
		"HelloFreshPfannkuchenAuflauf": 560
	},
	"EinHelloFreshSeehechtOrzoNudelnGurkenSalat": {
		"HelloFreshSeehechtOrzoNudelnGurkenSalat": 650
	},
	"GeneralTsoHaehnchenbrust": {
		"SojaSauce": threeTablespoon/generalTso,
		"BambooGardenHoisinSauce": twoTablespoon/generalTso,
		"BambooGardenReisessig": oneTablespoon/generalTso,
		"bioZentraleAhornsirup": oneTablespoon/generalTso,
		"MingChuSesamoel": oneTeaspoon/generalTso,
		"Ingwer": 30/generalTso,
		"Staerke": twoTeaspoon/generalTso,
		"Haehnchenbrust": 400/generalTso,
		"ThomySonnenblumenoel": oneTablespoon/generalTso,
		"Broccoli": 500/generalTso,
		"JasminReis": 150/generalTso,
	},
	"GeneralTsoTofu": {
		"SojaSauce": threeTablespoon/generalTso,
		"BambooGardenHoisinSauce": twoTablespoon/generalTso,
		"BambooGardenReisessig": oneTablespoon/generalTso,
		"bioZentraleAhornsirup": oneTablespoon/generalTso,
		"MingChuSesamoel": oneTeaspoon/generalTso,
		"Ingwer": 30/generalTso,
		"Staerke": twoTeaspoon/generalTso + 25,
		"AlnaturTofuNatur": 400/generalTso,
		"ThomySonnenblumenoel": twoTablespoon/generalTso,
		"Broccoli": 400/generalTso,
		"JasminReis": 150/generalTso,
	},
	"EinEmmiCaffeLatteHighProtein": {
		"EmmiCaffeLatteHighProtein": 230
	},
	"EsnProteinShakeRichChocolateWater": {
		"EsnDesignerWheyProteinRichChocolate": 30
	},
	"EsnProteinShakeCinnamonCerealWater": {
		"EsnDesignerWheyProteinCinnamonCereal": 30
	},
	"EsnProteinShakeVanilla": {
		"EsnDesignerWheyProteinVanilla": 30,
		"arlaLaktoseFrei": 300,
	},
	"EsnProteinShakeVanillaFettarmeHMlich": {
		"EsnDesignerWheyProteinVanilla": 30,
		"EdekaBioFettarmeHMilch": 300,
	},
	"EsnProteinShakeVanillaHVollmilch": {
		"EsnDesignerWheyProteinVanilla": 30,
		"EdekaBioHVollmilch": 300,
	},
	"EsnProteinShakeCinnamonCerealHVollmilch": {
		"EsnDesignerWheyProteinCinnamonCereal": 30,
		"EdekaBioHVollmilch": 300,
	},
	"EsnProteinShakeVanillaWater": {
		"EsnDesignerWheyProteinVanilla": 34,
	},
	"EsnProteinShakeIsoclearWater": {
		"EsnIsoclearWheyIsolate": 30,
	},
	"OvernightOatsSkyrWalnussHeidelbeerenWheyDeluxe": {
		"KoellnMultikornFlocken": 50,
		"AlproNotMilk": 150,
		"Walnuss": 50,
		"bodyAttackWheyDeluxe": 35,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"ExquisaSkyrMild": 100,
	},
	"OvernightOatsSkyrWalnussHeidelbeeren": {
		"KoellnMultikornFlocken": 50,
		"AlproNotMilk": 150,
		"Walnuss": 50,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"ExquisaSkyrMild": 100,
	},
	"OvernightDinkelOatsSkyrWalnussHeidelbeeren": {
		"KoellnBioDinkelflocken": 50,
		"AlproNotMilk": 150,
		"Walnuss": 50,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"ExquisaSkyrMild": 100,
	},
	"OvernightDinkelOatsSpeisequarkWalnussHeidelbeeren": {
		"KoellnBioDinkelflocken": 50,
		"AlproNotMilk": 150,
		"Walnuss": 50,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"BiggerOvernightDinkelOatsSpeisequarkWalnussHeidelbeeren": {
		"KoellnBioDinkelflocken": 75,
		"AlproNotMilk": 200,
		"Walnuss": 50,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"BiggerOvernightDinkelOatsSpeisequarkMandelmusHeidelbeeren": {
		"KoellnBioDinkelflocken": 75,
		"AlproNotMilk": 200,
		"KoroMandelmusZimtVanille": 50,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"OvernightDinkelOatsSkyrWalnussHimbeeren": {
		"KoellnBioDinkelflocken": 50,
		"AlproNotMilk": 150,
		"Walnuss": 50,
		"TkBioHimbeeren": 50,
		"Banane": 80,
		"ExquisaSkyrMild": 100,
	},
	"OvernightDinkelOatsSkyrWalnussHeidelbeerenPamSchoco": {
		"KoellnBioDinkelflocken": 50,
		"AlproNotMilk": 150,
		"Walnuss": 50,
		"TkBioHimbeeren": 50,
		"Banane": 80,
		"ExquisaSkyrMild": 100,
		"PamSchoco": 10,
	},
	"BiggerOvernightDinkelOatsSpeisequarkMandelmusBodyAttackHeidelbeeren": {
		"KoellnBioDinkelflocken": 75,
		"AlproNotMilk": 200,
		"KoroMandelmusZimtVanille": 20,
		"bodyAttackWheyDeluxe": 19,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"BiggerOvernightDinkelOatsSpeisequarkHaselnussmusBodyAttackHeidelbeeren": {
		"KoellnBioDinkelflocken": 75,
		"AlproNotMilk": 200,
		"KoroHaselnussmus": 20,
		"bodyAttackWheyDeluxe": 19,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"BiggerOvernightDinkelOatsSpeisequarkHaselnussmusEsnDesignerWheyHeidelbeeren": {
		"KoellnBioDinkelflocken": 75,
		"AlproNotMilk": 200,
		"KoroHaselnussmus": 20,
		"EsnDesignerWheyProteinVanilla": 17,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyHeidelbeeren": {
		"KoellnBioDinkelflocken": 75,
		"AlproNotMilk": 200,
		"KoroHaselnussmus": 40,
		"EsnDesignerWheyProteinVanilla": 17,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"OvernightDinkelOats75SpeisequarkBraunesMandelmus40EsnDesignerWheyHeidelbeeren": {
		"KoellnBioDinkelflocken": 75,
		"AlproNotMilk": 200,
		"KoroBraunesMandelmus": 40,
		"EsnDesignerWheyProteinVanilla": 17,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"OvernightDinkelOats75SpeisequarkErdnussmusKakao40EsnDesignerWheyHeidelbeeren": {
		"KoellnBioDinkelflocken": 75,
		"AlproNotMilk": 200,
		"KoroErdnussmusHaselnussKakao": 40,
		"EsnDesignerWheyProteinVanilla": 17,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"OvernightDinkelOats50SpeisequarkErdnussmusKakao20EsnDesignerWheyHeidelbeeren": {
		"KoellnBioDinkelflocken": 50,
		"AlproNotMilk": 150,
		"KoroErdnussmusHaselnussKakao": 20,
		"EsnDesignerWheyProteinVanilla": 17,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"OvernightDinkelOats50SpeisequarkReweBioWeissesMandelmus20EsnDesignerWheyHeidelbeeren": {
		"KoellnBioDinkelflocken": 50,
		"AlproNotMilk": 150,
		"ReweBioWeissesMandelmus": 20,
		"EsnDesignerWheyProteinVanilla": 17,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"OvernightDinkelOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren": {
		"KoellnBioDinkelflocken": 50,
		"AlproNotMilk": 150,
		"KoroBraunesMandelmus": 20,
		"EsnDesignerWheyProteinVanilla": 17,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeeren": {
		"KoroBioHaferflocken": 50,
		"AlproNotMilk": 150,
		"KoroBraunesMandelmus": 20,
		"EsnDesignerWheyProteinVanilla": 17,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyHeidelbeerenChia": {
		"KoroBioHaferflocken": 50,
		"AlproNotMilk": 150,
		"KoroBraunesMandelmus": 20,
		"EsnDesignerWheyProteinVanilla": 17,
		"TkBioHeidelbeeren": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
		"SeitenbacherBioChiasamen": 10,
	},
	"OvernightHaferOats50SpeisequarkKoroBraunesMandelmus20EsnDesignerWheyAnanas": {
		"KoroBioHaferflocken": 50,
		"AlproNotMilk": 150,
		"KoroBraunesMandelmus": 20,
		"EsnDesignerWheyProteinVanilla": 17,
		"Ananas": 70,
		"GutUndGuenstigSpeisequark": 100,
	},
	"OvernightDinkelOats75SpeisequarkHaselnussmus40EsnDesignerWheyMango": {
		"KoellnBioDinkelflocken": 75,
		"AlproNotMilk": 200,
		"KoroHaselnussmus": 40,
		"EsnDesignerWheyProteinVanilla": 17,
		"EdekaTkMango": 50,
		"Banane": 80,
		"GutUndGuenstigSpeisequark": 100,
	},
	"20230617WarmPorridge": {
		"KoellnBioDinkelflocken": 50,
		"AlproNotMilk": 150,
		"ReweBioWeissesMandelmus": 10,
		"Walnuss": 10,
		"EsnDesignerWheyProteinVanilla": 15,
		"Banane": 40,
		"GutUndGuenstigSpeisequark": 100,
		"Pfirsich": 50,
		"Erdbeere": 50,
		"NaturallyPamChocolateChips": 10,
		"KoroSojaProteinCrispies": 5,
	},
	"20230614WarmPorridge": {
		"KoellnBioDinkelflocken": 50,
		"AlproNotMilk": 200,
		"KoroMandelmusZimtVanille": 13,
		"Walnuss": 10,
		"EsnDesignerWheyProteinVanilla": 15,
		"Banane": 40,
		"GutUndGuenstigSpeisequark": 100,
		"Pfirsich": 50,
		"Erdbeere": 50,
		"NaturallyPamChocolateChips": 10,
		"KoroSojaProteinCrispies": 5,
	},
	"20230701WarmPorridge": {
		"KoroBioHaferflocken": 50,
		"AlproNotMilk": 200,
		"KoroMandelmusZimtVanille": 10,
		"Walnuss": 10,
		"EsnDesignerWheyProteinVanilla": 15,
		"Banane": 40,
		"GutUndGuenstigSpeisequark": 100,
		"Pfirsich": 24,
		"Erdbeere": 52,
		"NaturallyPamChocolateChips": 10,
		"KoroSojaProteinCrispies": 5,
	},
	"20230715WarmPorridge": {
		"KoroBioHaferflocken": 50,
		"AlproNotMilk": 200,
		"KoroMandelmusZimtVanille": 12,
		"Walnuss": 12,
		"EsnDesignerWheyProteinVanilla": 15,
		"Banane": 40,
		"GutUndGuenstigSpeisequark": 100,
		"Nektarine": 16,
		"Kirschen": 25,
		"EdekaRaspelSchokolade": 10,
		"KoroSojaProteinCrispies": 5,
	},
	"20230729WarmPorridge": {
		"KoroBioHaferflocken": 50,
		"AlproNotMilk": 200,
		"KoroMandelmusZimtVanille": 10,
		"Walnuss": 10,
		"EsnDesignerWheyProteinVanilla": 15,
		"Banane": 40,
		"GutUndGuenstigSpeisequark": 100,
		"TkBioHimbeeren": 33,
		"EdekaRaspelSchokolade": 10,
		"KoroSojaProteinCrispies": 5,
	},
	"20230805WarmPorridge": {
		"KoroBioHaferflocken": 50,
		"AlproNotMilk": 200,
		"KoroMandelmusZimtVanille": 11,
		"Walnuss": 10,
		"EsnDesignerWheyProteinVanilla": 15,
		"Banane": 40,
		"GutUndGuenstigSpeisequark": 100,
		"TkBioHimbeeren": 68,
		"EdekaRaspelSchokolade": 10,
		"KoroSojaProteinCrispies": 5,
	},
	"20230812WarmPorridge": {
		"KoroBioHaferflocken": 50,
		"AlproNotMilk": 200,
		"Banane": 40,
		"GutUndGuenstigSpeisequark": 108,
		"EsnDesignerWheyProteinVanilla": 15,
		"EdekaRaspelSchokolade": 10,
		"TkBioHimbeeren": 39,
		"Pfirsich": 56,
		"Walnuss": 10,
		"KoroMandelmusZimtVanille": 10,
		"KoroSojaProteinCrispies": 5,
	},
	"NutellaButterSandwich": {
		"KornMuehleButtertoast": oneSliceOfToast,
		"KerrygoldButter": 10,
		"FerreroNutella": 20,
	},
	"HalbesDanaNutellaSandwich": {
		"KornMuehleButtertoast": oneSliceOfToast/2,
		"FerreroNutella": 10,
	},
	"EinGlasApfelsaftSchorle": {
		"Apfelsaft": 100,
	},
	"EinHanuta": {
		"FerreroHanuta": 22,
	},
	"EinHanutaBrownieStyle": {
		"FerreroHanutaBrownieStyle": 22,
	},
	"EinePortionSmarties": {
		"Smarties": 19,
	},
	"EineSchwarzwaldhofLandjaeger": {
		"SchwarzwaldhofBioLandjaeger": 150/4,
	},
	"200Skyr20Krispies": {
		"ExquisaSkyrMild": 200,
		"KellogsChocoKrispies": 20,
	},
	"200Skyr40Krispies": {
		"ExquisaSkyrMild": 200,
		"KellogsChocoKrispies": 40,
	},
	"200Skyr20SojaCrispies": {
		"ExquisaSkyrMild": 200,
		"KoroSojaProteinCrispies": 20,
	},
	"EisbergsalatMitCroutonsUndHaehnchenstreifen": {
		"KnorrCroutinosMitSonnenblumenkernen": 25, // eine Packung
		"Haehnchenbrust": 200,
		"FloretteEisbergStreifen": 300,
		"LowCoJohannisbeerDressing": oneTablespoon * 5,
	},
	"EisbergsalatMitCroutonsUndHonigSenfDressing": {
		"KnorrCroutinosMitSonnenblumenkernen": 25, // eine Packung
		"FloretteEisbergStreifen": 300,
		"LowCoHonigSenfDressing": oneTablespoon * 5,
	},
	"EinKnoppers": {
		"StorckKnoppers": 25,
	},
	"ZweiPortionenIgloSchlemmerFiletAlaBordelaiseClassic": {
		"IgloSchlemmerFiletAlaBordelaiseClassic": 200,
	},
	"EinNutritionxPulledPorkSandwich": {
		"NutritionxPulledPorkSandwich": 100,
	},
	"ProteinWrapSandwichKaeseKochschinkenAvocadoHummus": {
		"EdekaProteinWraps": einEdekaProteinWrap,
		"HochlandSandwichScheibenCheddar": eineScheibeHochlandSandwichScheiben,
		"EdekaBioFrischkaese": 30,
		"HummusKichererbsenOlivenoel": 20,
		"Avocado": 20,
		"ReweBioKochschinken": oneSliceOfKochschinken,
		"Ei": (3/4)*50,
	},
	"ProteinWrapGoudaKochschinkenHummus": {
		"EdekaProteinWraps": einEdekaProteinWrap,
		"EdekaBioGouda": oneSliceOfEdekaBioGouda/2,
		"EdekaBioFrischkaese": 30,
		"HummusKichererbsenOlivenoel": 20,
		"ReweBioKochschinken": oneSliceOfKochschinken,
		"Ei": (3/4)*50,
	},
	"ProteinWrapEinEiGoudaKochschinkenHummus": {
		"EdekaProteinWraps": einEdekaProteinWrap,
		"EdekaBioGouda": oneSliceOfEdekaBioGouda/2,
		"EdekaBioFrischkaese": 30,
		"HummusKichererbsenOlivenoel": 20,
		"ReweBioKochschinken": oneSliceOfKochschinken,
		"Ei": 50,
	},
	"ProteinWrapEinEiSandwichKaeseKochschinkenAvocado": {
		"EdekaProteinWraps": einEdekaProteinWrap,
		"HochlandSandwichScheibenCheddar": eineScheibeHochlandSandwichScheiben,
		"EdekaBioFrischkaese": 30,
		"Avocado": 20,
		"ReweBioKochschinken": oneSliceOfKochschinken,
		"Ei": 50,
	},
	"ProteinWrapEinEiSandwichKaeseKochschinkenAvocadoOel": {
		"EdekaProteinWraps": einEdekaProteinWrap,
		"HochlandSandwichScheibenCheddar": eineScheibeHochlandSandwichScheiben,
		"EdekaBioFrischkaese": 30,
		"Avocado": 20,
		"ReweBioKochschinken": oneSliceOfKochschinken,
		"Ei": 50,
		"ThomyReinesSonnenblumenoel": oneTeaspoon/2,
	},
	"ProteinWrapEinSandwichKaeseKochschinken": {
		"EdekaProteinWraps": einEdekaProteinWrap,
		"HochlandSandwichScheibenCheddar": eineScheibeHochlandSandwichScheiben,
		"EdekaBioFrischkaese": 30,
		"ReweBioKochschinken": oneSliceOfKochschinken,
	},
	"ProteinWrapEinEiKaeseKochschinkenV2": {
		"ItzaProteinWraps": einItzaProteinWrap,
		"HochlandSandwichScheibenCheddar": eineScheibeHochlandSandwichScheiben,
		"EdekaBioFrischkaese": 30,
		"ReweBioKochschinken": oneSliceOfKochschinken,
		"ThomySonnenblumenoel": oneTeaspoon,
	},
	"WrapEinEiSandwichKaeseKochschinkenAvocado": {
		"TortillaCornWheat": einTortillaCornWheat,
		"HochlandSandwichScheibenCheddar": eineScheibeHochlandSandwichScheiben,
		"EdekaBioFrischkaese": 30,
		"Avocado": 20,
		"ReweBioKochschinken": oneSliceOfKochschinken,
		"Ei": 50,
	},
	"EinBecherEhrmannHighProteinChocolateMousse": {
		"EhrmannHighProteinChocolateMousse": 200,
	},
	"EinBecherEhrmannHighProteinDoubleChoc": {
		"EhrmannHighProteinDoubleChoc": 200,
	},
	"EinSelbstgemachterEiskaffee": {
		// Kaffee: 0 kCal :)
		"MoevenpickVanilleEis": 70,
	},
	"SalatBrokkoliPaprikaHaehnchenbrustSylterOriginal": {
		"SylterOriginal": 25,
		"Haehnchenbrust": 200,
		"Olivenoel": 2.5,
		"RotePaprika": 150,
		"Brokkoli": 125,
		"Bohnen": 100,
		"ReweBioFalafelBaellchen": 100
	},
	"EinStueckLindtEiscafeSchokolade": {
		"LindtEiscafeSchokolade": 10
	},
	"RicePudding50EsnDesignerWheyProteinVanilla30": {
		"EsnDesignerWheyProteinVanilla": 30,
		"VayuInstantRicePudding": 50,
	},
	"20230619Abendessen": {
		"Lamm": 100,
		"GoertzHerzschlag": oneSliceOfHerzschlag,
		"KerrygoldButter": 10
	},
	"FakeIceCoffeeWithVanillaProtein": {
		// Kaffee: 0 kCal :)
		"EsnDesignerWheyProteinVanilla": 35,
		"EdekaBioFettarmeHMilch": 150,
	},
	"FakeIceCoffeeWithVanillaProtein15FettarmeHMilch150": {
		// Kaffee: 0 kCal :)
		"EsnDesignerWheyProteinVanilla": 15,
		"EdekaBioFettarmeHMilch": 150,
	},
	"20230624Abendessen": {
		"RaeucherTofuSesamMandel": 100,
		"TkMango": 40,
		"Karotten": 40,
		"RotePaprika": 40,
		"Gurke": 40,
		"ZentisErdnussButter": 20,
		"WanKwaiMihoenReisnudeln": 22,
		"RicefieldCuChiReispapier": 40,
		"SojaSauce": oneTeaspoon,
		"EdekaBioAhornSirup": oneTeaspoon/2,
	},
	"20230626Breakfast": {
		"ExquisaSkyrMild": 280,
		"SeitenbacherDinoMuesli": 50,
	},
	"20230715AnanasCurryHaehnchenbrust": {
		"Haehnchenbrust": 300/2,
		"JasminReis": 125/2,
		"Ananas": 200/2,
		"RotePaprika": 40,
		"Zwiebel": 20/2,
		"RamaCremefine": 250/2,
		"MazolaKeimOel": oneTeaspoon,
		"Broccoli": 50,
	},
	"20231015Breakfast": {
		"GoertzLaugenbroetchen": 85,
		"KerrygoldButter": 10 + 10,
		"KoroBioDattelHaselnussCreme": 35,
		"Marmelade": 15,
		"WakeAndBakeWeizenmischbrotMitWalnuessen": (881-705)/2,
		"EdekaBioFrischkaese": 20,
		"ReweBioKochschinken": twoSliceOfKochschinken,
		"RotePaprika": 50,
		"KoroBioEiweiss": 75,
		"Ei": 3*60
	},
	// Normiert auf Brotleib von 881g vom 2023-10-15
	"WakeAndBakeWeizenmischbrotMitWalnuessen": {
		"Walnuss": (150 / 881) * 100,
		"AlnaturaRoggenVollkornMehl": ((30 + 90 + 15)/881)*100,
		"DemeterCampoVerdeWeizenMehl550": (410/881)*100,
	},
}
