import type { NextApiRequest, NextApiResponse } from 'next'
import calAndEnergyForTodayAndTomorrow from '../../numberCrunching/calAndEnergyForTodayAndTomorrow'
import { NamedCalAndEnergy } from '../../numberCrunching/calculateCalAndEnergyPerDayDiet'

export default async function handler(
    _req: NextApiRequest,
    res: NextApiResponse<NamedCalAndEnergy[]>
) {
    res.status(200).json(calAndEnergyForTodayAndTomorrow())
}