import { GetStaticProps } from 'next'
import calAndEnergyPerDayWeek, { CalAndEnergyPerDayWeek } from '../numberCrunching/calAndEnergyPerDayWeek';
import { sumReducer } from '../numberCrunching/sumAndAverage';
import { averageDailyCaloriePerWeek } from '../numberCrunching/averageCaloriesPerWeek';
import proteinIntakeTargetPerWeek from '../numberCrunching/proteinIntakeTargetPerWeek';
import { phases, Phases } from '../data/phases';
import formatDayCardHeader from '../numberCrunching/formatDayCardHeader';
import { calColor, proteinColor } from '../components/nutritionColors';
import MicroBarChart from '../perWeek/MicroBarChart';
import EnergiePieChart from '../components/EnergiePieChart';
import Header from '../components/Header';
import Navigation, { Pages } from '../components/Navigation';
import CardHeader from '../components/CardHeader';
import CardContent from '../components/CardContent';
import HorizontalGrid from '../components/HorizontalGrid';
import VerticalGrid from '../components/VerticalGrid';
import Card from '../components/Card';
import DayCardTable from '../components/DayCardTable';
import style from './index.module.css';

type IndexProps = {
  weeks: CalAndEnergyPerDayWeek;
  phases: Phases
}

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: {
      weeks: calAndEnergyPerDayWeek(),
      phases
    }
  }
}

export const enoughDaysPerWeek = 4;

export default function Page({ weeks, phases }: IndexProps) {
  return (
    <>
      <Header />
        <VerticalGrid className={style.indexVerticalGrid}>
          {
            Object.entries(weeks).map(([key, namedCalAndProteins]) =>
              <Card key={key}>
                <CardHeader title={key} />
                <CardContent>
                  <HorizontalGrid>
                    {namedCalAndProteins.length >= enoughDaysPerWeek &&
                      <Card className={style.dayCard} key={`${key}EnergiePieChart`}>
                        <CardContent className={style.energiePieChartContent}>
                          <EnergiePieChart
                            fat={namedCalAndProteins.map(({ fat }) => fat).reduce(sumReducer)}
                            carbs={namedCalAndProteins.map(({ carbs }) => carbs).reduce(sumReducer)}
                            protein={namedCalAndProteins.map(({ protein }) => protein).reduce(sumReducer)} />
                        </CardContent>
                      </Card>
                    }
                    {namedCalAndProteins.length >= enoughDaysPerWeek &&
                      <Card className={style.dayCard} key={`${key}CalMicroBarChart`}>
                        <CardHeader title="Cals: Burned vs. Eaten" />
                        <CardContent>
                          <MicroBarChart fillColor={calColor} height={81} width={300 - (16 * 2)}
                            data={namedCalAndProteins.map(({ cal }) => cal)}
                            actual={averageDailyCaloriePerWeek(key)} />
                        </CardContent>
                      </Card>
                    }
                    {namedCalAndProteins.length >= enoughDaysPerWeek &&
                      <Card className={style.dayCard} key={`${key}ProteinMicroBarChart`}>
                        <CardHeader title={`Protein${proteinIntakeTargetPerWeek(key, phases) ? ': Target vs. Eaten' : ''}`} />
                        <CardContent>
                          <MicroBarChart fillColor={proteinColor} height={81} width={300 - (16 * 2)}
                            data={namedCalAndProteins.map(({ protein }) => protein)}
                            actual={proteinIntakeTargetPerWeek(key, phases)} />
                        </CardContent>
                      </Card>
                    }
                    {namedCalAndProteins.map(({ name, cal, protein, fat, carbs }) =>
                      <Card className={style.dayCard} key={name}>
                        <CardHeader title={formatDayCardHeader(name)} />
                        <CardContent>
                          <DayCardTable cal={cal} protein={protein}></DayCardTable>
                          <EnergiePieChart fat={fat} carbs={carbs} protein={protein} height={150} />
                        </CardContent>
                      </Card>
                    )}
                  </HorizontalGrid>
                </CardContent>
              </Card>
            )
          }
        </VerticalGrid>
      <Navigation currentPage={Pages.weeks}/>
    </>
  );
}
