import { GetStaticProps } from 'next'
import recipes from '../numberCrunching/recipesAsNamedMacros'
import EnergiePieChart from '../components/EnergiePieChart';
import Header from '../components/Header';
import Navigation, { Pages } from '../components/Navigation';
import NutritionTable from '../components/NutritionTable';
import style from './recipes.module.css';
import HorizontalGrid from '../components/HorizontalGrid';
import Card from '../components/Card';
import CardHeader from '../components/CardHeader';
import CardContent from '../components/CardContent';
import { NamedMacrosMap } from '../data/ingredients';


type RecipeProps = {
  recipes: NamedMacrosMap;
}

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: { recipes: recipes() }
  }
}

export default function Page({ recipes }: RecipeProps) {
  return (
    <>
      <Header />
      <HorizontalGrid className={style.recipesHorizontalGrid}>
        {
          Object.entries(recipes).map(([key, { cal, fat, saturated, carbs, sugar, protein, salt }]) =>
            <Card className={style.recipeCard} key={key}>
              <CardHeader title={key} />
              <CardContent>
                <EnergiePieChart fat={fat} carbs={carbs} protein={protein} />
                <NutritionTable macros={{ cal, fat, saturated, carbs, sugar, protein, salt }}></NutritionTable>
              </CardContent>
            </Card>
          )
        }
      </HorizontalGrid>
      <Navigation currentPage={Pages.recipes} />
    </>
  );
}
