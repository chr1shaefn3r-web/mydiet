import { GetStaticProps } from 'next'
import { ingredients, Ingredients } from "../data/ingredients";
import EnergiePieChart from '../components/EnergiePieChart';
import Header from '../components/Header';
import Navigation, { Pages } from '../components/Navigation';
import HorizontalGrid from '../components/HorizontalGrid';
import Card from '../components/Card';
import CardHeader from '../components/CardHeader';
import CardContent from '../components/CardContent';
import NutritionTable from '../components/NutritionTable';
import style from './ingredient.module.css';

type IngredientProps = {
  ingredients: Ingredients;
}

export const getStaticProps: GetStaticProps = async context => {
  return {
    props: { ingredients }
  }
}

export default function Page({ ingredients }: IngredientProps) {
  return (
    <>
      <Header />
      <HorizontalGrid className={style.ingredientsHorizontalGrid}>
        {
          Object.entries(ingredients).map(([key, { name, cal, fat, saturated, carbs, sugar, protein, salt }]) =>
            <Card className={style.ingredientCard} key={key}>
              <CardHeader title={name} />
              <CardContent>
                <EnergiePieChart fat={fat} carbs={carbs} protein={protein} />
                <NutritionTable macros={{cal, fat, saturated, carbs, sugar, protein, salt}}></NutritionTable>
              </CardContent>
            </Card>
          )
        }
      </HorizontalGrid>
      <Navigation currentPage={Pages.ingredients}/>
    </>
  );
}
