import Header from '../components/Header';
import Navigation, { Pages } from '../components/Navigation';
import { GetStaticProps } from 'next';
import { computedPhases, ComputedPhases } from '../numberCrunching/computedPhases'
import { ActualTargetTrendLineChart } from '../phases/ActualTargetTrendLineChart';
import { Calendar } from '@nivo/calendar'
import style from './phases.module.css'
import Card from '../components/Card';
import CardContent from '../components/CardContent';
import CardHeader from '../components/CardHeader';
import HorizontalGrid from '../components/HorizontalGrid';

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: computedPhases()
  }
}

export default function Page({ computedPhases,
  minWeightData, maxWeightData,
  minBodyfatData, maxBodyfatData,
  minMuscleData, maxMuscleData }: ComputedPhases) {
  return (
    <>
      <Header />
      <HorizontalGrid className={style.content}>
        {
          computedPhases.map(({ name, startDay, endDay, totalWeeks, calendarData,
            lineChartWeightData, targetLineChartWeightData, trendLineChartWeightData,
            lineChartBodyfatData, targetLineChartBodyfatData, trendLineChartBodyfatData,
            lineChartMuscleData, targetLineChartMuscleData, trendLineChartMuscleData, }) =>
            <Card key={name}>
              <CardHeader title={name}
                subheader={`${startDay} - ${endDay} (${totalWeeks} Weeks, ${totalWeeks / 4} Months)`} />
              <CardContent>
                <ActualTargetTrendLineChart
                  min={minWeightData}
                  max={maxWeightData}
                  data={[{
                    id: "Weight (kg)",
                    data: lineChartWeightData
                  }, {
                    id: "Target (kg)",
                    data: targetLineChartWeightData
                  }, {
                    id: "Trend (kg)",
                    data: trendLineChartWeightData
                  }]}
                />
                <ActualTargetTrendLineChart
                  min={minBodyfatData}
                  max={maxBodyfatData}
                  data={[{
                    id: "Fat (%)",
                    data: lineChartBodyfatData
                  }, {
                    id: "Target (%)",
                    data: targetLineChartBodyfatData
                  }, {
                    id: "Trend (%)",
                    data: trendLineChartBodyfatData
                  }]}
                />
                <ActualTargetTrendLineChart
                  min={minMuscleData}
                  max={maxMuscleData}
                  data={[{
                    id: "Muscle (kg)",
                    data: lineChartMuscleData
                  }, {
                    id: "Target (kg)",
                    data: targetLineChartMuscleData
                  }, {
                    id: "Trend (kg)",
                    data: trendLineChartMuscleData
                  }]}
                />
                {calendarData.data.length > 0 && <>
                  <Calendar
                    height={250}
                    width={600}
                    from={calendarData.from}
                    to={calendarData.to}
                    data={calendarData.data}
                    emptyColor="#eeeeee"
                    colors={calendarData.colorScheme}
                    margin={{ top: 20, right: 10, bottom: 40, left: 40 }}
                    yearSpacing={40}
                    monthBorderColor="#ffffff"
                    dayBorderWidth={2}
                    dayBorderColor="#ffffff"
                    minValue={calendarData.minValue}
                    maxValue={calendarData.maxValue}
                    legends={[
                      {
                        anchor: 'bottom-right',
                        direction: 'row',
                        translateY: 36,
                        itemCount: 4,
                        itemWidth: 42,
                        itemHeight: 36,
                        itemsSpacing: 14,
                        itemDirection: 'right-to-left'
                      }
                    ]}
                  />
                </>}
              </CardContent>
            </Card>
          )
        }
      </HorizontalGrid>
      <Navigation currentPage={Pages.phases}/>
    </>
  );
}
