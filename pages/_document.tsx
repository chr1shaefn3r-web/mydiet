import Document, { Html, Head, Main, NextScript } from 'next/document';
import Manifest from "../public/manifest.json"

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <link href="/mydiet_96px.png" rel="icon" type="image/png" />
          <link href="/apple-touch-icon.png" rel="apple-touch-icon" type="image/png" />
          {/* PWA primary color */}
          <meta name="theme-color" content={Manifest.theme_color} />
          <link rel="preload" as="style" href="/css/md-roboto-font.css" />
          <link href="/manifest.json" rel="manifest" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
