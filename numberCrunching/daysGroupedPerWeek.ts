import { formatDayAsYearCW } from "./formatDayAsYearCW";
import concatArrayEntries from "./concatArrayEntries";

export const daysGroupedPerWeek = (days: string[]) => {
    return days
    .map((day: string) => {
        return [formatDayAsYearCW(day), day]
    })
    .reduce(concatArrayEntries, {});
};
