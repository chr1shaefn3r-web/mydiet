import { linearRegression } from "./linearRegression";

const linearSampleDataModels = {
    zeroGradient: {
        r2: NaN,
        equation: [0, 10],
        predicted: [10, 10],
        string: 'y = 0x + 10',
        data: [[10, 10], [10, 10], [10, 10]],
        points: [[10, 10], [10, 10], [10, 10]],
    },

    zeroIntercept: {
        r2: 1,
        equation: [2, 0],
        string: 'y = 2x',
        predicted: [10, 20],
        data: [[0, 0], [1, 2], [2, 4], [3, 6]],
        points: [[0, 0], [1, 2], [2, 4], [3, 6]],
    },

    positiveGradient: {
        r2: 1,
        equation: [2, 1],
        predicted: [20, 41],
        string: 'y = 2x + 1',
        data: [[10, 21], [100, 201], [1000, 2001], [10000, 20001]],
        points: [[10, 21], [100, 201], [1000, 2001], [10000, 20001]],
    },

    negativeGradient: {
        r2: 1,
        predicted: [3, -7],
        equation: [-2, -1],
        string: 'y = -2x + -1',
        data: [[10, -21], [100, -201], [1000, -2001], [10000, -20001]],
        points: [[10, -21], [100, -201], [1000, -2001], [10000, -20001]],
    },

    positiveGradientWithEmpty: {
        r2: 1,
        equation: [2, 1],
        predicted: [20, 41],
        string: 'y = 2x + 1',
        data: [[10, 21], [100, null], [1000, 2001], [10000, null]],
        points: [[10, 21], [100, 201], [1000, 2001], [10000, 20001]],
    },

};

describe('linearRegression', () => {
    Object.keys(linearSampleDataModels).forEach((name) => {
        const example = linearSampleDataModels[name];
        describe(name, () => {
            it(`correctly predicts ${name}`, () => {
                let result = linearRegression(example.data);
                delete result.predict;
                expect(result).toEqual({
                    r2: example.r2,
                    string: example.string,
                    points: example.points,
                    equation: example.equation,
                });
            });

            it('should correctly forecast data points', () => {
                const result = linearRegression(example.data);
                expect(result.predict(example.predicted[0])).toEqual(example.predicted);
            });
        });
    });
});