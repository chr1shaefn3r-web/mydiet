import { weightInAWeek } from "./weightInAWeek";

describe("weightInAWeek", () => {
  it("asks the predict function for a weight for all seven days in a week", () => {
    const mockPredict = jest.fn().mockReturnValue([0, 0]);
    weightInAWeek("2020CW02", mockPredict);
    expect(mockPredict.mock.calls.length).toBe(7);
  });
  it("returns the average of the predicted weights - same weight for every day", () => {
    const mockWeight = 3;
    const mockPredict = jest.fn().mockReturnValue([0, mockWeight]);
    const actual = weightInAWeek("2020CW02", mockPredict);
    expect(actual).toBe(mockWeight);
  });
  it("returns the average of the predicted weights - different weight for every day", () => {
    const mockPredict = jest.fn()
      .mockReturnValueOnce([0, 3])
      .mockReturnValueOnce([0, 3])
      .mockReturnValueOnce([0, 3])
      .mockReturnValueOnce([0, 4])
      .mockReturnValueOnce([0, 5])
      .mockReturnValueOnce([0, 5])
      .mockReturnValueOnce([0, 5]);
    const actual = weightInAWeek("2020CW02", mockPredict);
    expect(actual).toBe(4);
  });
});
