import dayjs from "dayjs";
import { dayFormat } from "./formatDay";

const dayCardHeaderFormat = `${dayFormat} (dd)`;

const formatDayCardHeader = (dayName: string) => {
    return dayjs(dayName).format(dayCardHeaderFormat);
};

export default formatDayCardHeader;
