import { Phase } from "../data/phases";
import { parseYearCWAsMoment } from "./parseYearCWAsMoment";

const totalWeeksInAPhase = (phase: Phase): number => {
    const {startWeek, endWeek} = phase;
    const startMoment = parseYearCWAsMoment(startWeek)
    const endMoment = parseYearCWAsMoment(endWeek)
    const yearDifference = endMoment.year() - startMoment.year();
    return endMoment.isoWeek() - startMoment.isoWeek() + 1 + (yearDifference * 52);
};

export default totalWeeksInAPhase
