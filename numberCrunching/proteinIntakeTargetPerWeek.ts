import { phaseOfWeek } from "./phaseOfWeek";
import { Phases } from "../data/phases";
import getBodyWeightOfAWeekInAPhase from "./getBodyWeightOfAWeekInAPhase";

const proteinIntakeTargetPerWeek = (
  week: string,
  phases: Phases,
): number => {
  const phase = phaseOfWeek(week, phases);
  if (!phase) {
    return 0;
  }
  if (!phase.targets.proteinIntakePerBodyWeight) {
    return 0;
  }
  const bodyWeight = getBodyWeightOfAWeekInAPhase(week, phase);
  return Math.floor(bodyWeight * phase.targets.proteinIntakePerBodyWeight);
};

export default proteinIntakeTargetPerWeek;
