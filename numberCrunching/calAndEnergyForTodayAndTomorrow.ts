import recipes from './recipesAsNamedMacros'
import calculateCalAndEnergyPerDayDiet, { NamedCalAndEnergy } from "./calculateCalAndEnergyPerDayDiet";
import { diet } from "../data/diet";
import { NamedDayDiet } from './weeksWithNamedDayDiets';
import { phases } from '../data/phases';
import { ingredients } from '../data/ingredients'
import dayjs from 'dayjs';
import formatDay from './formatDay';

const calAndEnergyForTodayAndTomorrow = (): NamedCalAndEnergy[] => {
    const recipesMap = recipes();
    const transformDayDietToCalAndProtein = calculateCalAndEnergyPerDayDiet(recipesMap, phases, ingredients);
    const todayString = formatDay(dayjs());
    const tomorrowString = formatDay(dayjs().add(1, 'day'));
    const todayAndTheDayAfter = [todayString, tomorrowString];
    const dayDiets = todayAndTheDayAfter.reduce<NamedDayDiet[]>((acc, dayString) => {
        const dayDiet = diet[dayString];
        if (dayDiet) {
            acc.push({ name: dayString, ...diet[dayString] });
        }
        return acc;
    }, []);
    return dayDiets.map(transformDayDietToCalAndProtein);
};

export default calAndEnergyForTodayAndTomorrow;
