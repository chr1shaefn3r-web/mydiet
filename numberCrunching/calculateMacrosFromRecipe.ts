import { Ingredients, Macros, Macro, Name } from "../data/ingredients";
import { Transform } from "./mapTransducer";
import { Recipe, Amount } from "../data/recipes";

type CalculateMacrosFromRecipe = (ingredients: Ingredients) => Transform<[Name, Recipe], [Name, Macros]>;

const zeroMacros: Macros = {
    "cal": 0,
    "fat": 0,
    "saturated": 0,
    "carbs": 0,
    "sugar": 0,
    "protein": 0,
    "salt": 0
};

const calculateMacrosFromRecipe: CalculateMacrosFromRecipe = (ingredients: Ingredients) => ([name, recipe]) => {
    const sumOfMacros = Object.entries(recipe).reduce((acc: Macros, [ingredientKey, amount]) => {
        const macros = ingredients[ingredientKey];
        if (!macros) {
            throw new Error (`No ingredient found unter key='${ingredientKey}'.`)
        }
        const { cal, fat, saturated, carbs, sugar, protein, salt } = macros;
        return {
            cal: acc.cal + byAmount(cal, amount),
            fat: acc.fat + byAmount(fat, amount),
            saturated: acc.saturated + byAmount(saturated, amount),
            carbs: acc.carbs + byAmount(carbs, amount),
            sugar: acc.sugar + byAmount(sugar, amount),
            protein: acc.protein + byAmount(protein, amount),
            salt: acc.salt + byAmount(salt, amount)
        }
    }, zeroMacros);
    return [name, sumOfMacros];
};

export default calculateMacrosFromRecipe

function byAmount(macro: Macro, amount: Amount) {
    return macro / 100 * amount;
}