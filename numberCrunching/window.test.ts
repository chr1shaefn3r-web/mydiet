import { window } from "./window";

describe("window", () => {
    it("returns empty array if given no data", () => {
        const actual = window({}, "key", 2);
        expect(actual).toStrictEqual([]);
    });
    const mockKey = "key";
    it("returns the number at key if there is no enough data to window", () => {
        const value = 4;
        const actual = window({
            [mockKey]: value
        }, mockKey, 3);
        expect(actual).toStrictEqual([value]);
    });
    it("returns the values starting at key, as many as there are", () => {
        const value = 4;
        const actual = window({
            "b": 2,
            [mockKey]: value
        }, mockKey, 3);
        expect(actual).toStrictEqual([2, value]);
    });
    it("returns the values starting at key going back to window", () => {
        const value = 3;
        const actual = window({
            "a": 1,
            "b": 2,
            [mockKey]: value
        }, mockKey, 3);
        expect(actual).toStrictEqual([1, 2, value]);
    });
    it("returns the values starting at key going back to window - additional values before", () => {
        const value = 3;
        const actual = window({
            "before": -2,
            "a": 1,
            "b": 2,
            [mockKey]: value
        }, mockKey, 3);
        expect(actual).toStrictEqual([1, 2, value]);
    });
    it("returns the values starting at key going back to window - additional values after", () => {
        const value = 3;
        const actual = window({
            "a": 1,
            "b": 2,
            [mockKey]: value,
            "d": 4,
        }, mockKey, 3);
        expect(actual).toStrictEqual([1, 2, value]);
    });
});
