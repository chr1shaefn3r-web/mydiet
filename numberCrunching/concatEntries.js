const concatEntries = (a, c) => {
    return Object.assign(a, {
        [c[0]]: c[1]
    })
}

export default concatEntries;
