import { Phase } from "../data/phases";
import { daysInAPhase } from "./daysInAPhase";
import { getWeightData } from "./computedPhases";
import { linearRegression } from "./linearRegression";
import { weightInAWeek } from "./weightInAWeek";

const getBodyWeightOfAWeekInAPhase = (week: string, phase: Phase): number => {
  if (phase.fakeWeight) {
    return phase.fakeWeight;
  }
  const days = daysInAPhase(phase.startWeek, phase.endWeek);
  const weightData = getWeightData(days);
  const result = linearRegression(weightData);
  return weightInAWeek(week, result.predict);
};

export default getBodyWeightOfAWeekInAPhase;
