import recipes from './recipesAsNamedMacros'
import calculateCalAndEnergyPerDayDiet, { NamedCalAndEnergy } from "./calculateCalAndEnergyPerDayDiet";
import { diet } from "../data/diet";
import weeksWithNamedDayDiet from './weeksWithNamedDayDiets';
import concatEntries from './concatEntries';
import { phases } from '../data/phases';
import { ingredients } from '../data/ingredients';

export type CalAndEnergyPerDayWeek = { [key: string]: NamedCalAndEnergy[]; }

const CalAndEnergyPerDayWeek = (): CalAndEnergyPerDayWeek => {
    const recipesMap = recipes();
    const transformDayDietToCalAndProtein = calculateCalAndEnergyPerDayDiet(recipesMap, phases, ingredients);
    const weeks = weeksWithNamedDayDiet(diet);
    return Object.entries(weeks)
    .map(([key, namedDayDiets]) => {
        const calAndProteins = namedDayDiets.map(transformDayDietToCalAndProtein);
        return [key, calAndProteins];
    })
    .sort((a, b) => {
		const first = (a[0] as string).toLowerCase();
		const second = (b[0] as string).toLowerCase();
		return (second < first ? -1 : (second > first ? 1 : 0));
    })
    .reduce(concatEntries, {});
};

export default CalAndEnergyPerDayWeek;
