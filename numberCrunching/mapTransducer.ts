export type Reducer<T> = (accumulator: T, current: T) => T;
export type Transducer = (reducer: Reducer<any>) => Reducer<any>;
export type Transform<T, P> = (element: T) => P
export type MapTransducer = (transform: Transform<any, any>) => Transducer;

const mapTransducer: MapTransducer = (transform: Transform<any, any>): Transducer =>
    (nextReducer: Reducer<any>) =>
        (accumulator, current) => nextReducer(accumulator, transform(current));

export default mapTransducer
