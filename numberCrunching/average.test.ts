import { average } from "./average";

describe("average", () => {
  it("returns 0 if given nothing", () => {
    const actual = average();
    expect(actual).toBe(0);
  });
  it("returns first number if one is given", () => {
    const actual = average([42]);
    expect(actual).toBe(42);
  });
  it("returns floored first number if one is given", () => {
    const actual = average([42.5]);
    expect(actual).toBe(42);
  });
  it("returns first number if two identical numbers are given", () => {
    const actual = average([10, 10]);
    expect(actual).toBe(10);
  });
  it("returns average of two numbers", () => {
    const actual = average([100, 200]);
    expect(actual).toBe(150);
  });
  it("returns floored average of two numbers", () => {
    const actual = average([3, 4]);
    expect(actual).toBe(3);
  });
});
