import { defFormat, padLeft } from "@thi.ng/strings";
import dayjs from "dayjs";
import isoWeek from "dayjs/plugin/isoWeek"
dayjs.extend(isoWeek);

const fmt = defFormat([padLeft(2, "0")]);
export const formatDayAsYearCW = (day: string) => {
  const m = dayjs(day);
  return `${m.year()}CW${fmt(m.isoWeek())}`;
};
