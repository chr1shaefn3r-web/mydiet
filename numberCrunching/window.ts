import { AverageDailyCalorieBurnPerWeek } from "../data/dayAverageCaloriesPerWeekFitbit";

export const window = (
    dataMap: AverageDailyCalorieBurnPerWeek,
    key: string,
    windowSize: number,
): number[] => {
    const endIndex = Object.keys(dataMap).findIndex((value) => value === key);
    const startIndex = endIndex - windowSize;
    const data = Object.values(dataMap);
    const sliceEndIndex = endIndex + 1;
    const sliceStartIndex = startIndex >= 0 ? startIndex + 1 : 0;
    return data.slice(sliceStartIndex, sliceEndIndex);
};
