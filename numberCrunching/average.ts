export const average = (
  numbers: number[] = []
): number => {
  if (numbers.length === 0) {
    return 0;
  }
  if (numbers.length === 1) {
    return Math.floor(numbers[0]);
  }
  return Math.floor(numbers.reduce((a,b)=>a+b) / numbers.length);
};
