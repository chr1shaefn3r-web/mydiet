import dayjs from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
dayjs.extend(isBetween);
import { StartWeek, EndWeek } from "../data/phases";
import { dayFormat } from "./formatDay";
import { parseYearCWAsMoment } from "./parseYearCWAsMoment";

export const daysInAPhase = (
  startWeek: StartWeek,
  endWeek?: EndWeek,
  regularWeighingDays?: number[]
): string[] => {
  if (startWeek === endWeek) {
    return [];
  }
  const startWeekMoment = parseYearCWAsMoment(startWeek);
  const startOfEndWeek = endWeek ? parseYearCWAsMoment(endWeek) : startWeekMoment;
  const endWeekMoment = startOfEndWeek.add(7, 'day');
  const days = daysBetween(startWeekMoment, endWeekMoment);
  if (regularWeighingDays) {
    return days.filter((day) => {
      const dayMoment = dayjs(day, dayFormat);
      return regularWeighingDays.includes(dayMoment.day());
    });
  }
  return days;
};

const daysBetween = (start: dayjs.Dayjs, end: dayjs.Dayjs): string[] => {
  let runningDay = start;
  const days = [];
  do {
      days.push(runningDay.format(dayFormat))
      runningDay = runningDay.add(1, 'day');
  } while(runningDay.isBetween(start, end))
  return days;
};
