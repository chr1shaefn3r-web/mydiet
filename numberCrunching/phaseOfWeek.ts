import { Phase } from "../data/phases";
import { parseYearCWAsMoment } from "./parseYearCWAsMoment";

// https://day.js.org/docs/en/plugin/is-between
const includeStartAndEndDate = "[]";

export const phaseOfWeek = (
  week: string,
  phases: Phase[],
): Phase | undefined => {
  return phases.find((phase) => {
    const weekMoment = parseYearCWAsMoment(week);
    const phaseStartWeekMoment = parseYearCWAsMoment(phase.startWeek);
    const phaseEndWeekMoment = parseYearCWAsMoment(phase.endWeek);
    return weekMoment.isBetween(phaseStartWeekMoment, phaseEndWeekMoment, 'day', includeStartAndEndDate);
  });
};
