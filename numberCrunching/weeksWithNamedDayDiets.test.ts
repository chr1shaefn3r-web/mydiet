import weeksWithNamedDayDiets, { NamedDayDiet } from "./weeksWithNamedDayDiets"
import { DayDiet } from "../data/diet";

describe('weeksWithNamedDayDiets', () => {
    it('returns no weeks given no DayDiets', () => {
        expect(weeksWithNamedDayDiets({})).toEqual({});
    })
    const emptyDayDiet: DayDiet = {
        "breakfast": [],
        "lunch": [],
        "dinner": []
    };
    it('returns one week with one DayDiet', () => {
        const firstEmptyNamedDayDiet = emptyNamedDayDiet("2020-01-01");
        expect(weeksWithNamedDayDiets({
            "2020-01-01": emptyDayDiet
        })).toEqual({
            "2020CW01": [firstEmptyNamedDayDiet]
        });
    });
    it('returns one week with two DayDiets', () => {
        const firstEmptyDayDiet = emptyNamedDayDiet("2020-01-01");
        const secondEmptyDayDiet = emptyNamedDayDiet("2020-01-02");
        expect(weeksWithNamedDayDiets({
            "2020-01-01": emptyDayDiet,
            "2020-01-02": emptyDayDiet
        })).toEqual({
            "2020CW01": [firstEmptyDayDiet, secondEmptyDayDiet]
        });
    })

    function emptyNamedDayDiet(name: string): NamedDayDiet {
        return {
            name,
            ...emptyDayDiet
        }
    }
})