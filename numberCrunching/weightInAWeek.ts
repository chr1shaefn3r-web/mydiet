import dayjs from "dayjs";
import { StartWeek } from "../data/phases";
import { Predict } from "./linearRegression";
import { daysInAPhase } from "./daysInAPhase";
import { Bodyweight } from "../data/weights";

export const weightInAWeek = (
  week: StartWeek,
  predict: Predict,
) => {
  const days = daysInAPhase(week);
  const weights = days
    .map((day) => predict(dayjs(day).unix()))
    .map(([_, weight]) => weight);
  return average(weights);
};

function average(weights: Bodyweight[]) {
  const sum = weights.reduce((acc, weight) => acc + weight);
  return sum / weights.length;
}
