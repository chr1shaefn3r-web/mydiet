import dayjs from "dayjs";
import { Phase } from "../data/phases";
import { dayFormat } from "./formatDay";
import { parseYearCWAsMoment } from "./parseYearCWAsMoment";

// https://day.js.org/docs/en/plugin/is-between
const includeStartAndEndDate = "[]";

export const phaseOfDay = (
  day: string,
  phases: Phase[],
): Phase | undefined => {
  return phases.find((phase) => {
    const dayMoment = dayjs(day, dayFormat);
    const phaseStartWeekMoment = parseYearCWAsMoment(phase.startWeek);
    const phaseEndWeekMoment = parseYearCWAsMoment(phase.endWeek);
    return dayMoment.isBetween(phaseStartWeekMoment, phaseEndWeekMoment, 'day', includeStartAndEndDate);
  });
};
