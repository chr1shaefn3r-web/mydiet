import dayjs from "dayjs";
import formatDay, { dayFormat } from "./formatDay";

describe("formatDay", () => {
    describe("formatDay", () => {
        it("test", () => {
            expect(formatDay(dayjs(0))).toBe("1970-01-01");
        })
    });
    describe("dayFormat", () => {
        it("exports a non-empty string", () => {
            expect(dayFormat.length).toBeGreaterThan(0);
        });
    })
});
