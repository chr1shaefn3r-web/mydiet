import { dayAverageCaloriesPerWeekByBodyweight } from "./dayAverageCaloriesPerWeekByBodyweight";
import { TotalEnergyExpenditure } from "../data/phases";

describe("dayAverageCaloriesPerWeekByBodyweight", () => {
    const daysInAWeek = 7;
    const mockTotalEnergyExpenditure: TotalEnergyExpenditure = {
      sleep: {
        duration: 1,
        intensity: 1,
      },
      normal: {
        duration: 1,
        intensity: 1,
      },
      training: []
    };
  it("returns sum of sleep and normal energy expenditure (1kg)", () => {
    const mockBodyweight = 1;

    const actual = dayAverageCaloriesPerWeekByBodyweight(mockTotalEnergyExpenditure, mockBodyweight);
    expect(actual).toBe(2);
  });
  it("returns sum of sleep and normal energy expenditure (70kg)", () => {
    const mockBodyweight = 70;

    const actual = dayAverageCaloriesPerWeekByBodyweight(mockTotalEnergyExpenditure, mockBodyweight);
    expect(actual).toBe(mockBodyweight*2);
  });
  const mockTotalEnergyExpenditureWithOneTraining: TotalEnergyExpenditure = {
    ...mockTotalEnergyExpenditure,
    training: [{
        duration: 1,
        intensity: 2,
      }]
  };
  it("returns sum of sleep, normal and one training energy expenditure (70kg)", () => {
    const mockBodyweight = 70;

    const actual = dayAverageCaloriesPerWeekByBodyweight(mockTotalEnergyExpenditureWithOneTraining, mockBodyweight);
    expect(actual).toBe(mockBodyweight*2 + mockBodyweight/daysInAWeek);
  });
});
