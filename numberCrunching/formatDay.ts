import dayjs from "dayjs";

export const dayFormat = "YYYY-MM-DD";

const formatDay = (day: dayjs.Dayjs) => {
    return day.format(dayFormat);
};

export default formatDay;
