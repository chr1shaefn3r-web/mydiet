import { Day } from "../data/phases";
import { daysInAPhase } from "./daysInAPhase";

describe("daysInAPhase", () => {
  it("returns the seven days of the week if only startweek is given", () => {
    const actual = daysInAPhase("2020CW27");
    expect(actual).toEqual(
      [
        "2020-07-03",
        "2020-07-04",
        "2020-07-05",
        "2020-07-06",
        "2020-07-07",
        "2020-07-08",
        "2020-07-09",
      ],
    );
  });
  it("returns an empty array if startWeek and endWeek are the same", () => {
    const actual = daysInAPhase("2020CW02", "2020CW02");
    expect(actual).toEqual([]);
  });
  it("returns fourteen days if startWeek and endWeek are one week appart", () => {
    const actual = daysInAPhase("2020CW27", "2020CW28");
    expect(actual.length).toBe(14);
  });
  it("returns the fourteen days if startWeek and endWeek are one week appart", () => {
    const actual = daysInAPhase("2020CW27", "2020CW28");
    expect(actual).toEqual(
      [
        "2020-07-03",
        "2020-07-04",
        "2020-07-05",
        "2020-07-06",
        "2020-07-07",
        "2020-07-08",
        "2020-07-09",
        "2020-07-10",
        "2020-07-11",
        "2020-07-12",
        "2020-07-13",
        "2020-07-14",
        "2020-07-15",
        "2020-07-16",
      ],
    );
  });
  it("throws exception for unparseable days", () => {
    const mockMalformedYearCw = "abcdefg";
    expect(() => daysInAPhase("2020CW02", mockMalformedYearCw)).toThrowError(
      mockMalformedYearCw,
    );
  });
  it("returns the two days a one day filter lets through in two weeks", () => {
    const actual = daysInAPhase("2020CW27", "2020CW28", [Day.Tuesday]);
    expect(actual).toEqual(
      [
        "2020-07-07",
        "2020-07-14",
      ],
    );
  });
});
