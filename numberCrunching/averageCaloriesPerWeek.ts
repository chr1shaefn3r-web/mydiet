import {
  AverageDailyCalorieBurn,
  averageDailyCalorieBurnPerWeek as byFitbit,
} from "../data/dayAverageCaloriesPerWeekFitbit";
import { phaseOfWeek } from "./phaseOfWeek";
import { Phase, phases } from "../data/phases";
import { average } from "./average";
import { window } from "./window";
import { dayAverageCaloriesPerWeekByBodyweight } from "./dayAverageCaloriesPerWeekByBodyweight";
import getBodyWeightOfAWeekInAPhase from "./getBodyWeightOfAWeekInAPhase";

export const averageDailyCaloriePerWeek = (
  key: string,
): AverageDailyCalorieBurn => {
  const phase = phaseOfWeek(key, phases);
  const byBodyweight = getBodyWeight(key, phase);
  // looks like fitbit data is alternating, e.g.: [hight, low, high, low]
  const fitbitWindowSize = 4;
  const fitbits = window(byFitbit, key, fitbitWindowSize);
  return average([byBodyweight, ...fitbits]);
};

function getBodyWeight(key: string, phase: Phase | undefined): number {
  if (!phase) {
    return 0;
  }
  const bodyWeight = getBodyWeightOfAWeekInAPhase(key, phase);
  return dayAverageCaloriesPerWeekByBodyweight(
    phase.totalEnergyExpenditure,
    bodyWeight,
  );
}
