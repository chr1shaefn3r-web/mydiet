import { ColoricDifference, Phase } from "../data/phases";
import {phaseOfDay} from "./phaseOfDay"

describe("phaseOfDay", () => {
    it("returns undefined if there are no phases given", () => {
        expect(phaseOfDay("2021-03-19", [])).toBeUndefined();
    });
    const mockPhase: Phase = {
        name: "mockphase",
        startWeek: "2020CW24",
        endWeek: "2021CW09",
        coloricDifference: ColoricDifference.MAINTENANCE,
        targets: {
          bodyWeight: 1,
          bodyfatInPercent: 1,
          muscleInKg: 1,
          proteinIntakePerBodyWeight: 1,
        },
        totalEnergyExpenditure: {
          sleep: {
            duration: 1,
            intensity: 1
          },
          normal: {
            duration: 1,
            intensity: 1
          },
          training: []
        }
    }
    it("returns undefined if given week is not within a given phase", () => {
        expect(phaseOfDay("2021-03-19", [mockPhase])).toBeUndefined();
    });
    it("returns the phase the given week is within", () => {
        expect(phaseOfDay("2021-01-19", [mockPhase])).toEqual(mockPhase);
    });
});
