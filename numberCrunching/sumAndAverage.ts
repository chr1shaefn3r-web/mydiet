export const sumReducer = (a: number, v: number): number => a + v;

export const sumAndaverage = (data: number[]) => {
    const sum = data.reduce(sumReducer);
    const average = Math.floor(sum / data.length);
    return {
        sum, average
    }
}
