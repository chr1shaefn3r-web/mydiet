import { getExtendedMockPhase } from "../data/phases";
import totalWeeksInAPhase from "./totalWeeksInAPhase";

describe("totalWeeksInAPhase", () => {
    it('returns two weeks given one CW and CW+1', () => {
        const actualTotalWeeks = totalWeeksInAPhase(getExtendedMockPhase({
            startWeek: "2021CW42",
            endWeek: "2021CW43",
        }));

        expect(actualTotalWeeks).toBe(2);
    });
    it('returns correct amount of weeks across years', () => {
        const actualTotalWeeks = totalWeeksInAPhase(getExtendedMockPhase({
            startWeek: "2020CW50",
            endWeek: "2021CW01",
        }));

        expect(actualTotalWeeks).toBe(4);
    });
});
