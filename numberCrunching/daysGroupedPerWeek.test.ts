import { daysGroupedPerWeek } from "./daysGroupedPerWeek";

describe("daysGroupedPerWeek", () => {
  it("should return an empty object when given no days", () => {
    const actual = daysGroupedPerWeek([]);
    expect(actual).toEqual({});
  });
  it("should return an object with one week and one day given one day (sunday)", () => {
    const actual = daysGroupedPerWeek(["2020-07-26"]);
    expect(actual).toEqual({
      ["2020CW30"]: ["2020-07-26"],
    });
  });
  it("should return an object with one week and one day given one day (monday)", () => {
    const actual = daysGroupedPerWeek(["2020-07-27"]);
    expect(actual).toEqual({
      ["2020CW31"]: ["2020-07-27"],
    });
  });
  it("should return an object with one week and two days given two days in one week", () => {
    const days = ["2020-07-25", "2020-07-26"];
    const actual = daysGroupedPerWeek(days);
    expect(actual).toEqual({
      ["2020CW30"]: days,
    });
  });
  it("should return an object with two weeks and one day each given two days in different weeks", () => {
    const days = ["2020-07-26", "2020-07-27"];
    const actual = daysGroupedPerWeek(days);
    expect(actual).toEqual({
      ["2020CW30"]: [days[0]],
      ["2020CW31"]: [days[1]],
    });
  });
});
