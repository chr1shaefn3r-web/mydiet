import dayjs from "dayjs";
import isoWeek from "dayjs/plugin/isoWeek"
dayjs.extend(isoWeek);
import isBetween from "dayjs/plugin/isBetween";
dayjs.extend(isBetween);

export const parseYearCWAsMoment = (week: string): dayjs.Dayjs => {
  const regex = new RegExp("(\\d{4})CW(\\d{2})");
  const parts = regex.exec(week);
  if (!parts) {
    throw new Error(`Found invalid YearCW: '${week}'`);
  }
  const [, year, cw] = parts;
  const m = dayjs()
    .millisecond(0)
    .second(0)
    .minute(0)
    .hour(0)
    .isoWeekday(1) // Monday
    .year(Number(year))
    .isoWeek(Number(cw));
  return m;
};
