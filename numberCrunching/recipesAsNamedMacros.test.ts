import recipes from "./recipesAsNamedMacros";
import { recipesAsNamedMacros } from "./recipesAsNamedMacros";

describe("recipesAsNamedMacros", () => {
    it('default export returns something', () => {
        const actual = recipes();
        expect(Object.entries(actual).length).toBeGreaterThan(0);
    });
    it('returns named macros for one recipe with one ingredient', () => {
        const mockIngredients = {
            "GoertzHerzschlag": {
                "name": "Goertz Herzschlag",
                "cal": 252,
                "fat": 3.2,
                "saturated": 0.5,
                "carbs": 45,
                "sugar": 0.6,
                "protein": 8.5,
                "salt": 1.2
            }
        };
        const mockRecipeName = "mockRecipeName";
        const mockRecipes = {
            [mockRecipeName]: {
                "GoertzHerzschlag": 100
            }
        }
        const actual = recipesAsNamedMacros(mockRecipes, mockIngredients);

        expect(actual[mockRecipeName]).toEqual({
            "cal": 252,
            "fat": 3.2,
            "saturated": 0.5,
            "carbs": 45,
            "sugar": 0.6,
            "protein": 8.5,
            "salt": 1.2
        })
    })
});
