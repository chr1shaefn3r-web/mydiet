import calculateMacrosFromRecipe from "./calculateMacrosFromRecipe";

describe("calculateMacrosFromRecipe", () => {
    it('returns accumulated macros for recipe with one ingredient', () => {
        const mockIngredients = {
            "GoertzHerzschlag": {
                "name": "Goertz Herzschlag",
                "cal": 252,
                "fat": 3.2,
                "saturated": 0.5,
                "carbs": 45,
                "sugar": 0.6,
                "protein": 8.5,
                "salt": 1.2
            }
        };
        const mockRecipe = {
            "GoertzHerzschlag": 100
        }
        const mockRecipeName = "mockRecipeName";
        const mappingFunction = calculateMacrosFromRecipe(mockIngredients);
        const actual = mappingFunction([mockRecipeName, mockRecipe]);

        const [name, macros] = actual;
        expect(name).toBe(mockRecipeName);
        expect(macros.cal).toBe(mockIngredients.GoertzHerzschlag.cal);
        expect(macros.fat).toBe(mockIngredients.GoertzHerzschlag.fat);
        expect(macros.saturated).toBe(mockIngredients.GoertzHerzschlag.saturated);
        expect(macros.carbs).toBe(mockIngredients.GoertzHerzschlag.carbs);
        expect(macros.sugar).toBe(mockIngredients.GoertzHerzschlag.sugar);
        expect(macros.protein).toBe(mockIngredients.GoertzHerzschlag.protein);
        expect(macros.salt).toBe(mockIngredients.GoertzHerzschlag.salt);
    });
    it('returns accumulated macros for recipe with multiple ingredients', () => {
        const mockIngredients = {
            "BioBierschinken": {
                "name": "Bio-Bierschinken",
                "cal": 185,
                "fat": 12.3,
                "saturated": 5.2,
                "carbs": 0.7,
                "sugar": 0.5,
                "protein": 17.9,
                "salt": 2
            },
            "Cornichons": {
                "name": "Cornichons",
                "cal": 22,
                "fat": 0.5,
                "saturated": 0.1,
                "carbs": 2.9,
                "sugar": 2.8,
                "protein": 1.2,
                "salt": 1.3
            },
            "GoertzHerzschlag": {
                "name": "Goertz Herzschlag",
                "cal": 252,
                "fat": 3.2,
                "saturated": 0.5,
                "carbs": 45,
                "sugar": 0.6,
                "protein": 8.5,
                "salt": 1.2
            }
        };
        const mockRecipe = {
            "GoertzHerzschlag": 140,
            "BioBierschinken": 200,
            "Cornichons": 100
        }
        const mockRecipeName = "mockRecipeName";
        const mappingFunction = calculateMacrosFromRecipe(mockIngredients);
        const actual = mappingFunction([mockRecipeName, mockRecipe]);

        const [name, macros] = actual;
        expect(name).toBe(mockRecipeName);
        expect(macros.cal).toBe(((252/100)*140) + (185*2) + (22));
        expect(macros.fat).toBe(((3.2/100)*140) + (12.3*2) + (0.5));
        expect(macros.saturated).toBe(((0.5/100)*140) + (5.2*2) + (0.1));
        expect(macros.carbs).toBe(((45/100)*140) + (0.7*2) + (2.9));
        expect(macros.sugar).toBe(((0.6/100)*140) + (0.5*2) + (2.8));
        expect(macros.protein).toBe(((8.5/100)*140) + (17.9*2) + (1.2));
        expect(macros.salt).toBe(((1.2/100)*140) + (2*2) + (1.3));
    });
    it('throws exception for missing ingredient', () => {
        const mockIngredients = {};
        const mockMissingIngredientName = "GoertzHerzschlag";
        const mockRecipe = {
            [mockMissingIngredientName]: 100
        }
        const mockRecipeName = "mockRecipeName";
        const mappingFunction = calculateMacrosFromRecipe(mockIngredients);
        expect(() => mappingFunction([mockRecipeName, mockRecipe])).toThrowError(mockMissingIngredientName);
    });
});
