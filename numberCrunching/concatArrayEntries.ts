const concatArrayEntries = (a: { [key: string]: any[]; }, c: any) => {
    return Object.assign(a, {
        [c[0]]: a[c[0]] ? a[c[0]].concat(c[1]) : [c[1]]
    })
}

export default concatArrayEntries;
