import { Name, Cal, Protein, Fat, Carbs, Ingredients, NamedMacrosMap } from "../data/ingredients";
import { Transform } from "./mapTransducer";
import { NamedDayDiet } from "./weeksWithNamedDayDiets";
import { RecipeName } from "../data/recipes";
import { Phases } from "../data/phases";
import { phaseOfDay } from "./phaseOfDay";

type CalculateCalAndEnergyPerDay = (recipes: NamedMacrosMap, phases: Phases, ingredients: Ingredients) => Transform<NamedDayDiet, NamedCalAndEnergy>;

export interface NamedCalAndEnergy {
    name: Name;
    cal: Cal;
    fat: Fat;
    carbs: Carbs;
    protein: Protein;
    recipes: RecipeName[]
}

const zeroCalAndProtein: NamedCalAndEnergy = {
    "name": "",
    "cal": 0,
    "fat": 0,
    "carbs": 0,
    "protein": 0,
    "recipes": []
};

const calculateCalAndEnergyPerDayDiet: CalculateCalAndEnergyPerDay = (recipes: NamedMacrosMap, phases: Phases, ingredients: NamedMacrosMap) => (namedDayDiet) => {
    const snacks = namedDayDiet.snack || [];
    const allYummis = [...namedDayDiet.breakfast, ...namedDayDiet.lunch, ...snacks, ...namedDayDiet.dinner];
    const sumOfCalAndProtein = allYummis.reduce((acc: NamedCalAndEnergy, recipeKey) => {
        let macros = recipes[recipeKey] || ingredients[recipeKey];
        if (!macros) {
            throw new Error(`No recipes found unter key='${recipeKey}'.`)
        }
        const { fat, carbs, protein } = macros;
        let { cal } = macros;
        const phase = phaseOfDay(namedDayDiet.name, phases);
        if (phase && phase.postprandialeThermogenese) {
            const factorThermogenese = (1 - (phase.postprandialeThermogenese / 100));
            cal = cal * factorThermogenese;
        }
        return {
            name: namedDayDiet.name,
            cal: acc.cal + cal,
            fat: acc.fat + fat,
            carbs: acc.carbs + carbs,
            protein: acc.protein + protein,
            recipes: allYummis
        }
    }, zeroCalAndProtein);
    return sumOfCalAndProtein;
};

export default calculateCalAndEnergyPerDayDiet
