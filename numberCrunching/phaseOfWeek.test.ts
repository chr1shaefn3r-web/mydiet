import { ColoricDifference, Phase } from "../data/phases";
import {phaseOfWeek} from "./phaseOfWeek"

describe("phaseOfWeek", () => {
    it("returns undefined if there are no phases given", () => {
        expect(phaseOfWeek("2020CW30", [])).toBeUndefined();
    });
    const mockPhase: Phase = {
        name: "mockphase",
        startWeek: "2020CW24",
        endWeek: "2021CW09",
        coloricDifference: ColoricDifference.MAINTENANCE,
        targets: {
          bodyWeight: 1,
          bodyfatInPercent: 1,
          muscleInKg: 1,
          proteinIntakePerBodyWeight: 1,
        },
        totalEnergyExpenditure: {
          sleep: {
            duration: 1,
            intensity: 1
          },
          normal: {
            duration: 1,
            intensity: 1
          },
          training: []
        }
    }
    it("returns undefined if given week is not within a given phase", () => {
        expect(phaseOfWeek("1995CW30", [mockPhase])).toBeUndefined();
    });
    it("returns the phase the given week is within", () => {
        expect(phaseOfWeek("2020CW30", [mockPhase])).toEqual(mockPhase);
    });
});
