import { Ingredients, NamedMacrosMap } from "../data/ingredients";
import { ColoricDifference, Phases } from "../data/phases";
import calculateCalAndEnergyPerDayDiet from "./calculateCalAndEnergyPerDayDiet";
import { NamedDayDiet } from "./weeksWithNamedDayDiets";

describe("calculateCalAndEnergyPerDayDiet", () => {
    const noPhases: Phases = [];
    const mockRecipeName = "mockRecipeName";
    const mockRecipes: NamedMacrosMap = {
        [mockRecipeName]: {
            "name": mockRecipeName,
            "cal": 252,
            "fat": 3.2,
            "saturated": 0.5,
            "carbs": 45,
            "sugar": 0.6,
            "protein": 8.5,
            "salt": 1.2
        }
    }
    const mockDayDiet: NamedDayDiet = {
        name: "2000-01-05",
        breakfast: [],
        lunch: [mockRecipeName],
        dinner: [],
    };
    const mockNoIngredients: NamedMacrosMap = {};
    it('returns accumulated macros for day with one recipe', () => {
        const mappingFunction = calculateCalAndEnergyPerDayDiet(mockRecipes, noPhases, mockNoIngredients);
        const actual = mappingFunction(mockDayDiet);

        expect(actual.recipes).toEqual([mockRecipeName]);
        expect(actual.cal).toBe(mockRecipes[mockRecipeName].cal);
        expect(actual.fat).toBe(mockRecipes[mockRecipeName].fat);
        expect(actual.carbs).toBe(mockRecipes[mockRecipeName].carbs);
        expect(actual.protein).toBe(mockRecipes[mockRecipeName].protein);
    });
    const onePhaseReducingMacrosToZeroViaPostprandialeThermogenese: Phases = [{
        name: "MockPhase",
        startWeek: "2000CW01",
        endWeek: "2000CW02",
        coloricDifference: ColoricDifference.LEAN_SURPLUS,
        postprandialeThermogenese: 100, // Energy spend on digesting food
        targets: {
          bodyWeight: 0,
          bodyfatInPercent: 0,
          muscleInKg: 0,
          proteinIntakePerBodyWeight: 0,
        },
        totalEnergyExpenditure: {
          sleep: {
            duration: 0,
            intensity: 0,
          },
          normal: {
            duration: 0,
            intensity: 0,
          },
          training: [],
        },
      }];
    it('returns accumulated macros reduced by phase postprandialeThermogenese for day with one recipe within a phase', () => {
        const mappingFunction = calculateCalAndEnergyPerDayDiet(mockRecipes, onePhaseReducingMacrosToZeroViaPostprandialeThermogenese, mockNoIngredients);
        const actual = mappingFunction(mockDayDiet);

        expect(actual.recipes).toEqual([mockRecipeName]);
        expect(actual.cal).toBe(0); // postprandialeThermogenese=100 means all energy spend on digesting food
        expect(actual.fat).toBe(mockRecipes[mockRecipeName].fat);
        expect(actual.carbs).toBe(mockRecipes[mockRecipeName].carbs);
        expect(actual.protein).toBe(mockRecipes[mockRecipeName].protein);
    });
    const mockNoRecipes: NamedMacrosMap = {};
    const mockIngredients: Ingredients = {
        [mockRecipeName]: {
            "name": mockRecipeName,
            "cal": 252,
            "fat": 3.2,
            "saturated": 0.5,
            "carbs": 45,
            "sugar": 0.6,
            "protein": 8.5,
            "salt": 1.2
        }
    }
    it('returns accumulated macros for day with one recipe which is a ingredient', () => {
        const mappingFunction = calculateCalAndEnergyPerDayDiet(mockNoRecipes, noPhases, mockIngredients);
        const actual = mappingFunction(mockDayDiet);

        expect(actual.recipes).toEqual([mockRecipeName]);
        expect(actual.cal).toBe(mockRecipes[mockRecipeName].cal);
        expect(actual.fat).toBe(mockRecipes[mockRecipeName].fat);
        expect(actual.carbs).toBe(mockRecipes[mockRecipeName].carbs);
        expect(actual.protein).toBe(mockRecipes[mockRecipeName].protein);
    });
    it('throws error if day contains neither recipe nor ingredient', () => {
        const mappingFunction = calculateCalAndEnergyPerDayDiet(mockNoRecipes, noPhases, mockNoIngredients);
        expect(() => mappingFunction(mockDayDiet)).toThrowError();
    });
});
