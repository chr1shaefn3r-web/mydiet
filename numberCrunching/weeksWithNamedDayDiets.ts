import { Diet, DayDiet } from "../data/diet";
import concatArrayEntries from "./concatArrayEntries"
import { Name } from "../data/ingredients";
import { Transform } from "./mapTransducer";
import { formatDayAsYearCW } from "./formatDayAsYearCW";

export interface NamedDayDiet extends DayDiet {
    name: Name;
}

export type WeeksWithDayDiet = { [key: string]: NamedDayDiet[]; };

type WeeksWithNamedDayDiet = Transform<Diet, WeeksWithDayDiet>;

const weeksWithNamedDayDiet: WeeksWithNamedDayDiet = (diet: Diet): WeeksWithDayDiet => {
    return Object.entries(diet).map(([key, dayDiet]): [Name, NamedDayDiet] => {
        return [formatDayAsYearCW(key), {
            name: key,
            ...dayDiet
        }];
    }).reduce(concatArrayEntries, {});
}

export default weeksWithNamedDayDiet;
