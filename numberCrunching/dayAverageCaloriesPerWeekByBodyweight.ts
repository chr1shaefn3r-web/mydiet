import { Bodyweight } from "../data/weights";
import { TotalEnergyExpenditure, EnergyOutput } from "../data/phases";

const daysInAWeek = 7;
export const dayAverageCaloriesPerWeekByBodyweight = ({sleep, normal, training}: TotalEnergyExpenditure, bodyWeight: Bodyweight): number => {
    const calculateCaloriesByBodyweight = calculateCalories.bind(null, bodyWeight);
    const sleepOutput = calculateCaloriesByBodyweight(sleep);
    const normalOutput = calculateCaloriesByBodyweight(normal);
    const trainingOutput = training.reduce((acc, training) => {
        return acc + calculateCaloriesByBodyweight({
            ...training,
            intensity: training.intensity - normal.intensity
        });
    }, 0);
    return (sleepOutput * daysInAWeek + normalOutput * daysInAWeek + trainingOutput)/daysInAWeek;
}

function calculateCalories(bodyWeight: Bodyweight, energieOutput: EnergyOutput): number {
    return energieOutput.duration * energieOutput.intensity * bodyWeight;
}