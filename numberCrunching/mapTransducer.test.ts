import mapTransducer from "./mapTransducer";

describe("mapTransducer", () => {
    const doNothing = () => {};
    const inputList = [1,2,3];
    it('should call transform for every item in the list', () => {
        const spy = jest.fn();
        const transducer = mapTransducer(spy);
        const xform = transducer(doNothing);

        inputList.reduce(xform, []);

        expect(spy).toHaveBeenCalledTimes(inputList.length);
    });

    it('should call the next reducer for every item in the list', () => {
        const spy = jest.fn();
        const transducer = mapTransducer(doNothing);
        const xform = transducer(spy);

        inputList.reduce(xform, []);

        expect(spy).toHaveBeenCalledTimes(inputList.length);
    });
});
