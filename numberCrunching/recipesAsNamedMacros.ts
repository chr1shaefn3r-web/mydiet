import mapTransducer from "./mapTransducer";
import calculateMacrosFromRecipe from "./calculateMacrosFromRecipe";
import { Ingredients, ingredients, NamedMacrosMap } from "../data/ingredients";
import compose from "./compose";
import concatEntries from "./concatEntries";
import { Recipes, recipes } from "../data/recipes";

const RecipesAndNamesMacrosFromJson = (): NamedMacrosMap => {
    return recipesAsNamedMacros(recipes, ingredients);
}

export const recipesAsNamedMacros = (recipes: Recipes, ingredients: Ingredients): NamedMacrosMap => {
    const a = mapTransducer(calculateMacrosFromRecipe(ingredients));
    const xform = compose(a)(concatEntries);
    return Object.entries(recipes).reduce(xform, {});
}

export default RecipesAndNamesMacrosFromJson;
