import dayjs from 'dayjs';
import { phases, Phase, ColorScheme, coloricDifferenceColorSchemas, coloricDifferenceMaxValue, coloricDifferenceMinValue } from '../data/phases';
import { bodyCompositionPerDay } from '../data/weights';
import { daysInAPhase } from './daysInAPhase';
import { linearRegression } from './linearRegression';
import { daysGroupedPerWeek } from './daysGroupedPerWeek';
import { averageDailyCaloriePerWeek } from './averageCaloriesPerWeek';
import calAndEnergyPerDayWeek, { CalAndEnergyPerDayWeek } from './calAndEnergyPerDayWeek';
import { sumAndaverage } from './sumAndAverage';
import { enoughDaysPerWeek } from '../pages';
import totalWeeksInAPhase from './totalWeeksInAPhase';
import { dayFormat } from './formatDay';

type dataPoint = { x: string, y: number};

interface MaxMins {
    minWeightData: number,
    maxWeightData: number,
    minBodyfatData: number,
    maxBodyfatData: number,
    minMuscleData: number,
    maxMuscleData: number,
}

interface InternalComputedPhase extends ComputedPhase, MaxMins {
}
interface ComputedPhase extends Phase {
    startDay: string;
    endDay: string;
    totalWeeks: number;
    lineChartWeightData: Array<dataPoint>;
    targetLineChartWeightData: Array<dataPoint>;
    trendLineChartWeightData: Array<dataPoint>;
    lineChartBodyfatData: Array<dataPoint>;
    targetLineChartBodyfatData: Array<dataPoint>;
    trendLineChartBodyfatData: Array<dataPoint>;
    lineChartMuscleData: Array<dataPoint>;
    targetLineChartMuscleData: Array<dataPoint>;
    trendLineChartMuscleData: Array<dataPoint>;
    calendarData: CalendarData
}

type CalendarRawData = Array<{ day: string, value: number}>;

interface CalendarData {
    from: string;
    to: string;
    data: CalendarRawData;
    colorScheme: ColorScheme;
    maxValue: number;
    minValue: number;
}

export interface ComputedPhases extends MaxMins {
    computedPhases: ComputedPhase[];

};

export const computedPhases = (): ComputedPhases => {
    const computedPhases = phases.map(computePhase);
    // Phases without weight data have NaN
    return {
        computedPhases,
        minWeightData: min(computedPhases.map(({minWeightData}) => minWeightData).filter(notNaN)) - 1,
        maxWeightData: max(computedPhases.map(({maxWeightData}) => maxWeightData).filter(notNaN)) + 1,
        minBodyfatData: min(computedPhases.map(({minBodyfatData}) => minBodyfatData).filter(notNaN)) - 1,
        maxBodyfatData: max(computedPhases.map(({maxBodyfatData}) => maxBodyfatData).filter(notNaN)) + 1,
        minMuscleData: min(computedPhases.map(({minMuscleData}) => minMuscleData).filter(notNaN)) - 2,
        maxMuscleData: max(computedPhases.map(({maxMuscleData}) => maxMuscleData).filter(notNaN)) + 2,
    }
}

const notNaN = (number: number) => !Number.isNaN(number);

function computePhase(phase: Phase): InternalComputedPhase {
    const {startWeek, endWeek, regularWeighingDays} = phase;
    const days = daysInAPhase(startWeek, endWeek);
    const startDay = days[0];
    const endDay = days[days.length - 1];
    const weightingDays = daysInAPhase(startWeek, endWeek, regularWeighingDays);
    const totalWeeks = totalWeeksInAPhase(phase);
    const weightData = computeActualTargetTrendWeightData(phase, weightingDays, startDay, endDay);
    const bodyfatData = computeActualTargetTrendBodyfatData(phase, weightingDays, startDay, endDay);
    const muscleData = computeActualTargetTrendMuscleData(phase, weightingDays, startDay, endDay);
    const calendarData = computeCalendarData(phase, calAndEnergyPerDayWeek(), days, startDay, endDay);
    return {
        startDay,
        endDay,
        totalWeeks,
        ...weightData,
        ...bodyfatData,
        ...muscleData,
        calendarData,
        ...phase
    };
}

function computeActualTargetTrendWeightData(phase: Phase, days: string[], startDay: string, endDay: string) {
    const weightData = getWeightData(days);
    const lineChartWeightData = days.map((day) => ({x: dayjs(day).format(dayFormat), y: bodyCompositionPerDay[day]?.weight})).filter(({y}) => y);
    const [estimatedStartWeight, estimatedEndWeight] = estimates(startDay, endDay, weightData);
    const targetLineChartWeightData = getTarget(startDay, endDay, estimatedStartWeight, phase.targets.bodyWeight);
    const trendLineChartWeightData = getTrend(startDay, endDay, estimatedStartWeight, estimatedEndWeight);
    const concatedWeightData = lineChartWeightData.map(ys)
        .concat(targetLineChartWeightData.map(ys))
        .concat(trendLineChartWeightData.map(ys));
    const minWeightData = min(concatedWeightData);
    const maxWeightData = max(concatedWeightData);
    return {
        lineChartWeightData, targetLineChartWeightData, trendLineChartWeightData, minWeightData, maxWeightData,
    }
}

const ys = ({ y }: dataPoint) => y;

const min = (numbers: number[]) => Math.floor(Math.min(...numbers));
const max = (numbers: number[]) => Math.ceil(Math.max(...numbers));

export function getWeightData(days: string[]): number[][] {
    return days.map((day) => [dayjs(day).unix(), bodyCompositionPerDay[day]?.weight]).filter(([_, weight]) => weight);
}

function computeActualTargetTrendMuscleData(phase: Phase, days: string[], startDay: string, endDay: string) {
    const muscleData = days.map((day) => [dayjs(day).unix(), bodyCompositionPerDay[day]?.muscleInKg]).filter(([_, muscleInKg]) => muscleInKg) as number[][];
    const lineChartMuscleData = days.map((day) => ({x: dayjs(day).format(dayFormat), y: bodyCompositionPerDay[day]?.muscleInKg})).filter(({y}) => y) as ComputedPhase["lineChartMuscleData"];
    const [estimatedStartMusle, estimatedEndMusle] = estimates(startDay, endDay, muscleData);
    const targetLineChartMuscleData = getTarget(startDay, endDay, estimatedStartMusle, phase.targets.muscleInKg);
    const trendLineChartMuscleData = getTrend(startDay, endDay, estimatedStartMusle, estimatedEndMusle);
    const concatedMuscleData = lineChartMuscleData.map(ys)
        .concat(targetLineChartMuscleData.map(ys))
        .concat(trendLineChartMuscleData.map(ys));
    const minMuscleData = min(concatedMuscleData);
    const maxMuscleData = max(concatedMuscleData);
    return {
        lineChartMuscleData, targetLineChartMuscleData, trendLineChartMuscleData, minMuscleData, maxMuscleData
    }
}

function computeActualTargetTrendBodyfatData(phase: Phase, days: string[], startDay: string, endDay: string) {
    const bodyfatData = days.map((day) => [dayjs(day).unix(), bodyCompositionPerDay[day]?.bodyfatInPercent]).filter(([_, bodyfatInPercent]) => bodyfatInPercent) as number[][];
    const lineChartBodyfatData = days.map((day) => ({x: dayjs(day).format(dayFormat), y: bodyCompositionPerDay[day]?.bodyfatInPercent})).filter(({y}) => y) as ComputedPhase["lineChartBodyfatData"];
    const [estimatedStartBodyfat, estimatedEndBodyfat] = estimates(startDay, endDay, bodyfatData);
    const targetLineChartBodyfatData = getTarget(startDay, endDay, estimatedStartBodyfat, phase.targets.bodyfatInPercent);
    const trendLineChartBodyfatData = getTrend(startDay, endDay, estimatedStartBodyfat, estimatedEndBodyfat);
    const concatedBodyfatData = lineChartBodyfatData.map(ys)
        .concat(targetLineChartBodyfatData.map(ys))
        .concat(trendLineChartBodyfatData.map(ys));
    const minBodyfatData = min(concatedBodyfatData);
    const maxBodyfatData = max(concatedBodyfatData);
    return {
        lineChartBodyfatData, targetLineChartBodyfatData, trendLineChartBodyfatData, minBodyfatData, maxBodyfatData
    }
}

function estimates(startDay: string, endDay: string, data: number[][]) {
    const result = linearRegression(data);
    return [
        result.predict(dayjs(startDay).unix())[1],
        result.predict(dayjs(endDay).unix())[1]
    ];
}

function getTarget(startDay: string, endDay: string, estimatedStart: number, target: number) {
    return [{
        x: dayjs(startDay).format(dayFormat),
        y: estimatedStart,
    }, {
        x: dayjs(endDay).format(dayFormat),
        y: target,
    }];
}

function getTrend(startDay: string, endDay: string, estimatedStart: number, estimatedEnd: number) {
    return [{
        x: dayjs(startDay).format(dayFormat),
        y: estimatedStart,
    }, {
        x: dayjs(endDay).format(dayFormat),
        y: estimatedEnd,
    }];
}

function computeCalendarData(phase: Phase, weeks: CalAndEnergyPerDayWeek, days: string[], startDay: string, endDay: string): CalendarData {
    const daysPerWeek = daysGroupedPerWeek(days);
    const data: CalendarRawData = Object.entries(daysPerWeek).reduce((acc: CalendarRawData, [week, days]: [string, string[]]) => {
        const caloriesSpend = averageDailyCaloriePerWeek(week);
        if(!weeks[week]) {
            return acc;
        }
        if(weeks[week].length < enoughDaysPerWeek) {
            return acc;
        }
        const dayRawData: CalendarRawData = days.map((day) => {
            const caloriesConsumed = weeks[week].map(({ cal }) => cal);
            const { average } = sumAndaverage(caloriesConsumed);
            return {
            day,
            value: average - caloriesSpend
        }
        });
        return acc.concat(dayRawData);
    }, []);
    return {
        from: startDay,
        to: endDay,
        data: data,
        colorScheme: coloricDifferenceColorSchemas[phase.coloricDifference],
        maxValue: coloricDifferenceMaxValue[phase.coloricDifference],
        minValue: coloricDifferenceMinValue[phase.coloricDifference]
    };
}
